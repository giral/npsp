<?php
header('Content-Type: text/html; charset=utf-8');
$filepath = realpath (dirname(__FILE__));

require_once($filepath."/webanalytics.php");
include "websettings.php";
include ("header.php");
$idAlbum = $_GET['id']; 
$datas = new publication();
$photos = $datas->selectAlbumPhoto($idAlbum);
$albums = $datas->selectAlbum();
foreach ($albums as $album) {
$nomAlbum = $album['nomAlbum'];
};

?>
<!DOCTYPE html>
<html lang="fr">
    <head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script src="wa.js"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135621511-2"></script>

<script>

window.dataLayer = window.dataLayer || [];

function gtag(){dataLayer.push(arguments);}

gtag('js', new Date());        

gtag('config', 'UA-135621511-2');

</script>


<!-- Primary Meta Tags -->
<title>La Nouvelle Pharmacie de la Santé Publique Côte d'Ivoire en images</title>
<meta name="title" content="Nouvelle Pharmacie de la Santé Publique Côte d'Ivoire en images">
<meta name="description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">

<!-- Open Graph / Facebook -->
<meta property="og:type" content="website">
<meta property="og:url" content="http://www.npsp.ci/">
<meta property="og:title" content="La Nouvelle Pharmacie de la Santé Publique de Côte d'Ivoire en images">
<meta property="og:description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">
<meta property="og:image" content="http://npsp.ci/assets/img/npsp-ci.png">

<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="http://www.npsp.ci/">
<meta property="twitter:title" content="La Nouvelle Pharmacie de la Santé de Publique Côte d'Ivoire en images">
<meta property="twitter:description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">
<meta property="twitter:image" content="http://npsp.ci/assets/img/npsp-ci.png">
<meta name="viewport" content="width=device-width, initial-scale=1">


<meta charset="UTF-8">      

<!-- fancybox CSS library -->
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css">
<!-- JS library -->
<!-- fancybox JS library -->
<script src="fancybox/jquery.fancybox.js"></script>

    </head>

<body>
<br/>
<div class="barbread">
    
    <div class="container">
    <div class="mbr-text col-12 col-md-12 text-black  mbr-fonts-style display-7">        
                    <a class="mbr-black" href="index.php">Accueil </a>| Multimédia
</div>
<br/>

<!---->

<section class="mbr-section article content1 cid-qpgqBVyKll" id="content1-14" data-rv-view="7376">
    <div class="container">
        <div class="media-container-row">            
            <div class="bar mbr-text" > 
            <div class="titre"><h3 class="mbr-section-subtitle align-center mbr-light mbr-white mbr-fonts-style display-5">Autres albums</h3></div>   
            <br/><br/>
            
            <div class="container">
            <div class="list-group">
            <?php foreach ($albums as $album):; ?>
            <a href="multimedia.php?id=<?=$album['idAlbum']?>" class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                <h4 class="mb-1 text-black"></h4>
                <small><img src="<?=$album['miniature']?>" width="40%" media-simple="true" style="float:right"></small>
                </div>      

                <p class="mb-1"><?= $album['nomAlbum'];?></p>
                <small><?=$album['dateCreationAlbum'] ?></small>
            </a>
            <?php endforeach; ?>

            </div>
            </div>
                         
                
                
        </div>

            
<div class="mbr-text text-justify col-12 col-md-9 mbr-fonts-style display-7">

<section class="mbr-gallery mbr-slider-carousel cid-qpV9Up2ikH" id="gallery1-8u" data-rv-view="7255"> 

<div class="container "><h1 class="mbr-section-subtitle display-5 align-center mbr-black mbr-fonts-style">Multimedia </h1><hr><br/>
    <div><!-- Filter --><!-- Gallery -->
        <div class="mbr-gallery-row "> 
        <h5 class="mbr-green align-center"> </h5> 
            <div class="mbr-gallery-layout-default align-center">
                <div>
                <?php foreach ($photos as $photo):;?>
                    <div class="mbr-gallery-item mbr-gallery-item--pNaN" data-video-url="false" data-tags="Awesome">
                    <a href="redirect/backoffice/images/uploads/galleriePhoto/<?= $photo['path']; ?>" data-fancybox="group" data-caption="<?= $photo["albumId"]; ?>" >
                        <img src="redirect/backoffice/images/uploads/galleriePhoto/<?= $photo['path']; ?>" alt="" />
                    </a>
                    </div>
                <?php endforeach;?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    
</div>

<br/><br/><hr>
</section>
            </div>
        </div>
    </div>
</div>

    
</section>
<br/><br/>


<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "Nouvelle PSP-CI",
    "url": "http://www.npsp.ci",
    "address": "Km4, Boulvard de Marseille, BP V5",
    "sameAs": [
      "https://web.facebook.com/NouvellePSPCI/",
      "https://www.linkedin.com/company/nouvelle-psp-ci/"
    ]
  }
</script>

<?php include ("footer.php");?>

<script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/popper/popper.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smooth-scroll/smooth-scroll.js"></script>
  <script src="assets/dropdown/js/script.min.js"></script>
  <script src="assets/touch-swipe/jquery.touch-swipe.min.js"></script>
  <script src="assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js"></script>
  <script src="assets/jquery-mb-ytplayer/jquery.mb.ytplayer.min.js"></script>
  <script src="assets/jquery-mb-vimeo_player/jquery.mb.vimeo_player.js"></script>
  <script src="assets/jarallax/jarallax.min.js"></script>
  <script src="assets/theme/js/script.js"></script>
  <script src="assets/mobirise-slider-video/script.js"></script>
</body>
</html>