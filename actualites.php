<!DOCTYPE html>
<html>
<head>
    <?php include ("header.php"); 
    
    $filepath = realpath (dirname(__FILE__));

    require_once($filepath."/webanalytics.php");
    include "websettings.php";?>
    
    <meta charset="utf-8" />
    <title>Actualités Nouvelle PSP</title>
    <script src="wa.js"></script>
</head>
<body>
<!--
<section class="engine"><a href="https://mobirise.co/l">free bootstrap theme</a></section><section class="header11 cid-qpE2tMG2rx" id="header11-4y" data-rv-view="7803">

  

    
    <div class="container align-left">
        <div class="media-container-column mbr-white col-md-12">
             
            <h1 class="mbr-section-title py-3 mbr-fonts-style display-1"><strong>Actualités Nouvelle PSP</strong></h1>
            
            
        </div>
    </div>
    
    <?php foreach ($reponses as $reponse):;?>
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link mbr-fonts-style active" role="tab" data-toggle="tab" href="#tabs1-2q_tab0" aria-expanded="true">
                        <?= substr ($reponse['titre_actu'] ,0,20) ;?>
                        </a>
                    </li>
                </ul> 
    <?php endforeach;?>
</section>
-->
<section class="tabs1 cid-qpgItzLyjE mbr-parallax-background" id="tabs1-2q" data-rv-view="7806"><!--mbr.additional.css--19573-->

    

    <div class="mbr-overlay" style="opacity: 0.4; background-color: #083d79">
    </div>
    <div class="container">
        <h2 class="mbr-white align-center pb-5 mbr-fonts-style mbr-bold display-2">
            Actualités Nouvelle PSP 
        </h2>
        <div class="media-container-row">
            <div class="col-12 col-md-8">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link mbr-fonts-style active" role="tab" data-toggle="tab" href="#tabs1-2q_tab0" aria-expanded="true">
                            Mobile Friendly
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mbr-fonts-style active" role="tab" data-toggle="tab" href="#tabs1-2q_tab1" aria-expanded="true">
                            No Coding
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mbr-fonts-style active" role="tab" data-toggle="tab" href="#tabs1-2q_tab2" aria-expanded="true">
                            Unique Styles
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mbr-fonts-style active" role="tab" data-toggle="tab" href="#tabs1-2q_tab0" aria-expanded="true">
                            Mobile Friendly
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mbr-fonts-style active" role="tab" data-toggle="tab" href="#tabs1-2q_tab1" aria-expanded="true">
                            No Coding
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mbr-fonts-style active" role="tab" data-toggle="tab" href="#tabs1-2q_tab2" aria-expanded="true">
                            Unique Styles
                        </a>
                    </li>
                    
                    
                    
                </ul>                
                    
                    
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mbr-section content4 cid-qpgqAfBMyz" id="content4-10" data-rv-view="7374">
    <div class="container">
        <div class="media-container-row">
            <div class="title col-12 col-md-8">
                
                <h3 class="mbr-section-subtitle align-center mbr-light mbr-fonts-style display-5">
                Pose de la 1ere pierre de l'agence Regionale Nouvelle PSP Côte d'Ivoire de Bouake.
                </h3>
                
            </div>
        </div>
    </div>
</section>

<section class="mbr-section article content1 cid-qpgqBVyKll" id="content1-14" data-rv-view="7376" >
    <div class="container">
        <div class="media-container-row">
            <div class="mbr-text col-12 col-md-8 mbr-fonts-style display-7">

                <div class="media-container-row">
                 <div class="mbr-figure" style="width: 100%;">
                    <img src="assets/img/slide-bebe.png" alt="Mobirise" media-simple="true" alt="Nos actualités - NPSP-CI">
                 </div>
                </div>
                <br/> <br/> 
                
                


                La Pharmacie de la Santé Publique de Côte d’Ivoire (PSP-CI) était une structure publique érigée en Etablissement Public National à caractère Industriel et Commercial (EPIC). Conformément à son décret d’organisation, elle était chargée de centraliser, de programmer, d’effectuer les achats de matériels techniques et de produits pharmaceutiques nécessaires au fonctionnement des formations sanitaires publiques ou privées, et notamment celles de l’Etat ou de toute autre personne morale de droit public ivoirien. Elle était également chargée d’organiser l’approvisionnement de ces formations sanitaires en médicaments et produits pharmaceutiques divers consommables médicaux, vaccins, réactifs, produits de laboratoire.

                <br/><br/>Au cours de ces dernières années, les études relatives à la situation de l’établissement ont mis en exergue des difficultés qui ont entrainé une perte de performance de la centrale.
                <br/><br/>

              

                <br/><br/>Cette dégradation continue des performances de la PSP-CI est due à plusieurs facteurs limitant. Il s’agit en l’occurrence de l’absence d’une réelle autonomie de gestion ; la forte concentration de ses activités ; les insuffisances de son système informatique et d’information ; des difficultés de gestion des programmes verticaux et une insuffisance dans la politique des prix et de recouvrement, qui ont conduit à une baisse de la performance de cet établissement important du système national de santé d’où la nécessité d’une réflexion sur le renforcement de son autonomie.

                <br/>Au regard de la politique sociale et de santé Gouvernement, une réflexion en vue de renforcer l’autonomie de gestion de la PSP-CI s’est imposée pour permettre à cette structure d’assurer et garantir la disponibilité des médicaments à tous les niveaux de la pyramide sanitaire.<br/><br/>
                
                

                
                <strong>ETAPES DE LA REFORME</strong><br/> <br/>

C’est dans ce contexte que le Gouvernement de la République de Côte d’Ivoire a affirmé sa volonté de mener les réformes nécessaires pour doter la PSP-CI d’un cadre juridique et organisationnel qui lui permet de mieux accomplir sa mission de santé publique. A cet effet, il a été créé par Arrêté Interministériel N°037/MSLS/MEF du 04 Mai 2012, un Comité de Pilotage chargé de la réforme de la PSP-CI.<br/> <br/>

Ce comité, présidé par le Ministre de la Santé et de la Lutte contre le Sida et Co-Présidé par le Ministre de l’Economie et des Finances, était composé également de la Présidence de la République, de la Primature, du Ministère en charge des Affaires Sociales, du Ministère en charge du Commerce, des Partenaires techniques et Financiers et des utilisateurs et clients de la PSP-CI.<br/> <br/>




Dans le cadre du processus de réforme de la PSP-CI, le Comité de Pilotage s’est appuyé sur un Comité technique, organe d’exécution. C’est ainsi qu’il s’est réuni à treize reprises en heures structurées : le 29 mai, les 05, 13, 19, 26 juin,les 03, 17, 24 juillet, les02, 10 août, le 25 septembre, le 20 novembre et le 27 décembre 2012.<br/> <br/>

A l’issue de ces rencontres, il a été retenue la nécessité du changement de statut de la PSP-CI notamment en une Association Sans But Lucratif (ASBL) et d’arrêter une méthode de travail pour garantir le succès de cette reforme.Ainsi le comité technique a combiné l’approche de la revue documentaire et les missions de prospection au Burkina Faso du 29 juillet au 02 août 2012 et au Bénin du 12 au 16 août 2012.<br/> <br/>

L’efficacité du Comité de Pilotage a permis de finaliser la phase de conception du projet. Et sur la base des statuts et du règlement intérieur adopté par le Comité de pilotage, Madame la Ministre de la Santé et de la Lutte contre le Sida a présidé l’Assemblée Générale constitutive, le 21 juin 2013 afin que soient posés les fonds baptismaux de la Nouvelle Pharmacie de la Santé Publique de Côte d’Ivoire (Nouvelle PSP Côte d’Ivoire).<br/> <br/>

Au terme de l’enquête de moralité, la Nouvelle PSP Côte d’Ivoire a été autorisée par récépissé N°321MEMIS/DGAT/DAG/SDVA du 04 octobre 2013 qui a été publié le 14 octobre 2013 au Journal Officiel de la République de Côte d’Ivoire (JORCI) sous le N° 610E.C. Cette procédure qui est la consécration de la personnalité juridique de la Nouvelle PSP Côte d’Ivoire, permet à l’association de mener tous les actes de la vie civile notamment, conclure des conventions et administrer des biens.           <br/> <br/><br/> <br/>
            </div>
        </div>
    </div>
</section>

<?php include ("footer.php");?>

<script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/popper/popper.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smooth-scroll/smooth-scroll.js"></script>
  <script src="assets/dropdown/js/script.min.js"></script>
  <script src="assets/touch-swipe/jquery.touch-swipe.min.js"></script>
  <script src="assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js"></script>
  <script src="assets/jquery-mb-ytplayer/jquery.mb.ytplayer.min.js"></script>
  <script src="assets/jquery-mb-vimeo_player/jquery.mb.vimeo_player.js"></script>
  <script src="assets/jarallax/jarallax.min.js"></script>
  <script src="assets/theme/js/script.js"></script>
  <script src="assets/mobirise-slider-video/script.js"></script>
</body>
</html>