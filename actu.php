<?php
//header('Content-Type: text/html; charset=utf-8');
require_once ("header.php");
$filepath = realpath (dirname(__FILE__));

require_once($filepath."/webanalytics.php");
include "websettings.php";
$id_actu = $_GET['id']; 
$result = new publication();
$reponses = $result->selectActuId($id_actu);
$reponses3 = $result->selectActuAll();


?> 
<!DOCTYPE html>
<html lang="fr">
    
    <head>
        <!-- actu CSS library -->
        <link rel="stylesheet" type="text/css" href="actu.css">
     <!-- Global site tag (gtag.js) - Google Analytics -->

     <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135621511-2"></script>

<script>

window.dataLayer = window.dataLayer || [];

function gtag(){dataLayer.push(arguments);}

gtag('js', new Date());        

gtag('config', 'UA-135621511-2');

</script>
<script src="wa.js"></script>

<!-- Primary Meta Tags -->
<title>Actualités Nouvelle Pharmacie de la Santé Publique de Côte d'Ivoire</title>
<meta name="title" content="Actualités Nouvelle Pharmacie de la Santé Publique Côte d'Ivoire">
<meta name="description" content="Quoi de neuf à la Nouvelle Pharmacie de la Santé Publique Côte d'Ivoire? Suivez nos actualités.">

<!-- Open Graph / Facebook -->
<meta property="og:type" content="website">
<meta property="og:url" content="http://www.npsp.ci/">
<meta property="og:title" content="Actualités La Nouvelle Pharmacie de la Santé Publique de Côte d'Ivoire">
<meta property="og:description" content="Quoi de neuf à la Nouvelle Pharmacie de la Santé Publique Côte d'Ivoire? Suivez nos actualités.">
<meta property="og:image" content="http://npsp.ci/assets/img/npsp-ci.png">

<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="http://www.npsp.ci/">
<meta property="twitter:title" content="La Nouvelle Pharmacie de la Santé de Publique Côte d'Ivoire">
<meta property="twitter:description" content="Quoi de neuf à la Nouvelle Pharmacie de la Santé Publique Côte d'Ivoire? Suivez nos actualités.">
<meta property="twitter:image" content="http://npsp.ci/assets/img/npsp-ci.png">
<meta name="viewport" content="width=device-width, initial-scale=1">


<meta charset="UTF-8"> 
    </head>

<body>
<br/>
<div class="barbread">
    
    <div class="container">
    <div class="mbr-text col-12 col-md-12 text-black  mbr-fonts-style display-7">        
                    <a class="mbr-black" href="index.php">Accueil </a>| Actualités
</div>
<br/>

<section class="mbr-section article content1 cid-qpgqBVyKll" id="content1-14" data-rv-view="7376">
    <div class="container">
        <div class="media-container-row">            
            <div class="bar mbr-text"> 
            <div class="titre"><h3 class="mbr-section-subtitle align-center mbr-light mbr-white mbr-fonts-style display-5">Autres actualités</h3></div>   
            <br/><br/> 
            
            
            <div class="container" >
            <div class="list-group">
            <?php foreach ($reponses3 as $reponse3):; /* echo '<pre>'; var_dump($reponse3); echo '</pre>'; */ ?>

            <a href="actu.php?id=<?=$reponse3['id']?>" class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex  justify-content-between" >
                <h4 class="mb-1 text-black"></h4>
                </div> 
      

                <p class="mb-1"><?= $reponse3['titre_actu'];?></p>
                <small><?=$reponse3['date_publication'] ?></small>
            </a>
            <?php endforeach; ?>

            </div>
            </div>
                        
                
                
        </div>

            
            <div class="mbr-text text-justify col-12 col-md-12 mbr-fonts-style display-7">

                <div class="media-container-row" >
                 <div class="mbr-figure" style="width: 100%;">
                    <img src="redirect/backoffice/<?php echo  ($reponses[0]['path_photo']);?>" alt="actualité" media-simple="true">
                 </div>
                </div>
                
                
                <section class="mbr-section content4 cid-qpgqAfBMyz" id="content4-10" data-rv-view="7374">
                    <div class="container"> 
                        <div class="media-container-row">
                            <div class="title col-12 col-md-10">
                             <h3 class="mbr-section-subtitle align-center mbr-light mbr-fonts-style display-5">
                             <?php echo ($reponses[0]['titre_actu'] ) ;?>
                             </h3>
                            </div>
                        </div>
                    </div>
                </section>

                <?php echo nl2br ($reponses[0]['contenu']);?>
            </div>
        </div>
    </div>
</div>

    
</section>
<br/><br/>


<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "Nouvelle PSP-CI",
    "url": "http://www.npsp.ci",
    "address": "Km4, Boulvard de Marseille, BP V5",
    "sameAs": [
      "https://web.facebook.com/NouvellePSPCI/",
      "https://www.linkedin.com/company/nouvelle-psp-ci/"
    ]
  }
</script>
<?php include ("footer.php");?>

<script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/popper/popper.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smooth-scroll/smooth-scroll.js"></script>
  <script src="assets/dropdown/js/script.min.js"></script>
  <script src="assets/touch-swipe/jquery.touch-swipe.min.js"></script>
  <script src="assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js"></script>
  <script src="assets/jquery-mb-ytplayer/jquery.mb.ytplayer.min.js"></script>
  <script src="assets/jquery-mb-vimeo_player/jquery.mb.vimeo_player.js"></script>
  <script src="assets/jarallax/jarallax.min.js"></script>
  <script src="assets/theme/js/script.js"></script>
  <script src="assets/mobirise-slider-video/script.js"></script>
</body>
</html>