<?php
header('Content-Type: text/html; charset=utf-8');
include ("header.php");
$filepath = realpath (dirname(__FILE__));

require_once($filepath."/webanalytics.php");
include "websettings.php";

?>
<!DOCTYPE html>
<html lang="fr">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script src="wa.js"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135621511-2"></script>

<script>

window.dataLayer = window.dataLayer || [];

function gtag(){dataLayer.push(arguments);}

gtag('js', new Date());        

gtag('config', 'UA-135621511-2');

</script>


<!-- Primary Meta Tags -->
<title>Liste des produits Nouvelle Pharmacie de la Santé Publique Côte d'Ivoire</title>
<meta name="title" content="Liste des produits Nouvelle Pharmacie de la Santé Publique Côte d'Ivoire">
<meta name="description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">

<!-- Open Graph / Facebook -->
<meta property="og:type" content="website">
<meta property="og:url" content="http://www.npsp.ci/">
<meta property="og:title" content="Liste des produits Nouvelle Pharmacie de la Santé Publique de Côte d'Ivoire">
<meta property="og:description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">
<meta property="og:image" content="http://npsp.ci/assets/img/npsp-ci.png">

<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="http://www.npsp.ci/">
<meta property="twitter:title" content="La Nouvelle Pharmacie de la Santé de Publique Côte d'Ivoire">
<meta property="twitter:description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">
<meta property="twitter:image" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">


<meta charset="UTF-8">

<script>
$(document).ready(function(){ 
    
        $('#produits').dataTable({ 
            "bDestroy": true,       
        "ajax":{
            "type" : "GET",
            "url" : "prod.php",                       
            "dataSrc": function ( json ) {
                
                 return json.data;             

        },    
    }, 
        
        "columns": [
            { "data": "code_Produit" },
            { "data": "designation" },            
            { "data": "unite" },
            { "data": "autorisation" },
            { "data": "PrixBaseFormule" }
            
        ],
        "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
        
     });     
    });

</script>
</head>
<body>

<section class="header11 cid-qpgItzLyjE" id="header11-y" data-rv-view="7364"> 

    
    <div class="container align-left">
        <div class="media-container-column mbr-white col-md-12">
             
            <h1 class="mbr-section-title py-3 mbr-fonts-style display-1"><strong> </strong></h1>
            
            
        </div>
    </div>

    
</section>

<div class="barbread">
    
<div class="container">
<div class="mbr-text col-12 col-md-12 text-black  mbr-fonts-style display-7">        
                <a href="index.php">Accueil</a> | Index pharmaceutique
</div>         
                
            
    </div>

</div>

<section class="section-table cid-qq0oaQXany" id="table1-9c" data-rv-view="7532">

  
  
  <div class="container container-table text-black">
      <h2 class="mbr-section-title mbr-fonts-style align-center pb-3 display-2">
      INDEX PHARMACEUTIQUE NOUVELLE PSP-CI      </h2>
      <h3 class="mbr-section-subtitle mbr-fonts-style align-center pb-5 mbr-light display-5">
     
    </h3>
      <div class="table-wrapper">
        <div class="container">
          <div class="row search">
            <div class="col-md-6"></div>
            
          </div>
        </div>

        <div class="container scroll">
          <table class="table isSearch text-gris" cellspacing="0" id="produits" >
            <thead>
              <tr class="table-heads ">
                     <th class="head-item mbr-fonts-style display-7"> CODE</th>
                      <th class="head-item mbr-fonts-style display-7"> DESIGNATION</th>
                      <th class="head-item mbr-fonts-style display-7"> UNITE</th>
                      <th class="head-item mbr-fonts-style display-7"> AUTORISATION</th>
                      <th class="head-item mbr-fonts-style display-7"> PRIX</th>

              </tr>
            </thead>

            <tbody>
            <tr>
              <td class="body-item mbr-fonts-style display-7"></td>
              <td class="body-item mbr-fonts-style display-7"></td>
              <td class="body-item mbr-fonts-style display-7"></td>
              <td class="body-item mbr-fonts-style display-7"></td>
              <td class="body-item mbr-fonts-style display-7"></td>
            </tr>
            
        </tbody>
          </table>
        </div>
        <div class="container">
          <div class="row info">
            
            <div class="col-md-6"></div>
          </div>
        </div>
      </div>
    </div>
</section>

<section class="mbr-section article content1 cid-qpgqBVyKll" id="content1-14" data-rv-view="7376">

    
<div class="container text-black">
     
<?php foreach ($datas as $data):;?>
<?= $data['designation'] . '<br/>' ?>

<?php endforeach; ?>


        
    </div>
</section>
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "Nouvelle PSP-CI",
    "url": "http://www.npsp.ci",
    "address": "Km4, Boulvard de Marseille, BP V5",
    "sameAs": [
      "https://web.facebook.com/NouvellePSPCI/",
      "https://www.linkedin.com/company/nouvelle-psp-ci/"
    ]
  }
</script>


<?php include ("footer.php");?>

<script src="assets/popper/popper.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smooth-scroll/smooth-scroll.js"></script>
  <script src="assets/dropdown/js/script.min.js"></script>
  <script src="assets/touch-swipe/jquery.touch-swipe.min.js"></script>
  <script src="assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js"></script>
  <script src="assets/jquery-mb-ytplayer/jquery.mb.ytplayer.min.js"></script>
  <script src="assets/jquery-mb-vimeo_player/jquery.mb.vimeo_player.js"></script>
  <script src="assets/jarallax/jarallax.min.js"></script>
  <script src="assets/theme/js/script.js"></script>
  <script src="assets/mobirise-slider-video/script.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
 
</body>
</html>





