<?php require_once('../Connections/connexion.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "connexion")) {
  $updateSQL = sprintf("UPDATE ``connect`` SET login=%s, mpass=%s WHERE id_connect=%s",
                       GetSQLValueString($_POST['login'], "text"),
                       GetSQLValueString($_POST['mpass'], "text"),
                       GetSQLValueString($_POST['id_connect'], "int"));

  mysql_select_db($database_connexion, $connexion);
  $Result1 = mysql_query($updateSQL, $connexion) or die(mysql_error());

  $updateGoTo = "Interface_client.php?id_connect=" . $row_Rsconnect['id_connect'] . "";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}


$colname_Rsconnect = "-1";
if (isset($_GET['id_connect'])) {
  $colname_Rsconnect = $_GET['id_connect'];
}
mysql_select_db($database_connexion, $connexion);
$query_Rsconnect = sprintf("SELECT * FROM `connect` WHERE id_connect = %s", GetSQLValueString($colname_Rsconnect, "int"));
$Rsconnect = mysql_query($query_Rsconnect, $connexion) or die(mysql_error());
$row_Rsconnect = mysql_fetch_assoc($Rsconnect);
$totalRows_Rsconnect = mysql_num_rows($Rsconnect);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NPSP::Identification</title>
<link href="styles.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
</script>
</head>

<body>
<center>
 	<p>&nbsp;</p>
   <p>&nbsp;</p>
   <p>&nbsp;</p>
   <form action="<?php echo $editFormAction; ?>" method="POST" enctype="application/x-www-form-urlencoded" name="connexion">
 	<table width="300" height="301" border="0" style="background-color: #E2E2E2">
  <tr>
    <td height="44" colspan="2"><center>
      <img src="images/logo-NPSP.png" width="150" height="94" />
    </center></td>
  </tr>
  <tr>
    <td height="40" colspan="2" ><img src="images/form-log3.png" width="293" height="52" /></td>
    </tr>
  <tr>
    <td height="23" >&nbsp;</td>
    <td><input name="id_connect" type="hidden" id="id_connect" value="<?php echo $row_Rsconnect['id_connect']; ?>" /></td>
  </tr>
  <tr>
    <td width="126" height="35" style="background-image:url(images/user.png); background-repeat:no-repeat">&nbsp;</td>
    <td width="164"><label for="code_racine"></label>
      <input type="text" name="login" id="login"  style="background-image:url(images/input-texte.png) ; background-repeat:no-repeat; width:162px" maxlength="8" /></td>
  </tr>
  <tr>
    <td height="38" style="background-image:url(images/mpass.png) ; background-repeat:no-repeat">&nbsp;</td>
    <td><input type="password" name="mpass" id="mpass"  style="background-image:url(images/input-texte.png) ; background-repeat:no-repeat; width:162px" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td ><input name="valider" type="submit" id="valider" style="background-image:url(images/submit1.png); width:140px; height:35px; color:#FFF; font-size:20px" onclick="MM_validateForm('code_racine','','R','mpass','','R');return document.MM_returnValue" value="Se connester" /></td>
  </tr>
</table>
 	<p><a href="http://127.0.0.1/www.npsp.org/frontend/accueil">Retour à la page d'accueil</a></p>
 	<input type="hidden" name="MM_update" value="connexion" />
   </form>

</center>

</center>
</body>
</html>
<?php
mysql_free_result($Rsconnect);
?>
