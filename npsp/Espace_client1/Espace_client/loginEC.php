<?php require_once('../Connections/connexion.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
?>
<?php
// *** Validate request to login to this site.
if (!isset($_SESSION)) {
  session_start();
}

$loginFormAction = $_SERVER['PHP_SELF'];
if (isset($_GET['accesscheck'])) {
  $_SESSION['PrevUrl'] = $_GET['accesscheck'];
}

if (isset($_POST['login'])) {
  $loginUsername=$_POST['login'];
  $password=$_POST['mpass'];
  $MM_fldUserAuthorization = "";
  $MM_redirectLoginSuccess = "Espace-client.php";
  $MM_redirectLoginFailed = "loginEC.php";
  $MM_redirecttoReferrer = false;
  mysql_select_db($database_connexion, $connexion);
  
  $LoginRS__query=sprintf("SELECT login, mpass FROM `connect` WHERE login=%s AND mpass=%s",
    GetSQLValueString($loginUsername, "text"), GetSQLValueString($password, "text")); 
   
  $LoginRS = mysql_query($LoginRS__query, $connexion) or die(mysql_error());
  $loginFoundUser = mysql_num_rows($LoginRS);
  if ($loginFoundUser) {
     $loginStrGroup = "";
    
	if (PHP_VERSION >= 5.1) {session_regenerate_id(true);} else {session_regenerate_id();}
    //declare two session variables and assign them
    $_SESSION['MM_Username'] = $loginUsername;
    $_SESSION['MM_UserGroup'] = $loginStrGroup;	      

    if (isset($_SESSION['PrevUrl']) && false) {
      $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];	
    }
    header("Location: " . $MM_redirectLoginSuccess );
  }
  else {
    header("Location: ". $MM_redirectLoginFailed );
  }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Espace client:: Athentification</title>
<link href="admin.css" rel="stylesheet" type="text/css" media="all" />
</head>

<body>
		<center>
				<p>&nbsp;</p>
				<form id="identifier" name="identifier" method="POST" action="<?php echo $loginFormAction; ?>">
				  <p>&nbsp;</p>
				  <p>&nbsp;</p>
				  <table width="232" border="0" cellspacing="2" cellpadding="2">
				    <tr>
				      <th height="30" scope="col" ><img src="images/logo-NPSP.png" width="150" height="94" /></th>
			        </tr>
				    <tr>
				      <th height="30" scope="col" >Identifiez- vous </th>
			        </tr>
				    <tr>
				      <td height="41" align="center"><center><input name="login" type="text" size="20" maxlength="5" class="login"/></center></td>
			        </tr>
				    <tr>
				      <td height="37" align="center"><center><input name="mpass" type="password" size="20" maxlength="14" class="login" /></center></td>
			        </tr>
				    <tr>
				      <td height="23" align="center"><input type="hidden" name="raison_social" id="raison_social" /></td>
			        </tr>
				    <tr>
				      <td height="40"><center><input name="Se connecter" type="submit" value="Se connecter" class="submit" /></center></td>
			        </tr>
			      </table>
          </form>
</center>
</body>
</html>