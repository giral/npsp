<?php require_once('../Connections/connexion.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
?>
<?php
// *** Validate request to login to this site.
if (!isset($_SESSION)) {
  session_start();
}

$loginFormAction = $_SERVER['PHP_SELF'];
if (isset($_GET['accesscheck'])) {
  $_SESSION['PrevUrl'] = $_GET['accesscheck'];
}

if (isset($_POST['login'])) {
  $loginUsername=$_POST['login'];
  $password=$_POST['mpass'];
  $MM_fldUserAuthorization = "";
  $MM_redirectLoginSuccess = "Interface_client.php";
  $MM_redirectLoginFailed = "index.php";
  $MM_redirecttoReferrer = false;
  mysql_select_db($database_connexion, $connexion);
  
  $LoginRS__query=sprintf("SELECT login, mpass FROM `connect` WHERE login=%s AND mpass=%s",
    GetSQLValueString($loginUsername, "text"), GetSQLValueString($password, "text")); 
   
  $LoginRS = mysql_query($LoginRS__query, $connexion) or die(mysql_error());
  $loginFoundUser = mysql_num_rows($LoginRS);
  if ($loginFoundUser) {
     $loginStrGroup = "";
    
	if (PHP_VERSION >= 5.1) {session_regenerate_id(true);} else {session_regenerate_id();}
    //declare two session variables and assign them
    $_SESSION['MM_Username'] = $loginUsername;
    $_SESSION['MM_UserGroup'] = $loginStrGroup;	      

    if (isset($_SESSION['PrevUrl']) && false) {
      $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];	
    }
    header("Location: " . $MM_redirectLoginSuccess );
  }
  else {
    header("Location: ". $MM_redirectLoginFailed );
  }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NPSP::Identification</title>
<link href="styles.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' doit contenir un nombre.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' doit contenir un nombre entre '+min+' et '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' est requis.\n'; }
    } if (errors) alert('Des erreurs ont été trouvées:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
</script>
</head>

<body>
 <center>
 	<p>&nbsp;</p>
   <p>&nbsp;</p>
   <p>&nbsp;</p>
   <form action="<?php echo $loginFormAction; ?>" method="POST" enctype="application/x-www-form-urlencoded" name="connexion">
 	<table width="300" height="293" border="0" style="background-image:url(images/form1.png)">
  <tr>
    <td height="44" colspan="2"><center>
      <img src="images/logo-NPSP.png" width="150" height="94" />
    </center></td>
  </tr>
  <tr>
    <td height="40" colspan="2" ><img src="images/form-log3.png" width="293" height="52" /></td>
    </tr>
  <tr>
    <td width="126" height="35" style="background-image:url(images/user.png); background-repeat:no-repeat">&nbsp;</td>
    <td width="164"><label for="code_racine"></label>
      <input type="text" name="login" id="login"   maxlength="8" /></td>
  </tr>
  <tr>
    <td height="38" style="background-image:url(images/mpass.png) ; background-repeat:no-repeat">&nbsp;</td>
    <td><input type="password" name="mpass" id="mpass"   /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td ><input name="valider" type="submit" id="valider" style="background-image:url(images/submit1.png); width:140px; height:35px; color:#FFF; font-size:20px" onclick="MM_validateForm('code_racine','','R','mpass','','R');return document.MM_returnValue" value="Se connecter" /></td>
  </tr>
</table>
 	<p><a href="http://127.0.0.1/www.npsp.org/frontend/accueil">Retour à la page d'accueil</a></p>
 	<p>&nbsp;</p>
   </form>

</center>
</body>
</html>
