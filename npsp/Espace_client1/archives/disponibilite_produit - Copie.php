<?php require_once('../Connections/connexion.php'); ?>
<?php
//initialize the session
if (!isset($_SESSION)) {
  session_start();
}

// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  $_SESSION['MM_Username'] =  $loginUsername;
  $_SESSION['MM_UserGroup'] = NULL;
  $_SESSION['PrevUrl'] = NULL;
  unset($_SESSION['MM_Username']);
  unset($_SESSION['MM_UserGroup']);
  unset($_SESSION['PrevUrl']);
	
  $logoutGoTo = "http://127.0.0.1/www.npsp.org/frontend/accueil";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if (isset($_SESSION['MM_Username'])){
mysql_select_db($database_connexion, $connexion);
$query_Rscodeclient = ("SELECT DISTINCT CodeClient FROM client inner join `connect`on client.id_login = connect.login WHERE login= 'C1001'")/*, GetSQLValueString ($_SESSION['MM_Username']), "text")*/;
$Rscodeclient = mysql_query($query_Rscodeclient, $connexion) or die(mysql_error());
$row_Rscodeclient = mysql_fetch_assoc($Rscodeclient);
$totalRows_Rscodeclient = mysql_num_rows($Rscodeclient);
$colname_Rsclient = "0";
if (isset($_GET['$loginUsername'])) {
  $colname_Rsclient =$_GET['$loginUsername'];
}

$colname_Rsdispo = "-1";
if (isset($_GET['Codeliste'])) {
  $colname_Rsdispo = $_GET['Codeliste'];
}
mysql_select_db($database_connexion, $connexion);
$query_Rsdispo = sprintf("SELECT B.CodeProduit,A.Designation FROM PRODUIT A INNER JOIN PRODUIT_PROJET B ON A.CodeProduit=B.CodeProduit INNER JOIN PROJET C ON B.CodeProjet=C.CodeProjet INNER JOIN CLIENT D ON C.CodeProjet= D.CodeProjet=%s WHERE  D.CodeClient= %s", GetSQLValueString($colname_Rsdispo, "text"));
$Rsdispo = mysql_query($query_Rsdispo, $connexion) or die(mysql_error());
$row_Rsdispo = mysql_fetch_assoc($Rsdispo);
$totalRows_Rsdispo = mysql_num_rows($Rsdispo);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Espace client: Disponibilité produit</title>
<link href="styles.css" rel="stylesheet" type="text/css" />
<link href="../Zoombox/zoombox.css" rel="stylesheet" type="text/css" media="screen" />

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="../Zoombox/zoombox.js"></script>

<script type="text/javascript">
        jQuery(function($){
            $('a.zoombox').zoombox();

           
            //Or You can also use specific options
            $('a.zoombox').zoombox({
                theme       : 'zoombox',        //available themes : zoombox,lightbox, prettyphoto, darkprettyphoto, simple
                opacity     : 0.8,              // Black overlay opacity
                duration    : 800,              // Animation duration
                animation   : true,             // Do we have to animate the box ?
                width       : 600,              // Default width
                height      : 400,              // Default height
                gallery     : true,             // Allow gallery thumb view
                autoplay : false                // Autoplay for video
            });
           
        });
		
		/***********************************************************************************************************/
		
        </script>
</head>

<body>
  <div class="container">
    	<div class="leftside">
        	<div class="logo"><img src="images/logo-PSP-180.png" width="180" height="122" /></div>
            <div class="menu">

<span id="date_heure"><?php if (isset($_SESSION['MM_Username'])){

// On teste pour voir si nos variables ont bien été enregistrées

echo '<b style font-size:8px>'.' Connecté en tant que:' .$_SESSION['MM_Username'].'</b>';
} ?></span>

	
<ul class="menu1">
               		<li class="smenu"><a href="Interface_client.php"> Informations  </a></li>
                	<li class="smenu"><a href="disponibilite_produit.php"> Disponibilité produit </a></li>                    
<!-- <li class="smenu"><a href="#"> Sondage</a></li>-->
                    <li class="smenu"><a href="modifmpass.php"> Modifier son mot de passe</a></li>
                    <li class="smenu"><a href="<?php echo $logoutAction ?>"> Se déconnecter</a></li>
              </ul>
            </div>
            <div class="pub"><a href="images/pub-kit.jpg" width="600" class="zoombox zgallery1" /><img src="images/pub-kit.jpg" width="200" /></a></div>
            <div class="pub"> <u><i>Nous Contacter?</i></u><br />
            <span style="font-size:14px">Tel: +225 27 21 21 73 00<br />
            Email: info@nppsp.ci</span>
            </div>
        </div>
    	<div class="rightside">
        	<div class="clientname"><marquee>
        	<b>Consultation de la disponibilité des produits</b>
       	  </marquee></div>
            <div class="texteclient">
              <form id="form1" name="form1" method="post" action="">
                <b>COMPTE CLIENT:</b>
               <!--<label for="CodeClient"></label>-->
                <label for="Codeliste"></label>
                <select name="Codeliste" id="Codeliste" >
                  <option  value=""  <?php if (!(strcmp("", $row_Rscodeclient['CodeClient']))) {echo "selected=\"selected\"";} ?>></option>
                  <?php
do {  
?>
                  <option value="<?php echo $row_Rscodeclient['CodeClient']?>"<?php if (!(strcmp($row_Rscodeclient['CodeClient'], $row_Rscodeclient['CodeClient']))) {echo "selected=\"selected\"";} ?>><?php echo $row_Rscodeclient['CodeClient']?></option>
                  <?php
} while ($row_Rscodeclient = mysql_fetch_assoc($Rscodeclient));
  $rows = mysql_num_rows($Rscodeclient);
  if($rows > 0) {
      mysql_data_seek($Rscodeclient, 0);
	  $row_Rscodeclient = mysql_fetch_assoc($Rscodeclient);
  }
?>
                </select>
                <input type="submit" name="consulter" id="consulter" value="Consulter" />
              </form>
            </div>
            
          <div class="clientname"><b>PRODUITS DISPONNIBLES A LA NOUVELLE PSP CÔTE D'IVOIRE</b></div>
            <div class="info"></div>
    	</div>
    
    
    
    
</div>
</body>
</html>
<?php
mysql_free_result($Rscodeclient);

mysql_free_result($Rsdispo);

mysql_free_result($Rsdisponibilite);
?>
