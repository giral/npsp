<?php require_once('../Connections/connexion.php'); ?>

<?php
//initialize the session
if (!isset($_SESSION)) {
  session_start();
}
// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  	 $_SESSION['MM_Username'] = $loginUsername;
    $_SESSION['MM_UserGroup'] = $loginStrGroup;	      
  $_SESSION['PrevUrl'] = NULL;
  unset($_SESSION['MM_Username']);
  unset($_SESSION['MM_UserGroup']);
  unset($_SESSION['PrevUrl']);

  $logoutGoTo = "../frontend/accueil";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
if (isset($_SESSION['MM_Username'])){
mysql_select_db($database_connexion, $connexion);
$query_Rslogin = sprintf("select DISTINCT login from connect where login =%s", GetSQLValueString($_SESSION['MM_Username'], "text"));
$Rslogin = mysql_query($query_Rslogin, $connexion) or die(mysql_error());
$row_Rslogin = mysql_fetch_assoc($Rslogin);
$totalRows_Rslogin = mysql_num_rows($Rslogin);
 }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bienvenue a votre espace client</title>
<link href="styles.css" rel="stylesheet" type="text/css" />
<link href="../Zoombox/zoombox.css" rel="stylesheet" type="text/css" media="screen" />

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
 <script type="text/javascript" src="../Zoombox/zoombox.js"></script>
 <script type="text/javascript">
        jQuery(function($){
            $('a.zoombox').zoombox();

           
            //Or You can also use specific options
            $('a.zoombox').zoombox({
                theme       : 'zoombox',        //available themes : zoombox,lightbox, prettyphoto, darkprettyphoto, simple
                opacity     : 0.8,              // Black overlay opacity
                duration    : 800,              // Animation duration
                animation   : true,             // Do we have to animate the box ?
                width       : 600,              // Default width
                height      : 400,              // Default height
                gallery     : true,             // Allow gallery thumb view
                autoplay : false                // Autoplay for video
            });
           
        });
		
		/***********************************************************************************************************/
		
        </script>
</head>

<body>
	<div class="container">
    	<div class="leftside">
        	<div class="logo"><img src="images/logo-PSP-180.png" width="180" height="122" /><br />
            <?php if (isset($_SESSION['MM_Username'])){

// On teste pour voir si nos variables ont bien été enregistrées

echo '<b style font-size:8px>'.' Connecté en tant que:' .$_SESSION['MM_Username'].'</b>';
} ?>
      </div>
            <div class="menu">
        
	
       	  <ul class="menu1">
            <li class="smenu"><a href="Interface_client.php"> Informations  </a></li>
             <li class="smenu"><a href="disponibilite_produit.php?log=<?php echo rawurldecode($row_Rslogin['login']); ?>"> Disponibilité produit</a></li>                    
<!-- <li class="smenu"><a href="#"> Sondage</a></li>-->
                    <li class="smenu"><a href="modifmpass.php"> Modifier son mot de passe</a></li>
                    <li class="smenu"><a href="<?php echo $logoutAction ?>"> Se déconnecter</a></li>
                </ul>
            </div>
            <div class="pub"><a href="images/pub-kit.jpg" width="600" class="zoombox zgallery1" /><img src="images/pub-kit.jpg" width="200" /></a></div>
            <div class="pub"> <u><i>Nous Contacter?</i></u><br />
            <span style="font-size:14px">Tel: +225 27 21 21 73 00<br />
            Email: info@nppsp.ci</span>
            </div>
        </div>
    	<div class="rightside">
       	  <div class="clientname"><marquee><b>Bienvenue </b></marquee></div>
            <div class="texteclient"><strong style="font-size:24px">C</strong>her client, cet espace a été pensé pour vous permettre d'avoir plus d'information sur nos produits et Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ac elit mi. Pellentesque molestie felis nisl, vitae convallis purus sagittis nec. Pellentesque non ultrices orci. In hac habitasse platea dictumst. Sed tincidunt nulla a volutpat molestie. In vestibulum augue vitae commodo volutpat. Sed at nunc vitae nisi vulputate efficitur in sed quam. Nullam dignissim rutrum nisl. Suspendisse tristique cursus velit, non congue dolor luctus eget. Nullam id porta dolor. Sed sit a</div>
            
          <div class="clientname"><b>Informations</b></div>
            <div class="info">
       		  <div class="infoimg"><a href="images/debridat.jpg" width="400" class="zoombox zgallery1" /><img src="images/debridat.jpg" width="150" /></a></div>
                    <div class="infotexte"> Pellentesque molestie felis nisl, vitae convallis purus sagittis nec. Pellentesque non ultrices orci. In hac habitasse platea dictumst. Sed tincidunt nulla a volutpat molestie. In vestibulum augue vitae commodo volutpat. Sed at nunc vitae nisi vulputate efficitur in sed quam. Nullam dignissim rutrum nisl. Suspendisse tristique cursus velit, non congue dolor luctus eget. Nullam id porta dolor.</div>
            </div>
            
          <div class="info">
       		  <div class="infoimg"><a href="images/test-de-palu.jpg" width="400" class="zoombox zgallery1" /><img src="images/test-de-palu.jpg" width="150"  /></a></div>
                    <div class="infotexte"> Pellentesque molestie felis nisl, vitae convallis purus sagittis nec. Pellentesque non ultrices orci. In hac habitasse platea dictumst. Sed tincidunt nulla a volutpat molestie. In vestibulum augue vitae commodo volutpat. Sed at nunc vitae nisi vulputate efficitur in sed quam. Nullam dignissim rutrum nisl. Suspendisse tristique cursus velit, non congue dolor luctus eget. Nullam id porta dolor.</div>
            </div>
    	</div>
    
    
    
    
    </div>
</body>
</html>
<?php
mysql_free_result($Rslogin);
?>
