<?php require_once('../Connections/connexion.php'); ?>


<!---------------------------------------------------------Accès restreint----------------------------------------------->
<?php
//initialize the session
if (!isset($_SESSION)) {
  session_start();
}

// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  $_SESSION['MM_Username'] = NULL;
  $_SESSION['MM_UserGroup'] = NULL;
  $_SESSION['PrevUrl'] = NULL;
  unset($_SESSION['MM_Username']);
  unset($_SESSION['MM_UserGroup']);
  unset($_SESSION['PrevUrl']);
	
  $logoutGoTo = "loginEC.php";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
/*********************************************************************************************************/
?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "loginEC.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

if (isset($_SESSION['MM_Username'])) {
mysql_select_db($database_connexion, $connexion);
$query_Rsclient = sprintf("SELECT DISTINCT CodeClient FROM `connect`, client WHERE `connect`.login=client.id_login and `connect`.login=%s", GetSQLValueString($_SESSION['MM_Username'], "text"));
$Rsclient = mysql_query($query_Rsclient, $connexion) or die(mysql_error());
$row_Rsclient = mysql_fetch_assoc($Rsclient);
$totalRows_Rsclient = mysql_num_rows($Rsclient);
}
/*********************************************************************************************************************************************/

$col5_RsautorisationClient = "-1";
if (isset($_GET['cclient'])) {
  $col5_RsautorisationClient = $_GET['cclient'];
}
mysql_select_db($database_connexion, $connexion);
$query_RsautorisationClient = sprintf("SELECT DISTINCT Autorisation FROM client WHERE CodeClient =  %s", GetSQLValueString($col5_RsautorisationClient, "text"));
$RsautorisationClient = mysql_query($query_RsautorisationClient, $connexion) or die(mysql_error());
$row_RsautorisationClient = mysql_fetch_assoc($RsautorisationClient);
$totalRows_RsautorisationClient = mysql_num_rows($RsautorisationClient);

/******************************************************************************************************************************************/


/*********************************************************************************************************************************************/
/*$col4_RsautorisationClient = "-1";
if (isset($_GET['CodeClient'])) {
  $col4_RsautorisationClient = $_GET['CodeClient'];
}*/

 if (isset($_POST['consulter'])) {
$maxRows_Rdispo = 10;
$pageNum_Rdispo = 0;
if (isset($_GET['pageNum_Rdispo'])) {
  $pageNum_Rdispo = $_GET['pageNum_Rdispo'];
}
$startRow_Rdispo = $pageNum_Rdispo * $maxRows_Rdispo;

mysql_select_db($database_connexion, $connexion);
$query_Rdispo = "SELECT B.CodeProduit,A.Designation, A.autorisation FROM PRODUIT A INNER JOIN PRODUIT_PROJET B ON A.CodeProduit=B.CodeProduit INNER JOIN PROJET C ON B.CodeProjet=C.CodeProjet INNER JOIN CLIENT D ON C.CodeProjet= D.CodeProjet WHERE D.CodeClient='C10010' and A.autorisation LIKE '%A%'";
$query_limit_Rdispo = sprintf("%s LIMIT %d, %d", $query_Rdispo, $startRow_Rdispo, $maxRows_Rdispo);
$Rdispo = mysql_query($query_limit_Rdispo, $connexion) or die(mysql_error());
$row_Rdispo = mysql_fetch_assoc($Rdispo);

if (isset($_GET['totalRows_Rdispo'])) {
  $totalRows_Rdispo = $_GET['totalRows_Rdispo'];
} else {
  $all_Rdispo = mysql_query($query_Rdispo);
  $totalRows_Rdispo = mysql_num_rows($all_Rdispo);
}
$totalPages_Rdispo = ceil($totalRows_Rdispo/$maxRows_Rdispo)-1;

/************************************************************************************************************************************************/

 }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Espace client::Disponibilité produit</title>

<link href="admin.css" rel="stylesheet" type="text/css" media="all" />

<link href="../Zoombox/zoombox.css" rel="stylesheet" type="text/css" media="screen" />

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="../Zoombox/zoombox.js"></script>
<script type="text/javascript">
        jQuery(function($){
            $('a.zoombox').zoombox();

           
            //Or You can also use specific options
            $('a.zoombox').zoombox({
                theme       : 'zoombox',        //available themes : zoombox,lightbox, prettyphoto, darkprettyphoto, simple
                opacity     : 0.8,              // Black overlay opacity
                duration    : 800,              // Animation duration
                animation   : true,             // Do we have to animate the box ?
                width       : 600,              // Default width
                height      : 400,              // Default height
                gallery     : true,             // Allow gallery thumb view
                autoplay : false                // Autoplay for video
            });
           
        });
		
		/***********************************************************************************************************/
		
        </script>
        
        
        
</head>

<body>
<div class="container">
   	  <div class="header">
        	<div class="logo"></div>
        	<div class="menu">
            		<ul class="menu1">
            <li class="smenu"><a href="Espace-client.php"> Informations  </a></li>
             <li class="smenu"><a href="#"> Disponibilité produit</a></li>                    
<!-- <li class="smenu"><a href="#"> Sondage</a></li>-->
                    <li class="smenu"><a href="updatpass.php?login=<?php echo urlencode($row_Rslogin['login']); ?>"> Modifier son mot de passe</a></li>
                    <li class="smenu"><a href="<?php echo $logoutAction ?>"> Se déconnecter</a></li>
                </ul>
            </div>
        </div>
    <!-------------------------------------content------------------------------------------------------------------>
    
<div class="leftside">
        	
            <div class="pub"><a href="images/pub-kit.jpg" width="600" class="zoombox zgallery1" />
            <span id="date_heure"><?php if (isset($_SESSION['MM_Username'])){

// On teste pour voir si nos variables ont bien été enregistrées

echo '<b style font-size:8px>'.' Connecté en tant que:' .$_SESSION['MM_Username'].'</b>';
} ?></span><br />
            
<img src="images/pub-kit.jpg" width="200" /></a></div>
            <div class="pub"> <u><i>Nous Contacter?</i></u><br />
            <span style="font-size:14px">Tel: +225 27 21 21 73 00<br />
            Email: info@nppsp.ci</span>
            </div>
        </div>
    
    <!----------------------------------------------------------rigthside--------------------------------------------------->
<div class="rightside">
    <div class="clientname"><marquee>
       	    <b>Consultation de la disponibilité des produits au  23 Décembre 2014 </b>
       	  </marquee>
   </div>
            <div class="texteclient">
            Choisissez votre compte client puis valider pour consulter
              <form action="disponibilite8.php" method="get" enctype="application/x-www-form-urlencoded" name="form1" id="form1">
                <b>COMPTE CLIENT:</b>
                <!--<label for="CodeClient"></label>-->
                              
                <select name="cclient" id="cclient" class="select">
                  <?php
do {  
?>
                  <option value="<?php echo $row_Rsclient['CodeClient']?>"><?php echo $row_Rsclient['CodeClient']?></option>
                  <?php
} while ($row_Rsclient = mysql_fetch_assoc($Rsclient));
  $rows = mysql_num_rows($Rsclient);
  if($rows > 0) {
      mysql_data_seek($Rsclient, 0);
	  $row_Rsclient = mysql_fetch_assoc($Rsclient);
  }
?>
                </select>
<input type="submit" name="consulter" id="consulter" value="Consulter" class="submit2" />

              </form>
    </div>
            
          <div class="clientname"><b>PRODUITS DISPONNIBLES A LA NOUVELLE PSP CÔTE D'IVOIRE</b></div>
    <div class="info">
      <table border="1" cellpadding="1" cellspacing="1">
        <tr>
          <td>CodeProduit</td>
          <td>Designation</td>
          <td>autorisation</td>
        </tr>
        <?php do { ?>
          <tr>
            <td><?php echo $row_Rdispo['CodeProduit']; ?></td>
            <td><?php echo $row_Rdispo['Designation']; ?></td>
            <td><?php echo $row_Rdispo['autorisation']; ?></td>
          </tr>
          <?php } while ($row_Rdispo = mysql_fetch_assoc($Rdispo)); ?>
      </table>
<br />
<br />
imprimer
<br />

</p>
    </div>
          
  </div>
    
    
    
    
</div>
</body>
</html>
<?php
mysql_free_result($Rsclient);

mysql_free_result($Rdispo);

mysql_free_result($Rslogin);

mysql_free_result($RsautorisationClient);
?>
