<?php require_once('../Connections/connexion.php'); ?>
<!---------------------------------------------------------Accès restreint----------------------------------------------->
<?php
//initialize the session
if (!isset($_SESSION)) {
  session_start();
}

// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  $_SESSION['MM_Username'] = NULL;
  $_SESSION['MM_UserGroup'] = NULL;
  $_SESSION['PrevUrl'] = NULL;
  unset($_SESSION['MM_Username']);
  unset($_SESSION['MM_UserGroup']);
  unset($_SESSION['PrevUrl']);
	
  $logoutGoTo = "loginEC.php";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
/*********************************************************************************************************/
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
/*******************************************************Compte client rattaché au login********************************************************/
if (isset($_SESSION['MM_Username'])) {
mysql_select_db($database_connexion, $connexion);
$query_Rsclient = sprintf("SELECT DISTINCT CodeClient, Autorisation FROM `connect`, client WHERE `connect`.login=client.id_login and `connect`.login=%s", GetSQLValueString($_SESSION['MM_Username'], "text"));
$Rsclient = mysql_query($query_Rsclient, $connexion) or die(mysql_error());
$row_Rsclient = mysql_fetch_assoc($Rsclient);
$totalRows_Rsclient = mysql_num_rows($Rsclient);
}
/********************************************************Recupération des autorisations*****************************************************/
/*

$colname_Rautorisation = "-1";
if (isset($_GET['cclient'])) {
  $colname_Rautorisation = $_GET['cclient'];
}
mysql_select_db($database_connexion, $connexion);
$query_Rautorisation = sprintf("SELECT Autorisation FROM client WHERE CodeClient = %s", GetSQLValueString($colname_Rautorisation, "text"));
$Rautorisation = mysql_query($query_Rautorisation, $connexion) or die(mysql_error());
$row_Rautorisation = mysql_fetch_assoc($Rautorisation);
$totalRows_Rautorisation = mysql_num_rows($Rautorisation);

$B_Rdispo = "C10010";
if (isset($_GET['cclient'])) {
  $B_Rdispo = $_GET['cclient'];
}
$C_Rdispo = "A";
if (isset($_GET['autorisation'])) {
  $C_Rdispo = $_GET['autorisation'];
}
mysql_select_db($database_connexion, $connexion);
$query_Rdispo = sprintf("SELECT %s.CodeProduit,A.Designation,D.Autorisation, D.CodeClient, A.autorisation FROM PRODUIT A INNER JOIN PRODUIT_PROJET %s ON A.CodeProduit=%s.CodeProduit INNER JOIN PROJET %s ON %s.CodeProjet=%s.CodeProjet INNER JOIN CLIENT D ON %s.CodeProjet= D.CodeProjet WHERE  D.CodeClient=%s and A.autorisation like %s", GetSQLValueString($B_Rdispo, "text"),GetSQLValueString($B_Rdispo, "text"),GetSQLValueString($B_Rdispo, "text"),GetSQLValueString($C_Rdispo, "text"),GetSQLValueString($B_Rdispo, "text"),GetSQLValueString($C_Rdispo, "text"),GetSQLValueString($C_Rdispo, "text"),GetSQLValueString($B_Rdispo, "text"),GetSQLValueString("%" . $C_Rdispo . "%", "text"));
$Rdispo = mysql_query($query_Rdispo, $connexion) or die(mysql_error());
$row_Rdispo = mysql_fetch_assoc($Rdispo);
$totalRows_Rdispo = mysql_num_rows($Rdispo);
*/


/********************************************************Produits disponibles par autorisation *************************************************/
//if (isset($_POST['consulter'])) {
/*	
$maxRows_Rdispo = 20;
$pageNum_Rdispo = 0;
if (isset($_GET['pageNum_Rdispo'])) {
  $pageNum_Rdispo = $_GET['pageNum_Rdispo'];
}
$startRow_Rdispo = $pageNum_Rdispo * $maxRows_Rdispo;

$B_Rdispo = "C10010";
if (isset($_GET['cclient'])) {
  $B_Rdispo = $_GET['cclient'];
}
$C_Rdispo = "A";
if (isset($_GET['autorisation'])) {
  $C_Rdispo = $_GET['autorisation'];
}
mysql_select_db($database_connexion, $connexion);
$query_Rdispo = sprintf("SELECT %s.CodeProduit,A.Designation,D.Autorisation, D.CodeClient, A.autorisation FROM PRODUIT A INNER JOIN PRODUIT_PROJET %s ON A.CodeProduit=%s.CodeProduit INNER JOIN PROJET %s ON %s.CodeProjet=%s.CodeProjet INNER JOIN CLIENT D ON %s.CodeProjet= D.CodeProjet WHERE  D.CodeClient=%s and A.autorisation like %s", GetSQLValueString($B_Rdispo, "text"),GetSQLValueString($C_Rdispo, "text"));
$Rdispo = mysql_query($query_Rdispo, $connexion) or die(mysql_error());
$row_Rdispo = mysql_fetch_assoc($Rdispo);
$totalRows_Rdispo = mysql_num_rows($Rdispo);

/*
$col1_Rdispo = "C10010";
if (isset($_GET['cclient'])) {
  $col1_Rdispo = $_GET['cclient'];
}
$col2_Rdispo = "A";
if (isset($_GET['autorisation'])) {
  $col2_Rdispo = $_GET['autorisation'];
}
mysql_select_db($database_connexion, $connexion);
$query_Rdispo = sprintf("SELECT B.CodeProduit,A.Designation FROM PRODUIT A INNER JOIN PRODUIT_PROJET B ON A.CodeProduit=B.CodeProduit INNER JOIN PROJET C ON B.CodeProjet=C.CodeProjet INNER JOIN CLIENT D ON C.CodeProjet= D.CodeProjet WHERE D.CodeClient= %s and A.autorisation like %s", GetSQLValueString($col1_Rdispo, "text"),GetSQLValueString("%" . $col2_Rdispo . "%", "text"));
$query_limit_Rdispo = sprintf("%s LIMIT %d, %d", $query_Rdispo, $startRow_Rdispo, $maxRows_Rdispo);
$Rdispo = mysql_query($query_Rdispo, $connexion) or die(mysql_error());
$row_Rdispo = mysql_fetch_assoc($Rdispo);*/

if (isset($_GET['totalRows_Rdispo'])) {
  $totalRows_Rdispo = $_GET['totalRows_Rdispo'];
} else {
  $all_Rdispo = mysql_query($query_Rdispo);
  $totalRows_Rdispo = mysql_num_rows($all_Rdispo);
}
$totalPages_Rdispo = ceil($totalRows_Rdispo/$maxRows_Rdispo)-1;

$currentPage = $_SERVER["PHP_SELF"];

	

/******************************************************************************************************************/



/************************************************************************************************************************************************/
	//  }

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Espace client::Disponibilité produit</title>

<link href="admin.css" rel="stylesheet" type="text/css" media="all" />

<link href="../Zoombox/zoombox.css" rel="stylesheet" type="text/css" media="screen" />

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="../Zoombox/zoombox.js"></script>
<script type="text/javascript">
        jQuery(function($){
            $('a.zoombox').zoombox();

           
            //Or You can also use specific options
            $('a.zoombox').zoombox({
                theme       : 'zoombox',        //available themes : zoombox,lightbox, prettyphoto, darkprettyphoto, simple
                opacity     : 0.8,              // Black overlay opacity
                duration    : 800,              // Animation duration
                animation   : true,             // Do we have to animate the box ?
                width       : 600,              // Default width
                height      : 400,              // Default height
                gallery     : true,             // Allow gallery thumb view
                autoplay : false                // Autoplay for video
            });
           
        });
		
		/***********************************************************************************************************/
		
        </script>
        
        
        
</head>

<body>
<div class="container">
   	  <div class="header">
        	<div class="logo"></div>
        	<div class="menu">
            		<ul class="menu1">
            <li class="smenu"><a href="Espace-client.php"> Informations  </a></li>
             <li class="smenu"><a href="#"> Disponibilité produit</a></li>                    
<!-- <li class="smenu"><a href="#"> Sondage</a></li>-->
                    <li class="smenu"><a href="updatpass.php?login=<?php echo urlencode($row_Rslogin['login']); ?>"> Modifier son mot de passe</a></li>
                    <li class="smenu"><a href="<?php echo $logoutAction ?>"> Se déconnecter</a></li>
                </ul>
            </div>
</div>
    <!-------------------------------------content------------------------------------------------------------------>
    
<div class="leftside">
        	
            <div class="pub"><a href="images/pub-kit.jpg" width="600" class="zoombox zgallery1" />
            <span id="date_heure"><?php if (isset($_SESSION['MM_Username'])){

// On teste pour voir si nos variables ont bien été enregistrées

echo '<b style font-size:8px>'.' Connecté en tant que:' .$_SESSION['MM_Username'].'</b>';
} ?></span><br />
            
<img src="images/pub-kit.jpg" width="200" /></a></div>
            <div class="pub"> <u><i>Nous Contacter?</i></u><br />
            <span style="font-size:14px">Tel: +225 27 21 21 73 00<br />
            Email: info@nppsp.ci</span>
            </div>
</div>
    
    <!----------------------------------------------------------rigthside--------------------------------------------------->
<div class="rightside">
    <div class="clientname"><marquee>
       	    <b>Consultation de la disponibilité des produits au  23 Décembre 2014 </b>
       	  </marquee>
   </div>
            <div class="texteclient">
            Choisissez votre compte client puis valider pour consulter
              <form id="form1" name="form1" method="get" action="disponibilite5.php">
                <b>COMPTE CLIENT:</b>
                <label for="test"></label>
                
                <!--<label for="CodeClient"></label>-->
                        <input name="autorisation" type="hidden" value="<?php echo $row_Rsclient['Autorisation']; ?>" />      
                <select name="cclient" id="cclient" class="select">
                  <?php
do {  
?>					
                  <option value="<?php echo $row_Rsclient['CodeClient']?>"><?php echo $row_Rsclient['CodeClient']?></option>
                  <?php
} while ($row_Rsclient = mysql_fetch_assoc($Rsclient));
  $rows = mysql_num_rows($Rsclient);
  if($rows > 0) {
      mysql_data_seek($Rsclient, 0);
	  $row_Rsclient = mysql_fetch_assoc($Rsclient);
  }
?>
                </select>
<input type="submit" name="consulter" id="consulter" value="Consulter" class="submit2" />

              <a href="disponibilitebis.php?cclient=<?php echo $row_Rsclient['CodeClient']; ?>&amp;autorisation=<?php echo $row_Rsclient['Autorisation']; ?>">consulter </a>
              </form>
      </div>
          
          <div class="clientname"><b>PRODUITS DISPONNIBLES A LA NOUVELLE PSP CÔTE D'IVOIRE</b></div>
    <div class="info">
      <table border="1" cellpadding="1" cellspacing="1">
        <tr>
          <td width="121">CodeProduit</td>
          <td width="420">Designation</td>
        </tr>
        <?php do { ?>
          <tr>
            <td><?php echo $row_Rdispo['CodeProduit']; ?></td>
            <td><?php echo $row_Rdispo['Designation']; ?></td>
          </tr>
          <?php } while ($row_Rdispo); ?>
      </table>
<br />
<br />
imprimer
<br />

</p>
  </div>
          
</div>
    
    
    
    

</body>
</html>
<?php
mysql_free_result($Rsclient);

mysql_free_result($Rautorisation);

mysql_free_result($Rdispo);

mysql_free_result($Rslogin);


?>
