<?php require_once('../Connections/connexion.php'); ?>
<?php
//initialize the session
if (!isset($_SESSION)) {
  session_start();
}

// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  $_SESSION['MM_Username'] =  $loginUsername;
  $_SESSION['MM_UserGroup'] = NULL;
  $_SESSION['PrevUrl'] = NULL;
  unset($_SESSION['MM_Username']);
  unset($_SESSION['MM_UserGroup']);
  unset($_SESSION['PrevUrl']);
	
  $logoutGoTo = "http://127.0.0.1/www.npsp.org/frontend/accueil";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

if (isset($_POST['Consulter'])){

$d=
$maxRows_Rsdispo = 20;
$pageNum_Rsdispo = 0;
if (isset($_GET['pageNum_Rsdispo'])) {
  $pageNum_Rsdispo = $_GET['pageNum_Rsdispo'];
}
$startRow_Rsdispo = $pageNum_Rsdispo * $maxRows_Rsdispo;

mysql_select_db($database_connexion, $connexion);
$query_Rsdispo = "SELECT B.CodeProduit,A.Designation FROM PRODUIT A INNER JOIN PRODUIT_PROJET B ON A.CodeProduit=B.CodeProduit INNER JOIN PROJET C ON B.CodeProjet=C.CodeProjet INNER JOIN CLIENT D ON C.CodeProjet= D.CodeProjet WHERE D.CodeClient='C10058'";
$query_limit_Rsdispo = sprintf("%s LIMIT %d, %d", $query_Rsdispo, $startRow_Rsdispo, $maxRows_Rsdispo);
$Rsdispo = mysql_query($query_limit_Rsdispo, $connexion) or die(mysql_error());
$row_Rsdispo = mysql_fetch_assoc($Rsdispo);

if (isset($_GET['totalRows_Rsdispo'])) {
  $totalRows_Rsdispo = $_GET['totalRows_Rsdispo'];
} else {
  $all_Rsdispo = mysql_query($query_Rsdispo);
  $totalRows_Rsdispo = mysql_num_rows($all_Rsdispo);
}
$totalPages_Rsdispo = ceil($totalRows_Rsdispo/$maxRows_Rsdispo)-1;

$queryString_Rsdispo = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_Rsdispo") == false && 
        stristr($param, "totalRows_Rsdispo") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_Rsdispo = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_Rsdispo = sprintf("&totalRows_Rsdispo=%d%s", $totalRows_Rsdispo, $queryString_Rsdispo);
}
/*if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}}*/
if (isset($_SESSION['MM_Username'])){
mysql_select_db($database_connexion, $connexion);
$query_Rscodeclient = sprintf("SELECT DISTINCT CodeClient FROM client inner join `connect`on client.id_login = connect.login WHERE login=%s",GetSQLValueString ($_SESSION['MM_Username'], "text"));
$Rscodeclient = mysql_query($query_Rscodeclient, $connexion) or die(mysql_error());
$row_Rscodeclient = mysql_fetch_assoc($Rscodeclient);
$totalRows_Rscodeclient = mysql_num_rows($Rscodeclient);
$colname_Rsclient = "0";
if (isset($_GET['$loginUsername'])) {
  $colname_Rsclient =$_GET['$loginUsername'];
}

/*
if (isset($_POST['Consulter'])){
$maxRows_Rsdispo = 15;
$pageNum_Rsdispo = 0;
if (isset($_GET['pageNum_Rsdispo'])) {
  $pageNum_Rsdispo = $_GET['pageNum_Rsdispo'];
}
$startRow_Rsdispo = $pageNum_Rsdispo * $maxRows_Rsdispo;

$colname_Rsdispo = "-1";
if (isset($_POST['Codeliste'])) {
  $colname_Rsdispo = $_POST['Codeliste'];
}
mysql_select_db($database_connexion, $connexion);
$query_Rsdispo = sprintf("SELECT B.CodeProduit,A.Designation
FROM PRODUIT A INNER JOIN PRODUIT_PROJET B ON A.CodeProduit=B.CodeProduit
INNER JOIN PROJET C ON B.CodeProjet=C.CodeProjet INNER JOIN CLIENT D ON C.CodeProjet= D.CodeProjet
WHERE  D.CodeClient='C10058')")/*, GetSQLValueString($colname_Rsdispo, "text"));
$query_limit_Rsdispo = sprintf("%s LIMIT %d, %d", $query_Rsdispo, $startRow_Rsdispo, $maxRows_Rsdispo);
$Rsdispo = mysql_query($query_limit_Rsdispo, $connexion) or die(mysql_error());
$row_Rsdispo = mysql_fetch_assoc($Rsdispo);


if (isset($_GET['totalRows_Rsdispo'])) {
  $totalRows_Rsdispo = $_GET['totalRows_Rsdispo'];
} else {
  $all_Rsdispo = mysql_query($query_Rsdispo);
  $totalRows_Rsdispo = mysql_num_rows($all_Rsdispo);
}
$totalPages_Rsdispo = ceil($totalRows_Rsdispo/$maxRows_Rsdispo)-1;
}
/*mysql_select_db($database_connexion, $connexion);
$query_Rscodeclient = sprintf("SELECT DISTINCT CodeClient FROM client inner join `connect`on client.id_login = connect.login WHERE login=%s", GetSQLValueString ($_SESSION['MM_Username']), "text");
$Rscodeclient = mysql_query($query_Rscodeclient, $connexion) or die(mysql_error());
$row_Rscodeclient = mysql_fetch_assoc($Rscodeclient);
$totalRows_Rscodeclient = mysql_num_rows($Rscodeclient);*/
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Espace client: Disponibilité produit</title>
<link href="styles.css" rel="stylesheet" type="text/css" />
<link href="../Zoombox/zoombox.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" href="print.css" type="text/css" media="print" />

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="../Zoombox/zoombox.js"></script>

<!--<script type="text/javascript">
function imprimer_page(){
  window.print();
}
</script>-->
<script language="javascript"> 
function imprimer_bloc(titre, objet) { 
// Définition de la zone à imprimer 
var zone = document.getElementById(objet).innerHTML; 
 
// Ouverture du popup 
var fen = window.open("", "", "height=500, width=600,toolbar=0, menubar=0, scrollbars=1, resizable=1,status=0, location=0, left=10, top=10"); 
 
// style du popup 
fen.document.body.style.color = '#000000'; 
fen.document.body.style.backgroundColor = '#FFFFFF'; 
fen.document.body.style.padding = "20px"; 
 
// Ajout des données a imprimer 
fen.document.title = titre; 
fen.document.body.innerHTML += " " + zone + " "; 
 
// Impression du popup 
fen.window.print(); 
 
//Fermeture du popup 
fen.window.close(); 
return true; 
} 
</script>

<script type="text/javascript">
        jQuery(function($){
            $('a.zoombox').zoombox();

           
            //Or You can also use specific options
            $('a.zoombox').zoombox({
                theme       : 'zoombox',        //available themes : zoombox,lightbox, prettyphoto, darkprettyphoto, simple
                opacity     : 0.8,              // Black overlay opacity
                duration    : 800,              // Animation duration
                animation   : true,             // Do we have to animate the box ?
                width       : 600,              // Default width
                height      : 400,              // Default height
                gallery     : true,             // Allow gallery thumb view
                autoplay : false                // Autoplay for video
            });
           
        });
		
		/***********************************************************************************************************/
		
        </script>
</head>

<body>
  <div class="container">
    	<div class="leftside">
        	<div class="logo"><img src="images/logo-PSP-180.png" width="180" height="122" /></div>
            <div class="menu">

<span id="date_heure"><?php if (isset($_SESSION['MM_Username'])){

// On teste pour voir si nos variables ont bien été enregistrées

echo '<b style font-size:8px>'.' Connecté en tant que:' .$_SESSION['MM_Username'].'</b>';
} ?></span>

	
<ul class="menu1">
               		<li class="smenu"><a href="Interface_client.php"> Informations  </a></li>
                	<li class="smenu"><a href="disponibilite_produit.php"> Disponibilité produit </a></li>                    
<!-- <li class="smenu"><a href="#"> Sondage</a></li>-->
                    <li class="smenu"><a href="modifmpass.php"> Modifier son mot de passe</a></li>
                    <li class="smenu"><a href="<?php echo $logoutAction ?>"> Se déconnecter</a></li>
              </ul>
            </div>
            <div class="pub"><a href="images/pub-kit.jpg" width="600" class="zoombox zgallery1" /><img src="images/pub-kit.jpg" width="200" /></a></div>
            <div class="pub"> <u><i>Nous Contacter?</i></u><br />
            <span style="font-size:14px">Tel: +225 27 21 21 73 00<br />
            Email: info@nppsp.ci</span>
            </div>
        </div>
    	<div class="rightside">
        	<div class="clientname"><marquee>
        	<b>Consultation de la disponibilité des produits</b>
       	  </marquee></div>
            <div class="texteclient">
              <form id="form1" name="form1" method="post" action="">
                <b>COMPTE CLIENT:</b>
               <!--<label for="CodeClient"></label>-->
                <label for="Codeliste"></label>
                <select name="Codeliste" id="Codeliste" this.Codeliste.SelectedIndex = 2; >
                  <option  value=""  <?php if (!(strcmp("", $row_Rscodeclient['CodeClient']))) {echo "selected=\"selected\"";} ?>></option>
                  <?php
do {  
?>
                  <option value="<?php echo $row_Rscodeclient['CodeClient']?>"<?php if (!(strcmp($row_Rscodeclient['CodeClient'], $row_Rscodeclient['CodeClient']))) {echo "selected=\"selected\"";} ?>><?php echo $row_Rscodeclient['CodeClient']?></option>
                  <?php
} while ($row_Rscodeclient = mysql_fetch_assoc($Rscodeclient));
  $rows = mysql_num_rows($Rscodeclient);
  if($rows > 0) {
      mysql_data_seek($Rscodeclient, 0);
	  $row_Rscodeclient = mysql_fetch_assoc($Rscodeclient);
  }
?>
                </select>
                <input type="submit" name="consulter" id="consulter" value="Consulter" />
              </form>
            </div>
            
          <div class="clientname"><b>PRODUITS DISPONNIBLES A LA NOUVELLE PSP CÔTE D'IVOIRE</b></div>
            <div class="info">
              <table width="587" border="1" cellpadding="1" cellspacing="1">
                <tr>
                  <td width="133" style="background-color:#CCC; font-size:16px"><b>CodeProduit</b></td>
                  <td width="353" style="background-color:#CCC; font-size:16px"><b>Designation</b></td>
                </tr>
                <?php do { ?>
                  <tr>
                    <td><?php echo $row_Rsdispo['CodeProduit']; ?></td>
                    <td><?php echo $row_Rsdispo['Designation']; ?></td>
                  </tr>
                  <?php } while ($row_Rsdispo = mysql_fetch_assoc($Rsdispo)); ?>
              </table>
              <table border="0">
                <tr>
                  <td><?php if ($pageNum_Rsdispo > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_Rsdispo=%d%s", $currentPage, 0, $queryString_Rsdispo); ?>">Premier</a>
                      <?php } // Show if not first page ?></td>
                  <td><?php if ($pageNum_Rsdispo > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_Rsdispo=%d%s", $currentPage, max(0, $pageNum_Rsdispo - 1), $queryString_Rsdispo); ?>">Pr&eacute;c&eacute;dent</a>
                      <?php } // Show if not first page ?></td>
                  <td><?php if ($pageNum_Rsdispo < $totalPages_Rsdispo) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_Rsdispo=%d%s", $currentPage, min($totalPages_Rsdispo, $pageNum_Rsdispo + 1), $queryString_Rsdispo); ?>">Suivant</a>
                      <?php } // Show if not last page ?></td>
                  <td><?php if ($pageNum_Rsdispo < $totalPages_Rsdispo) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_Rsdispo=%d%s", $currentPage, $totalPages_Rsdispo, $queryString_Rsdispo); ?>">Dernier</a>
                      <?php } // Show if not last page ?></td>
                </tr>
              </table>   <form>
  <input id="impression" name="impression" type="button" onclick="imprimer_page()"value="Imprimer la page"  />
</form>
              <br />

          </div>
    	</div>
    
    
    
    
</div>
</body>
</html>
<?php
mysql_free_result($Rscodeclient);

mysql_free_result($Rsdispo);

?>
