<?php require_once('../Connections/connexion.php'); ?>
<!---------------------------------------------------------Accès restreint----------------------------------------------->
<?php
//initialize the session
if (!isset($_SESSION)) {
  session_start();
}

// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  $_SESSION['MM_Username'] = NULL;
  $_SESSION['MM_UserGroup'] = NULL;
  $_SESSION['PrevUrl'] = NULL;
  unset($_SESSION['MM_Username']);
  unset($_SESSION['MM_UserGroup']);
  unset($_SESSION['PrevUrl']);
	
  $logoutGoTo = "loginEC.php";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
/*********************************************************************************************************/
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];
/*******************************************************Compte client rattaché au login********************************************************/
if (isset($_SESSION['MM_Username'])) {
mysql_select_db($database_connexion, $connexion);
$query_Rsclient = sprintf("SELECT DISTINCT CodeClient, Autorisation FROM `connect`, client WHERE `connect`.login=client.id_login and `connect`.login=%s", GetSQLValueString($_SESSION['MM_Username'], "text"));
$Rsclient = mysql_query($query_Rsclient, $connexion) or die(mysql_error());
$row_Rsclient = mysql_fetch_assoc($Rsclient);
$totalRows_Rsclient = mysql_num_rows($Rsclient);
}
/**********************************************************************************************************************************************/
 $totalRows_Rdisponibilite = 0;
if (isset($_GET['consulter'])) {

$codeclient=$_GET['cclient'];

$autorisationClient=$_GET['autorisation'];


$maxRows_Rdisponibilite = 20;
$pageNum_Rdisponibilite = 0;
if (isset($_GET['pageNum_Rdisponibilite'])) {
  $pageNum_Rdisponibilite = $_GET['pageNum_Rdisponibilite'];
}
$startRow_Rdisponibilite = $pageNum_Rdisponibilite * $maxRows_Rdisponibilite;

mysql_select_db($database_connexion, $connexion);
$query_Rdisponibilite = "SELECT B.CodeProduit,A.Designation FROM PRODUIT A INNER JOIN PRODUIT_PROJET B ON A.CodeProduit=B.CodeProduit INNER JOIN PROJET C ON B.CodeProjet=C.CodeProjet INNER JOIN CLIENT D ON C.CodeProjet= D.CodeProjet WHERE  D.CodeClient= '$codeclient' and A.autorisation like '%$autorisationClient%'";
$query_limit_Rdisponibilite = sprintf("%s LIMIT %d, %d", $query_Rdisponibilite, $startRow_Rdisponibilite, $maxRows_Rdisponibilite);
$Rdisponibilite = mysql_query($query_limit_Rdisponibilite, $connexion) or die(mysql_error());
$row_Rdisponibilite = mysql_fetch_assoc($Rdisponibilite);

if (isset($_GET['totalRows_Rdisponibilite'])) {
  $totalRows_Rdisponibilite = $_GET['totalRows_Rdisponibilite'];
} else {
  $all_Rdisponibilite = mysql_query($query_Rdisponibilite);
  $totalRows_Rdisponibilite = mysql_num_rows($all_Rdisponibilite);
}
$totalPages_Rdisponibilite = ceil($totalRows_Rdisponibilite/$maxRows_Rdisponibilite)-1;

$queryString_Rdisponibilite = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_Rdisponibilite") == false && 
        stristr($param, "totalRows_Rdisponibilite") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_Rdisponibilite = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_Rdisponibilite = sprintf("&totalRows_Rdisponibilite=%d%s", $totalRows_Rdisponibilite, $queryString_Rdisponibilite);

/*********************************************************************************************************************************************/

/********************************************************Recupération des autorisations*****************************************************/
}

  //$totalRows_Rdisponibilite = $_GET['totalRows_Rdisponibilite'];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Espace client::Disponibilité produit</title>

<link href="admin.css" rel="stylesheet" type="text/css" media="all" />

<link href="../Zoombox/zoombox.css" rel="stylesheet" type="text/css" media="screen" />

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="../Zoombox/zoombox.js"></script>
<script type="text/javascript">
        jQuery(function($){
            $('a.zoombox').zoombox();

           
            //Or You can also use specific options
            $('a.zoombox').zoombox({
                theme       : 'zoombox',        //available themes : zoombox,lightbox, prettyphoto, darkprettyphoto, simple
                opacity     : 0.8,              // Black overlay opacity
                duration    : 800,              // Animation duration
                animation   : true,             // Do we have to animate the box ?
                width       : 600,              // Default width
                height      : 400,              // Default height
                gallery     : true,             // Allow gallery thumb view
                autoplay : false                // Autoplay for video
            });
           
        });
		
		/***********************************************************************************************************/
		
        </script>
        
        
        
</head>

<body>
<div class="container">
   	  <div class="header">
        	<div class="logo"></div>
        	<div class="menu">
            		<ul class="menu1">
            <li class="smenu"><a href="Espace-client.php"> Informations  </a></li>
             <li class="smenu"><a href="#"> Disponibilité produit</a></li>                    
<!-- <li class="smenu"><a href="#"> Sondage</a></li>-->
                    <li class="smenu"><a href="updatpass.php?login=<?php echo urlencode($row_Rslogin['login']); ?>"> Modifier son mot de passe</a></li>
                    <li class="smenu"><a href="<?php echo $logoutAction ?>"> Se déconnecter</a></li>
                </ul>
            </div>
</div>
    <!-------------------------------------content------------------------------------------------------------------>
    
<div class="leftside">
        	
            <div class="pub"><a href="images/pub-kit.jpg" width="600" class="zoombox zgallery1" />
            <span id="date_heure"><?php if (isset($_SESSION['MM_Username'])){

// On teste pour voir si nos variables ont bien été enregistrées

echo '<b style font-size:8px>'.' Connecté en tant que:' .$_SESSION['MM_Username'].'</b>';
} ?></span><br />
            
<img src="images/pub-kit.jpg" width="200" /></a></div>
            <div class="pub"> <u><i>Nous Contacter?</i></u><br />
            <span style="font-size:14px">Tel: +22527 21 21 73 00<br />
            Email: info@nppsp.ci</span>
            </div>
</div>
    
    <!----------------------------------------------------------rigthside--------------------------------------------------->
<div class="rightside">
    <div class="clientname"><marquee>
       	    <b>Consultation de la disponibilité des produits au  23 Décembre 2014 </b>
       	  </marquee>
   </div>
            <div class="texteclient">
            Choisissez votre compte client puis valider pour consulter
              <form id="form1" name="form1" method="get" action="disponibilite10.php">
                <b>COMPTE CLIENT:</b>
                <label for="test"></label>
                
                <!--<label for="CodeClient"></label>-->
                        <input name="autorisation" type="hidden" value="<?php echo $row_Rsclient['Autorisation']; ?>" />      
                <select name="cclient" id="cclient" class="select">
                  <?php
do {  
?>					
                  <option value="<?php echo $row_Rsclient['CodeClient']?>"><?php echo $row_Rsclient['CodeClient']?></option>
                  <?php
} while ($row_Rsclient = mysql_fetch_assoc($Rsclient));
  $rows = mysql_num_rows($Rsclient);
  if($rows > 0) {
      mysql_data_seek($Rsclient, 0);
	  $row_Rsclient = mysql_fetch_assoc($Rsclient);
  }
?>
                </select>
<input type="submit" name="consulter" id="consulter" value="Consulter" class="submit2" />
              </form>
      </div>
           <?php if ($totalRows_Rdisponibilite > 0) { // Show if recordset not empty ?>
          <div class="clientname"><b>PRODUITS DISPONNIBLES A LA NOUVELLE PSP CÔTE D'IVOIRE</b> <?php echo $row_Rsclient['CodeClient']; ?></div>
    <div class="info">
      <?php if ($totalRows_Rdisponibilite > 0) { // Show if recordset not empty ?>
  <table border="1" cellpadding="1" cellspacing="1">
    <tr>
      <td width="159">CodeProduit</td>
      <td width="383">Designation</td>
    </tr>
    <?php do { ?>
      <tr>
        <td><?php echo $row_Rdisponibilite['CodeProduit']; ?></td>
        <td><?php echo $row_Rdisponibilite['Designation']; ?></td>
      </tr>
      <?php } while ($row_Rdisponibilite = mysql_fetch_assoc($Rdisponibilite)); ?>
  </table>
  
  <table border="0">
    <tr>
      <td><?php if ($pageNum_Rdisponibilite > 0) { // Show if not first page ?>
          <a href="<?php printf("%s?pageNum_Rdisponibilite=%d%s", $currentPage, 0, $queryString_Rdisponibilite); ?>">Premier</a>
          <?php } // Show if not first page ?></td>
      <td><?php if ($pageNum_Rdisponibilite > 0) { // Show if not first page ?>
          <a href="<?php printf("%s?pageNum_Rdisponibilite=%d%s", $currentPage, max(0, $pageNum_Rdisponibilite - 1), $queryString_Rdisponibilite); ?>">Pr&eacute;c&eacute;dent</a>
          <?php } // Show if not first page ?></td>
      <td><?php if ($pageNum_Rdisponibilite < $totalPages_Rdisponibilite) { // Show if not last page ?>
          <a href="<?php printf("%s?pageNum_Rdisponibilite=%d%s", $currentPage, min($totalPages_Rdisponibilite, $pageNum_Rdisponibilite + 1), $queryString_Rdisponibilite); ?>">Suivant</a>
          <?php } // Show if not last page ?></td>
      <td><?php if ($pageNum_Rdisponibilite < $totalPages_Rdisponibilite) { // Show if not last page ?>
          <a href="<?php printf("%s?pageNum_Rdisponibilite=%d%s", $currentPage, $totalPages_Rdisponibilite, $queryString_Rdisponibilite); ?>">Dernier</a>
          <?php } // Show if not last page ?></td>
    </tr>
  </table>
Enregistrements <?php echo ($startRow_Rdisponibilite + 1) ?> &agrave; <?php echo min($startRow_Rdisponibilite + $maxRows_Rdisponibilite, $totalRows_Rdisponibilite) ?> sur <?php echo $totalRows_Rdisponibilite ?> <br />
<?php } }// Show if recordset not empty ?>
<br />
<br />
</p>

</p>
  </div>
          
</div>
 </div>   
    
    
    

</body>
</html>
<?php
mysql_free_result($Rsclient);

mysql_free_result($Rdisponibilite);

?>
