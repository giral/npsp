<?php
class SiteController extends Controller {
	const ARCHIVES = 'archives';
	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
		);
	}

	public function filters()  {
		return array(
			'seo -poll',
			'insertEmail',
		);
	}

	public function filterSeo($filterChain) {
		$seoImpl = new SeoImpl;
		$seoImpl->build();
		$this->pageTitle = SeoImpl::$pageTitle;
		$filterChain->run();
	}

	public function filterInsertEmail($filterChain) {
		/** @var $cs  CHttpRequest */
		$cs = Yii::app()->request;
		$email = $cs->getPost('email');
		if(isset($email) && !empty($email)) {
			$emailModel = new EmailExtend;
			$emailModel->email = $email;
			$emailModel->status = StatusBehavior::STATUS_ACTIV;
			$emailModel->save();
		}
		$filterChain->run();
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		$homeModels = HomeExtend::model('HomeExtend')->frontEnd()->findAll();

        if (isset($_POST["mail"])) {
            if (!empty($_POST["mail"]) && !empty($_POST["object"]) && !empty($_POST["message"]) && !empty($_POST["tel"]) )
            {
                $from = $_POST["mail"]; // sender
                $subject = $_POST["object"];
                $message = 'Nom : '.$_POST["name"];
                $message .= '<br/>Adresse mail : '.$_POST["mail"];
                $message .= '<br/>Téléphone : '.$_POST["tel"];
                $message .= '<br/>Message : '.$_POST["message"];
                // message lines should not exceed 70 characters (PHP rule), so wrap it
                $message = wodwrap($message, 70);
                // send mail

                mail("webmaster@npsp-ci.com",$subject,$message,"From: $from\n");
            }
        }
		
		/*poll_form is not defined in the code ... */
        if (isset($_POST['PollUserAnswerExtend']) && !empty($_POST['PollUserAnswerExtend'])) {
            $valid = true;

            $items=$_POST['PollUserAnswerExtend'];
           foreach ($items as $i => $value)
           {
               $pollUserAnswer = new PollUserAnswerExtend();
                   $pollUserAnswer->poll_answer_id = $value['poll_answer_id'];
                   $pollUserAnswer->status =1;
                   $pollUserAnswer->answer =1;
                   $valid=$pollUserAnswer->validate() && $valid;

                   $pollUserAnswer->save();
           }
		}

        $this->render('index', array(
			'homeModels' => $homeModels,
		));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if($error=Yii::app()->errorHandler->error) {
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionPartner() {
		$partnerModels = PartnerExtend::model('PartnerExtend')->frontEnd()->findAll();
		$this->render('partner', array(
			'partnerModels' => $partnerModels,
		));
	}

	
	public function actionNewsAll() {
		$page_size = 12;
		$criteria = new CDbCriteria(array(
			"condition" => "t.status = '".StatusBehavior::STATUS_ACTIV."'",
			"order" => "t.create_time DESC",
		));

		$items_count = NewsExtend::model('NewsExtend')->count($criteria);
		$pages = new CPagination($items_count);
		$pages->setPageSize($page_size);
		$pages->applyLimit($criteria);
		$newsModels = NewsExtend::model('NewsExtend')->findAll($criteria);

		if (!empty($newsModels)) {
			$this->render('news_all', array(
				'newsModels' => $newsModels,
				'pages'         =>  $pages,
				'page_size'     =>  $page_size,
				'items_count'   =>  $items_count,
			));
		} else {
			$this->redirect(array('site/index'));
		}
	}
	
	public function actionPublicationAll() {
		$page_size = 12;
		$criteria = new CDbCriteria(array(
			"condition" => "t.status = '".StatusBehavior::STATUS_ACTIV."'",
			"order" => "t.create_time DESC",
		));

		$items_count = PublicationExtend::model('PublicationExtend')->count($criteria);
		$pages = new CPagination($items_count);
		$pages->setPageSize($page_size);
		$pages->applyLimit($criteria);
		$pModels = PublicationExtend::model('PublicationExtend')->findAll($criteria);

		if (!empty($pModels)) {
			$this->render('publication_all', array(
				'pModels' => $pModels,
				'pages'         =>  $pages,
				'page_size'     =>  $page_size,
				'items_count'   =>  $items_count,
			));
		} else {
			$this->redirect(array('site/index'));
		}
	}

	public function toBeDeleted() {

	}

	public function actionNews() {
		$newsModels = NewsExtend::model('NewsExtend')->findAll(array(
            "condition" => "t.status = '".StatusBehavior::STATUS_ACTIV."'",
            "order" => "t.create_time DESC",
            'limit' => 3,
			));

		if(isset($_GET['id']) && !empty($_GET['id'])){
            $id = $_GET['id'];
            $currnewsModel = NewsExtend::model('NewsExtend')->frontEnd()->find(array( "condition" => "t.id = '".$id."'",));
        } else {
            $currnewsModel = NewsExtend::model('NewsExtend')->frontEnd()->find(array(
                "condition" => "t.status = '".StatusBehavior::STATUS_ACTIV."'",
                "order" => "t.create_time DESC",
                "limit" => 1,));
        }

		if (!empty($currnewsModel)) {
			$this->render('news', array(
				'newsModels' => $newsModels,
				'currentnews' => $currnewsModel,
			));
		} else {
			$this->redirect(array('site/index'));
		}
	}

	private function commonLogic($className, $title, $extension = 'Extend') {
		$fullClass = $className.$extension;
		$objectModels = $fullClass::model($fullClass)->frontEnd()->findAll();

        if(isset($_GET['id']) && !empty($_GET['id'])){
            $id = $_GET['id'];
            $currObjectModel = $fullClass::model($fullClass)->frontEnd()->find(array( "condition" => "t.id = '".$id."'",));
        } else {
            $currObjectModel = $fullClass::model($fullClass)->frontEnd()->find(array(
                "condition" => "t.status = '".StatusBehavior::STATUS_ACTIV."'",
                "order" => "t.create_time DESC",
                "limit" => 1,));
        }

        //todo:Remove from $objectsModel the currentObject

		if (!empty($currObjectModel)) {
			$this->render('commonView', array(
				'objectModels' => $objectModels,
				'currentObject' => $currObjectModel,
				'title' => $title,
			));
		} else {
			$this->redirect(array('site/index'));
		}
	}

	public function actionOrganisation() {
		$this->commonLogic('Organisation', 'Organisation');
	}

	public function actionPresentation() {
		$this->commonLogic('Presentation', 'Présentation');
	}

	public function actionActivities() {
		$this->commonLogic('Activity', 'Activités');
	}

	public function actionContact() {
		$contactModel = ContactExtend::model('ContactExtend')->frontEnd()->find();
		if (!empty($contactModel)) {
		$this->render('contact', array(
			'contactModel' => $contactModel,
					));
		} else {
			$this->redirect(array('site/index'));
		}
	}

    public function actionGalleryPhoto() {
        $galleryPhotosModels = GalleryPhotoExtend::model('GalleryPhotoExtend')->frontEnd()->findAll(array(
	        "order" => "t.create_time DESC",
	        'limit' => 5,
	    ));

        if(isset($_GET['id']) && !empty($_GET['id'])){
            $id = $_GET['id'];
            $currphotos = PhotoExtend::model('PhotoExtend')->frontEnd()->findAll(array( "condition" => "t.gallery_photo_id = '".$id."'",));
        } else {
            $currphotos = $models = Yii::app()->db->createCommand()
                ->select('p.name as name ,p.id as id , g.id as gallery_photo_id')
                ->from('photo p')
                ->join('(select id,name from gallery_photo where status = 1  order by create_time DESC limit 1) g', 'g.id=p.gallery_photo_id')
                ->where('p.status=:status', array(':status'=>StatusBehavior::STATUS_ACTIV))
                ->queryAll();
        }

		if (!empty($currphotos)) {
			$cs = Yii::app()->clientScript;
			$cs->registerCoreScript('jquery');
			$cs->registerScript('gallerie', '
				$(".fancy").click(function(){
					var rel = $(this).attr("rel");
					rel = "."+rel;
					$(rel).trigger("click");
					return false;
				});
			', CClientScript::POS_END);
			$this->render('galleryphoto', array(
				'galleryPhotosModels' => $galleryPhotosModels,
				'currphotos'=> $currphotos,
			));
		} else 	{
			$homeModels = HomeExtend::model('HomeExtend')->frontEnd()->findAll();
			$this->render('index', array(
					'homeModels' => $homeModels,
			));
		}
    }

    public function actionGalleryVideo() {
        $galleryVideosModels = GalleryVideoExtend::model('GalleryVideoExtend')->frontEnd()->findAll(array(
	        "order" => "t.create_time DESC",
			'limit' => 5,
        ));

        if(isset($_GET['id']) && !empty($_GET['id'])){
            $id = $_GET['id'];
            $currvideoModel = GalleryVideoExtend::model('GalleryVideoExtend')->frontEnd()->find(array( "condition" => "t.id = '".$id."'",));
        } else {
            $currvideoModel = GalleryVideoExtend::model('GalleryVideoExtend')->frontEnd()->find(array(
                "condition" => "t.status = '".StatusBehavior::STATUS_ACTIV."'",
                "order" => "t.create_time DESC",
                "limit" => 1,));
        }
		
		if (!empty($currvideoModel)) {
			$this->render('galleryvideo', array(
				'videoModels' => $galleryVideosModels,
				'currentvideo' => $currvideoModel,
			));
		} else {
			$homeModels = HomeExtend::model('HomeExtend')->frontEnd()->findAll();
			$this->render('index', array(
					'homeModels' => $homeModels,
			));
		}
    }

    public function actionPoll() {

        if (isset($_POST['PollUserAnswerExtend']) && !empty($_POST['PollUserAnswerExtend'])) {
           // print_r($_POST);
            $valid = true;

            $items=$_POST['PollUserAnswerExtend'];
           foreach ($items as $i => $value)
           {
               $pollUserAnswer = new PollUserAnswerExtend();
                   $pollUserAnswer->poll_answer_id = $value['poll_answer_id'];
                   $pollUserAnswer->status =1;
                   $pollUserAnswer->answer =1;
                   $valid=$pollUserAnswer->validate() && $valid;

                   $pollUserAnswer->save();
           }
            $pollModel = PollExtend::model('PollExtend')->frontEnd()->find();
            $pollQuestions = array();
            if (isset($pollModel->pollQuestions) && !empty($pollModel->pollQuestions)) {
                $pollQuestions = $pollModel->pollQuestions(array('scopes' => 'frontEnd'));
            }
            $seoResult = array(SeoExtend::model('SeoExtend')->getSeoParameters('pollresult'));
            $this->render('pollresult', array(
                'pollModel' => $pollModel,
                'pollQuestions' => $pollQuestions,
                'seoResult' => $seoResult,
            ));

        }
        else {
            $pollModel = PollExtend::model('PollExtend')->frontEnd()->find();
            $pollQuestions = array();
            if (isset($pollModel->pollQuestions) && !empty($pollModel->pollQuestions)) {
                $pollQuestions = $pollModel->pollQuestions(array('scopes' => 'frontEnd'));
            }
            $model=new PollUserAnswerExtend();
           // $pollAnswers = $pollQuestions->pollAnswers(array('scopes', 'frontend'));
            $seoResult = array(SeoExtend::model('SeoExtend')->getSeoParameters('poll'));
            $this->render('poll', array(
                'pollQuestions' => $pollQuestions,
                'model' => $model,
                'seoResult' => $seoResult,
            ));
        }
    }

}