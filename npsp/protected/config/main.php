<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
Yii::setPathOfAlias('root', realpath('./'));
Yii::setPathOfAlias('bootstrap', realpath('./common/protected/extensions/bootstrap'));
Yii::setPathOfAlias('yiiwheels', realpath('./common/protected/extensions/yiiwheels'));
Yii::setPathOfAlias('simpleWorkflow', realpath('./common/protected/extensions/simpleWorkflow'));

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'NPSP',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'bootstrap.helpers.TbHtml',
		'bootstrap.helpers.TbArray',
        'bootstrap.behaviors.TbWidget',
		'root.common.protected.extensions.components.yush.*',
		'root.common.protected.models.*',
		'root.common.protected.components.*',
		'application.models.*',
		'application.components.*',
		'simpleWorkflow.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		/*
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'Enter Your Password Here',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		*/
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		'bootstrap' => array(
			'class' => 'bootstrap.components.TbApi',
		),
		'yush' => array(
			'class' => 'root.common.protected.extensions.components.yush.YushComponent',
		),
        'yiiwheels' => array(
            'class' => 'yiiwheels.YiiWheels',
        ),
        'swSource'=> array(
            'class'=>'simpleWorkflow.SWPhpWorkflowSource',
            'basePath'=>'root.common.protected.models.workflows',
        ),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'class'=>'NpspUrlManager',
			'urlFormat'=>'path',
			'showScriptName'=>true,

		),
		'db'=>require Yii::getPathOfAlias('root.common.protected.config.partial').'/db.php',
		// uncomment the following to use a MySQL database
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);