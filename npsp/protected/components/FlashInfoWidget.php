<?php

class FlashInfoWidget extends CWidget {

    public function run() {
        $model = FlashInfoExtend::model('FlashInfoExtend')->findAll(array(
                                                                    "condition" => "t.status = '".StatusBehavior::STATUS_ACTIV."'",
                                                                    "order" => "t.create_time DESC",
                                                                    "limit" => 3,
                                                                ));
        $this->render('flashInfo', array(
            'flashInfos'=>$model
        ));
    }
}

?>