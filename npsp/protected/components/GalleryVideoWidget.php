<?php

class GalleryVideoWidget extends CWidget {

    public function run() {
        $models = GalleryVideoExtend::model('GalleryVideoExtend')->find(array(
                                                                    "condition" => "t.status = '".StatusBehavior::STATUS_ACTIV."'",
                                                                    "order" => "t.create_time DESC",
                                                                    "limit" => 1,
                                                                ));
        $this->render('galleryvideo', array(
            'video'=>$models
        ));
    }
}

?>