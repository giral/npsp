<?php

class PublicationWidget extends CWidget {
	public $title = 'Publications';
    public function run() {
        $models = PublicationExtend::model('PublicationExtend')->findAll(array(
            "condition" => "t.status = '".StatusBehavior::STATUS_ACTIV."'",
            "order" => "t.position,t.create_time DESC",
            "limit" => 3,
        ));
        $this->render('publication', array(
            'models'=>$models,
            'title'=>$this->title,
        ));
    }
}

?>