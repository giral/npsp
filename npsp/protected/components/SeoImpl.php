<?php
Yii::import('root.common.protected.extensions.behaviors.StatusBehavior');
class SeoImpl extends CComponent {

	public static $actionClassMappings = array(
		'index' => SeoExtend::PAGE_HOME,
		'partner' => SeoExtend::PAGE_PARTNER,
		'activities' => SeoExtend::PAGE_ACTIVITY,
		'news' => SeoExtend::PAGE_NEWS,
		'organisation' => SeoExtend::PAGE_ORGANISATION,
		'presentation' => SeoExtend::PAGE_PRESENTATION,
		'contact' => SeoExtend::PAGE_CONTACT,
		'galleryPhoto' => SeoExtend::PAGE_GALLERYPHOTO,
		'galleryVideo' => SeoExtend::PAGE_GALLERYVIDEO,
		'publication' => SeoExtend::PAGE_PUBLICATION,
		'news' => SeoExtend::PAGE_NEWS_ALL,
		'publications' => SeoExtend::PAGE_PUBLICATION_ALL,
	);

	public static $pageTitle = '';

	public function build() {
		$actionName = $this->getActionName();
		$page = array_key_exists($actionName, self::$actionClassMappings) ? self::$actionClassMappings[$actionName] : '';
		if(isset($page) && !empty($page)) {
			/** @var $seoModel SeoExtend */
			$seoModel = SeoExtend::getSeoParameters($page);
			if($seoModel) {
				/** @var $cs CClientScript */
				$cs = Yii::app()->clientScript;
				$cs->registerMetaTag($seoModel->metadesc, 'description');
				$cs->registerMetaTag($seoModel->tagh1, 'name');
				self::$pageTitle = $seoModel->title;
			}
		}
	}

	private function getActionName() {
		return Yii::app()->controller->action->id;
	}


} 