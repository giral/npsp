<?php

class GalleryPhotoWidget extends CWidget {

    public function run() {
		/*$models = Yii::app()->db->createCommand()
            ->select('g.id as galleryId,p.name as photo ,p.id as photo_id ,g.name as gallery')
            ->from('photo p')
            ->join('(select id,name from gallery_photo where status = 1  order by create_time DESC limit 3) g', 'g.id=p.gallery_photo_id')
            ->where('p.status=:status', array(':status'=>StatusBehavior::STATUS_ACTIV))
            ->limit(3)
            ->queryAll();*/
        $models = GalleryPhotoExtend::model('GalleryPhotoExtend')->photoFrontEnd()->with('photosLimitOne')->findAll();
		$this->render('galleryphoto', array(
			'models'=>$models
		));
	}

} ?>