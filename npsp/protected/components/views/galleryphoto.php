<div class="photos-container col-md-8 col-xs-10 col-xs-offset-1 col-md-offset-0">
	<span class="cartridge">Photos</span>
	<?php
	if (isset($models) && !empty($models)) {
		foreach ($models as $i => $model) {
			/** @var $onePhotoModel PhotoExtend */
			$photomodel = $model->photosLimitOne;
			$url = Yii::app()->createUrl('/site/galleryPhoto', array('id'=>$model->id));
			echo '<a href="'.$url.'"><img src="'.$photomodel->getImageUrl('name').'" alt="" /></a>';
			if($i > 4)
				break;
		}
	}
	?>
</div>