<?php

   $html = "";
   $link_histo = "<a style='color: #FDAB15;font-size: 0.9em;font-style: italic;font-weight: bold;'  href='".Yii::app()->createUrl('/site/newsAll')."'>Plus d'actualités</a>";
    $cpt =0;
   foreach ($models as $NewsModel) {
	    $url = Yii::app()->createUrl('/site/news', array('id'=>$NewsModel['id']));
        if ($cpt == 0) {
            $html .=  '<li class="col-xs-12 col-md-3"><a href="'.$url.'">';
        } else {
            $html .=  '<li class="col-xs-12 col-md-3 col-md-offset-1"><a href="'.$url.'">';
        }
        $html .=  '<span><img src="'.$NewsModel->getImageUrl('preview_image_name').'"></span>';
        $html .=  '<p>'.str_replace("'","''",$NewsModel['preview']).'</p></a></li>';
        $cpt++;
    }
?>

<section class="news row">
    <h2 class="label  label-title">Actualités</h2>
    <div class="row">
        <ul class="list-inline col-md-offset-1 col-md-10">

            <?php echo $html; ?>
        </ul>	
    </div>
	<?php echo $link_histo; ?>
</section>