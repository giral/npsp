<?php

    $html = "";
    $cpt =0;
    foreach ($models as $partnerModel) {
        if ($cpt == 0) {
            $html .=  "<div class='item active'>";
        }
        else {
            $html .=  "<div class='item'>";
        }
        $html .=  "<a href='".$partnerModel['link']."' target='_blank' class='inner-img'><img  src='".$partnerModel->getImageUrl('image_name')."'></a></div>";
        $cpt++;
    }
?>


<section class="partner row">
    <div class="col-lg-12">
        <div class="carousel slide" id="carousel">
            <!-- <div class="carousel-inner col-md-offset-1 col-md-10"> -->
         <div class="carousel-inner">
             <?php echo $html; ?>
            </div>
            <a class="left carousel-control" href="#carousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </div>
</section>










