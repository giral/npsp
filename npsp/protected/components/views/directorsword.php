<?php
if (isset($homeContentModel) && !empty($homeContentModel['image_name_title'])) {
    $content = $homeContentModel->content;
    $image_name_title = $homeContentModel->getImageUrl('image_name_title');
    $image_name = $homeContentModel->getImageUrl('image_name');
    echo '
        <article class="article-content">
            <h1>
              <span class="inner-img">
                <img src="'.$image_name_title.'">
              </span>
            </h1>
            <p>
                <img src="'.$image_name.'"  class="pull-left">
                '. $content.'
            </p>
        </article>';
    }
?>



