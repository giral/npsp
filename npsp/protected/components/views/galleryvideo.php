<div class="videos-container col-xs-10 col-xs-offset-1 col-md-3 col-md-offset-1">
    <span class="cartridge"> Videos</span>
    <?php
    if (isset($video) && !empty($video)) {
        $img = CHtml::image($video->getImageUrl('image_name_preview'), 'Vidéo');
        echo CHtml::link($img,Yii::app()->createUrl('/site/galleryVideo', array('id'=>$video->id)));
    }
    ?>
</div>