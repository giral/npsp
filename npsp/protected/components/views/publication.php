<?php
/**
 * @var $title string
 */
?>
<aside class="publication col-sm-6 col-md-12">
    <h2 class="label  label-title"><?php echo $title; ?></h2>
    
   <ul class="list-unstyled">
        <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/Avis_d'appel_a_manifestation_d'intérêt.pdf" target="_blank">
                            <span class="picto-doc"><img src="" alt="" /></span>
                            <span> Appel d'offre international a manifestation d'interet pour la preselectiondes fournisseurs de medicaments et consomables Medico-pharmaceutique</span>
                        </a>
                    </li>
    </ul>
    
    
    <ul class="list-unstyled">

<?php

$link_histo = "<a style='color: #FDAB15;font-size: 0.9em;font-style: italic;font-weight: bold;' href='".Yii::app()->createUrl('/site/publicationAll')."'>Plus de publications</a>";
$tmpContent = '';
$cpt = 0;
if (isset ($models) && !empty($models)) {
    foreach ($models as $publication) {


        $tmpContent .= '<li>
                        <a href="' . $publication->getImageUrl('document') . '" target="_blank">
                            <span class="picto-doc"><img src="'.$publication->getImageUrl('icon').'" alt="" /></span>
                            <span>' . $publication['description'] . '</span>
                        </a>
                    </li>';


    }
    echo $tmpContent;
}
?>
        </ul>
		<?php echo $link_histo; ?>
    </aside>