<?php

class PartnerWidget extends CWidget {

    public function run() {
        $models = PartnerExtend::model('PartnerExtend')->findAll(array(
            "condition" => "t.status = '".StatusBehavior::STATUS_ACTIV."'",
            "order" => "t.position,t.create_time DESC",
        ));

        $this->render('partner', array(
            'models'=>$models
        ));
    }
}

?>