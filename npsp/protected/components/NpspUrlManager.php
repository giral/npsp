<?php
/**
 * Created by IntelliJ IDEA.
 * User: Carlitox
 * Date: 24/07/2014
 * Time: 20:24
 */

class NpspUrlManager extends CUrlManager {

	const CONTROLLER_NAME = 'site/';

	private function getRules() {
		/* Default values */
        $rules = array(
			''=>'index',
			'presentation'=>'presentation',
			'news'=>'news',
			'partner'=>'partner',
			'organisation'=>'organisation',
			'activites'=>'activities',
			'galleryPhoto'=>'galleryPhoto',
			'galleryVideo'=>'galleryVideo',
			'contact'=>'contact',
			'poll'=>'poll',
			'publication'=>'publication',
			'activities'=>'activities',
			'newsAll'=>'newsAll',
			'publicationAll'=>'publicationAll',
		);
		/** @var $seoExtendModels SeoExtend[] */
		$seoExtendModels = SeoExtend::model('SeoExtend')->frontEnd()->findAll();
		if($seoExtendModels) {
			foreach ($seoExtendModels as $seoExtendModel) {
				$urlrw = $seoExtendModel->rewrittenurl;
				$page = $seoExtendModel->page;
				if(!empty($urlrw) && in_array($page, $rules)) {
					$key = array_search($page, $rules);
					$currentValue = $rules[$key];
					unset($rules[$key]);
					$rules[$urlrw] = $currentValue;
				}
			}
		}
		foreach($rules as $i => $rule) {
			$rules[$i] = self::CONTROLLER_NAME.$rule;
		}
		return $rules;
	}

	protected function processRules() {
		$this->rules = $this->getRules();
		return parent::processRules();
	}

}