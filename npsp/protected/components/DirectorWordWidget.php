<?php

class DirectorWordWidget extends CWidget {

    public function run() {
        $model = HomeContentExtend::model('HomeContentExtend')->find(array(
            "condition" => "t.status = '".StatusBehavior::STATUS_ACTIV."' ",
        ));
        if($model) {
            $this->render('directorsword', array(
                'homeContentModel'=>$model
            ));
        }

    }
}

?>