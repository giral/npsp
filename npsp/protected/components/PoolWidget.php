<?php

class PoolWidget extends CWidget {

    public function run() {
        $pollModel = PollExtend::model('PollExtend')->frontEnd()->find();

        $pollQuestions = array();
        if (isset($pollModel->pollQuestions) && !empty($pollModel->pollQuestions)) {
            $pollQuestions = $pollModel->pollQuestions(array('scopes' => 'frontEnd'));
        }

        $this->render('poolprev', array(
            'models'=>$pollQuestions
        ));
    }
}

?>