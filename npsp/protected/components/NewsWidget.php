<?php

class NewsWidget extends CWidget {

    public function run() {
        $models = NewsExtend::model('NewsExtend')->findAll(array(
            "condition" => "t.status = '".StatusBehavior::STATUS_ACTIV."'",
            "order" => "t.create_time DESC",
            'limit' => 3,
        ));

        $this->render('news', array(
            'models'=>$models
        ));
    }
}

?>