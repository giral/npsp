<section class="main-content withpagination row">
<div class="col-md-8">
<h1>
<span class="inner-img">
    <img src="../../images/Actualites.png">
</span>
</h1>
<article class="pagination-content">
    <div class="row">
     <ul class="list-inline col-md-12">
	<?php
	if(isset($newsModels) && ! empty($newsModels)) {
		$i = 0;
		$content = '';
		foreach ($newsModels as $NewsModel) {
		$url = Yii::app()->createUrl('/site/news', array('id'=>$NewsModel['id']));
		if ($i == 0){$content .= '<div class="row">';}
			$content .=  '<li class="col-xs-12 col-md-3 col-md-offset-1"><a href="'.$url.'">';
			$content .=  '<span><img src="'.$NewsModel->getImageUrl('preview_image_name').'"></span>';
			$content .=  '<p>'.str_replace("'","''",$NewsModel['preview']).'</p></a></li>';
			$i++;
			if ($i  == 3){$content .= '</div>'; $i = 0;}
		}
		if ($i  != 0){$content .= '</div>';}
		echo  $content;
	}
	?>
    </ul>
              <div class="col-md-12 row pagination-nav">
                <div>
                  <!--<a href="" class="label page">1</a>-->
<?php
/**
 * @var $this SiteController
 * @var $newsModels NewsExtend[]
 */

if(isset($pages) && isset($items_count) && isset($page_size)) {



	$this->widget('CLinkPager', array('currentPage' => $pages->getCurrentPage(), 'itemCount' => $items_count,
	                                  'pageSize' => $page_size, 'maxButtonCount' => 10,'cssFile' => false,
	                                  'prevPageLabel' => 'Page Précédente', 'nextPageLabel' => 'Page Suivante',
	                                  'firstPageLabel' => '', 'lastPageLabel' => '', 'header' => '',));
}
 ?>
 				</div>                
              </div>
             </div>
          </article>
        </div>
     <div class="col-md-4 row">
        <div class="row">
            <?php $this->widget('PublicationWidget'); ?>
            <div class="col-sm-6 col-md-12">
                <aside class="order-img">
                    <div class="inner-img">
                        <img src="images/order.png" />
                    </div>
                </aside>
               <?php $this->widget('PoolWidget'); ?>
            </div>
        </div>
    </div>
</section>
 
<section class="multimedia row">
    <h2 class="label  label-title">Galerie Multimedia</h2>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="multimedia-inner row">
                <?php
                    $this->widget('GalleryPhotoWidget');
                    $this->widget('GalleryVideoWidget');
                ?>
            </div>
        </div>
    </div>
</section>