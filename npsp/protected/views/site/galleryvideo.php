<?php
/**
 * @var $this SiteController
 * @var $videoModels GalleryVideoExtend[]
 * @var $currentvideo GalleryVideoExtend Current
 */


$menuContent = '';
$video = '';
$content = '';
$title='';
if (isset($currentvideo) && !empty($currentvideo)) {
    $video = '<iframe width="735" height="450" src="' . $currentvideo['video_link'] . '" frameborder="0" allowfullscreen></iframe>';
    $content = '<p>'.$currentvideo['content'].'</p>';
    $title = '<h3>'.$currentvideo['name'].'</h3>';
}

if (isset($videoModels) && !empty($videoModels)) {
    foreach ($videoModels as $videoModel) {
        if (isset($videoModel) && !empty($videoModel)) {
            $url = $this->createUrl('/site/galleryVideo', array('id'=>$videoModel['id']));
            $menuContent .= '<li><a href="'.$url.'"><span class="thumb"><img src="' . $videoModel->getImageUrl('icon') . '" alt=""/></span>
                                    <span>' . $videoModel['name'] . '<br/>
                                    <small>' . $videoModel['create_time'] . '</small></span>
                                    </a></li>';

        }
    }
}
?>

<section class="main-content videos row">
    <div class="col-md-8">
        <h1>
            <span class="inner-img">
              <img src="images/video.png">
            </span>
        </h1>
        <div class="inner-img main-img">
           <?php echo $video; ?>
        </div>
        <article class="article-content">
            <?php echo  $title; ?>
            <?php echo  $content; ?>

        </article>
    </div>
    <div class="col-md-4 row">
        <div class="row">
            <aside class="videos-list col-md-12 col-sm-6">
                <h2 class="label  label-title">Autres videos</h2>
                <ul class="list-unstyled">
                    <?php echo $menuContent; ?>
                </ul>
            </aside>
            <?php $this->widget('PublicationWidget'); ?>
            <div class="col-sm-6 col-md-12">
                <?php $this->widget('PoolWidget'); ?>
            </div>
        </div>
    </div>
</section>
<?php $this->widget('NewsWidget'); ?>
