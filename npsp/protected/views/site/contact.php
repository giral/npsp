<?php
/**
 * @var $this SiteController
 * @var $contactModel ContactExtend
 */
?>
<section class="main-content contact row">
    <div class="col-sm-7">
        <h1>
            <?php echo $contactModel->part_name; ?>
        </h1>
        <div class="address">
            <?php echo nl2br($contactModel->up_content); ?>
        </div>
        <div class="direction">
            <?php echo nl2br($contactModel->down_content);  ?>
        </div>
    </div>
    <div class="col-sm-5">
        <div class="inner-img">
            <img src="<?php echo $contactModel->getImageUrl('image_name_illustration_1'); ?>" alt=""/>
        </div>
    </div>
</section>
<section class="contact-us row">
    <h2 class="label  label-title">Nous &eacute;crire</h2>
    <form class="contact-form col-md-7" method="post" name="contact_form" action="<?php echo $this->createUrl('/site/index'); ?>">
        <div class="row">
            <div class="form-group col-md-12">
                <label for="name" class="col-md-2">Nom: </label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="name" id="name">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label for="tel" class="col-md-2">T&eacute;l&eacute;phone: </label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="tel" id="tel">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label for="mail" class="col-md-2">Email: </label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="mail" id="mail">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label for="object" class="col-md-2">Objet: </label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="objet" id="object">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label for="message" class="col-md-2">Message: </label>
                <div class="col-md-10">
                    <textarea class="form-control" name="message" id="message">
                    </textarea>
                </div>
            </div>
        </div>
        <div class="form-group">
            <input type="submit" class="pull-right btn" name="submit" value="Envoyer">
        </div>
    </form>
</section>



