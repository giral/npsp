<?php
/**
 * @var $this SiteController
 * @var $homeModels HomeExtend[]
 * */

$SideContent = '';

$preview ='';
$title = '';
$news = '';
$content = '';
$author = '';

if(isset($currentnews) && ! empty($currentnews)) {
    $preview = $currentnews->getImageUrl('image_name');
    $title = $currentnews['title'];
    $news = $currentnews['content'];
    $author = $currentnews['author'];
}
if(isset($newsModels) && ! empty($newsModels)) {
	foreach ($newsModels as $newsModel) {
		$url = $this->createUrl('/site/news', array('id'=>$newsModel['id']));
		$SideContent .= '
		<li>
			<a href="'.$url.'">
				<span class="thumb"><img src="'.$newsModel->getImageUrl('preview_image_name').'" alt="" /></span>
				<span>"'.$newsModel['title'].'"<br/>
				<small>'. $newsModel['create_time'] .' | '. $newsModel['author'].'</small></span>
			</a>
		</li>';
	}
}
?>
<section class="main-content article row">
    <div class="col-md-8">
        <div class="inner-img main-img">
            <img src="<?php echo  $preview;?>" />
        </div>
        <article class="article-content">
            <h3>
                <?php echo  $title;?>
            </h3>
            <p>
               <?php  echo $news;?>
            </p>
           <span class="autor">
              <?php  echo $author;?>
            </span>
        </article>
    </div>
    <div class="col-md-4 row">
        <div class="row">
            <aside class="news-list col-md-12 col-sm-6">
                <h2 class="label  label-title">Actualités</h2>
                <ul class="list-unstyled">
                    <?php
                        echo $SideContent;
                    ?>
                </ul>
            </aside>
            <div class="col-sm-6 col-md-12">
                <?php $this->widget('PoolWidget'); ?>
            </div>
        </div>
    </div>
</section>
<?php $this->widget('NewsWidget'); ?>
<section class="multimedia row">
    <h2 class="label  label-title">Galerie Multimedia</h2>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="multimedia-inner row">
                <?php
                $this->widget('GalleryPhotoWidget');
                $this->widget('GalleryVideoWidget');
                ?>
            </div>
        </div>
    </div>
</section>