<?php
    /**
     * @var $this SiteController
     * @var $contactModels ContactExtend[]
     */

    $title = "";
    $metaDesc = "";
    $tagH1 = "";

    if (isset($seoResult['title'])) {
        $title = $seoResult['title'];
    }
    if(isset($seoResult['metadesc'])){
        $metaDesc = $seoResult['metadesc'];
    }
    if(isset($seoResult['tagh1'])){
        $tagH1 = $seoResult['tagh1'];
    }
    $this->pageTitle =$title;
    Yii::app()->clientScript->registerMetaTag($metaDesc, 'description');
    Yii::app()->clientScript->registerMetaTag($tagH1, 'name');
