﻿<section class="main-content publicationAll withpagination row">
<div class="col-md-8">
<h1>
<span class="inner-img">
    <img src="../../images/publications.png">
</span>
</h1>
<article class="pagination-content">
    <div class="row">
     <ul class="list-inline col-md-12">
	<?php
	if(isset($pModels) && ! empty($pModels)) {
		$i = 0;
		$content = '';
		foreach ($pModels as $publication) {
		if ($i == 0){$content .= '<div class="row">';}
			$content .= 
			'<li class="col-xs-12 col-md-3">
                        <a target="_blank"  href="' . $publication->getImageUrl('document') . '">
                            <span><img height="76" width="55" src="'.$publication->getImageUrl('icon').'" alt="" /></span>
                            <p>'. $publication['description'] . '</p>
                        </a>
             </li>';
			 $i++;
			if ($i == 3){$content .= '</div>';$i = 0;}
		}
		if ($i != 0){$content .= '</div>';}
		echo  $content;
	}
	?>
    </ul>
              <div class="col-md-12 row pagination-nav">
                <div>
                  <!--<a href="" class="label page">1</a>-->
                  
                
        

<?php
/**
 * @var $this SiteController
 * @var $pModels PublicationExtend[]
 */	
if(isset($pages) && isset($items_count) && isset($page_size)) {


	$this->widget('CLinkPager', array(
        'currentPage'       => $pages->getCurrentPage(),
        'itemCount'         => $items_count,
        'pageSize'          => $page_size,
        'maxButtonCount'    => 10,
        'prevPageLabel'     => 'Page Précédente',
        'nextPageLabel'     => 'Page Suivante',
        'firstPageLabel'    => '',
        'lastPageLabel'     => '',
        'header'            => '',
		'cssFile' => false,
	));
 }
 ?>
				</div>                
              </div>
             </div>
          </article>
        </div>
     <div class="col-md-4 row">
        <div class="row">
            <?php $this->widget('PublicationWidget'); ?>
            <div class="col-sm-6 col-md-12">
                <aside class="order-img">
                    <div class="inner-img">
                        <img src="images/order.png" />
                    </div>
                </aside>
               <?php $this->widget('PoolWidget'); ?>
            </div>
        </div>
    </div>
</section>
 
<section class="multimedia row">
    <h2 class="label  label-title">Galerie Multimedia</h2>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="multimedia-inner row">
                <?php
                    $this->widget('GalleryPhotoWidget');
                    $this->widget('GalleryVideoWidget');
                ?>
            </div>
        </div>
    </div>
</section>