<?php
/* @var $this ContactController */
/* @var $model PoolQuestionExtend */
/* @var $model PoolAnswerExtend */
/* @var $form TbActiveForm */
?>
    <div class="form">

    <?php

    $form= $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'poll-form',

        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    $tmpContent = '';
    $cpt = 0;
	foreach ($pollQuestions as $i => $pollQuestion) {
        $cpt = $i + 1;
        $pollAnswers = $pollQuestion->pollAnswers;
        $pollAnswer1 = $pollAnswers[0];
        $pollAnswer2 = $pollAnswers[1];
        $tmpContent .= $form->label($pollQuestion, $pollQuestion['content']);
        $tmpContent .= $form->radioButtonList($model , "[$cpt]poll_answer_id", array(
            $pollAnswer1['id'] => $pollAnswer1['content'],
            $pollAnswer2['id'] => $pollAnswer2['content']
        ));
    }

    echo $tmpContent;
?>
        <div class="form-actions">
            <?php echo TbHtml::submitButton( 'Soumettre' ,array(
                'color'=>TbHtml::BUTTON_COLOR_WARNING,
                'size'=>TbHtml::BUTTON_SIZE_LARGE,
            )); ?>
        </div>

        <?php $this->endWidget(); ?>

    </div>