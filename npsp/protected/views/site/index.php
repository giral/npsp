<?php
/**
 * @var $this SiteController
 * @var $homeModels HomeExtend[]
 * */
?>
<section class="main-content row">
    <div class="col-md-8">
        <div class="inner-img">
	        <?php $this->beginWidget('ext.eCycleWidget.ECycleWidget');
                foreach ($homeModels as $homeModel) {
                    $image = CHtml::image($homeModel->getImageUrl('image_name'));
                    echo $image;
                }
                $this->endWidget();
            ?>
            <?php /*  echo TbHtml::carousel($valuesArray); */ ?>
        </div>
        <?php $this->widget('DirectorWordWidget'); ?>
    </div>
    <div class="col-md-4 row">
    	<aside class="order-img">
                   
                </aside>
        <div class="row">
            <?php $this->widget('PublicationWidget'); ?>
            <div class="col-sm-6 col-md-12">
                <aside class="order-img">
                    <div class="inner-img">
                      <a href="http://www.sante.gouv.ci/" target="_blank">  <img src="../../images/order.png" /></a>
                    </div>
                </aside>
               <?php $this->widget('PoolWidget'); ?>
            </div>
        </div>
    </div>
</section>
<?php $this->widget('NewsWidget'); ?>
<section class="multimedia row">
    <h2 class="label  label-title">Galerie Multimedia</h2>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="multimedia-inner row">
                <?php
                    $this->widget('GalleryPhotoWidget');
                    $this->widget('GalleryVideoWidget');
                ?>
            </div>
        </div>
    </div>
</section>