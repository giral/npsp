<?php
/**
 * @var $this SiteController
 * @var $presentationModels PresentationExtend[]
* @var $currentpresentation PresentationExtend Current
 */


$menuContent = '';
$titleImage = '';
$content = '';
$url = '';
$partName = '';

if (isset($currentpresentation) && !empty($currentpresentation)) {
    $titleImage = '<img src='.$currentpresentation->getImageUrl('image_name_title').' />';
    $content = $currentpresentation->content;
	$partName = $currentpresentation->part_name;
}

if (isset($presentationModels) && !empty($presentationModels)) {
    foreach ($presentationModels as $presentationModel) {
        if ((isset($presentationModel) && !empty($presentationModel) && $partName != $presentationModel->part_name)) {
			$url = $this->createUrl('', array('id'=>$presentationModel->id));
			$menuContent .= '<li><a href="'.$url.'"><span>' .$presentationModel->part_name. '</span></a></li>';
           // $menuContent .= '<li><a href=presentation?id="' . $presentationModel->id. '"><span>' .$presentationModel->part_name. '</span></a></li>';
        }
    }
}
?>

<section class="main-content presentation row">
<div class="col-md-8">
    <h1>
	    <span class="inner-img">
	      <?php  echo $titleImage; ?>
	    </span>
    </h1>
    <article class="article-content">
        <p><?php echo $content; ?></p>
    </article>
</div>
<div class="col-md-4 row">
    <div class="row">
        <aside class="presentations-list col-md-12 col-sm-6">
            <h2 class="label  label-title">Présentations</h2>
            <ul class="list-unstyled">
                <?php echo $menuContent; ?>
            </ul>
        </aside>
            <?php $this->widget('PublicationWidget'); ?>
        <div class="col-sm-6 col-md-12">
             <?php $this->widget('PoolWidget'); ?>
        </div>
    </div>
</div>
</section>
<?php $this->widget('NewsWidget'); ?>
<section class="multimedia row">
    <h2 class="label  label-title">Galerie Multimedia</h2>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="multimedia-inner row">
                <?php
                $this->widget('GalleryPhotoWidget');
                $this->widget('GalleryVideoWidget');
                ?>
            </div>
        </div>
    </div>
</section>