<?php
    /**
	 * @var $this SiteController
	 * @var $activityModels ActivityExtend[]
	 */
    foreach ($activityModels as $activityModel)
    {
        echo CHtml::encode($activityModel['part_name']);
        echo CHtml::image($activityModel->getImageUrl('image_name_1'));
        echo CHtml::image($activityModel->getImageUrl('image_name_2'));
        echo CHtml::image($activityModel->getImageUrl('image_name_3'));
    }
   ?>