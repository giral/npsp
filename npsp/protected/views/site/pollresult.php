<?php
/* @var $this PollController */
/* @var $pollModel PollExtend */
?>
<?php $content = '';
$questionModels = $pollModel->pollQuestions;
if(isset($questionModels) && !empty($questionModels)) {
    foreach ($questionModels as $questionModel) {
        $data = array(array('Task','Answers'));
        $answerModels = $questionModel->pollAnswers;
        if(isset($answerModels) && !empty($answerModels)) {
            foreach($answerModels as $answerModel) {
                $data[] = array($answerModel->content, count($answerModel->pollUserAnswers));
            }
        }
        $content .= $this->widget('yiiwheels.widgets.google.WhVisualizationChart', array(
            'visualization' => 'BarChart',
            'data' => $data,
            'options' => array(
                'title' => $questionModel->content,
            )
        ), true);
    }
}
$this->widget('yiiwheels.widgets.box.WhBox', array(
    'title' => $pollModel->name,
    'content' =>$content,
)); ?>