<?php
/**
 * @var $this SiteController
 * @var $organisationModels OrganisationExtend[]
 */
    foreach ($organisationModels as $organisationModel)
    {
        echo CHtml::encode($organisationModel['part_name']);
        echo CHtml::image($organisationModel->getImageUrl('image_name_1'));
        echo CHtml::image($organisationModel->getImageUrl('image_name_2'));
        echo CHtml::image($organisationModel->getImageUrl('image_name_3'));
    }
?>