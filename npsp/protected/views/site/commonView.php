<?php
/**
 * @var $this SiteController
 * @var $objectModels PresentationExtend[]
 * @var $currentObject PresentationExtend Current
 * @var $dynamicRoute String
 * @var $title String
 */

$menuContent = '';
$titleImage = '';
$content = '';

if(isset($currentObject) && !empty($currentObject)) {
	$url = $this->createUrl('', array('id'=>$currentObject->id));
	$menuContent .= '<li><a href="'.$url.'"><span>' .$currentObject->part_name. '</span></a></li>';
	$titleImage = '<img src='.$currentObject->getImageUrl('image_name_title').' />';
	$content = $currentObject->content;
}
if (isset($objectModels) && !empty($objectModels)) {
    foreach ($objectModels as $objectModel) {
        if (isset($objectModel) && !empty($objectModel)) {
            if($objectModel->id != $currentObject->id) {
	            $url = $this->createUrl('', array('id'=>$objectModel->id));
				$menuContent .= '<li><a href="'.$url.'"><span>' .$objectModel->part_name. '</span></a></li>';
            }
        }
    }
}
?>

<section class="main-content presentation row">
<div class="col-md-8">
    <h1>
	    <span class="inner-img">
	      <?php  echo $titleImage; ?>
	    </span>
    </h1>
    <article class="article-content">
        <p><?php echo $content; ?></p>
    </article>
</div>
<div class="col-md-4 row">
    <div class="row">
        <aside class="presentations-list col-md-12 col-sm-6">
            <h2 class="label label-title"><?php echo $title; ?></h2>
            <ul class="list-unstyled">
                <?php echo $menuContent; ?>
            </ul>
        </aside>
            <?php $this->widget('PublicationWidget'); ?>
        <div class="col-sm-6 col-md-12">
             <?php $this->widget('PoolWidget'); ?>
        </div>
    </div>
</div>
</section>
<?php $this->widget('NewsWidget'); ?>
<section class="multimedia row">
    <h2 class="label  label-title">Galerie Multimedia</h2>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="multimedia-inner row">
                <?php
                $this->widget('GalleryPhotoWidget');
                $this->widget('GalleryVideoWidget');
                ?>
            </div>
        </div>
    </div>
</section>