<?php
/**
 * @var $this SiteController
 * @var $videoModels GalleryVideoExtend[]
 * @var $currentvideo GalleryVideoExtend Current
 */
$menuContent = '';
$name= '';
$text= '';
$content = '';
$gid =0;
if (isset($currphotos) && !empty($currphotos)) {
	$maskedPhotos = '';
    foreach ($currphotos as $currphoto) {
        if (isset($currphoto) && !empty($currphoto)) {
            $photomodel = PhotoExtend::model('PhotoExtend')->find(array(
                "condition" => "t.id = '".$currphoto['id']."'",
            ));

            $gallery_id = $currphoto['gallery_photo_id'];
            $url = $this->createUrl('/site/galleryPhoto', array('id'=>$gallery_id));
            $content .= '
                <div class="col-xs-4 col-sm-3 col-md-4 ">
                    <span class="thumbnail">
                        <img src="'.$photomodel->getImageUrl('name').'" class="fancy" rel="photos_Gal" />
                    </span>
                </div>';
	        $maskedPhotos .= '<a href="'.$photomodel->getImageUrl('name').'" rel="photos_rel" class="photos_Gal">&nbsp;</a>';
        }
    }
	$this->widget('ext.fancybox.EFancyBox', array(
		'target'=>'.photos_Gal',
		'mouseEnabled'=>true,
		'easingEnabled'=>true,
		'config'=>array(
		),
	)); ?>
	<div style="display: none;">
		<?php echo $maskedPhotos; ?>
	</div>
<?php
}

if (isset($galleryPhotosModels) && !empty($galleryPhotosModels)) {
    foreach ($galleryPhotosModels as $galleryPhotosModel) {
        if (isset($galleryPhotosModel) && !empty($galleryPhotosModel)) {
            $url = $this->createUrl('/site/galleryPhoto', array('id'=>$galleryPhotosModel['id']));
            $menuContent .= '<li><a href="'.$url.'"><span>' . $galleryPhotosModel['name'].
                    '<br/><small>' . $galleryPhotosModel['create_time'] . '</span></small></a></li>';

            if($galleryPhotosModel['id'] == $gid ) {
                $text = $galleryPhotosModel['name'];
                 //$content = $galleryPhotosModel['content'];
            }
        }
    }
}
?>

<section class="main-content photos row">
    <div class="col-md-8">
        <h1>
            <span class="inner-img">
              <img src="images/photo.png">
            </span>
        </h1>
        <article class="article-content">
            <h3>
                <?php  $name; ?>
            </h3>
            <p>
                <?php echo $text; ?>
            </p>
			<div class="row">
            <?php echo $content; ?>
			</div>
        </article>
    </div>
    <div class="col-md-4 row">
        <div class="row">
            <aside class="videos-list col-md-12 col-sm-6">
                <h2 class="label  label-title">Voir aussi ...</h2>
                <ul class="list-unstyled">
                    <?php echo $menuContent; ?>
                </ul>
            </aside>
            <?php $this->widget('PublicationWidget', array(
                'title'=>'Voir aussi...',
            )); ?>
        </div>
    </div>
</section>
<?php $this->widget('NewsWidget'); ?>
