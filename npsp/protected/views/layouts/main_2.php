<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="fr" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
	<script type="text/javascript">
        window.location = "http://www.npsp.ci/update_navigator.html";
    </script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<?php
//Yii::app()->bootstrap->register();
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();

?>
<body class="home">
<div class="container">
<div class="surheader row" style="background-image: url('../../../images/arabesque.png');">
    <div class="col-lg-12">
        <form class="form-email pull-right" method="post">
            <label for="email">Votre email</label>
            <input id="email" name="email" type="text" style="width:150px" class="input-small" placeholder="">
            <button type="submit" class="btn btn-xs">OK</button>
        </form>
    </div>
</div>
<header class="header row" style="min-height: 232px;">
</header>

<nav class="menu row">
    <ul class="list-unstyled col-md-9">
        <li class="<?php if (Yii::app()->controller->getRoute() == "site/index") { echo "active"; } ?> col-sm-2"><a href="<?php echo $this->createUrl('/site/index'); ?>">Accueil</a></li>
        <li class="<?php if (Yii::app()->controller->getRoute() == "site/presentation") { echo "active"; } ?> col-sm-2"><a href="<?php echo $this->createUrl('/site/presentation'); ?>">Présentation</a></li>
        <li class="<?php if (Yii::app()->controller->getRoute() == "site/organisation") { echo "active"; } ?> col-sm-2"><a href="<?php echo $this->createUrl('/site/organisation'); ?>">Organisation</a> </li>
	    <li class="<?php if (Yii::app()->controller->getRoute() == "site/activities") { echo "active"; } ?> col-sm-2"><a href="<?php echo $this->createUrl('/site/activities'); ?>">Activités</a> </li>
        <li class="<?php if (Yii::app()->controller->getRoute() == "site/contact") { echo "active"; } ?> col-sm-2"><a href="<?php echo $this->createUrl('/site/contact'); ?>">Nous écrire</a></li>
         <li class="<?php if (Yii::app()->controller->getRoute() == "site/site") { echo "active"; } ?> col-sm-2"><a href="http://www.npsp.ci/Espace_client/loginEC.php">Espace Client</a></li>
    </ul>
</nav>
    <?php $this->widget('FlashInfoWidget'); ?>
    <?php echo $content; ?>

    <?php $this->widget('PartnerWidget'); ?>


<footer class="footer row">
    <p class="col-md-offset-1 col-md-10">Copyright @ Nouvelle PSP Côte d'ivoire 2014 | tous droits réservés | Administration</p>
</footer>
</div>
</body>
<?php
$cs->registerCoreScript('jquery');
$cs->registerCssFile($baseUrl.'/css/bootstrap.min.css');
$cs->registerCssFile($baseUrl.'/css/app.css');
$cs->registerScriptFile($baseUrl.'/js/bootstrap.min.js');
$cs->registerScriptFile($baseUrl.'/js/app.js');
$cs->registerScriptFile($baseUrl.'/js/j_marquee.js');
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56971301-1', 'auto');
  ga('send', 'pageview');

</script>
</html>