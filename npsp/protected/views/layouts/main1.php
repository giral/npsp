<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="fr" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
	<script type="text/javascript">
        window.location = "http://www.npsp.ci/update_navigator.html";
    </script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<?php
//Yii::app()->bootstrap->register();
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();

?>
<body class="home">
<div class="container">
<div class="surheader row" style="background-image: url('../../../images/arabesque.png');">
    <div class="col-lg-12">
        <form class="form-email pull-right" method="post">
            <label for="email">Votre email</label>
            <input id="email" name="email" type="text" style="width:150px" class="input-small" placeholder="">
            <button type="submit" class="btn btn-xs">OK</button>
        </form>
    </div>
</div>
<header class="header row" style="min-height: 232px;">
</header>

<nav class="menu row">
    <ul class="list-unstyled col-md-9">
        <li class="<?php if (Yii::app()->controller->getRoute() == "site/index") { echo "active"; } ?> col-sm-2"><a href="<?php echo $this->createUrl('/site/index'); ?>">Accueil</a></li>
        <li class="<?php if (Yii::app()->controller->getRoute() == "site/presentation") { echo "active"; } ?> col-sm-2"><a href="<?php echo $this->createUrl('/site/presentation'); ?>">Présentation</a></li>
        <li class="<?php if (Yii::app()->controller->getRoute() == "site/organisation") { echo "active"; } ?> col-sm-2"><a href="<?php echo $this->createUrl('/site/organisation'); ?>">Organisation</a> </li>
	    <li class="<?php if (Yii::app()->controller->getRoute() == "site/activities") { echo "active"; } ?> col-sm-2"><a href="<?php echo $this->createUrl('/site/activities'); ?>">Activités</a> </li>
        <li class="<?php if (Yii::app()->controller->getRoute() == "site/contact") { echo "active"; } ?> col-sm-2"><a href="<?php echo $this->createUrl('/site/contact'); ?>">Nous écrire</a></li>
         <li class="<?php if (Yii::app()->controller->getRoute() == "site/site") { echo "active"; } ?> col-sm-2"><a href="http://www.npsp.ci/Espace_client/loginEC.php">Espace Client</a></li>
    </ul>
</nav>
    <?php $this->widget('FlashInfoWidget'); ?>
    <?php echo $content; ?>

    <?php $this->widget('PartnerWidget'); ?>


<footer class="footer row">

	<div class="lien_utile" style="width:100%; height:auto; padding-left:10px; padding-right:10px; padding-top:10px; padding-bottom:10px"><center>
	<div style="clear:both"></div>
    <h4 align="center">----------------------------------------------Liens utiles----------------------------------------</h4>
    	<div style=" float:left; display:block; color:#030; font-size:12px; width:auto; height:17px; padding:3px; margin-left:5px; margin-top:5px"><a href="www.insp-ci.org" target="_blank">INSTITUT NATIONAL DE LA SANTE PUBLIQUE |</a></div>
        	<div style=" float:left; display:block; color:#030; font-size:12px; width:auto; height:17px; padding:3px; margin-left:5px ; margin-top:5px"><a href="www.cnts-ci.org" target="_blank">CENTRE NATIONAL DE TRANSFUSION SANGUINE |</a></div>
            <div style=" float:left; display:block; color:#030; font-size:12px; width:auto; height:17px; padding:3px; margin-left:5px; margin-top:5px" ><a href="www.cardiologie.gouv.ci" target="_blank">INSTITUT DE CARDIOLOGIE D'ABIDJAN |</a></div>
            <div style=" float:left; display:block; color:#030; font-size:12px; width:auto; height:17px; padding:3px; margin-left:5px; margin-top:5px"><a href="www.inhp.ci" target="_blank">INSTITUT NATIONAL D'HYGIENE PUBLIQUE</a></div>
            <div style=" float:left; display:block; color:#030; font-size:12px; width:auto;  height:17px;padding:3px; margin-left:5px; margin-top:5px"><a href="www.pasteur.ci" target="_blank">INSTITUT PASTEUR COTE D'IVOIRE |</a></div>
            <div style=" float:left; display:block; color:#030; font-size:12px; width:auto;  height:17px;padding:3px; margin-left:5px; margin-top:5px"><a href="www.samu-ci.com" target="_blank">SAMU |</a></div>
            <div style=" float:left; display:block; color:#030; font-size:12px; width:auto; height:17px; padding:3px; margin-left:5px; margin-top:5px"><a href="www.activitepharma-ci.org" target="_blank">PROGRAMME NATIONAL DE DEVELOPPEMENT DE LACTIVITE PHARMACEUTIQUE (PNDAP) |</a></div>
            <div style=" float:left; display:block; color:#030; font-size:12px; width:auto; padding:3px; margin-left:5px; margin-top:5px"><a href="www.dphm.ci" target="_blank">DIRECTION DE LA PHARMACIE ET DU MEDICAMENT (DPM) |</a></div>
            <div style=" float:left; display:block; color:#030; font-size:12px; width:auto; height:17px; padding:3px; margin-left:5px; margin-top:5px"><a href="www.ordrepharmacien.ci" target="_blank">CONSEIL NATIONAL DE LORDRE DES PHARMACIENS DE COTE DIVOIRE (CNOP-CI) |</a></div>
            <div style=" float:left; display:block; color:#030; font-size:12px; width:auto; height:17px; padding:3px; margin-left:5px; margin-top:5px"><a href="www.who.int" target="_blank">ORGANISATION MONDIALE DE LA SANTE (OMS) |</a></div>
                <div style=" float:left; display:block; color:#030; font-size:12px; width:auto; height:17px; padding:3px; margin-left:5px; margin-top:5px"><a href="www.synacassci.net" target="_blank">SYNACASS-CI |</a></div>
                <div style=" float:left; color:#030; font-size:12px; width:auto; height:17px; padding:3px; margin-left:5px; margin-top:5px"><a href="www.univ-fhb.edu.ci" target="_blank">UFR SCIENCES MEDICALES |</a></div>
                    <div style=" float:left; display:block; color:#030; font-size:12px; width:auto; height:17px; padding:3px; margin-left:5px; margin-top:5px"><a href="www.univ-fhb.edu.ci" target="_blank">UFR SCIENCES PHARMACEUTIQUES ET BIOLOGIQUES |</a></div>
                        
                                        <div style=" float:left; color:#030; font-size:12px; width:auto; height:17px; padding:3px; margin-left:5px; margin-top:5px"><a href="www.wahooas.org" target="_blank">ORGANISATION OUEST AFRICAINE DE LA SANTE (OOAS) |</a></div>
  </center>
    <div style="clear:both"></div>
</div>
    <p class="col-md-offset-1 col-md-10">Copyright @ Nouvelle PSP Côte d'ivoire 2014 | tous droits réservés | Administration</p>
</footer>
</div>
</body>
<?php
$cs->registerCoreScript('jquery');
$cs->registerCssFile($baseUrl.'/css/bootstrap.min.css');
$cs->registerCssFile($baseUrl.'/css/app.css');
$cs->registerScriptFile($baseUrl.'/js/bootstrap.min.js');
$cs->registerScriptFile($baseUrl.'/js/app.js');
$cs->registerScriptFile($baseUrl.'/js/j_marquee.js');
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56971301-1', 'auto');
  ga('send', 'pageview');

</script>
</html>