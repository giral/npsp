<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="fr" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<?php
function getBrowser() 
{ 
    $u_agent = $_SERVER['HTTP_USER_AGENT']; 
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";
	$ub = "";
    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
    
    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Internet Explorer'; 
        $ub = "MSIE"; 
    } 
    elseif(preg_match('/Firefox/i',$u_agent)) 
    { 
        $bname = 'Mozilla Firefox'; 
        $ub = "Firefox"; 
    } 
    elseif(preg_match('/Chrome/i',$u_agent)) 
    { 
        $bname = 'Google Chrome'; 
        $ub = "Chrome"; 
    } 
    elseif(preg_match('/Safari/i',$u_agent)) 
    { 
        $bname = 'Apple Safari'; 
        $ub = "Safari"; 
    } 
    elseif(preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Opera'; 
        $ub = "Opera"; 
    } 
    elseif(preg_match('/Netscape/i',$u_agent)) 
    { 
        $bname = 'Netscape'; 
        $ub = "Netscape"; 
    } 
    
    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
    
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }
    
    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
    
    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
} 

// now try it
$ua=getBrowser();
if  ($ua['name'] == "MSIE" && $ua['version'] > 9)
{

}else{
?>
<?php
//Yii::app()->bootstrap->register();
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();

?>
<body class="home">
<div class="container">
<div class="surheader row" style="background-image: url('../../../images/arabesque.png');">
    <div class="col-lg-12">
        <form class="form-email pull-right" method="post">
            <label for="email">Votre email</label>
            <input id="email" name="email" type="text" style="width:150px" class="input-small" placeholder="">
            <button type="submit" class="btn btn-xs">OK</button>
        </form>
    </div>
</div>
<header class="header row" style="min-height: 232px;">
</header>

<nav class="menu row">
    <ul class="list-unstyled col-md-9">
        <li class="<?php if (Yii::app()->controller->getRoute() == "site/index") { echo "active"; } ?> col-sm-2"><a href="<?php echo $this->createUrl('/site/index'); ?>">Accueil</a></li>
        <li class="<?php if (Yii::app()->controller->getRoute() == "site/presentation") { echo "active"; } ?> col-sm-2"><a href="<?php echo $this->createUrl('/site/presentation'); ?>">Présentation</a></li>
        <li class="<?php if (Yii::app()->controller->getRoute() == "site/organisation") { echo "active"; } ?> col-sm-2"><a href="<?php echo $this->createUrl('/site/organisation'); ?>">Organisation</a> </li>
	    <li class="<?php if (Yii::app()->controller->getRoute() == "site/activities") { echo "active"; } ?> col-sm-2"><a href="<?php echo $this->createUrl('/site/activities'); ?>">Activités</a> </li>
        <li class="<?php if (Yii::app()->controller->getRoute() == "site/contact") { echo "active"; } ?> col-sm-2"><a href="<?php echo $this->createUrl('/site/contact'); ?>">Nous écrire</a></li>
         <li class="<?php if (Yii::app()->controller->getRoute() == "site/site") { echo "active"; } ?> col-sm-2"><a href="http://www.npsp.ci/Espace_client/loginEC.php">Espace Client</a></li>
    </ul>
</nav>
    <?php $this->widget('FlashInfoWidget'); ?>
    <?php echo $content; ?>

    <?php $this->widget('PartnerWidget'); ?>


<footer class="footer row">
    <p class="col-md-offset-1 col-md-10">Copyright @ Nouvelle PSP Côte d'ivoire 2014 | tous droits réservés | Administration</p>
</footer>
</div>
</body>
<?php
$cs->registerCoreScript('jquery');
$cs->registerCssFile($baseUrl.'/css/bootstrap.min.css');
$cs->registerCssFile($baseUrl.'/css/app.css');
$cs->registerScriptFile($baseUrl.'/js/bootstrap.min.js');
$cs->registerScriptFile($baseUrl.'/js/app.js');
$cs->registerScriptFile($baseUrl.'/js/j_marquee.js');
}
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56971301-1', 'auto');
  ga('send', 'pageview');

</script>
</html>