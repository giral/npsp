ActivityExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search
 [x] Search extend
 [x] Search workflow
 [x] After save

ContactExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search
 [x] Search extend
 [x] Search workflow
 [x] After save

EmailExtend
 [x] Rules
 [x] Attributes
 [x] Search
 [x] Search extend

FlashInfoExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search
 [x] Search extend
 [x] Search workflow

GalleryPhotoExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search
 [x] Search extend
 [x] Search workflow

GalleryVideoExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search
 [x] Search extend
 [x] Search workflow

HomeExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search
 [x] Search extend
 [x] Search workflow

NewsExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search
 [x] Search extend
 [x] Search workflow

OrganisationExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search
 [x] Search extend
 [x] Search workflow
 [x] After save

PartnerExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search
 [x] Search extend
 [x] Search workflow

PhotoExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search

PoolAnswerExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search

PollExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search
 [x] Search extend
 [x] Search workflow
 [x] After logical delete
 [x] After save activation

PollQuestionExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search

PollUserAnswerExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search

PresentationExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search
 [x] Search extend
 [x] Search workflow
 [x] After save

SeoExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search extend
 [x] Search
 [x] Get seo parameters

SWFStatusesExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search

SWFStatusesRelationsExtendSWFStatusesExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search

SWFWorkflowExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search

UserExtend
 [x] Rules
 [x] Scopes
 [x] Attributes
 [x] Search
 [x] Search extend
 [x] Crypt pass

