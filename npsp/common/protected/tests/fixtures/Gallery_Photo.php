<?php
return array(
    'galleryPhoto1'=>array(
        'id'=>1,
        'name'=>'GalleryPhoto1',
        'position'=>1,
        'status'=>1,
        'sw_status'=>'',
        'create_time'=>'2014-09-10 10:09:10',
        'update_time'=>null,
    ),
    'galleryPhoto2'=>array(
        'id'=>2,
        'name'=>'GalleryPhoto2',
        'position'=>2,
        'status'=>1,
        'sw_status'=>'',
        'create_time'=>'2014-09-11 10:09:10',
        'update_time'=>'2014-09-11 14:09:10',
    ),
    'galleryPhotoDeleted'=>array(
        'id'=>10,
        'name'=>'GalleryPhotoDeleted',
        'position'=>10,
        'status'=>3,
        'sw_status'=>'',
        'create_time'=>'2014-09-11 10:09:10',
        'update_time'=>'2014-09-11 14:09:10',
    ),
    'galleryPhotoNotNullWorjflow'=>array(
        'id'=>3,
        'name'=>'GalleryPhotoNotNullWorjflow',
        'position'=>3,
        'status'=>1,
        'sw_status'=>'GalleryPhotoExtend/Node1',
        'create_time'=>'2014-09-11 10:09:10',
        'update_time'=>'2014-09-11 14:09:10',
    ),
);