<?php
return array(
    'swfstatusesrelations1'=>array(
        'id'=>1,
        'SWFStatuses_id_primary'=>1,
        'SWFStatuses_id_secondary'=>2,
        'status'=>1,
        'create_time'=>'2014-09-10 10:09:10',
        'update_time'=>null,
    ),
    'swfstatusesrelations2'=>array(
        'id'=>2,
        'SWFStatuses_id_primary'=>2,
        'SWFStatuses_id_secondary'=>3,
        'status'=>1,
        'create_time'=>'2014-09-10 10:09:10',
        'update_time'=>null,
    ),
    'swfstatusesrelationsDeleted'=>array(
        'id'=>3,
        'SWFStatuses_id_primary'=>1,
        'SWFStatuses_id_secondary'=>3,
        'status'=>3,
        'create_time'=>'2014-09-10 10:09:10',
        'update_time'=>null,
    ),
);