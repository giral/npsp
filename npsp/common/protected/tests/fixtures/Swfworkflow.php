<?php
return array(
    'swfworkflow1'=>array(
        'id'=>1,
        'name'=>'swfworkflow1',
        'workflowName'=>'swfContact',
        'status'=>1,
        'position'=>1,
        'create_time'=>'2014-09-10 10:09:10',
        'update_time'=>null,
    ),
    'swfworkflow2'=>array(
        'id'=>2,
        'name'=>'swfworkflow2',
        'workflowName'=>'swfActivity',
        'status'=>1,
        'position'=>2,
        'create_time'=>'2014-09-10 10:09:10',
        'update_time'=>null,
    ),
    'swfworkflowDeleted'=>array(
        'id'=>3,
        'name'=>'swfworkflow3',
        'workflowName'=>'swfPoll',
        'status'=>3,
        'position'=>3,
        'create_time'=>'2014-09-10 10:09:10',
        'update_time'=>null,
    ),
);