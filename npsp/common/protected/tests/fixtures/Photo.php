<?php
return array(
    'photo1'=>array(
        'id'=>1,
        'name'=>'Photo1',
        'gallery_photo_id'=>1,
        'position'=>1,
        'status'=>1,
        'create_time'=>'2014-09-10 10:09:10',
        'update_time'=>null,
    ),
    'photo2'=>array(
        'id'=>2,
        'name'=>'Photo2',
        'gallery_photo_id'=>2,
        'position'=>2,
        'status'=>1,
        'create_time'=>'2014-09-11 10:09:10',
        'update_time'=>'2014-09-11 14:09:10',
    ),
);