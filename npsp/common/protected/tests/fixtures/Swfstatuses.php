<?php
return array(
    'swfstatuses1'=>array(
        'id'=>1,
        'name'=>'swfstatuses1',
        'position'=>1,
        'SWFWorkflow_Id'=>1,
        'constraint'=>'swfstatuses_constraint1',
        'status'=>1,
        'create_time'=>'2014-09-10 10:09:10',
        'update_time'=>null,
    ),
    'swfstatuses2'=>array(
        'id'=>2,
        'name'=>'swfstatuses2',
        'position'=>2,
        'SWFWorkflow_Id'=>1,
        'constraint'=>'swfstatuses_constraint2',
        'status'=>1,
        'create_time'=>'2014-09-10 10:09:10',
        'update_time'=>null,
    ),
    'swfstatusesDeleted'=>array(
        'id'=>10,
        'name'=>'swfstatuses3',
        'position'=>22,
        'SWFWorkflow_Id'=>1,
        'constraint'=>'swfstatuses_constraint3',
        'status'=>3,
        'create_time'=>'2014-09-10 10:09:10',
        'update_time'=>null,
    ),
);