<?php
return array(
    'email1'=>array(
        'id'=>'1',
        'email'=>'toto@npsp.ci',
        'status'=>'1',
        'create_time'=>'2014-01-20 10:09:19',
        'update_time'=>null,
    ),
    'email2'=>array(
	    'id'=>'2',
        'email'=>'tata@npsp.ci',
        'status'=>'0',
        'create_time'=>'2014-01-20 11:20:00',
        'update_time'=>'2014-01-20 11:21:00',
    ),
    'emailDeleted'=>array(
	    'id'=>'10',
	    'email'=>'deleted@npsp.ci',
	    'status'=>3,
	    'create_time'=>'2014-01-20 11:20:00',
	    'update_time'=>'2014-01-20 11:21:00',
	),
);