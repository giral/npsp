<?php

class GalleryPhotoExtendTest extends CDbTestCase {

    public $fixtures = array(
        'galleryPhotos'=>'GalleryPhoto',
    );

    private $rulesTest = array(
        0 => array (
            0 => 'name, status',
            1 => 'required',
        ),
        1 => array (
            0 => 'position, status',
            1 => 'numerical',
            'integerOnly' => true,
        ),
        2 => array (
            0 => 'name',
            1 => 'length',
            'max' => 90,
        ),
        3 => array (
            0 => 'create_time, update_time',
            1 => 'safe',
        ),
        4 => array (
            0 => 'id, name, position, status, create_time, update_time',
            1 => 'safe',
            'on' => 'search',
        ),
		5 => array(
			'sw_status',
			'SWValidator',
		));

    private $scopesTest = array('frontEnd'=>array(
        'condition' => 't.status = 1',
        'order' => 't.position ASC',
    ),
        'notDeleted'=>array(
            'condition'=> 't.status != 3'
        ),
    );

    private $attributes =  array(
        'id' => 'ID',
        'name' => 'Name',
        'position' => 'Position',
        'status' => 'Status',
        'create_time' => 'Create Time',
        'update_time' => 'Update Time',
    );

    public function testRules() {
        $rules = GalleryPhotoExtend::model('GalleryPhotoExtend')->rules();
        sort($this->rulesTest);
        sort($rules);
        $this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
    }

    public function testScopes() {
        $scopes = GalleryPhotoExtend::model('GalleryPhotoExtend')->scopes();
        sort($this->scopesTest);
        sort($scopes);
        $this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
    }

    public function testAttributes() {
        $attributes = GalleryPhotoExtend::model()->attributeLabels();
        sort($this->attributes);
        sort($attributes);
        $this->assertEquals(json_encode($this->attributes), json_encode($attributes));
    }

    public function testSearch() {
        $galleryPhoto1Fixture = $this->galleryPhotos['galleryPhoto1'];
        $galleryPhoto = new GalleryPhoto();
        $galleryPhoto->name = $galleryPhoto1Fixture['name'];
        $galleryPhotoDataProvider = $galleryPhoto->search();
        $extract = array();
        foreach($galleryPhotoDataProvider->getData() as $record) {
            $extract[] = $record;
        }
        $extract = $extract[0];
        $this->assertTrue($extract instanceof GalleryPhoto);
        $keys = array_keys(GalleryPhoto::model()->attributeLabels());
        foreach($keys as $key) {
            $this->assertEquals($galleryPhoto1Fixture[$key], $extract->$key);
        }
    }

	public function testSearchExtend() {
		$galleryPhotoExtend = new GalleryPhotoExtend;
		$galleryPhotoExtendDataProvider = $galleryPhotoExtend->search();
		$this->assertCount(2, $galleryPhotoExtendDataProvider->data);
	}

	public function testSearchWorkflow() {
		$galleryPhotoExtend = new GalleryPhotoExtend;
		$galleryPhotoExtendDataProvider = $galleryPhotoExtend->searchWorkflow();
		$this->assertCount(1, $galleryPhotoExtendDataProvider->data);
	}

}