<?php

class OrganisationExtendTest extends CDbTestCase {

	public $fixtures = array(
		'organisations'=>'Organisation',
	);

	private $rulesTest = array(
		0 => array (
	        0 => 'part_name, image_name_1, image_name_2, image_name_3, status',
	        1 => 'required',
	    ),
		1 => array (
	        0 => 'status',
	        1 => 'numerical',
	        'integerOnly' => true,
	    ),
	    2 => array (
	        0 => 'part_name, image_name_1, image_name_2',
	        1 => 'length',
	        'max' => 90,
	    ),
	    3 => array (
	        0 => 'image_name_3, position',
	        1 => 'length',
	        'max' => 45,
	    ),
	    4 => array (
	        0 => 'create_time, update_time',
	        1 => 'safe',
	    ),
	    5 => array (
	        0 => 'id, part_name, image_name_1, image_name_2, image_name_3, position, status, create_time, update_time',
			1 => 'safe',
			'on' => 'search',
	    ),
	    6 => array (
	        0 => 'image_name_1',
	        1 => 'file',
	        'allowEmpty' => true,
	        'on' => 'update',
	    ),
	    7 => array (
			0 => 'image_name_2',
	        1 => 'file',
	        'allowEmpty' => true,
	        'on' => 'update',
	    ),
	    8 => array (
	        0 => 'image_name_3',
	        1 => 'file',
	        'allowEmpty' => true,
	        'on' => 'update',
	    ),
	    9 => array(
	        'sw_status',
	        'SWValidator',
	    ));

	private $attributes =  array(
		'id' => 'ID',
		'part_name' => 'Part Name',
		'image_name_1' => 'Image Name 1',
		'image_name_2' => 'Image Name 2',
		'image_name_3' => 'Image Name 3',
		'position' => 'Position',
		'status' => 'Status',
		'create_time' => 'Create Time',
		'update_time' => 'Update Time',
	);

	private $scopesTest = array('frontEnd'=>array(
		'condition' => 't.status = 1',
		'order' => 't.position ASC',
	),
    'notDeleted'=>array(
	    'condition'=>'t.status != 3'
	));

	public function testRules() {
		$rules = OrganisationExtend::model('OrganisationExtend')->rules();
		sort($this->rulesTest);
		sort($rules);
		$this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
	}

	public function testScopes() {
		$scopes = OrganisationExtend::model('OrganisationExtend')->scopes();
		sort($this->scopesTest);
		sort($scopes);
		$this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
	}

	public function testAttributes() {
		$attributes = OrganisationExtend::model()->attributeLabels();
		sort($this->attributes);
		sort($attributes);
		$this->assertEquals(json_encode($this->attributes), json_encode($attributes));
	}

	public function testSearch() {
		$organisation1Fixture = $this->organisations['organisation1'];
		$organisation = new Organisation();
		$organisation->part_name = $organisation1Fixture['part_name'];
		$OrganisationDataProvider = $organisation->search();
		$extract = array();
		foreach($OrganisationDataProvider->getData() as $record) {
		    $extract[] = $record;
		}
		$extract = $extract[0];
		$this->assertTrue($extract instanceof Organisation);
		$keys = array_keys(Organisation::model()->attributeLabels());
		foreach($keys as $key) {
			$this->assertEquals($organisation1Fixture[$key], $extract->$key);
		}
	}

	public function testSearchExtend() {
		$organisationExtend = new OrganisationExtend;
		$organisationExtendDataProvider = $organisationExtend->search();
		$this->assertCount(2, $organisationExtendDataProvider->data);
	}

	public function testSearchWorkflow() {
		$organisationExtend = new OrganisationExtend;
		$organisationExtendDataProvider = $organisationExtend->searchWorkflow();
		$this->assertCount(1, $organisationExtendDataProvider->data);
	}

	public function testAfterSave() {
		$organisation = new OrganisationExtend;
		$organisation->part_name = 'organisation3';
		$organisation->image_name_1 = 'image1_organisation3.jpg';
		$organisation->image_name_2 = 'image2_organisation3.jpg';
		$organisation->image_name_3 = 'image3_organisation3.jpg';
		$organisation->position = 3;
		$organisation->status = 1;
		$organisation->save();
		$organisationModels = OrganisationExtend::model('OrganisationExtend')->frontEnd()->findAll();
		$this->assertCount(1 , $organisationModels);
		$organisationModel = $organisationModels[0];
		$this->assertEquals($organisationModel->part_name, 'organisation3');
	}

}