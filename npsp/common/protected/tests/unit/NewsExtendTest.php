<?php

class NewsExtendTest extends CDbTestCase {

    public $fixtures = array(
        'news'=>'News',
    );

    private $rulesTest = array(
        0 => array (
            0 => 'title, preview, content, status',
            1 => 'required',
        ),
        1 => array (
            0 => 'status',
            1 => 'numerical',
            'integerOnly' => true,
        ),
        2 => array (
            0 => 'title',
            1 => 'length',
            'max' => 90,
        ),
        3 => array (
            0 => 'create_time, update_time',
            1 => 'safe',
        ),
        4 => array (
            0 => 'id, title, preview, content, status, create_time, update_time',
            1 => 'safe',
            'on' => 'search',
        ),
        5 => array(
	        'sw_status',
	        'SWValidator',
        ));

    private $scopesTest = array('frontEnd'=>array(
        'condition' => 't.status = 1',
        'order' => 't.position ASC,t.create_time ASC',
    ),
        'notDeleted'=>array(
            'condition'=> 't.status != 3'
        ),
    );

    private $attributes =  array(
        'id' => 'ID',
        'title' => 'Title',
        'preview' => 'Preview',
        'content' => 'Content',
        'status' => 'Status',
        'create_time' => 'Create Time',
        'update_time' => 'Update Time',
    );

    public function testRules() {
        $rules = NewsExtend::model('NewsExtend')->rules();
        sort($this->rulesTest);
        sort($rules);
        $this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
    }

    public function testScopes() {
        $scopes = NewsExtend::model('NewsExtend')->scopes();
        sort($this->scopesTest);
        sort($scopes);
        $this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
    }

    public function testAttributes() {
        $attributes = NewsExtend::model()->attributeLabels();
        sort($this->attributes);
        sort($attributes);
        $this->assertEquals(json_encode($this->attributes), json_encode($attributes));
    }

    public function testSearch() {
        $news1Fixture = $this->news['news1'];
        $news = new News();
        $news->title = $news1Fixture['title'];
        $newsDataProvider = $news->search();
        $extract = array();
        foreach($newsDataProvider->getData() as $record) {
            $extract[] = $record;
        }
        $extract = $extract[0];
        $this->assertTrue($extract instanceof News);
        $keys = array_keys(News::model()->attributeLabels());
        foreach($keys as $key) {
            $this->assertEquals($news1Fixture[$key], $extract->$key);
        }
    }

	public function testSearchExtend() {
		$newsExtend = new NewsExtend;
		$newsExtendDataProvider = $newsExtend->search();
		$this->assertCount(2, $newsExtendDataProvider->data);
	}

	public function testSearchWorkflow() {
		$newsExtend = new NewsExtend;
		$newsExtendDataProvider = $newsExtend->searchWorkflow();
		$this->assertCount(1, $newsExtendDataProvider->data);
	}

}