<?php

class ContactExtendTest extends CDbTestCase {

	public $fixtures = array(
		'contacts'=>'Contact',
	);

	private $rulesTest = array(
		0 => array (
	        0 => 'part_name, image_name_1, image_name_2, image_name_3, status',
	        1 => 'required',
	    ),
		1 => array (
	        0 => 'status',
	        1 => 'numerical',
	        'integerOnly' => true,
	    ),
	    2 => array (
	        0 => 'part_name, image_name_1, image_name_2',
	        1 => 'length',
	        'max' => 90,
	    ),
	    3 => array (
	        0 => 'image_name_3, position',
	        1 => 'length',
	        'max' => 45,
	    ),
	    4 => array (
	        0 => 'create_time, update_time',
	        1 => 'safe',
	    ),
	    5 => array (
	        0 => 'id, part_name, image_name_1, image_name_2, image_name_3, position, status, create_time, update_time',
			1 => 'safe',
			'on' => 'search',
	    ),
	    6 => array (
	        0 => 'image_name_1',
	        1 => 'file',
	        'allowEmpty' => true,
	        'on' => 'update',
	    ),
	    7 => array (
			0 => 'image_name_2',
	        1 => 'file',
	        'allowEmpty' => true,
	        'on' => 'update',
	    ),
	    8 => array (
	        0 => 'image_name_3',
	        1 => 'file',
	        'allowEmpty' => true,
	        'on' => 'update',
	    ),
		9 =>array(
	        'sw_status',
	        'SWValidator'
	    ));

	private $scopesTest = array('frontEnd'=>array(
		'condition' => 't.status = 1',
		'order' => 't.position ASC',
	),
    'notDeleted'=>array(
        'condition'=>'t.status != 3'
    ));

	private $attributes =  array(
		'id' => 'ID',
		'part_name' => 'Part Name',
		'image_name_1' => 'Image Name 1',
		'image_name_2' => 'Image Name 2',
		'image_name_3' => 'Image Name 3',
		'position' => 'Position',
		'status' => 'Status',
		'create_time' => 'Create Time',
		'update_time' => 'Update Time',
	);

	public function testRules() {
		$rules = ContactExtend::model('ContactExtend')->rules();
		sort($this->rulesTest);
		sort($rules);
		$this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
	}

	public function testScopes() {
		$scopes = ContactExtend::model('ContactExtend')->scopes();
		sort($this->scopesTest);
		sort($scopes);
		$this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
	}

	public function testAttributes() {
		$attributes = ContactExtend::model()->attributeLabels();
		sort($this->attributes);
		sort($attributes);
		$this->assertEquals(json_encode($this->attributes), json_encode($attributes));
	}

	public function testSearch() {
		$contact1Fixture = $this->contacts['contact1'];
		$contact = new Contact;
		$contact->part_name = $contact1Fixture['part_name'];
		$contactDataProvider = $contact->search();
		$extract = array();
		foreach($contactDataProvider->getData() as $record) {
		    $extract[] = $record;
		}
		$extract = $extract[0];
		$this->assertTrue($extract instanceof Contact);
		$keys = array_keys(Contact::model()->attributeLabels());
		foreach($keys as $key) {
			$this->assertEquals($contact1Fixture[$key], $extract->$key);
		}
	}

	public function testSearchExtend() {
		$contactExtend = new ContactExtend;
		$contactExtendDataProvider = $contactExtend->search();
		$this->assertCount(2, $contactExtendDataProvider->data);
	}

	public function testSearchWorkflow() {
		$contactExtend = new ContactExtend;
		$contactExtendDataProvider = $contactExtend->searchWorkflow();
		$this->assertCount(1, $contactExtendDataProvider->data);
	}

	public function testAfterSave() {
		$contactExtend = new ContactExtend;
		$contactExtend->part_name = 'contact3';
		$contactExtend->image_name_1 = 'image1_contact3.jpg';
		$contactExtend->image_name_2 = 'image2_contact3.jpg';
		$contactExtend->image_name_3 = 'image3_contact3.jpg';
		$contactExtend->position = 3;
		$contactExtend->status = 1;
		$contactExtend->save();
		$contactModels = ContactExtend::model('ContactExtend')->frontEnd()->findAll();
		$this->assertCount(1 , $contactModels);
		$contactModel = $contactModels[0];
		$this->assertEquals($contactModel->part_name, 'contact3');
	}

}