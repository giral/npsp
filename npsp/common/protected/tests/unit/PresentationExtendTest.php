<?php

class PresentationExtendTest extends CDbTestCase {

	public $fixtures = array(
		'presentations'=>'Presentation',
	);

	private $rulesTest = array(
		0 => array (
	        0 => 'part_name, image_name_1, image_name_2, image_name_3, status',
	        1 => 'required',
	    ),
		1 => array (
	        0 => 'status',
	        1 => 'numerical',
	        'integerOnly' => true,
	    ),
	    2 => array (
	        0 => 'part_name, image_name_1, image_name_2',
	        1 => 'length',
	        'max' => 90,
	    ),
	    3 => array (
	        0 => 'image_name_3, position',
	        1 => 'length',
	        'max' => 45,
	    ),
	    4 => array (
	        0 => 'create_time, update_time',
	        1 => 'safe',
	    ),
	    5 => array (
	        0 => 'id, part_name, image_name_1, image_name_2, image_name_3, position, status, create_time, update_time',
			1 => 'safe',
			'on' => 'search',
	    ),
	    6 => array (
	        0 => 'image_name_1',
	        1 => 'file',
	        'allowEmpty' => true,
	        'on' => 'update',
	    ),
	    7 => array (
			0 => 'image_name_2',
	        1 => 'file',
	        'allowEmpty' => true,
	        'on' => 'update',
	    ),
	    8 => array (
	        0 => 'image_name_3',
	        1 => 'file',
	        'allowEmpty' => true,
	        'on' => 'update',
	    ),
		9 => array(
			'sw_status',
			'SWValidator',
		));

	private $scopesTest = array('frontEnd'=>array(
		'condition' => 't.status = 1',
		'order' => 't.position ASC',
	),'notDeleted'=>array(
		'condition'=>'t.status != 3',
	));

	private $attributes =  array(
		'id' => 'ID',
		'part_name' => 'Part Name',
		'image_name_1' => 'Image Name 1',
		'image_name_2' => 'Image Name 2',
		'image_name_3' => 'Image Name 3',
		'position' => 'Position',
		'status' => 'Status',
		'create_time' => 'Create Time',
		'update_time' => 'Update Time',
	);

	public function testRules() {
		$rules = PresentationExtend::model('PresentationExtend')->rules();
		sort($this->rulesTest);
		sort($rules);
		$this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
	}

	public function testScopes() {
		$scopes = PresentationExtend::model('PresentationExtend')->scopes();
		sort($this->scopesTest);
		sort($scopes);
		$this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
	}

	public function testAttributes() {
		$attributes = PresentationExtend::model()->attributeLabels();
		sort($this->attributes);
		sort($attributes);
		$this->assertEquals(json_encode($this->attributes), json_encode($attributes));
	}

	public function testSearch() {
		$presentation1Fixture = $this->presentations['presentation1'];
		$presentation = new Presentation();
		$presentation->part_name = $presentation1Fixture['part_name'];
		$presentationDataProvider = $presentation->search();
		$extract = array();
		foreach($presentationDataProvider->getData() as $record) {
		    $extract[] = $record;
		}
		$extract = $extract[0];
		$this->assertTrue($extract instanceof Presentation);
		$keys = array_keys(Presentation::model()->attributeLabels());
		foreach($keys as $key) {
			$this->assertEquals($presentation1Fixture[$key], $extract->$key);
		}
	}

	public function testSearchExtend() {
		$presentationExtend = new PresentationExtend;
		$presentationExtendDataProvider = $presentationExtend->search();
		$this->assertCount(2, $presentationExtendDataProvider->data);
	}

	public function testSearchWorkflow() {
		$presentationExtend = new PresentationExtend;
		$presentationExtendDataProvider = $presentationExtend->searchWorkflow();
		$this->assertCount(1, $presentationExtendDataProvider->data);
	}

	public function testAfterSave() {
		$presentationExtend = new PresentationExtend;
		$presentationExtend->part_name = 'presentation3';
		$presentationExtend->image_name_1 = 'image1_presentation3.jpg';
		$presentationExtend->image_name_2 = 'image2_presentation3.jpg';
		$presentationExtend->image_name_3 = 'image3_presentation3.jpg';
		$presentationExtend->position = 3;
		$presentationExtend->status = 1;
		$presentationExtend->save();
		$presentationModels = PresentationExtend::model('PresentationExtend')->frontEnd()->findAll();
		$this->assertCount(1 , $presentationModels);
		$presentationModel = $presentationModels[0];
		$this->assertEquals($presentationModel->part_name, 'presentation3');
	}

}