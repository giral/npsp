<?php

class SWFStatusesExtendTest extends CDbTestCase {

    public $fixtures = array(
        'Swfstatusess'=>'Swfstatuses',
    );

    private $rulesTest = array(
        0 => array (
            0 => 'name, position, status',
            1 => 'required',
        ),
        1 => array (
            0 => 'position, status, SWFWorkflow_Id',
            1 => 'numerical',
            'integerOnly' => true,
        ),
        2 => array (
            0 => 'constraint',
            1 => 'length',
            'max' => 100,
        ),
        3 => array (
            0 => 'name',
            1 => 'length',
            'max' => 90,
        ),
        4 => array (
            0 => 'create_time, update_time',
            1 => 'safe',
        ),
        5 => array (
            0 => 'id, name, position, constraint, status, create_time, update_time, SWFWorkflow_Id',
            1 => 'safe',
            'on' => 'search',
        ));

    private $scopesTest = array(
        'notDeleted'=>array(
            'condition'=> 't.status != 3'
        ),
        'lowestPosition'=>array(
            'select'=>'id, name, MIN(position) as position'
        ),
        'toDelete'=>array(
			'select'=>'id',
			'condition'=>'t.status = 3',
		),
    );

    private $attributes =  array(
        'id' => 'ID',
        'name' => 'Name',
        'position' => 'Position',
        'constraint' => 'Constraint',
        'status' => 'Status',
        'create_time' => 'Create Time',
        'update_time' => 'Update Time',
        'SWFWorkflow_Id' => 'SWFWorkflowExtend',
    );

    public function testRules() {
        $rules = SWFStatusesExtend::model('SWFStatusesExtend')->rules();
        sort($this->rulesTest);
        sort($rules);
        $this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
    }

    public function testScopes() {
        $scopes = SWFStatusesExtend::model('SWFStatusesExtend')->scopes();
        sort($this->scopesTest);
        sort($scopes);
        $this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
    }

    public function testAttributes() {
        $attributes = SWFStatusesExtend::model()->attributeLabels();
        sort($this->attributes);
        sort($attributes);
        $this->assertEquals(json_encode($this->attributes), json_encode($attributes));
    }

    public function testSearch() {
        $Swfstatuses1Fixture = $this->Swfstatusess['swfstatuses1'];
        $Swfstatuses = new Swfstatuses();
        $Swfstatuses->name = $Swfstatuses1Fixture['name'];
        $SwfstatusesDataProvider = $Swfstatuses->search();
        $extract = array();
        foreach($SwfstatusesDataProvider->getData() as $record) {
            $extract[] = $record;
        }
        $extract = $extract[0];
        $this->assertTrue($extract instanceof Swfstatuses);
        $keys = array_keys(Swfstatuses::model()->attributeLabels());
        foreach($keys as $key) {
            $this->assertEquals($Swfstatuses1Fixture[$key], $extract->$key);
        }
    }

}