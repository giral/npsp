<?php

class EmailExtendTest extends CDbTestCase {

	public $fixtures = array(
		'emails'=>'Email',
	);

	private $rulesTest =  array(
		array('email, status', 'required'),
		array('status', 'numerical', 'integerOnly'=>true),
		array('email', 'length', 'max'=>90),
		array('create_time, update_time', 'safe'),
		array('id, email, status, create_time, update_time', 'safe', 'on'=>'search'),
	);

	private $attributes = array(
		'id' => 'ID',
		'email' => 'Email',
		'status' => 'Status',
		'create_time' => 'Create Time',
		'update_time' => 'Update Time',
	);

	public function testRules() {
		$rules = EmailExtend::model('EmailExtend')->rules();
		sort($this->rulesTest);
		sort($rules);
		$this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
	}

	public function testAttributes() {
		$attributes = EmailExtend::model()->attributeLabels();
		sort($this->attributes);
		sort($attributes);
		$this->assertEquals(json_encode($this->attributes), json_encode($attributes));
	}

	public function testSearch() {
		$email1Fixture = $this->emails['email1'];
		$email = new Email;
		$email->email = $email1Fixture['email'];
		$emailDataProvider = $email->search();
		$extract = array();
		foreach($emailDataProvider->getData() as $record) {
		    $extract[] = $record;
		}
		$extract = $extract[0];
		$this->assertTrue($extract instanceof Email);
		$keys = array_keys(Email::model()->attributeLabels());
		foreach($keys as $key) {
			$this->assertEquals($email1Fixture[$key], $extract->$key);
		}
	}

	public function testSearchExtend() {
		$emailExtend = new EmailExtend;
		$emailExtendDataProvider = $emailExtend->search();
		$this->assertCount(2, $emailExtendDataProvider->data);
	}

}