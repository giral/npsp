<?php

class PhotoExtendTest extends CDbTestCase {

    public $fixtures = array(
        'photos'=>'Photo',
    );

    private $rulesTest = array(
        0 => array (
            0 => 'name, status, gallery_photo_id',
            1 => 'required',
        ),
        1 => array (
            0 => 'position, status, gallery_photo_id',
            1 => 'numerical',
            'integerOnly' => true,
        ),
        2 => array (
            0 => 'create_time, update_time',
            1 => 'safe',
        ),
        3 => array (
            0 => 'id, name, position, status, create_time, update_time, gallery_photo_id',
            1 => 'safe',
            'on' => 'search',

        ),
        4 => array (
            0 => 'name',
            1 => 'length',
            'max' => 90,
        ),
    );

    private $scopesTest = array('frontEnd'=>array(
        'condition' => 't.status = 1',
        'order' => 't.position ASC',
    ),
        'notDeleted'=>array(
            'condition'=> 't.status != 3'
        ),
    );

    private $attributes =  array(
        'id' => 'ID',
        'name' => 'Name',
        'position' => 'Position',
        'status' => 'Status',
        'create_time' => 'Create Time',
        'update_time' => 'Update Time',
        'gallery_photo_id' => 'Gallery Photo',
    );

    public function testRules() {
        $rules = PhotoExtend::model('PhotoExtend')->rules();
        sort($this->rulesTest);
        sort($rules);
        $this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
    }

    public function testScopes() {
        $scopes = PhotoExtend::model('PhotoExtend')->scopes();
        sort($this->scopesTest);
        sort($scopes);
        $this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
    }

    public function testAttributes() {
        $attributes = PhotoExtend::model()->attributeLabels();
        sort($this->attributes);
        sort($attributes);
        $this->assertEquals(json_encode($this->attributes), json_encode($attributes));
    }

    public function testSearch() {
        $photo1Fixture = $this->photos['photo1'];
        $photo = new Photo();
        $photo->name = $photo1Fixture['name'];
        $photoDataProvider = $photo->search();
        $extract = array();
        foreach($photoDataProvider->getData() as $record) {
            $extract[] = $record;
        }
        $extract = $extract[0];
        $this->assertTrue($extract instanceof Photo);
        $keys = array_keys(Photo::model()->attributeLabels());
        foreach($keys as $key) {
            $this->assertEquals($photo1Fixture[$key], $extract->$key);
        }
    }

}