<?php

class ActivityExtendTest extends CDbTestCase {

	public $fixtures = array(
		'activities'=>'Activity',
	);

	private $rulesTest = array(
		0 => array (
	        0 => 'part_name, image_name_1, image_name_2, image_name_3, status',
	        1 => 'required',
	    ),
		1 => array (
	        0 => 'status',
	        1 => 'numerical',
	        'integerOnly' => true,
	    ),
	    2 => array (
	        0 => 'part_name, image_name_1, image_name_2',
	        1 => 'length',
	        'max' => 90,
	    ),
	    3 => array (
	        0 => 'image_name_3, position',
	        1 => 'length',
	        'max' => 45,
	    ),
	    4 => array (
	        0 => 'create_time, update_time',
	        1 => 'safe',
	    ),
	    5 => array (
	        0 => 'id, part_name, image_name_1, image_name_2, image_name_3, position, status, create_time, update_time',
			1 => 'safe',
			'on' => 'search',
	    ),
	    6 => array (
	        0 => 'image_name_1',
	        1 => 'file',
	        'allowEmpty' => true,
	        'on' => 'update',
	    ),
	    7 => array (
			0 => 'image_name_2',
	        1 => 'file',
	        'allowEmpty' => true,
	        'on' => 'update',
	    ),
	    8 => array (
	        0 => 'image_name_3',
	        1 => 'file',
	        'allowEmpty' => true,
	        'on' => 'update',
	    ),
	    9 =>array(
	        'sw_status',
	        'SWValidator'
	    ));

	private $scopesTest = array(
		'frontEnd'=>array(
			'condition' => 't.status = 1',
			'order' => 't.position ASC',
		),
		'notDeleted'=>array(
			'condition'=>'t.status != 3'
		),
	);

	private $attributes =  array(
		'id' => 'ID',
		'part_name' => 'Part Name',
		'image_name_1' => 'Image Name 1',
		'image_name_2' => 'Image Name 2',
		'image_name_3' => 'Image Name 3',
		'position' => 'Position',
		'status' => 'Status',
		'create_time' => 'Create Time',
		'update_time' => 'Update Time',
	);

	public function testRules() {
		$rules = ActivityExtend::model('ActivityExtend')->rules();
		sort($this->rulesTest);
		sort($rules);
		$this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
	}

	public function testScopes() {
		$scopes = ActivityExtend::model('ActivityExtend')->scopes();
		sort($this->scopesTest);
		sort($scopes);
		$this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
	}

	public function testAttributes() {
		$attributes = ActivityExtend::model()->attributeLabels();
		sort($this->attributes);
		sort($attributes);
		$this->assertEquals(json_encode($this->attributes), json_encode($attributes));
	}

	public function testSearch() {
		$activity1Fixture = $this->activities['activity1'];
		$activity = new Activity;
		$activity->part_name = $activity1Fixture['part_name'];
		$activityDataProvider = $activity->search();
		$extract = array();
		foreach($activityDataProvider->getData() as $record) {
		    $extract[] = $record;
		}
		$extract = $extract[0];
		$this->assertTrue($extract instanceof Activity);
		$keys = array_keys(Activity::model()->attributeLabels());
		foreach($keys as $key) {
			$this->assertEquals($activity1Fixture[$key], $extract->$key);
		}
	}

	public function testSearchExtend() {
		$activityExtend = new ActivityExtend;
		$activityExtendDataProvider = $activityExtend->search();
		$this->assertCount(2, $activityExtendDataProvider->data);
	}

	public function testSearchWorkflow() {
		$activityExtend = new ActivityExtend;
		$activityExtendDataProvider = $activityExtend->searchWorkflow();
		$this->assertCount(1, $activityExtendDataProvider->data);
	}

	public function testAfterSave() {
		$activityExtend = new ActivityExtend;
		$activityExtend->part_name = 'activity3';
		$activityExtend->image_name_1 = 'image1_activity3.jpg';
		$activityExtend->image_name_2 = 'image2_activity3.jpg';
		$activityExtend->image_name_3 = 'image3_activity3.jpg';
		$activityExtend->position = 3;
		$activityExtend->status = 1;
		$activityExtend->save();
		$activityModels = ActivityExtend::model('ActivityExtend')->frontEnd()->findAll();
		$this->assertCount(1 , $activityModels);
		$activityModel = $activityModels[0];
		$this->assertEquals($activityModel->part_name, 'activity3');
	}

}