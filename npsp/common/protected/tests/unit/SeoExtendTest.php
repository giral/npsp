<?php

class SeoExtendTest extends CDbTestCase {

    public $fixtures = array(
        'seos'=>'Seo',
    );

    private $rulesTest = array(
        0 => array (
            0 => 'status, tagh1, title, metadesc, rewrittenurl,realurl',
            1 => 'required',
        ),
        1 => array (
            0 => 'status',
            1 => 'numerical',
            'integerOnly' => true,
        ),
        2 => array (
            0 => 'page, tagh1',
            1 => 'length',
            'max' => 90,
        ),
        3 => array (
            0 => 'title',
            1 => 'length',
            'max' => 125,
        ),
        4 => array (
            0 => 'metadesc,realurl',
            1 => 'length',
            'max' => 256,
        ),
        5 => array (
            0 => 'rewrittenurl',
            1 => 'length',
            'max' => 80,
        ),
        6 => array (
            0 => 'create_time, update_time',
            1 => 'safe',
        ),
        7 => array (
            0 => 'id, pageid, page, status, create_time, update_time, tagh1, title, metadesc, rewrittenurl,realurl',
            1 => 'safe',
            'on' => 'search',
        ));

    private $scopesTest = array('frontEnd'=>array(
                                                    'condition' => 't.status = 1',
                                                  ),
                                'notDeleted'=>array(
                                                    'condition'=> 't.status != 3'
                                ),
    );

    private $attributes =  array(
        'id' => 'ID',
        'pageid' => 'Pageid',
        'page' => 'Page',
        'status' => 'Status',
        'create_time' => 'Create Time',
        'update_time' => 'Update Time',
        'tagh1' => 'Tagh1',
        'title' => 'Title',
        'metadesc' => 'Meta Description',
        'rewrittenurl' => 'Rewriten Url',
        'realurl' => 'Real Url',
    );

    public function testRules() {
        $rules = SeoExtend::model('SeoExtend')->rules();
        sort($this->rulesTest);
        sort($rules);
        $this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
    }

    public function testScopes() {
        $scopes = SeoExtend::model('SeoExtend')->scopes();
        sort($this->scopesTest);
        sort($scopes);
        $this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
    }

    public function testAttributes() {
        $attributes = SeoExtend::model()->attributeLabels();
        sort($this->attributes);
        sort($attributes);
        $this->assertEquals(json_encode($this->attributes), json_encode($attributes));
    }

	public function testSearchExtend() {
		$seoExtend = new SeoExtend;
		$seoExtendDataProvider = $seoExtend->search();
		$this->assertCount(2, $seoExtendDataProvider->data);
	}

    public function testSearch() {
        $seo1Fixture = $this->seos['seo1'];
        $seo = new Seo();
        $seo->rewrittenurl = $seo1Fixture['rewrittenurl'];
        $seoDataProvider = $seo->search();
        $extract = array();
        foreach($seoDataProvider->getData() as $record) {
            $extract[] = $record;
        }
        $extract = $extract[0];
        $this->assertTrue($extract instanceof Seo);
        $keys = array_keys(Seo::model()->attributeLabels());
        foreach($keys as $key) {
            $this->assertEquals($seo1Fixture[$key], $extract->$key);
        }
    }

    public function testGetSeoParameters() {
		$model = SeoExtend::getSeoParameters('news');
		$this->assertEquals(2, $model->id);
    }

}