<?php

class GalleryVideoExtendTest extends CDbTestCase {

    public $fixtures = array(
        'galleryVideos'=>'GalleryVideo',
    );

    private $rulesTest = array(
        0 => array (
            0 => 'name, content, status',
            1 => 'required',
        ),
        1 => array (
            0 => 'position, status',
            1 => 'numerical',
            'integerOnly' => true,
        ),
        2 => array (
            0 => 'name',
            1 => 'length',
            'max' => 90,
        ),
        3 => array (
            0 => 'create_time, update_time',
            1 => 'safe',
        ),
        4 => array (
            0 => 'id, name, content, position, status, create_time, update_time',
            1 => 'safe',
            'on' => 'search',
        ),
        5 => array(
	        'sw_status',
	        'SWValidator',
	    ));

    private $scopesTest = array('frontEnd'=>array(
        'condition' => 't.status = 1',
        'order' => 't.position ASC',
    ),
        'notDeleted'=>array(
            'condition'=> 't.status != 3'
        ),
    );

    private $attributes =  array(
        'id' => 'ID',
        'name' => 'Name',
        'position' => 'Position',
        'status' => 'Status',
        'content' => 'Content',
        'create_time' => 'Create Time',
        'update_time' => 'Update Time',
    );

    public function testRules() {
        $rules = GalleryVideoExtend::model('GalleryVideoExtend')->rules();
        sort($this->rulesTest);
        sort($rules);
        $this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
    }

    public function testScopes() {
        $scopes = GalleryVideoExtend::model('GalleryVideoExtend')->scopes();
        sort($this->scopesTest);
        sort($scopes);
        $this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
    }

    public function testAttributes() {
        $attributes = GalleryVideoExtend::model()->attributeLabels();
        sort($this->attributes);
        sort($attributes);
        $this->assertEquals(json_encode($this->attributes), json_encode($attributes));
    }

    public function testSearch() {
        $galleryVideo1Fixture = $this->galleryVideos['galleryVideo1'];
        $galleryVideo = new GalleryVideo();
        $galleryVideo->name = $galleryVideo1Fixture['name'];
        $galleryVideoDataProvider = $galleryVideo->search();
        $extract = array();
        foreach($galleryVideoDataProvider->getData() as $record) {
            $extract[] = $record;
        }
        $extract = $extract[0];
        $this->assertTrue($extract instanceof GalleryVideo);
        $keys = array_keys(GalleryVideo::model()->attributeLabels());
        foreach($keys as $key) {
            $this->assertEquals($galleryVideo1Fixture[$key], $extract->$key);
        }
    }

	public function testSearchExtend() {
		$galleryVideoExtend = new GalleryVideoExtend;
		$galleryVideoExtendDataProvider = $galleryVideoExtend->search();
		$this->assertCount(2, $galleryVideoExtendDataProvider->data);
	}

	public function testSearchWorkflow() {
		$galleryVideoExtend = new GalleryVideoExtend;
		$galleryVideoExtendDataProvider = $galleryVideoExtend->searchWorkflow();
		$this->assertCount(1, $galleryVideoExtendDataProvider->data);
	}

}