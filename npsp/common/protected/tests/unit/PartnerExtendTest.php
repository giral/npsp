<?php

class PartnerExtendTest extends CDbTestCase {

	public $fixtures = array(
		'partners'=>'Partner',
	);

	private $rulesTest = array(
		0 => array (
	        0 => 'name, image_name, status',
	        1 => 'required',
	    ),
		1 => array (
	        0 => 'position, status',
	        1 => 'numerical',
	        'integerOnly' => true,
	    ),
	    2 => array (
	        0 => 'name, image_name, link', 'length',
	        1 => 'length',
	        'max' => 90,
	    ),
	    3 => array (
	        0 => 'create_time, update_time',
	        1 => 'safe',
	    ),
	    4 => array (
	        0 => 'id, name, image_name, link, position, status, create_time, update_time',
			1 => 'safe',
			'on' => 'search',
	    ),
	    5 => array (
	        0 => 'image_name',
	        1 => 'file',
	        'allowEmpty' => true,
	        'on' => 'update',
	    ),
	    6=>array(
			'sw_status',
			'SWValidator',
		));

	private $scopesTest = array('frontEnd'=>array(
		'condition' => 't.status = 1',
		'order' => 't.position ASC',
	),
	'notDeleted'=>array(
		'condition'=>'t.status != 3'
	));

	private $attributes =  array(
		'id' => 'ID',
		'name' => 'Name',
		'image_name' => 'Image Name',
		'link' => 'Lien',
		'position' => 'Position',
		'status' => 'Status',
		'create_time' => 'Create Time',
		'update_time' => 'Update Time',
	);

	public function testRules() {
		$rules = PartnerExtend::model('PartnerExtend')->rules();
		sort($this->rulesTest);
		sort($rules);
		$this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
	}

	public function testScopes() {
		$scopes = PartnerExtend::model('PartnerExtend')->scopes();
		sort($this->scopesTest);
		sort($scopes);
		$this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
	}

	public function testAttributes() {
		$attributes = PartnerExtend::model()->attributeLabels();
		sort($this->attributes);
		sort($attributes);
		$this->assertEquals(json_encode($this->attributes), json_encode($attributes));
	}

	public function testSearch() {
		$partner1Fixture = $this->partners['partner1'];
		$partner = new Partner;
		$partner->name = $partner1Fixture['name'];
		$partnerDataProvider = $partner->search();
		$extract = array();
		foreach($partnerDataProvider->getData() as $record) {
		    $extract[] = $record;
		}
		$extract = $extract[0];
		$this->assertTrue($extract instanceof Partner);
		$keys = array_keys(Partner::model()->attributeLabels());
		foreach($keys as $key) {
			$this->assertEquals($partner1Fixture[$key], $extract->$key);
		}
	}

	public function testSearchExtend() {
		$partnerExtend = new PartnerExtend;
		$partnerExtendDataProvider = $partnerExtend->search();
		$this->assertCount(2, $partnerExtendDataProvider->data);
	}

	public function testSearchWorkflow() {
		$partnerExtend = new PartnerExtend;
		$partnerExtendDataProvider = $partnerExtend->searchWorkflow();
		$this->assertCount(1, $partnerExtendDataProvider->data);
	}

}