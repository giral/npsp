<?php

class FlashInfoExtendTest extends CDbTestCase {

	public $fixtures = array(
		'flashInfos'=>'FlashInfo',
	);

	private $rulesTest = array(
		array('title, preview, content, status', 'required'),
		array('status', 'numerical', 'integerOnly'=>true),
		array('sw_status', 'SWValidator'),
		array('title', 'length', 'max'=>90),
		array('create_time, update_time', 'safe'),
		array('id, title, preview, content, status, create_time, update_time', 'safe', 'on'=>'search'),
	);

	private $scopesTest = array('frontEnd'=>array(
		'condition' => 't.status = 1',
	),'notDeleted'=>array(
		'condition'=>'t.status != 3'
	));

	private $attributes =  array(
		'id' => 'ID',
		'title' => 'Title',
		'preview' => 'Preview',
		'content' => 'Content',
		'status' => 'Status',
		'create_time' => 'Create Time',
		'update_time' => 'Update Time',
	);

	public function testRules() {
		$rules = FlashInfoExtend::model('FlashInfoExtend')->rules();
		sort($this->rulesTest);
		sort($rules);
		$this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
	}

	public function testScopes() {
		$scopes = FlashInfoExtend::model('FlashInfoExtend')->scopes();
		sort($this->scopesTest);
		sort($scopes);
		$this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
	}

	public function testAttributes() {
		$attributes = FlashInfoExtend::model()->attributeLabels();
		sort($this->attributes);
		sort($attributes);
		$this->assertEquals(json_encode($this->attributes), json_encode($attributes));
	}

	public function testSearch() {
		$flashInfo1Fixture = $this->flashInfos['flashInfo1'];
		$flashInfo = new FlashInfo;
		$flashInfo->title = $flashInfo1Fixture['title'];
		$emailDataProvider = $flashInfo->search();
		$extract = array();
		foreach($emailDataProvider->getData() as $record) {
		    $extract[] = $record;
		}
		$extract = $extract[0];
		$this->assertTrue($extract instanceof FlashInfo);
		$keys = array_keys(FlashInfo::model()->attributeLabels());
		foreach($keys as $key) {
			$this->assertEquals($flashInfo1Fixture[$key], $extract->$key);
		}
	}

	public function testSearchExtend() {
		$flashInfoExtend = new FlashInfoExtend;
		$flashInfoExtendDataProvider = $flashInfoExtend->search();
		$this->assertCount(2, $flashInfoExtendDataProvider->data);
	}

	public function testSearchWorkflow() {
		$flashInfoExtend = new FlashInfoExtend;
		$flashInfoExtendDataProvider = $flashInfoExtend->searchWorkflow();
		$this->assertCount(1, $flashInfoExtendDataProvider->data);
	}

}