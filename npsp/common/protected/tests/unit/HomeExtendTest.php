<?php

class HomeExtendTest extends CDbTestCase {

	public $fixtures = array(
		'homes'=>'Home',
	);

	private $rulesTest = array(
		0 => array (
	        0 => 'name, image_name, status',
	        1 => 'required',
	    ),
		1 => array (
	        0 => 'position, status',
	        1 => 'numerical',
	        'integerOnly' => true,
	    ),
	    2 => array('name, image_name', 'length', 'max'=>90),
	    3 => array (
	        0 => 'comment, create_time, update_time',
	        1 => 'safe',
	    ),
	    4 => array (
	        0 => 'id, name, image_name, comment, position, status, create_time, update_time',
			1 => 'safe',
			'on' => 'search',
	    ),
	    5 => array (
	        0 => 'image_name',
	        1 => 'file',
	        'allowEmpty' => true,
	        'on' => 'update',
	    ),
	    6 => array(
            'sw_status',
            'SWValidator',
        ));

	private $scopesTest = array('frontEnd'=>array(
		'condition' => 't.status = 1',
		'order' => 't.position ASC',
	),
	'notDeleted'=>array(
		'condition'=>'t.status != 3',
	));

	private $attributes =  array(
		'id' => 'ID',
		'name' => 'Name',
		'image_name' => 'Image Name',
		'comment' => 'Comment',
		'position' => 'Position',
		'status' => 'Status',
		'create_time' => 'Create Time',
		'update_time' => 'Update Time',
	);

	public function testRules() {
		$rules = HomeExtend::model('HomeExtend')->rules();
		sort($this->rulesTest);
		sort($rules);
		$this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
	}

	public function testScopes() {
		$scopes = HomeExtend::model('HomeExtend')->scopes();
		sort($this->scopesTest);
		sort($scopes);
		$this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
	}

	public function testAttributes() {
		$attributes = HomeExtend::model()->attributeLabels();
		sort($this->attributes);
		sort($attributes);
		$this->assertEquals(json_encode($this->attributes), json_encode($attributes));
	}

	public function testSearch() {
		$home1Fixture = $this->homes['home1'];
		$home = new Home;
		$home->name = $home1Fixture['name'];
		$homeDataProvider = $home->search();
		$extract = array();
		foreach($homeDataProvider->getData() as $record) {
		    $extract[] = $record;
		}
		$extract = $extract[0];
		$this->assertTrue($extract instanceof Home);
		$keys = array_keys(Home::model()->attributeLabels());
		foreach($keys as $key) {
			$this->assertEquals($home1Fixture[$key], $extract->$key);
		}
	}

	public function testSearchExtend() {
		$homeExtend = new HomeExtend;
		$homeExtendDataProvider = $homeExtend->search();
		$this->assertCount(2, $homeExtendDataProvider->data);
	}

	public function testSearchWorkflow() {
		$homeExtend = new HomeExtend;
		$homeExtendDataProvider = $homeExtend->searchWorkflow();
		$this->assertCount(1, $homeExtendDataProvider->data);
	}

}