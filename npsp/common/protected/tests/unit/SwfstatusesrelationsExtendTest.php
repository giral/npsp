<?php

class SWFStatusesRelationsExtendSWFStatusesExtendTest extends CDbTestCase {

    public $fixtures = array(
        'SWFStatusesRelationss'=>'SWFStatusesRelations',
    );

    private $rulesTest = array(
        0 => array (
            0 => 'SWFStatuses_id_primary, SWFStatuses_id_secondary, status',
            1 => 'required',
        ),
        1 => array (
            0 => 'SWFStatuses_id_primary, SWFStatuses_id_secondary, status',
            1 => 'numerical',
            'integerOnly' => true,
        ),
        2 => array (
            0 => 'create_time, update_time',
            1 => 'safe',
        ),
        3 => array (
            0 => 'id, SWFStatuses_id_primary, SWFStatuses_id_secondary, status, create_time, update_time',
            1 => 'safe',
            'on' => 'search',
        ));

    private $scopesTest = array('notDeleted'=>array(
        'condition'=> 't.status != 3'
    ),
    );

    private $attributes =  array(
        'id' => 'ID',
        'SWFStatuses_id_primary' => 'Swfstatuses Id Primary',
        'SWFStatuses_id_secondary' => 'Swfstatuses Id Secondary',
        'status' => 'Status',
        'create_time' => 'Create Time',
        'update_time' => 'Update Time',
    );

    public function testRules() {
        $rules = SWFStatusesRelationsExtendSWFStatusesExtend::model('SWFStatusesRelationsExtendSWFStatusesExtend')->rules();
        sort($this->rulesTest);
        sort($rules);
        $this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
    }

    public function testScopes() {
        $scopes = SWFStatusesRelationsExtendSWFStatusesExtend::model('SWFStatusesRelationsExtendSWFStatusesExtend')->scopes();
        sort($this->scopesTest);
        sort($scopes);
        $this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
    }

    public function testAttributes() {
        $attributes = SWFStatusesRelationsExtendSWFStatusesExtend::model()->attributeLabels();
        sort($this->attributes);
        sort($attributes);
        $this->assertEquals(json_encode($this->attributes), json_encode($attributes));
    }

    public function testSearch() {
        $SWFStatusesRelations1Fixture = $this->SWFStatusesRelationss['swfstatusesrelations1'];
        $SWFStatusesRelations = new SWFStatusesRelations();
        $SWFStatusesRelationsDataProvider = $SWFStatusesRelations->search();
        $extract = array();
        foreach($SWFStatusesRelationsDataProvider->getData() as $record) {
            $extract[] = $record;
        }
        $extract = $extract[0];
        $this->assertTrue($extract instanceof SWFStatusesRelations);
        $keys = array_keys(SWFStatusesRelations::model()->attributeLabels());
        foreach($keys as $key) {
            $this->assertEquals($SWFStatusesRelations1Fixture[$key], $extract->$key);
        }
    }

}