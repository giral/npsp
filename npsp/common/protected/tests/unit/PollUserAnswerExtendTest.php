<?php

class PollUserAnswerExtendTest extends CDbTestCase {

    public $fixtures = array(
        'pollUserAnswers'=>'PollUserAnswer',
    );

    private $rulesTest = array(
        0 => array (
            0 => 'answer, status, poll_answer_id',
            1 => 'required',
        ),
        1 => array (
            0 => 'answer, status, poll_answer_id',
            1 => 'numerical',
            'integerOnly' => true,
        ),
        2 => array (
            0 => 'create_time, update_time',
            1 => 'safe',
        ),
        3 => array (
            0 => 'id, answer, status, create_time, update_time, poll_answer_id',
            1 => 'safe',
            'on' => 'search',
        ));

    private $scopesTest = array();

    private $attributes =  array(
        'id' => 'ID',
        'answer' => 'Answer',
        'status' => 'Status',
        'create_time' => 'Create Time',
        'update_time' => 'Update Time',
        'poll_answer_id' => 'Poll Answer',
    );

    public function testRules() {
        $rules = PollUserAnswerExtend::model('PollUserAnswerExtend')->rules();
        sort($this->rulesTest);
        sort($rules);
        $this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
    }

    public function testScopes() {
        $scopes = PollUserAnswerExtend::model('PollUserAnswerExtend')->scopes();
        sort($this->scopesTest);
        sort($scopes);
        $this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
    }

    public function testAttributes() {
        $attributes = PollUserAnswerExtend::model()->attributeLabels();
        sort($this->attributes);
        sort($attributes);
        $this->assertEquals(json_encode($this->attributes), json_encode($attributes));
    }

    public function testSearch() {
        $pollUserAnswer1Fixture = $this->pollUserAnswers['pollUserAnswer1'];
        $pollUserAnswer = new PollUserAnswer();
        $pollUserAnswer->poll_answer_id = $pollUserAnswer1Fixture['poll_answer_id'];
        $pollUserAnswerDataProvider = $pollUserAnswer->search();
        $extract = array();
        foreach($pollUserAnswerDataProvider->getData() as $record) {
            $extract[] = $record;
        }
        $extract = $extract[0];
        $this->assertTrue($extract instanceof PollUserAnswer);
        $keys = array_keys(PollUserAnswer::model()->attributeLabels());
        foreach($keys as $key) {
            $this->assertEquals($pollUserAnswer1Fixture[$key], $extract->$key);
        }
    }

}