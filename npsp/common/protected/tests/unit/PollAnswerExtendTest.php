<?php

class PoolAnswerExtendTest extends CDbTestCase {

    public $fixtures = array(
        'pollAnswers'=>'PollAnswer',
    );

    private $rulesTest = array(
        0 => array (
            0 => 'content, status, poll_question_id',
            1 => 'required',
        ),
        1 => array (
            0 => 'position, status, poll_question_id',
            1 => 'numerical',
            'integerOnly' => true,
        ),
        2 => array (
            0 => 'content',
            1 => 'length',
            'max' => 90,
        ),
        3 => array (
            0 => 'create_time, update_time',
            1 => 'safe',
        ),
        4 => array (
            0 => 'id, content, position, status, create_time, update_time, poll_question_id',
            1 => 'safe',
            'on' => 'search',
        ));

    private $scopesTest = array('frontEnd'=>array(
        'condition' => 't.status = 1',
        'order' => 't.position ASC',
    ),
        'notDeleted'=>array(
            'condition'=> 't.status != 3'
        ),
    );

    private $attributes =  array(
        'id' => 'ID',
        'content' => 'Content',
        'position' => 'Position',
        'status' => 'Status',
        'create_time' => 'Create Time',
        'update_time' => 'Update Time',
        'poll_question_id' => 'Poll Question',
    );

    public function testRules() {
        $rules = PollAnswerExtend::model('PollAnswerExtend')->rules();
        sort($this->rulesTest);
        sort($rules);
        $this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
    }

    public function testScopes() {
        $scopes = PollAnswerExtend::model('PollAnswerExtend')->scopes();
        sort($this->scopesTest);
        sort($scopes);
        $this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
    }

    public function testAttributes() {
        $attributes = PollAnswerExtend::model()->attributeLabels();
        sort($this->attributes);
        sort($attributes);
        $this->assertEquals(json_encode($this->attributes), json_encode($attributes));
    }

    public function testSearch() {
        $pollAnswer1Fixture = $this->pollAnswers['pollAnswer1'];
        $pollAnswer = new PollAnswer();
        $pollAnswer->content = $pollAnswer1Fixture['content'];
        $pollAnswerDataProvider = $pollAnswer->search();
        $extract = array();
        foreach($pollAnswerDataProvider->getData() as $record) {
            $extract[] = $record;
        }
        $extract = $extract[0];
        $this->assertTrue($extract instanceof PollAnswer);
        $keys = array_keys(PollAnswer::model()->attributeLabels());
        foreach($keys as $key) {
            $this->assertEquals($pollAnswer1Fixture[$key], $extract->$key);
        }
    }

}