<?php

class PollExtendTest extends CDbTestCase {

    public $fixtures = array(
        'polls'=>'Poll',
    );

    private $rulesTest = array(
        0 => array (
            0 => 'name, status',
            1 => 'required',
        ),
        1 => array (
            0 => 'position, status',
            1 => 'numerical',
            'integerOnly' => true,
        ),
        2 => array (
            0 => 'name',
            1 => 'length',
            'max' => 90,
        ),
        3 => array (
            0 => 'create_time, update_time',
            1 => 'safe',
        ),
        4 => array (
            0 => 'id, name, position, status, create_time, update_time',
            1 => 'safe',
            'on' => 'search',
        ),
		5 => array(
			'sw_status',
			'SWValidator',
		));

    private $scopesTest = array('frontEnd'=>array(
        'condition' => 't.status = 1',
        'order' => 't.position ASC',
    ),
        'notDeleted'=>array(
            'condition'=> 't.status != 3'
        ),
    );

    private $attributes =  array(
        'id' => 'ID',
        'name' => 'Name',
        'position' => 'Position',
        'status' => 'Status',
        'create_time' => 'Create Time',
        'update_time' => 'Update Time',
    );

    public function testRules() {
        $rules = PollExtend::model('PollExtend')->rules();
        sort($this->rulesTest);
        sort($rules);
        $this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
    }

    public function testScopes() {
        $scopes = PollExtend::model('PollExtend')->scopes();
        sort($this->scopesTest);
        sort($scopes);
        $this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
    }

    public function testAttributes() {
        $attributes = PollExtend::model()->attributeLabels();
        sort($this->attributes);
        sort($attributes);
        $this->assertEquals(json_encode($this->attributes), json_encode($attributes));
    }

    public function testSearch() {
        $poll1Fixture = $this->polls['poll1'];
        $poll= new Poll();
        $poll->name = $poll1Fixture['name'];
        $pollDataProvider = $poll->search();
        $extract = array();
        foreach($pollDataProvider->getData() as $record) {
            $extract[] = $record;
        }
        $extract = $extract[0];
        $this->assertTrue($extract instanceof Poll);
        $keys = array_keys(Poll::model()->attributeLabels());
        foreach($keys as $key) {
            $this->assertEquals($poll1Fixture[$key], $extract->$key);
        }
    }

	public function testSearchExtend() {
		$pollExtend = new PollExtend;
		$pollExtendDataProvider = $pollExtend->search();
		$this->assertCount(3, $pollExtendDataProvider->data);
	}

	public function testSearchWorkflow() {
		$pollExtend = new PollExtend;
		$pollExtendDataProvider = $pollExtend->searchWorkflow();
		$this->assertCount(1, $pollExtendDataProvider->data);
	}

	public function testAfterLogicalDelete() {
		$pollFixtureTobeDeleted = $this->polls['toBeDeleted'];
		$poll = new PollExtend;
		$poll->attributes = $pollFixtureTobeDeleted;
		$poll->id = $pollFixtureTobeDeleted['id'];
		$poll->status = StatusBehavior::STATUS_DELETED;
		$poll->isNewRecord = false;
		$poll->save();
		$poll->afterLogicalDelete();
		$this->assertEquals(3, $poll->status);
		$questions = $poll->pollQuestions;
		foreach($questions as $question) {
			$this->assertEquals(3, $question->status);
			$answers = $question->pollAnswers;
			foreach($answers as $answer) {
				$this->assertEquals(3, $answer->status);
				$answerUsers = $answer->pollUserAnswers;
				foreach($answerUsers as $answerUser) {
					$this->assertEquals(3, $answerUser->status);
				}
			}
		}
	}

	public function testAfterSaveActivation() {
		$poll = new PollExtend;
		$poll->name = 'pollActivated';
		$poll->position = 3;
		$poll->status = 1;
		$poll->create_time = '2014-09-11 10:09:10';
		$poll->update_time = '2014-09-11 14:09:10';
		$poll->save();

		$pollQuestion = new PollQuestionExtend;
		$pollQuestion->content = 'Question1';
		$pollQuestion->multi_ans = 0;
		$pollQuestion->position = 1;
		$pollQuestion->status = 1;
		$pollQuestion->create_time = '2014-09-11 10:09:10';
		$pollQuestion->update_time = '2014-09-11 14:09:10';
		$pollQuestion->poll_id = $poll->id;
		$pollQuestion->save();

		$idAnswers = array();
		for($cpt=0; $cpt<=1; $cpt++) {
			$pollAnswer = new PollAnswerExtend;
			$pollAnswer->content = 'Answer'.$cpt;
			$pollAnswer->position = $cpt + 1;
			$pollAnswer->status = 1;
			$pollAnswer->create_time = '2014-09-11 10:09:10';
			$pollAnswer->update_time = '2014-09-11 14:09:10';
			$pollAnswer->poll_question_id = $pollQuestion->id;
			$pollAnswer->save();
			$idAnswers[] = $pollAnswer->id;
		}

		foreach($idAnswers as $id) {
			$pollUserAnswer = new PollUserAnswerExtend;
			$pollUserAnswer->answer = 1;
			$pollUserAnswer->status = 1;
			$pollUserAnswer->create_time = '2014-09-11 10:09:10';
			$pollUserAnswer->update_time = '2014-09-11 14:09:10';
			$pollUserAnswer->poll_answer_id = $id;
			$pollAnswer->save();
		}

		$pollModels = PollExtend::model('PollExtend')->findAllByAttributes(array('status'=>1));
		$this->assertCount(1, $pollModels);

		$questionModels = PollQuestionExtend::model('PollQuestionExtend')->findAllByAttributes(array('status'=>1));
		$this->assertCount(1, $questionModels);

		$answerModels = PollAnswerExtend::model('PollAnswerExtend')->findAllByAttributes(array('status'=>1));
		$this->assertCount(2, $answerModels);

		$answerUserModels = PollUserAnswerExtend::model('PollAnswerExtend')->findAllByAttributes(array('status'=>1));
		$this->assertCount(2, $answerUserModels);
	}

}