<?php

class SWFWorkflowExtendTest extends CDbTestCase {

    public $fixtures = array(
        'SWFWorkflowExtends'=>'SWFWorkflowExtend',
    );

    private $rulesTest = array(
        0 => array (
            0 => 'name, workflowName, position, status',
            1 => 'required',
        ),
        1 => array (
            0 => 'position, status',
            1 => 'numerical',
            'integerOnly' => true,
        ),
        2 => array (
            0 => 'name, workflowName',
            1 => 'length',
            'max' => 90,
        ),
        3 => array (
            0 => 'create_time, update_time',
            1 => 'safe',
        ),
        4 => array (
            0 => 'id, name, workflowName, position, status, create_time, update_time',
            1 => 'safe',
            'on' => 'search',
        ));

    private $scopesTest = array('notDeleted'=>array(
        'condition'=> 't.status != 3'
    ),
    );

    private $attributes =  array(
        'id' => 'ID',
        'name' => 'Name',
        'position' => 'Position',
        'workflowName' => 'Workflow Name',
        'status' => 'Status',
        'create_time' => 'Create Time',
        'update_time' => 'Update Time',
    );

    public function testRules() {
        $rules = SWFWorkflowExtend::model('SWFWorkflowExtend')->rules();
        sort($this->rulesTest);
        sort($rules);
        $this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
    }

    public function testScopes() {
        $scopes = SWFWorkflowExtend::model('SWFWorkflowExtend')->scopes();
        sort($this->scopesTest);
        sort($scopes);
        $this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
    }

    public function testAttributes() {
        $attributes = SWFWorkflowExtend::model()->attributeLabels();
        sort($this->attributes);
        sort($attributes);
        $this->assertEquals(json_encode($this->attributes), json_encode($attributes));
    }

    public function testSearch() {
        $SWFWorkflowExtend1Fixture = $this->SWFWorkflowExtends['swfworkflow1'];
        $SWFWorkflowExtend = new SWFWorkflowExtend();
        $SWFWorkflowExtend->name = $SWFWorkflowExtend1Fixture['name'];
        $SWFWorkflowExtendDataProvider = $SWFWorkflowExtend->search();
        $extract = array();
        foreach($SWFWorkflowExtendDataProvider->getData() as $record) {
            $extract[] = $record;
        }
        $extract = $extract[0];
        $this->assertTrue($extract instanceof SWFWorkflowExtend);
        $keys = array_keys(SWFWorkflowExtend::model()->attributeLabels());
        foreach($keys as $key) {
            $this->assertEquals($SWFWorkflowExtend1Fixture[$key], $extract->$key);
        }
    }

}