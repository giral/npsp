<?php

class UserExtendTest extends CDbTestCase {

    public $fixtures = array(
        'users'=>'User',
    );

    private $rulesTest = array(
        0 => array (
            0 => 'username, password, firstname, lastname, status',
            1 => 'required',
        ),
        1 => array (
            0 => 'status',
            1 => 'numerical',
            'integerOnly' => true,
        ),
        2 => array (
            0 => 'username, password, firstname, lastname',
            1 => 'length',
            'max' => 90,
        ),
        3 => array (
            0 => 'create_time, update_time, last_connexion',
            1 => 'safe',
        ),
        4 => array (
            0 => 'id, username, password, firstname, lastname, status, create_time, update_time, last_connexion',
            1 => 'safe',
            'on' => 'search',
        ));

    private $scopesTest = array('notDeleted'=>array(
            'condition'=> 't.status != 3'
        ),
    );

    private $attributes =  array(
        'id' => 'ID',
        'username' => 'Username',
        'password' => 'Password',
        'firstname' => 'Firstname',
        'lastname' => 'Lastname',
        'status' => 'Status',
        'create_time' => 'Create Time',
        'update_time' => 'Update Time',
        'last_connexion' => 'Last Connexion',
    );

    public function testRules() {
        $rules = UserExtend::model('UserExtend')->rules();
        sort($this->rulesTest);
        sort($rules);
        $this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
    }

    public function testScopes() {
        $scopes = UserExtend::model('UserExtend')->scopes();
        sort($this->scopesTest);
        sort($scopes);
        $this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
    }

    public function testAttributes() {
        $attributes = UserExtend::model()->attributeLabels();
        sort($this->attributes);
        sort($attributes);
        $this->assertEquals(json_encode($this->attributes), json_encode($attributes));
    }

    public function testSearch() {
        $user1Fixture = $this->users['user1'];
        $user = new User();
        $user->username = $user1Fixture['username'];
        $userDataProvider = $user->search();
        $extract = array();
        foreach($userDataProvider->getData() as $record) {
            $extract[] = $record;
        }
        $extract = $extract[0];
        $this->assertTrue($extract instanceof User);
        $keys = array_keys(User::model()->attributeLabels());
        foreach($keys as $key) {
            $this->assertEquals($user1Fixture[$key], $extract->$key);
        }
    }

	public function testSearchExtend() {
		$userExtend = new UserExtend;
		$userExtendDataProvider = $userExtend->search();
		$this->assertCount(2, $userExtendDataProvider->data);
	}

	public function testCryptPass() {
		$userExtend = new UserExtend;
		$userExtend->username = 'username1';
		$userExtend->password = 'password';
		$userExtend->firstname = 'firstname1';
		$userExtend->lastname = 'lastname1';
		$userExtend->status = 1;
		$userExtend->last_connexion = '2014-09-12 10:09:10';
		$userExtend->save();
		$this->assertEquals('papAq5PwY/QQM', $userExtend->password);
	}

}