<?php

class PollQuestionExtendTest extends CDbTestCase {

    public $fixtures = array(
        'pollQuestions'=>'PollQuestion',
    );

    private $rulesTest = array(
        0 => array (
            0 => 'content, multi_ans, status, poll_id',
            1 => 'required',
        ),
        1 => array (
            0 => 'multi_ans, position, status, poll_id',
            1 => 'numerical',
            'integerOnly' => true,
        ),
        2 => array (
            0 => 'content',
            1 => 'length',
            'max' => 90,
        ),
        3 => array (
            0 => 'create_time, update_time',
            1 => 'safe',
        ),
        4 => array (
            0 => 'id, content, multi_ans, position, status, create_time, update_time, poll_id',
            1 => 'safe',
            'on' => 'search',
        ));

    private $scopesTest = array('frontEnd'=>array(
        'condition' => 't.status = 1',
        'order' => 't.position ASC',
    ),
        'notDeleted'=>array(
            'condition'=> 't.status != 3'
        ),
    );

    private $attributes =  array(
        'id' => 'ID',
        'content' => 'Content',
        'multi_ans' => 'Multi Ans',
        'position' => 'Position',
        'status' => 'Status',
        'create_time' => 'Create Time',
        'update_time' => 'Update Time',
        'poll_id' => 'Poll',
    );

    public function testRules() {
        $rules = PollQuestionExtend::model('PollQuestionExtend')->rules();
        sort($this->rulesTest);
        sort($rules);
        $this->assertEquals(json_encode($this->rulesTest), json_encode($rules));
    }

    public function testScopes() {
        $scopes = PollQuestionExtend::model('PollQuestionExtend')->scopes();
        sort($this->scopesTest);
        sort($scopes);
        $this->assertEquals(json_encode($this->scopesTest), json_encode($scopes));
    }

    public function testAttributes() {
        $attributes = PollQuestionExtend::model()->attributeLabels();
        sort($this->attributes);
        sort($attributes);
        $this->assertEquals(json_encode($this->attributes), json_encode($attributes));
    }

    public function testSearch() {
        $pollQuestion1Fixture = $this->pollQuestions['pollQuestion1'];
        $pollQuestion = new PollQuestion();
        $pollQuestion->content = $pollQuestion1Fixture['content'];
        $pollQuestionDataProvider = $pollQuestion->search();
        $extract = array();
        foreach($pollQuestionDataProvider->getData() as $record) {
            $extract[] = $record;
        }
        $extract = $extract[0];
        $this->assertTrue($extract instanceof PollQuestion);
        $keys = array_keys(PollQuestion::model()->attributeLabels());
        foreach($keys as $key) {
            $this->assertEquals($pollQuestion1Fixture[$key], $extract->$key);
        }
    }

}