<?php

class Audit extends CComponent{

	const ACTION_INSERT_LABEL = 'Insertion';
	const ACTION_DELETE_LABEL = 'Suppression';
	const ACTION_MODIFICATION_LABEL = 'Modification';

} 