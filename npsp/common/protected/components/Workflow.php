<?php

class Workflow extends CComponent {

	const SW_ACTIVITY = 'swActivity';
	const SW_CONTACT = 'swContact';
	const SW_FLASHINFO = 'swFlashInfo';
	const SW_GALLERYPHOTO = 'swGalleyPhoto';
	const SW_GALLERYVIDEO = 'swGalleryVideo';
	const SW_HOME = 'swHome';
	const SW_NEWS = 'swNews';
	const SW_ORGANISATION = 'swOrganisation';
	const SW_PARTNER = 'swPartner';
	const SW_POLL = 'swPoll';
	const SW_PRESENTATION = 'swPresentation';
	const SW_PUBLICATION = 'swPublication';

	const ID = 'id';
	const CONSTRAINT = 'constraint';
	const TRANSITION = 'transition';
	const INITIAl = 'initial';
	const NODE = 'node';

	const DUMMY_INIT_NODE = 'dummyInitialNode';

	public $workflowName;
	public $workflowId;
	public $initialNodeName = null;
	public $workflow = null;
	public $subNodeTransitions = array();

	public function __construct($workflowName) {
	    $this->workflowName = $workflowName;
	}

	private function getWorkflowId() {
		/**
		 * @var $workflowLinkOnModel SWFWorkflowExtendlinkonmodelExtend
		 */
		$criteria = new CDbCriteria;
		$criteria->addColumnCondition(array('workflowName'=>$this->workflowName));
		$workflowModel = SWFWorkflowExtend::model('SWFWorkflowExtend')->notDeleted()->find($criteria);
		if($workflowModel) {
			return $workflowModel->id;
		}
		return null;
	}

	private function setInitial() {
		$this->workflowId = $this->getWorkflowId();
		if($this->workflowId == null) {
			$this->initialNodeName = self::DUMMY_INIT_NODE;
		} else {
			$criteria = new CDbCriteria;
			$criteria->addCondition('SWFWorkflow_id = '.$this->workflowId);
			$initialStatusModel  = SWFStatusesExtend::model('SWFStatusesExtend')->lowestPosition()->notDeleted()->find($criteria);
			$this->initialNodeName = $initialStatusModel->name ? $initialStatusModel->name : self::DUMMY_INIT_NODE;
		}
	}

	private function pushSubNodeTransitions() {
		if($this->initialNodeName == self::DUMMY_INIT_NODE) {
			$statusTmp = array(
								self::ID => self::DUMMY_INIT_NODE,
								self::CONSTRAINT => '',
								self::TRANSITION => array(),);
			$this->subNodeTransitions[] = $statusTmp;
		} else {
			$statusesTmp = array();
			/** @var $statuses SWFStatusesExtend[] */
			$criteria = new CDbCriteria;
			$criteria->addCondition('SWFWorkflow_id = '.$this->workflowId);
			$statuses = SWFStatusesExtend::model('SWFStatusesExtend')->notDeleted()->findAll($criteria);
			/** @var $relations SWFStatusesRelationsExtend[] */
			$relations = SWFStatusesRelationsExtend::model('SWFStatusesRelationsExtend')->notDeleted()->findAll();
			if($statuses) {
				foreach($statuses as $status) {
					$statusesTmp[$status->id] = array(
						self::ID => $status->name,
						self::CONSTRAINT => $status->constraint,
						self::TRANSITION => array(),);
				}
			}
			if($relations) {
				foreach ($relations as $relation) {
					if(array_key_exists($relation->SWFStatuses_id_secondary, $statusesTmp)) {
						$chilNodeName = $statusesTmp[$relation->SWFStatuses_id_secondary][self::ID];
						$statusesTmp[$relation->SWFStatuses_id_primary][self::TRANSITION][] = $chilNodeName;
					}
				}
			}
			foreach($statusesTmp as $statusTmp) {
				$this->subNodeTransitions[] = $statusTmp;
			}
		}
	}

	private function buildWorkflow() {
		$this->workflow = array(
			self::INITIAl => $this->initialNodeName,
		    self::NODE=> $this->subNodeTransitions
		);
	}

	public function getWorkflow() {
		$this->setInitial();
		$this->pushSubNodeTransitions();
		$this->buildWorkflow();
		return $this->workflow;
	}

	public static function isWorkflowComposedOfAtLeastTwoSteps($model) {
		return count(SWHelper::allStatuslistData($model)) > 1 ? true : false;
	}

}