<?php
return array(
	'connectionString' => 'mysql:host=localhost;dbname=npsp_db;unix_socket:/var/lib/mysql/mysql.sock',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => '',
	'charset' => 'utf8',
	'schemaCachingDuration'=>3600,
	'enableProfiling' => false,
	'enableParamLogging' => false,
)
?>
