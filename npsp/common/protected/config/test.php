<?php
Yii::setPathOfAlias('root', realpath('../../../'));
Yii::setPathOfAlias('simpleWorkflow', realpath('../../../common/protected/extensions/simpleWorkflow'));
return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
		'import'=>array(
			'root.common.protected.extensions.simpleWorkflow.*',
		),
		'components'=>array(
			'fixture'=>array(
				'class'=>'system.test.CDbFixtureManager',
			),
			'db'=>array(
				'connectionString' => 'mysql:host=localhost;dbname=npsp-test1',
				'emulatePrepare' => true,
				'username' => 'root',
				'password' => '',
				'charset' => 'utf8',
			),
			'swSource'=> array(
				'class'=>'root.common.protected.extensions.simpleWorkflow.SWPhpWorkflowSource',
				'basePath'=>'root.common.protected.models.workflows',
			),
		),
	)
);
