SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `npsp`.`SWFStatusesRelations` DROP FOREIGN KEY `fk_swfsatusestype1` ;

ALTER TABLE `npsp`.`validation_tmpl_has_user` DROP FOREIGN KEY `fk_validation_tmpl_has_user_validation_tmpl` ;

ALTER TABLE `npsp`.`validation_tmpl_has_user`
  ADD CONSTRAINT `fk_validation_tmpl_has_user_validation_tmpl`
  FOREIGN KEY (`validation_tmpl_id` )
  REFERENCES `npsp`.`validation_tmpl` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `npsp`.`SWFStatusesRelations` DROP COLUMN `SWFStatusesType_id`
, DROP INDEX `fk_swfsatusestype1` ;

ALTER TABLE `npsp`.`SWFStatuses` CHANGE COLUMN `constraint` `constraint` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL  ;

DROP TABLE IF EXISTS `npsp`.`SWFStatusesType` ;

ALTER TABLE `npsp`.`SWFWorkflow` ADD COLUMN `workflowName` VARCHAR(90) NOT NULL  AFTER `name` ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
