SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `npsp`.`validation_tmpl_has_user` DROP FOREIGN KEY `fk_validation_tmpl_has_user_validation_tmpl` ;

ALTER TABLE `npsp`.`validation_tmpl_has_user`
  ADD CONSTRAINT `fk_validation_tmpl_has_user_validation_tmpl`
  FOREIGN KEY (`validation_tmpl_id` )
  REFERENCES `npsp`.`validation_tmpl` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

DROP TABLE IF EXISTS `npsp`.`SWFWorkflowLinkOnModel` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
