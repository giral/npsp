SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';


CREATE  TABLE IF NOT EXISTS `npsp`.`SWFWorkflow` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(90) NOT NULL ,
  `position` INT(11) NOT NULL ,
  `status` INT(11) NOT NULL ,
  `create_time` TIMESTAMP NULL DEFAULT NULL ,
  `update_time` TIMESTAMP NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE  TABLE IF NOT EXISTS `npsp`.`SWFWorkflowLinkOnModel` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `SWFWorkflow_Id` INT(11) NOT NULL ,
  `modelName`  VARCHAR(90) NOT NULL ,
  `status` INT(11) NOT NULL ,
  `create_time` TIMESTAMP NULL DEFAULT NULL ,
  `update_time` TIMESTAMP NULL DEFAULT NULL ,
  PRIMARY KEY (`id`),
  INDEX `fk_swfworkflow2` (`SWFWorkflow_Id` ASC) ,
  CONSTRAINT `fk_swfworkflow2`
    FOREIGN KEY (`SWFWorkflow_Id` )
    REFERENCES `npsp`.`SWFWorkflow` (`id` )
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


ALTER TABLE `npsp`.`SWFStatuses` ADD COLUMN `SWFWorkflow_Id` INT(11) NOT NULL DEFAULT 1  AFTER `update_time`;

ALTER TABLE `npsp`.`SWFStatuses`
    ADD CONSTRAINT `fk_swfworkflow1`
    FOREIGN KEY (`SWFWorkflow_Id` )
    REFERENCES `npsp`.`SWFWorkflow` (`id` )


