SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE  TABLE IF NOT EXISTS `npsp`.`SWFStatuses` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(90) NOT NULL ,
  `position` INT(11) NOT NULL ,
  `constraint` VARCHAR(100) NOT NULL ,
  `status` INT(11) NOT NULL ,
  `create_time` TIMESTAMP NULL DEFAULT NULL ,
  `update_time` TIMESTAMP NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE  TABLE IF NOT EXISTS `npsp`.`SWFStatusesType` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(90) NOT NULL ,
  `status` INT(11) NOT NULL ,
  `create_time` TIMESTAMP NULL DEFAULT NULL ,
  `update_time` TIMESTAMP NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE  TABLE IF NOT EXISTS `npsp`.`SWFStatusesRelations` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `SWFStatusesType_id` INT(11) NOT NULL ,
  `SWFStatuses_id_primary` INT(11) NOT NULL ,
  `SWFStatuses_id_secondary` INT(11) NOT NULL ,
  `status` INT(11) NOT NULL ,
  `create_time` TIMESTAMP NULL DEFAULT NULL ,
  `update_time` TIMESTAMP NULL DEFAULT NULL ,
  PRIMARY KEY (`id`),
  INDEX `fk_swfstatuses1` (`SWFStatuses_id_primary` ASC) ,
  CONSTRAINT `fk_swfstatuses1`
    FOREIGN KEY (`SWFStatuses_id_primary` )
    REFERENCES `npsp`.`SWFStatuses` (`id` ),
    INDEX `fk_swfstatuses2` (`SWFStatuses_id_secondary` ASC) ,
  CONSTRAINT `fk_swfstatuses2`
    FOREIGN KEY (`SWFStatuses_id_secondary` )
    REFERENCES `npsp`.`SWFStatuses` (`id` ),
    INDEX `fk_swfsatusestype1` (`SWFStatusesType_id` ASC) ,
  CONSTRAINT `fk_swfsatusestype1`
    FOREIGN KEY (`SWFStatusesType_id` )
    REFERENCES `npsp`.`SWFStatusesType` (`id` )
   )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
