SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `npsp` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `npsp` ;

-- -----------------------------------------------------
-- Table `npsp`.`partner`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`partner` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(90) NOT NULL ,
  `image_name` VARCHAR(90) NOT NULL ,
  `link` VARCHAR(90) NULL ,
  `position` INT NULL ,
  `status` INT NOT NULL ,
  `sw_status` VARCHAR(50) NULL ,
  `create_time` TIMESTAMP NULL ,
  `update_time` TIMESTAMP NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `npsp`.`home`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`home` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(90) NOT NULL ,
  `image_name` VARCHAR(90) NOT NULL ,
  `comment` TEXT NULL ,
  `position` INT NULL ,
  `status` INT NOT NULL ,
  `sw_status` VARCHAR(50) NULL ,
  `create_time` TIMESTAMP NULL ,
  `update_time` TIMESTAMP NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `npsp`.`presentation`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`presentation` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `part_name` VARCHAR(90) NOT NULL ,
  `image_name_1` VARCHAR(90) NOT NULL ,
  `image_name_2` VARCHAR(90) NOT NULL ,
  `image_name_3` VARCHAR(45) NOT NULL ,
  `position` VARCHAR(45) NULL ,
  `status` INT NOT NULL ,
  `sw_status` VARCHAR(50) NULL ,
  `create_time` TIMESTAMP NULL ,
  `update_time` TIMESTAMP NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `npsp`.`organisation`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`organisation` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `part_name` VARCHAR(90) NOT NULL ,
  `image_name_1` VARCHAR(90) NOT NULL ,
  `image_name_2` VARCHAR(90) NOT NULL ,
  `image_name_3` VARCHAR(45) NOT NULL ,
  `position` VARCHAR(45) NULL ,
  `status` INT NOT NULL ,
  `sw_status` VARCHAR(50) NULL ,
  `create_time` TIMESTAMP NULL ,
  `update_time` TIMESTAMP NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `npsp`.`activity`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`activity` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `part_name` VARCHAR(90) NOT NULL ,
  `image_name_1` VARCHAR(90) NOT NULL ,
  `image_name_2` VARCHAR(90) NOT NULL ,
  `image_name_3` VARCHAR(45) NOT NULL ,
  `position` VARCHAR(45) NULL ,
  `status` INT NOT NULL ,
  `sw_status` VARCHAR(50) NULL ,
  `create_time` TIMESTAMP NULL ,
  `update_time` TIMESTAMP NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `npsp`.`contact`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`contact` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `part_name` VARCHAR(90) NOT NULL ,
  `image_name_1` VARCHAR(90) NOT NULL ,
  `image_name_2` VARCHAR(90) NOT NULL ,
  `image_name_3` VARCHAR(45) NOT NULL ,
  `position` VARCHAR(45) NULL ,
  `status` INT NOT NULL ,
  `sw_status` VARCHAR(50) NULL ,
  `create_time` TIMESTAMP NULL ,
  `update_time` TIMESTAMP NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `npsp`.`flash_info`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`flash_info` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `title` VARCHAR(90) NOT NULL ,
  `preview` TEXT NOT NULL ,
  `content` TEXT NOT NULL ,
  `sw_status` VARCHAR(50) NULL ,
  `status` INT NOT NULL ,
  `create_time` TIMESTAMP NULL ,
  `update_time` TIMESTAMP NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `npsp`.`user`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `username` VARCHAR(90) NOT NULL ,
  `password` VARCHAR(90) NOT NULL ,
  `firstname` VARCHAR(90) NOT NULL ,
  `lastname` VARCHAR(90) NOT NULL ,
  `status` INT NOT NULL ,
  `create_time` TIMESTAMP NULL ,
  `update_time` TIMESTAMP NULL ,
  `last_connexion` TIMESTAMP NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `npsp`.`email`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`email` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `email` VARCHAR(90) NOT NULL ,
  `status` INT NOT NULL ,
  `create_time` TIMESTAMP NULL ,
  `update_time` TIMESTAMP NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `npsp`.`seo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`seo` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `pageid` INT NULL ,
  `page` VARCHAR(90) NULL ,
  `status` INT NOT NULL ,
  `create_time` TIMESTAMP NULL ,
  `update_time` TIMESTAMP NULL ,
  `tagh1` VARCHAR(90) NOT NULL ,
  `title` VARCHAR(125) NOT NULL ,
  `metadesc` VARCHAR(256) NOT NULL ,
  `rewrittenurl` VARCHAR(80) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `npsp`.`gallery_video`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`gallery_video` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(90) NOT NULL ,
  `content` TEXT NOT NULL ,
  `position` INT NULL ,
  `status` INT NOT NULL ,
  `sw_status` VARCHAR(50) NULL ,
  `create_time` TIMESTAMP NULL ,
  `update_time` TIMESTAMP NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `npsp`.`gallery_photo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`gallery_photo` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(90) NOT NULL ,
  `position` INT NULL ,
  `status` INT NOT NULL ,
  `sw_status` VARCHAR(50) NULL ,
  `create_time` TIMESTAMP NULL ,
  `update_time` TIMESTAMP NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `npsp`.`photo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`photo` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(90) NOT NULL ,
  `position` INT NULL ,
  `status` INT NOT NULL ,
  `create_time` TIMESTAMP NULL ,
  `update_time` TIMESTAMP NULL ,
  `gallery_photo_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_photo_gallery_photo1` (`gallery_photo_id` ASC) ,
  CONSTRAINT `fk_photo_gallery_photo1`
    FOREIGN KEY (`gallery_photo_id` )
    REFERENCES `npsp`.`gallery_photo` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `npsp`.`AuthItem`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`AuthItem` (
  `name` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `type` INT(11) NOT NULL ,
  `description` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `bizrule` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `data` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  PRIMARY KEY (`name`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `npsp`.`Rights`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`Rights` (
  `itemname` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `type` INT(11) NOT NULL ,
  `weight` INT(11) NOT NULL ,
  PRIMARY KEY (`itemname`) ,
  CONSTRAINT `rights_ibfk_1`
    FOREIGN KEY (`itemname` )
    REFERENCES `npsp`.`AuthItem` (`name` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `npsp`.`AuthItemChild`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`AuthItemChild` (
  `parent` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `child` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  PRIMARY KEY (`parent`, `child`) ,
  INDEX `child` (`child` ASC) ,
  CONSTRAINT `authitemchild_ibfk_1`
    FOREIGN KEY (`parent` )
    REFERENCES `npsp`.`AuthItem` (`name` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `authitemchild_ibfk_2`
    FOREIGN KEY (`child` )
    REFERENCES `npsp`.`AuthItem` (`name` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `npsp`.`AuthAssignment`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`AuthAssignment` (
  `itemname` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `userid` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `bizrule` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `data` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  PRIMARY KEY (`itemname`, `userid`) ,
  CONSTRAINT `authassignment_ibfk_1`
    FOREIGN KEY (`itemname` )
    REFERENCES `npsp`.`AuthItem` (`name` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `npsp`.`poll`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`poll` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(90) NOT NULL ,
  `position` INT NULL ,
  `status` INT NOT NULL ,
  `sw_status` VARCHAR(50) NULL ,
  `create_time` TIMESTAMP NULL ,
  `update_time` TIMESTAMP NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `npsp`.`poll_question`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`poll_question` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `content` VARCHAR(90) NOT NULL ,
  `multi_ans` INT NOT NULL ,
  `position` INT NULL ,
  `status` INT NOT NULL ,
  `create_time` TIMESTAMP NULL ,
  `update_time` TIMESTAMP NULL ,
  `poll_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_poll_question_poll1` (`poll_id` ASC) ,
  CONSTRAINT `fk_poll_question_poll1`
    FOREIGN KEY (`poll_id` )
    REFERENCES `npsp`.`poll` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `npsp`.`poll_answer`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`poll_answer` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `content` VARCHAR(90) NOT NULL ,
  `position` INT NULL ,
  `status` INT NOT NULL ,
  `create_time` TIMESTAMP NULL ,
  `update_time` TIMESTAMP NULL ,
  `poll_question_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_poll_answer_poll_question1` (`poll_question_id` ASC) ,
  CONSTRAINT `fk_poll_answer_poll_question1`
    FOREIGN KEY (`poll_question_id` )
    REFERENCES `npsp`.`poll_question` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `npsp`.`poll_user_answer`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`poll_user_answer` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `answer` INT NOT NULL ,
  `status` INT NOT NULL ,
  `create_time` TIMESTAMP NULL ,
  `update_time` TIMESTAMP NULL ,
  `poll_answer_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_poll_user_answer_poll_answer1` (`poll_answer_id` ASC) ,
  CONSTRAINT `fk_poll_user_answer_poll_answer1`
    FOREIGN KEY (`poll_answer_id` )
    REFERENCES `npsp`.`poll_answer` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `npsp`.`news`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`news` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `title` VARCHAR(90) NOT NULL ,
  `preview` TEXT NOT NULL ,
  `content` TEXT NOT NULL ,
  `status` INT NOT NULL ,
  `sw_status` VARCHAR(50) NULL ,
  `create_time` TIMESTAMP NULL ,
  `update_time` TIMESTAMP NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `npsp`.`SWFWorkflow`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`SWFWorkflow` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(90) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `workflowName` VARCHAR(90) NOT NULL ,
  `position` INT(11) NOT NULL ,
  `status` INT(11) NOT NULL ,
  `create_time` TIMESTAMP NULL DEFAULT NULL ,
  `update_time` TIMESTAMP NULL DEFAULT NULL ,
  `iiii` VARCHAR(90) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `npsp`.`SWFStatuses`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`SWFStatuses` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(90) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `constraint` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL ,
  `position` INT(11) NOT NULL ,
  `status` INT(11) NOT NULL ,
  `create_time` TIMESTAMP NULL DEFAULT NULL ,
  `update_time` TIMESTAMP NULL DEFAULT NULL ,
  `SWFWorkflow_Id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_SWFStatuses_SWFWorkflow1` (`SWFWorkflow_Id` ASC) ,
  CONSTRAINT `fk_SWFStatuses_SWFWorkflow1`
    FOREIGN KEY (`SWFWorkflow_Id` )
    REFERENCES `npsp`.`SWFWorkflow` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `npsp`.`SWFStatusesRelations`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `npsp`.`SWFStatusesRelations` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `SWFStatuses_id_primary` INT(11) NOT NULL ,
  `SWFStatuses_id_secondary` INT(11) NOT NULL ,
  `status` INT(11) NOT NULL ,
  `create_time` TIMESTAMP NULL DEFAULT NULL ,
  `update_time` TIMESTAMP NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_swfstatuses1` (`SWFStatuses_id_primary` ASC) ,
  INDEX `fk_swfstatuses2` (`SWFStatuses_id_secondary` ASC) ,
  CONSTRAINT `fk_swfstatuses1`
    FOREIGN KEY (`SWFStatuses_id_primary` )
    REFERENCES `npsp`.`SWFStatuses` (`id` ),
  CONSTRAINT `fk_swfstatuses2`
    FOREIGN KEY (`SWFStatuses_id_secondary` )
    REFERENCES `npsp`.`SWFStatuses` (`id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
