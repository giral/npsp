<?php
class ImgBehavior extends CBehavior {

	public function backHtmlOptions() {
		return array(
			'style'=>'height: 100px;',
		);
	}

	public function displayHintSize($width = 0, $height = 0) {
		return 'La taille de l\'image doit être : '.$width.'px * '.$height.'px (largeur * hauteur)';
	}

}