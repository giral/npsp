<?php

class YushBehavior extends CBehavior {

	public $self;

	public function cleanName($name, $extension = '') {
		$uglyName = strtolower($name);
		$mediocreName = preg_replace('/[^a-zA-Z0-9]+/', '_', $uglyName);
		$beautifulName = trim($mediocreName, '_') . "." . $extension;
		return $beautifulName;
	}

	public function getImageUrl($image_name_attribute = 'image_name') {
		return Yush::getUrl($this->self, Yush::SIZE_ORIGINAL, $this->self->$image_name_attribute);
	}

}