<?php
class PagesBehavior extends CBehavior {

	public $page;

    CONST PAGES_UNKNOWN_LABEL = 'Indéfini';

	public static function pages() {
		return array(
            SeoExtend::PAGE_HOME => 'Accueil',
			SeoExtend::PAGE_ACTIVITY => 'Activité',
			SeoExtend::PAGE_CONTACT => 'Contact',
            SeoExtend::PAGE_FLASHINFO => 'FlashInfo',
            SeoExtend::PAGE_ORGANISATION => 'Organisation',
			SeoExtend::PAGE_PARTNER => 'Partenaire',
            SeoExtend::PAGE_PRESENTATION => 'Présentation',
            SeoExtend::PAGE_PUBLICATION_ALL => 'Archive Publication',
            SeoExtend::PAGE_NEWS_ALL => 'Archive News',
		);
	}

	public function labelPages() {
		if(array_key_exists($this->page, self::pages())) {
			$page = self::pages();
			return $page[$this->page];
		}
		else
			return self::PAGES_UNKNOWN_LABEL;
	}

}