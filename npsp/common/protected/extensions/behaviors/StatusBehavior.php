<?php
class StatusBehavior extends CBehavior {

	public $status;

	CONST STATUS_INACTIV = 0;
	CONST STATUS_ACTIV = 1;
	const STATUS_DELETED = 3;
	CONST STATUS_UNKNOWN_LABEL = 'Indéfini';

	public static function statuses() {
		return array(
			self::STATUS_ACTIV => 'Activé',
			self::STATUS_INACTIV => 'Désactivé',
			self::STATUS_DELETED => 'Supprimé',
		);
	}

	public function labelStatus() {
		if(array_key_exists($this->status, self::statuses())) {
			$statuses = self::statuses();
			return $statuses[$this->status];
		}
		else
			return self::STATUS_UNKNOWN_LABEL;
	}

}