<?php
class SwfModelsBehavior extends CBehavior {

    public $swfmodel;

    public static function swfmodels() {
        return array(
            Workflow::SW_ACTIVITY => 'Activité',
            Workflow::SW_CONTACT => 'Contact',
            Workflow::SW_FLASHINFO => 'Flash Infos',
            Workflow::SW_GALLERYPHOTO => 'Gallerie photo',
            Workflow::SW_GALLERYVIDEO => 'Gallerie vidéo',
            Workflow::SW_HOME => 'Home',
            Workflow::SW_NEWS => 'News',
            Workflow::SW_ORGANISATION => 'Organisation',
            Workflow::SW_PARTNER => 'Partenaires',
            Workflow::SW_POLL => 'Sondage',
            Workflow::SW_PRESENTATION => 'Presentation',
            Workflow::SW_PUBLICATION => 'Publication',
        );
    }

    public function labelModels() {
        if(array_key_exists($this->swfmodel, self::swfmodels())) {
            $swfmodels = self::swfmodels();
            return $swfmodels[$this->swfmodel];
        }
        else
            return 'Unknow';
    }

}