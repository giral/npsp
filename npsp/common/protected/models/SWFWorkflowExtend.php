<?php

/**
 * This is the model class for table "SWFWorkflow".
 *
 * The followings are the available columns in table 'SWFWorkflow':
 * @property integer $id
 * @property string $name
 * @property string $workflowName
 * @property integer $position
 * @property integer $status
 * @property string $create_time
 * @property string $update_time
 *
 * The followings are the available model relations:
 * @property SWFWorkflowExtendlinkonmodel[] $swfworkflowlinkonmodels
 */
class SWFWorkflowExtend extends SWFWorkflow
{
	const OBJ_AUD = 'Workflow';

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
            ),
            'StatusBehavior'=>array(
                'class'=>'root.common.protected.extensions.behaviors.StatusBehavior',
                'status'=>$this->status
            ),
            'SwfModelsBehavior'=>array(
                'class'=>'root.common.protected.extensions.behaviors.SwfModelsBehavior',
            ),
        );
    }

    public function scopes() {
        $alias = $this->getTableAlias();
        $parentScope = parent::scopes();
        $sonScope = array(
            'notDeleted'=>array(
                'condition'=>$alias.'.status != '.StatusBehavior::STATUS_DELETED
            )
        );
        return array_merge($parentScope, $sonScope);
    }


    public function afterLogicalDelete() {
        $idList = array($this->id);
        /* ********* Update status of SWFStatusesExtend table **** */
        $criteria = new CDbCriteria;
        $criteria->addInCondition('SWFWorkflow_Id', $idList);
        SWFStatusesExtend::model('SWFStatusesExtend')->updateAll(array('status'=>StatusBehavior::STATUS_DELETED), $criteria);
        //SWFWorkflowExtendlinkonmodelExtend::model('SWFWorkflowExtendlinkonmodelExtend')->updateAll(array('status'=>StatusBehavior::STATUS_DELETED), $criteria);

        SWFStatusesExtend::afterLogicalDistantDelete();
        parent::afterDelete();
    }

    public function afterReActivation() {
        $idList = array($this->id);
        /* ********* Update status of SWFStatusesExtend table **** */
        $criteria = new CDbCriteria;
        $criteria->addInCondition('SWFWorkflow_Id', $idList);
        SWFStatusesExtend::model('SWFStatusesExtend')->updateAll(array('status'=>StatusBehavior::STATUS_ACTIV), $criteria);
        //SWFWorkflowExtendlinkonmodelExtend::model('SWFWorkflowExtendlinkonmodelExtend')->updateAll(array('status'=>StatusBehavior::STATUS_ACTIV), $criteria);

    }

	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'name' => 'Nom',
			'workflowName' => 'Nom du workflow',
			'position' => 'Position',
			'status' => 'Statut',
			'create_time' => 'Date de création',
			'update_time' => 'Date de modification',
		);
	}

    public function afterSave() {
        ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
        parent::afterSave();
    }

    public function afterDelete() {
        ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
        parent::afterDelete();
    }

    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('workflowName',$this->workflowName,true);
        $criteria->compare('position',$this->position);
        $criteria->compare('status',$this->status);
        $criteria->compare('create_time',$this->create_time,true);
        $criteria->compare('update_time',$this->update_time,true);

        $criteria->scopes = 'notDeleted';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

}
