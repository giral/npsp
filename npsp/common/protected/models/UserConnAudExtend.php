<?php

class UserConnAudExtend extends UserConnAud {

	const DEFAULT_ACTION = 'Connection';
	const DECONNECTION_ACTION = 'Deconnection';

	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'create_time',
				'updateAttribute' => 'update_time',
			),
		);
	}

	public static function add() {
		$userConnAudModel = new UserConnAudExtend;
		$userConnAudModel->action = self::DEFAULT_ACTION;
		$userConnAudModel->username = Yii::app()->user->name;
		$userConnAudModel->save();
	}

	public static function deconnection() {
		$userConnAudModel = new UserConnAudExtend;
		$userConnAudModel->action = self::DECONNECTION_ACTION;
		$userConnAudModel->username = Yii::app()->user->name;
		$userConnAudModel->save();
	}

	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		$criteria->order = 'id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

}