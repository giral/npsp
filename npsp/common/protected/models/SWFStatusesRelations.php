<?php

/**
 * This is the model class for table "SWFStatusesRelations".
 *
 * The followings are the available columns in table 'SWFStatusesRelations':
 * @property integer $id
 * @property integer $SWFStatuses_id_primary
 * @property integer $SWFStatuses_id_secondary
 * @property integer $status
 * @property string $create_time
 * @property string $update_time
 *
 * The followings are the available model relations:
 * @property SWFStatuses $sWFStatusesIdPrimary
 * @property SWFStatuses $sWFStatusesIdSecondary
 */
class SWFStatusesRelations extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SWFStatusesRelations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'SWFStatusesRelations';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('SWFStatuses_id_primary, SWFStatuses_id_secondary, status', 'required'),
			array('SWFStatuses_id_primary, SWFStatuses_id_secondary, status', 'numerical', 'integerOnly'=>true),
			array('create_time, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, SWFStatuses_id_primary, SWFStatuses_id_secondary, status, create_time, update_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'sWFStatusesIdPrimary' => array(self::BELONGS_TO, 'SWFStatuses', 'SWFStatuses_id_primary'),
			'sWFStatusesIdSecondary' => array(self::BELONGS_TO, 'SWFStatuses', 'SWFStatuses_id_secondary'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'SWFStatuses_id_primary' => 'Swfstatuses Id Primary',
			'SWFStatuses_id_secondary' => 'Swfstatuses Id Secondary',
			'status' => 'Status',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('SWFStatuses_id_primary',$this->SWFStatuses_id_primary);
		$criteria->compare('SWFStatuses_id_secondary',$this->SWFStatuses_id_secondary);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}