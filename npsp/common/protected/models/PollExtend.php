<?php

/**
 * Class PollExtend
 * @method statuses()
 * @method labelStatus()
 */
class PollExtend extends Poll {

	const OBJ_AUD = 'Sondage';

	public function behaviors() {
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'create_time',
				'updateAttribute' => 'update_time',
			),
			'StatusBehavior'=>array(
				'class'=>'root.common.protected.extensions.behaviors.StatusBehavior',
				'status'=>$this->status
			),
			'swBehavior'=>array(
				'class' => 'simpleWorkflow.SWActiveRecordBehavior',
				'statusAttribute' => 'sw_status',
			),
		);
	}

	public function rules() {
		$parentRules = parent::rules();
		$sonRules = array(
			array('sw_status', 'SWValidator', 'on'=>'create, updateWorkflow'),
			array('sw_status', 'safe'),
		);
		return array_merge($parentRules, $sonRules);
	}

	public function relations() {
			return array(
				'pollQuestions' => array(self::HAS_MANY, 'PollQuestionExtend', 'poll_id'),
			);
		}

	public function scopes() {
		$alias = $this->getTableAlias();
		$parentScope = parent::scopes();
		$sonScope = array(
			'notDeleted'=>array(
				'condition'=>$alias.'.status != '.StatusBehavior::STATUS_DELETED
			),
            'frontEnd'=>array(
                'condition' => $alias.'.status = '.StatusBehavior::STATUS_ACTIV,
                'order' => $alias.'.position ASC',
            ),
		);
		return array_merge($parentScope, $sonScope);
	}

	public function finalStatus($event) {
		$this->swRemoveFromWorkflow();
	}

	public function search() {

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('commentWF',$this->commentWF,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		$criteria->addCondition('sw_status = ""');

		$criteria->scopes = 'notDeleted';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchWorkflow() {

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('commentWF',$this->commentWF,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		$criteria->addCondition('sw_status <> ""');

		$criteria->scopes = 'notDeleted';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function afterSave() {
		if($this->status == StatusBehavior::STATUS_ACTIV) {
			$condition = 'id != '.$this->id.' AND status != '.StatusBehavior::STATUS_DELETED;
			$criteria = new CDbCriteria;
			$criteria->condition = $condition;
			$idList = CHtml::listData(PollExtend::model('PollExtend')->findAll($criteria), 'id', 'id');

			/* ********* Update status of poll table ******** */
			$criteria = new CDbCriteria;
			$criteria->addInCondition('id', $idList);
			$criteria->addCondition('status != '.StatusBehavior::STATUS_DELETED);
			PollExtend::model('PollExtend')->updateAll(array('status'=>StatusBehavior::STATUS_INACTIV), $criteria);

			/* ********* Update status of poll_question table **** */
			$criteria = new CDbCriteria;
			$criteria->addInCondition('poll_id', $idList);
			$criteria->addCondition('status != '.StatusBehavior::STATUS_DELETED);
			PollQuestionExtend::model('PollQuestionExtend')->updateAll(array('status'=>StatusBehavior::STATUS_INACTIV), $criteria);

			/* ********* Update status of poll_answer table ****** */
			$idList = CHtml::listData(PollQuestionExtend::model('PollQuestionExtend')->findAll($criteria), 'id', 'id');
			$criteria = new CDbCriteria;
			$criteria->addInCondition('poll_question_id', $idList);
			$criteria->addCondition('status != '.StatusBehavior::STATUS_DELETED);
			PollAnswerExtend::model('PollAnswerExtend')->updateAll(array('status'=>StatusBehavior::STATUS_INACTIV), $criteria);

			/* ********* Update status of poll_answer_user table ****** */
			$idList = CHtml::listData(PollAnswerExtend::model('PollAnswerExtend')->findAll($criteria), 'id', 'id');
			$criteria = new CDbCriteria;
			$criteria->addInCondition('poll_answer_id', $idList);
			$criteria->addCondition('status != '.StatusBehavior::STATUS_DELETED);
			PollUserAnswerExtend::model('PollUserAnswerExtend')->updateAll(array('status'=>StatusBehavior::STATUS_INACTIV), $criteria);
		}
		ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
		parent::afterSave();
	}

	public function afterLogicalDelete() {
		$idList = array($this->id);
		/* ********* Update status of poll_question table **** */
		$criteria = new CDbCriteria;
		$criteria->addInCondition('poll_id', $idList);
		PollQuestionExtend::model('PollQuestionExtend')->updateAll(array('status'=>StatusBehavior::STATUS_DELETED), $criteria);

		/* ********* Update status of poll_answer table ****** */
		$idList = CHtml::listData(PollQuestionExtend::model('PollQuestionExtend')->findAll($criteria), 'id', 'id');
		$criteria = new CDbCriteria;
		$criteria->addInCondition('poll_question_id', $idList);
		PollAnswerExtend::model('PollAnswerExtend')->updateAll(array('status'=>StatusBehavior::STATUS_DELETED), $criteria);

		/* ********* Update status of poll_answer_user table ****** */
		$idList = CHtml::listData(PollAnswerExtend::model('PollAnswerExtend')->findAll($criteria), 'id', 'id');
		$criteria = new CDbCriteria;
		$criteria->addInCondition('poll_answer_id', $idList);
		PollUserAnswerExtend::model('PollUserAnswerExtend')->updateAll(array('status'=>StatusBehavior::STATUS_DELETED), $criteria);

		ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
		parent::afterDelete();
	}

	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'name' => 'Nom',
			'commentWF' => 'Commentaire Workflow',
			'position' => 'Position',
			'status' => 'Statut',
			'create_time' => 'Date de création',
			'update_time' => 'Date de modification',
		);
	}

}