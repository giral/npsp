<?php

/**
 * Class ContactExtend
 * @method statuses()
 * @method labelStatus()
 * @method getImageUrl()
 * @method backHtmlOptions()
 * @method displayHintSize()
 */
class ContactExtend extends Contact {

	const IMAGE_WIDTH = '?';
	const IMAGE_HEIGHT = '?';
	const OBJ_AUD = 'Contact';

	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'create_time',
				'updateAttribute' => 'update_time',
			),
			'StatusBehavior'=>array(
				'class'=>'root.common.protected.extensions.behaviors.StatusBehavior',
				'status'=>$this->status
			),
			'YushBehavior' => array(
				'class'=>'root.common.protected.extensions.behaviors.YushBehavior',
				'self'=>$this,
			),
			'ImgBehavior'=>array(
				'class'=>'root.common.protected.extensions.behaviors.ImgBehavior',
			),
			'swBehavior'=>array(
				'class' => 'simpleWorkflow.SWActiveRecordBehavior',
				'statusAttribute' => 'sw_status',
			),
		);
	}

	public function rules() {
		$parentRules = parent::rules();
		$sonRules = array(
			array('sw_status', 'SWValidator', 'on'=>'create, updateWorkflow'),
			array('sw_status', 'safe'),
			array('image_name_illustration_1', 'file', 'allowEmpty'=>true, 'on'=>'update'),
		);
		return array_merge($parentRules, $sonRules);
	}

   
	public function scopes() {
		$alias = $this->getTableAlias();
		$parentScope = parent::scopes();
		$sonScope = array(
			'frontEnd'=>array(
				'condition' => $alias.'.status = '.StatusBehavior::STATUS_ACTIV,
				'order' => $alias.'.position ASC',
			),
			'notDeleted'=>array(
				'condition'=>$alias.'.status != '.StatusBehavior::STATUS_DELETED
			)
		);
		return array_merge($parentScope, $sonScope);
	}

	public function afterSave() {
		if($this->status == StatusBehavior::STATUS_ACTIV) {
			$condition = 'id != '.$this->id;
			ContactExtend::model('ContactExtend')->updateAll(array('status'=>StatusBehavior::STATUS_INACTIV), $condition);
		}
		ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
		parent::afterSave();
	}

	public function afterDelete() {
		ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
		parent::afterDelete();
	}

	public function finalStatus($event) {
		$this->swRemoveFromWorkflow();
	}

	public function search() {

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('part_name',$this->part_name,true);
		$criteria->compare('image_name_illustration_1',$this->image_name_illustration_1,true);
		$criteria->compare('up_content',$this->up_content,true);
		$criteria->compare('down_content',$this->down_content,true);
		$criteria->compare('commentWF',$this->commentWF,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		$criteria->addCondition('sw_status = ""');

		$criteria->scopes = 'notDeleted';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchWorkflow() {

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('part_name',$this->part_name,true);
		$criteria->compare('image_name_illustration_1',$this->image_name_illustration_1,true);
		$criteria->compare('up_content',$this->up_content,true);
		$criteria->compare('down_content',$this->down_content,true);
		$criteria->compare('commentWF',$this->commentWF,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		$criteria->addCondition('sw_status <> ""');

		$criteria->scopes = 'notDeleted';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'part_name' => 'Titre',
			'image_name_illustration_1' => 'Image Illustration',
			'up_content' => 'Contenu Haut',
			'down_content' => 'Contenu Bas',
			'commentWF' => 'Commentaire Workflow',
			'position' => 'Position',
			'status' => 'Statut',
			'create_time' => 'Date de création',
			'update_time' => 'Date de modification',
		);
	}

}