<?php

/**
 * Class PhotoExtend
 * @method statuses()
 * @method labelStatus()
 * @method getImageUrl($attributeName = 'image_name')
 * @method backHtmlOptions()
 * @method displayHintSize()
 */
class PhotoExtend extends Photo {

	const IMAGE_WIDTH = 161;
	const IMAGE_HEIGHT = 127;

	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'create_time',
				'updateAttribute' => 'update_time',
			),
			'StatusBehavior'=>array(
				'class'=>'root.common.protected.extensions.behaviors.StatusBehavior',
				'status'=>$this->status
			),
			'YushBehavior' => array(
				'class'=>'root.common.protected.extensions.behaviors.YushBehavior',
				'self'=>$this,
			),
			'ImgBehavior'=>array(
				'class'=>'root.common.protected.extensions.behaviors.ImgBehavior',
			),
		);
	}

    public function scopes()
    {
        $alias = $this->getTableAlias();
        $parentScope = parent::scopes();
        $sonScope = array(
            'notDeleted' => array(
                'condition' => $alias . '.status != ' . StatusBehavior::STATUS_DELETED
            ),
            'frontEnd' => array(
                'condition' => $alias . '.status = ' . StatusBehavior::STATUS_ACTIV,
                'order' => $alias . '.position ASC',
            ),
        );
        return array_merge($parentScope, $sonScope);
    }

	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'name' => 'Nom',
			'position' => 'Position',
			'status' => 'Statut',
			'create_time' => 'Date de création',
			'update_time' => 'Date de modificaiton',
			'gallery_photo_id' => 'Gallery Photo',
		);
	}

} 