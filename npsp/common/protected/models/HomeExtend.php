<?php

/**
 * Class HomeExtend
 * @method statuses()
 * @method labelStatus()
 * @method getImageUrl()
 * @method backHtmlOptions()
 * @method displayHintSize()
 */
class HomeExtend extends Home {

	const IMAGE_WIDTH = 661;
	const IMAGE_HEIGHT = 317;

	const OBJ_AUD = 'Accueil';

	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'create_time',
				'updateAttribute' => 'update_time',
			),
			'StatusBehavior'=>array(
				'class'=>'root.common.protected.extensions.behaviors.StatusBehavior',
				'status'=>$this->status
			),
			'YushBehavior' => array(
				'class'=>'root.common.protected.extensions.behaviors.YushBehavior',
				'self'=>$this,
			),
			'ImgBehavior'=>array(
				'class'=>'root.common.protected.extensions.behaviors.ImgBehavior',
			),
			'swBehavior'=>array(
				'class' => 'simpleWorkflow.SWActiveRecordBehavior',
				'statusAttribute' => 'sw_status',
			),
		);
	}

	public function rules() {
		$parentRules = parent::rules();
		$sonRules = array(
			array('image_name', 'file', 'allowEmpty'=>true, 'on'=>'update'),
			array('sw_status', 'SWValidator', 'on'=>'create, updateWorkflow'),
			array('sw_status', 'safe'),
		);
		return array_merge($parentRules, $sonRules);
	}

	public function scopes() {
		$alias = $this->getTableAlias();
		$parentScope = parent::scopes();
		$sonScope = array(
			'frontEnd'=>array(
				'condition' => $alias.'.status = '.StatusBehavior::STATUS_ACTIV,
				'order' => $alias.'.position ASC',
			),
			'notDeleted'=>array(
				'condition'=>$alias.'.status != '.StatusBehavior::STATUS_DELETED
			)
		);
		return array_merge($parentScope, $sonScope);
	}

	public function finalStatus($event) {
		$this->swRemoveFromWorkflow();
	}

	public function search() {

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('image_name',$this->image_name,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('commentWF',$this->commentWF,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		$criteria->addCondition('sw_status = ""');

		$criteria->scopes = 'notDeleted';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchWorkflow() {

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('image_name',$this->image_name,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('commentWF',$this->commentWF,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		$criteria->addCondition('sw_status <> ""');

		$criteria->scopes = 'notDeleted';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'name' => 'Nom',
			'image_name' => 'Image',
			'comment' => 'Commentaire',
			'commentWF' => 'Commentaire Workflow',
			'position' => 'Position',
			'status' => 'Statut',
			'create_time' => 'Date de création',
			'update_time' => 'Date de modification',
		);
	}

	public function afterSave() {
		ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
		parent::afterSave();
	}

	public function afterDelete() {
		ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
		parent::afterDelete();
	}

} 