<?php

/**
 * Class PollQuestionExtend
 * @method statuses()
 * @method labelStatus()
 */
class PollQuestionExtend extends PollQuestion {

	const LABEL_MULTI_ANS = 'Réponses Multiples';

	public function behaviors() {
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'create_time',
				'updateAttribute' => 'update_time',
			),
			'StatusBehavior'=>array(
				'class'=>'root.common.protected.extensions.behaviors.StatusBehavior',
				'status'=>$this->status
			),
		);
	}

	public function scopes() {
		$alias = $this->getTableAlias();
		$parentScope = parent::scopes();
		$sonScope = array(
			'notDeleted'=>array(
				'condition'=>$alias.'.status != '.StatusBehavior::STATUS_DELETED
			),
            'frontEnd'=>array(
                'condition' => $alias.'.status = '.StatusBehavior::STATUS_ACTIV,
                'order' => $alias.'.position ASC',
            ),
		);
		return array_merge($parentScope, $sonScope);
	}

	public function relations() {
		return array(
			'pollAnswers' => array(self::HAS_MANY, 'PollAnswerExtend', 'poll_question_id'),
			'poll' => array(self::BELONGS_TO, 'PollExtend', 'poll_id'),
		);
	}

	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'content' => 'Contentu',
			'multi_ans' => 'Réponse multiple',
			'position' => 'Position',
			'status' => 'Statut',
			'create_time' => 'Date de création',
			'update_time' => 'Date de modification',
			'poll_id' => 'Poll',
		);
	}

} 