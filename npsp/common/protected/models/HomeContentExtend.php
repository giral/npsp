<?php

class HomeContentExtend extends HomeContent {
	const IMAGE_WIDTH = 148;
	const IMAGE_HEIGHT = 172;
	const IMAGE_TITLE_WIDTH = 324;
	const IMAGE_TITLE_HEIGHT = 89;

	const OBJ_AUD = 'Le Mot du Directeur';

	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'create_time',
				'updateAttribute' => 'update_time',
			),
			'StatusBehavior'=>array(
				'class'=>'root.common.protected.extensions.behaviors.StatusBehavior',
				'status'=>$this->status
			),
			'YushBehavior' => array(
				'class'=>'root.common.protected.extensions.behaviors.YushBehavior',
				'self'=>$this,
			),
			'ImgBehavior'=>array(
				'class'=>'root.common.protected.extensions.behaviors.ImgBehavior',
			),
		);
	}

	public function rules() {
		$parentRules = parent::rules();
		$sonRules = array(
			array('image_name', 'file', 'allowEmpty'=>true, 'on'=>'update'),
			array('image_name_title', 'file', 'allowEmpty'=>true, 'on'=>'update'),
		);
		return array_merge($parentRules, $sonRules);
	}

	public function scopes() {
		$alias = $this->getTableAlias();
		$parentScope = parent::scopes();
		$sonScope = array(
			'frontEnd'=>array(
				'condition' => $alias.'.status = '.StatusBehavior::STATUS_ACTIV,
			),
			'notDeleted'=>array(
				'condition'=>$alias.'.status != '.StatusBehavior::STATUS_DELETED
			)
		);
		return array_merge($parentScope, $sonScope);
	}

	public function search() {

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('image_name_title',$this->image_name_title,true);
		$criteria->compare('image_name',$this->image_name,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		$criteria->scopes = 'notDeleted';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'title' => 'Titre',
			'image_name_title' => 'Image du titre',
			'image_name' => 'Image',
			'content' => 'Contenu',
			'status' => 'Statut',
			'create_time' => 'Date de création',
			'update_time' => 'Date de modification',
		);
	}

	public function afterSave() {
		ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
		parent::afterSave();
	}

	public function afterDelete() {
		ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
		parent::afterDelete();
	}

}