<?php

/**
 * This is the model class for table "swfstatusesrelations".
 *
 * The followings are the available columns in table 'swfstatusesrelations':
 * @property integer $id
 * @property integer $SWFStatuses_id_primary
 * @property integer $SWFStatuses_id_secondary
 * @property integer $status
 * @property string $create_time
 * @property string $update_time
 */
class SWFStatusesRelationsExtend extends SWFStatusesRelations
{

	const OBJ_AUD = 'Statut Workflow Relation';
    public function behaviors(){
        return array(
         'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
            ),
            'StatusBehavior'=>array(
                'class'=>'root.common.protected.extensions.behaviors.StatusBehavior',
                'status'=>$this->status
            ),);
    }

    public function scopes() {
        $alias = $this->getTableAlias();
        $parentScope = parent::scopes();
        $sonScope = array(
            'notDeleted'=>array(
                'condition'=>$alias.'.status != '.StatusBehavior::STATUS_DELETED
            )
        );
        return array_merge($parentScope, $sonScope);
    }

    public function search() {

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('SWFStatuses_id_primary',$this->SWFStatuses_id_primary,true);
        $criteria->compare('SWFStatuses_id_secondary',$this->SWFStatuses_id_secondary,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('create_time',$this->create_time,true);
        $criteria->compare('update_time',$this->update_time,true);

        $criteria->scopes = 'notDeleted';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public function relations() {
        return array(
            'sWFStatusesIdPrimary' => array(self::BELONGS_TO, 'SWFStatusesExtend', 'SWFStatuses_id_primary'),
            'sWFStatusesIdSecondary' => array(self::BELONGS_TO, 'SWFStatusesExtend', 'SWFStatuses_id_secondary'),
        );
    }

	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'SWFStatuses_id_primary' => 'Statut workflow primaire',
			'SWFStatuses_id_secondary' => 'Statut workflow secondaire',
			'status' => 'Statut',
			'create_time' => 'Date de création',
			'update_time' => 'Date de modification',
		);
	}

    public function afterSave() {
        ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
        parent::afterSave();
    }

    public function afterDelete() {
        ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
        parent::afterDelete();
    }

    public function getPrimaryStatus() {
        return $this->sWFStatusesIdPrimary ? $this->sWFStatusesIdPrimary->name : '';
    }

    public function getSecondaryStatus() {
        return $this->sWFStatusesIdSecondary ? $this->sWFStatusesIdSecondary->name : '';
    }

}
