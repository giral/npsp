<?php

/**
 * Class PollUserAnswerExtend
 * @method statuses()
 * @method labelStatus()
 */
class PollUserAnswerExtend extends PollUserAnswer {

	public function behaviors() {
	return array(
		'CTimestampBehavior' => array(
			'class' => 'zii.behaviors.CTimestampBehavior',
			'createAttribute' => 'create_time',
			'updateAttribute' => 'update_time',
		),
		'StatusBehavior'=>array(
			'class'=>'root.common.protected.extensions.behaviors.StatusBehavior',
			'status'=>$this->status
		),
	);
	}

	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'answer' => 'Répnse',
			'status' => 'Statut',
			'create_time' => 'Date de création',
			'update_time' => 'Date de modification',
			'poll_answer_id' => 'Poll Answer',
		);
	}

} 