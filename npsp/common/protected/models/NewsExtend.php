<?php

/**
 * Class NewsExtend
 * @method statuses()
 * @method labelStatus()
 */
class NewsExtend extends News {

	const IMAGE_WIDTH = 634;
	const IMAGE_HEIGHT = 318;
	const PREVIEW_IMAGE_WIDTH = 221;
	const PREVIEW_IMAGE_HEIGHT = 166;

	const OBJ_AUD = 'News';

	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'create_time',
				'updateAttribute' => 'update_time',
			),
			'StatusBehavior'=>array(
				'class'=>'root.common.protected.extensions.behaviors.StatusBehavior',
				'status'=>$this->status
			),
            'YushBehavior' => array(
                'class'=>'root.common.protected.extensions.behaviors.YushBehavior',
                'self'=>$this,
            ),
            'ImgBehavior'=>array(
                'class'=>'root.common.protected.extensions.behaviors.ImgBehavior',
            ),
			'swBehavior'=>array(
				'class' => 'simpleWorkflow.SWActiveRecordBehavior',
				'statusAttribute' => 'sw_status',
			),
		);
	}

	public function rules() {
		$parentRules = parent::rules();
		$sonRules = array(
			array('preview_image_name', 'file', 'allowEmpty'=>true, 'on'=>'update'),
			array('image_name', 'file', 'allowEmpty'=>true, 'on'=>'update'),
			array('sw_status', 'SWValidator', 'on'=>'create, updateWorkflow'),
			array('sw_status', 'safe'),
		);
		return array_merge($parentRules, $sonRules);
	}

	public function scopes() {
		$alias = $this->getTableAlias();
		$parentScope = parent::scopes();
		$sonScope = array(
			'frontEnd'=>array(
				'condition' => $alias.'.status = '.StatusBehavior::STATUS_ACTIV,
                'order' => $alias.'.create_time ASC',
			),
			'notDeleted'=>array(
				'condition'=>$alias.'.status != '.StatusBehavior::STATUS_DELETED
			)
		);
		return array_merge($parentScope, $sonScope);
	}

	public function finalStatus($event) {
		$this->swRemoveFromWorkflow();
	}

	public function search() {

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('preview_image_name',$this->preview_image_name,true);
		$criteria->compare('preview',$this->preview,true);
		$criteria->compare('image_name',$this->image_name,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('author',$this->author,true);
		$criteria->compare('commentWF',$this->commentWF,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		$criteria->addCondition('sw_status = ""');

		$criteria->scopes = 'notDeleted';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchWorkflow() {

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('preview_image_name',$this->preview_image_name,true);
		$criteria->compare('preview',$this->preview,true);
		$criteria->compare('image_name',$this->image_name,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('author',$this->author,true);
		$criteria->compare('commentWF',$this->commentWF,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		$criteria->addCondition('sw_status <> ""');

		$criteria->scopes = 'notDeleted';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'title' => 'Titre',
			'preview_image_name' => 'Image Aperçu',
			'preview' => 'Aperçu',
			'image_name' => 'Image',
			'content' => 'Contenu',
			'author' => 'Auteur',
			'commentWF' => 'Commentaire Workflow',
			'status' => 'Statut',
			'create_time' => 'Date de création',
			'update_time' => 'Date de modification',
		);
	}

	public function afterSave() {
		ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
		parent::afterSave();
	}

	public function afterDelete() {
		ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
		parent::afterDelete();
	}

} 