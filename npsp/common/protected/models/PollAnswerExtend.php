<?php

/**
 * Class PollAnswerExtend
 * @method statuses()
 * @method labelStatus()
 */
class PollAnswerExtend extends PollAnswer {

	public function behaviors() {
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'create_time',
				'updateAttribute' => 'update_time',
			),
			'StatusBehavior'=>array(
				'class'=>'root.common.protected.extensions.behaviors.StatusBehavior',
				'status'=>$this->status
			),
		);
	}

    public function scopes()
    {
        $alias = $this->getTableAlias();
        $parentScope = parent::scopes();
        $sonScope = array(
            'notDeleted' => array(
                'condition' => $alias . '.status != ' . StatusBehavior::STATUS_DELETED
            ),
            'frontEnd' => array(
                'condition' => $alias . '.status = ' . StatusBehavior::STATUS_ACTIV,
                'order' => $alias . '.position ASC',
            ),
        );
        return array_merge($parentScope, $sonScope);
    }

        public function relations() {
		return array(
			'pollQuestion' => array(self::BELONGS_TO, 'PollQuestionExtend', 'poll_question_id'),
			'pollUserAnswers' => array(self::HAS_MANY, 'PollUserAnswerExtend', 'poll_answer_id'),
		);
	}

	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'content' => 'Contenu',
			'position' => 'Position',
			'status' => 'Statut',
			'create_time' => 'Date de création',
			'update_time' => 'Date de modification',
			'poll_question_id' => 'Poll Question',
		);
	}

} 