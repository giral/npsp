<?php

/**
 * This is the model class for table "poll_question".
 *
 * The followings are the available columns in table 'poll_question':
 * @property integer $id
 * @property string $content
 * @property integer $multi_ans
 * @property integer $position
 * @property integer $status
 * @property string $create_time
 * @property string $update_time
 * @property integer $poll_id
 *
 * The followings are the available model relations:
 * @property PollAnswer[] $pollAnswers
 * @property Poll $poll
 */
class PollQuestion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PollQuestion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'poll_question';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('content, multi_ans, status, poll_id', 'required'),
			array('multi_ans, position, status, poll_id', 'numerical', 'integerOnly'=>true),
			array('content', 'length', 'max'=>90),
			array('create_time, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, content, multi_ans, position, status, create_time, update_time, poll_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pollAnswers' => array(self::HAS_MANY, 'PollAnswer', 'poll_question_id'),
			'poll' => array(self::BELONGS_TO, 'Poll', 'poll_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'content' => 'Content',
			'multi_ans' => 'Multi Ans',
			'position' => 'Position',
			'status' => 'Status',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'poll_id' => 'Poll',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('multi_ans',$this->multi_ans);
		$criteria->compare('position',$this->position);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('poll_id',$this->poll_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}