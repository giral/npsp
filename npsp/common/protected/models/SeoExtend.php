<?php

/**
 * This is the model class for table "seo".
 *
 * The followings are the available columns in table 'seo':
 * @property integer $id
 * @property integer $pageid
 * @property string $page
 * @property integer $status
 * @property string $create_time
 * @property string $update_time
 * @property string $tagh1
 * @property string $title
 * @property string $metadesc
 * @property string $rewrittenurl
 */
class SeoExtend  extends Seo
{
	const PAGE_HOME = 'index';
	const PAGE_ACTIVITY = 'activity';
	const PAGE_CONTACT = 'contact';
	const PAGE_FLASHINFO = 'flashInfo';
	const PAGE_ORGANISATION = 'organisation';
	const PAGE_PARTNER = 'partner';
	const PAGE_PRESENTATION = 'presentation';
	const PAGE_NEWS = 'news';
	const PAGE_NEWS_ALL = 'newsAll';
	const PAGE_GALLERYPHOTO = 'galleryPhoto';
	const PAGE_GALLERYVIDEO = 'galleryVideo';
	const PAGE_PUBLICATION = 'publication';
	const PAGE_PUBLICATION_ALL = 'publicationAll';

	const OBJ_AUD = 'SEO';

  	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'create_time',
				'updateAttribute' => 'update_time',
			),
			'PagesBehavior'=>array(
				'class'=>'root.common.protected.extensions.behaviors.PagesBehavior',
				'page'=>$this->page
			),
            'StatusBehavior'=>array(
				'class'=>'root.common.protected.extensions.behaviors.StatusBehavior',
				'status'=>$this->status
			),
		);
	}

	public function scopes() {
		$alias = $this->getTableAlias();
		$parentScope = parent::scopes();
		$sonScope = array(
			'notDeleted'=>array(
				'condition'=>$alias.'.status != '.StatusBehavior::STATUS_DELETED
			),
            'frontEnd' => array(
                'condition' => $alias . '.status = ' . StatusBehavior::STATUS_ACTIV,
            ),
		);
		return array_merge($parentScope, $sonScope);
	}

    public static function getSeoParameters($page) {
		$criteria=New CDbCriteria();
        $criteria->condition='status='.StatusBehavior::STATUS_ACTIV;
		$result =  SeoExtend::model('SeoExtend')->findByAttributes(
			array('page'=>$page),
			$criteria
        );
        return  $result;
   }

   public function search() {

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('pageid',$this->pageid);
		$criteria->compare('page',$this->page,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('tagh1',$this->tagh1,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('metadesc',$this->metadesc,true);
		$criteria->compare('rewrittenurl',$this->rewrittenurl,true);

		$criteria->scopes = 'notDeleted';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'pageid' => 'Pageid',
			'page' => 'Page',
			'status' => 'Statut',
			'create_time' => 'Date de création',
			'update_time' => 'Date de modification',
			'tagh1' => 'Tagh1',
			'title' => 'Titre',
			'metadesc' => 'Description META',
			'rewrittenurl' => 'URL Réécrite',
		);
	}

	public function afterSave() {
		ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
		parent::afterSave();
	}

	public function afterDelete() {
		ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
		parent::afterDelete();
	}

}
