<?php

/**
 * Class GalleryPhotoExtend
 * @method statuses()
 * @method labelStatus()
 * @property PhotoExtend[] $photos
 */
Yii::import('root.common.protected.extensions.behaviors.StatusBehavior');
class GalleryPhotoExtend extends GalleryPhoto {

	const OBJ_AUD = 'Gallerie Photo';

	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
					'class' => 'zii.behaviors.CTimestampBehavior',
					'createAttribute' => 'create_time',
					'updateAttribute' => 'update_time',
				),
				'StatusBehavior'=>array(
					'class'=>'root.common.protected.extensions.behaviors.StatusBehavior',
					'status'=>$this->status
				),
				'YushBehavior' => array(
					'class'=>'root.common.protected.extensions.behaviors.YushBehavior',
					'self'=>$this,
				),
				'ImgBehavior'=>array(
					'class'=>'root.common.protected.extensions.behaviors.ImgBehavior',
				),
				'swBehavior'=>array(
					'class' => 'simpleWorkflow.SWActiveRecordBehavior',
					'statusAttribute' => 'sw_status',
				),
		);
	}

	public function rules() {
		$parentRules = parent::rules();
		$sonRules = array(
			array('sw_status', 'SWValidator', 'on'=>'create, updateWorkflow'),
			array('sw_status', 'safe'),
		);
		return array_merge($parentRules, $sonRules);
	}

	public function scopes() {
		$alias = $this->getTableAlias();
		$parentScope = parent::scopes();
		$sonScope = array(
			'notDeleted'=>array(
				'condition'=>$alias.'.status != '.StatusBehavior::STATUS_DELETED
			),
            'frontEnd'=>array(
				'condition' => $alias.'.status = '.StatusBehavior::STATUS_ACTIV,
				'order' => $alias.'.id DESC',
			),
			'photoFrontEnd'=> array(
				'condition' => $alias.'.status = '.StatusBehavior::STATUS_ACTIV,
				'order' => $alias.'.id DESC',
			)
		);
		return array_merge($parentScope, $sonScope);
	}

	public function relations() {
		$alias = $this->getTableAlias();
		return array(
			'photos' => array(self::HAS_MANY, 'PhotoExtend', 'gallery_photo_id'),
			'photosLimitOne' => array(self::HAS_ONE, 'PhotoExtend', 'gallery_photo_id',
				'condition'=>$alias.'.status = '.StatusBehavior::STATUS_ACTIV,
				'order'=>$alias.'.id DESC'),
		);
	}

	public function finalStatus($event) {
		$this->swRemoveFromWorkflow();
	}

	public function search() {

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('commentWF',$this->commentWF,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		$criteria->addCondition('sw_status = ""');

		$criteria->scopes = 'notDeleted';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchWorkflow() {

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('commentWF',$this->commentWF,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		$criteria->addCondition('sw_status <> ""');

		$criteria->scopes = 'notDeleted';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'name' => 'Nom',
			'content' => 'Contenu',
			'commentWF' => 'Commentaire Workflow',
			'position' => 'Position',
			'status' => 'Statut',
			'create_time' => 'Date de création',
			'update_time' => 'Date de modification',
		);
	}

	/**
	 * @return int
	 */
	public function getLatestPosition() {
		$max = 0;
		$photos = $this->photos;
		if($photos) {
			foreach($photos as $photo) {
				if($photo->position) {
					if($photo->position > $max) {
						$max = $photo->position;
					}
				}
			}
		}
		return $max;
	}

	public function afterSave() {
		ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
		parent::afterSave();
	}

	public function afterDelete() {
		ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
		parent::afterDelete();
	}


} 