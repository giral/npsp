<?php

/**
 * This is the model class for table "swfstatuses".
 *
 * The followings are the available columns in table 'swfstatuses':
 * @property integer $id
 * @property string $name
 * @property integer $position
 * @property string $constraint
 * @property integer $status
 * @property string $create_time
 * @property string $update_time
 */
class SWFStatusesExtend extends SWFStatuses
{

	const OBJ_AUD = 'Statut Workflow';

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
            ),
            'StatusBehavior'=>array(
                'class'=>'root.common.protected.extensions.behaviors.StatusBehavior',
                'status'=>$this->status
            ),);
    }

    public function scopes() {
        $alias = $this->getTableAlias();
        $parentScope = parent::scopes();
        $sonScope = array(
            'notDeleted'=>array(
                'condition'=>$alias.'.status != '.StatusBehavior::STATUS_DELETED
            ),
            'lowestPosition'=>array(
                'select'=>'id, name, MIN(position) as position'
            ),
            'toDelete'=>array(
                'select'=>'id',
                'condition'=>$alias.'.status = '.StatusBehavior::STATUS_DELETED
            ),
        );
        return array_merge($parentScope, $sonScope);
    }

    public function search() {

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('position',$this->position,true);
        $criteria->compare('constraint',$this->constraint,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('create_time',$this->create_time,true);
        $criteria->compare('update_time',$this->update_time,true);
        $criteria->compare('SWFWorkflow_Id',$this->SWFWorkflow_Id,true);

        $criteria->scopes = 'notDeleted';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public function afterLogicalDelete() {
        $idList = array($this->id);
        /* ********* Update status of SWFStatusesExtend table **** */
        $criteria = new CDbCriteria;
        $criteria->addInCondition('SWFStatuses_id_primary', $idList,'OR');
        $criteria->addInCondition('SWFStatuses_id_secondary', $idList);
        SWFStatusesRelationsExtend::model('SWFStatusesRelationsExtend')->updateAll(array('status'=>StatusBehavior::STATUS_DELETED), $criteria);
        parent::afterDelete();
    }

    public static function afterLogicalDistantDelete()
    {
        $idList = CHtml::listData(SWFStatusesExtend::model('SWFStatusesExtend')->toDelete()->findAll(), 'id', 'id');

        $criteria = new CDbCriteria;
        $criteria->addInCondition('SWFStatuses_id_primary', $idList,'OR');
        $criteria->addInCondition('SWFStatuses_id_secondary', $idList);
        SWFStatusesRelationsExtend::model('SWFStatusesRelationsExtend')->updateAll(array('status'=>StatusBehavior::STATUS_DELETED), $criteria);

    }

	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'name' => 'Nom',
			'position' => 'Position',
			'constraint' => 'Contrainte',
			'status' => 'Statut',
			'create_time' => 'Date de création',
			'update_time' => 'Date de modification',
			'SWFWorkflow_Id' => 'SWFWorkflowExtend',
		);
	}

	public function getFullName() {
	    return $this->sWFWorkflow->name.' - '.$this->name;
	}

    public function afterSave() {
        ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
        parent::afterSave();
    }

    public function afterDelete() {
        ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
        parent::afterDelete();
    }

}
