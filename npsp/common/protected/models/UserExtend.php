<?php
/**
 * Class UserExtend
 * @method statuses()
 * @method labelStatus()
 */
class UserExtend extends User {

	public $roles;
	const OBJ_AUD = 'Utiisateur';

	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'create_time',
				'updateAttribute' => 'update_time',
			),
			'StatusBehavior'=>array(
				'class'=>'root.common.protected.extensions.behaviors.StatusBehavior',
				'status'=>$this->status
			),
		);
	}

	public function scopes() {
		$alias = $this->getTableAlias();
		$parentScope = parent::scopes();
		$sonScope = array(
			'notDeleted'=>array(
				'condition'=>$alias.'.status != '.StatusBehavior::STATUS_DELETED
			)
		);
		return array_merge($parentScope, $sonScope);
	}

	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('last_connexion',$this->last_connexion,true);

		$criteria->scopes = 'notDeleted';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function beforeSave() {
		$this->cryptPassword();
	    return parent::beforeSave();
	}

	public static function cryptPass($password, $salt) {
		return crypt($password, $salt);
	}

	private function cryptPassword() {
		$this->password = self::cryptPass($this->password,$this->password);
	}

	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'username' => 'Nom d\'utilisateur',
			'password' => 'Mot de passe',
			'firstname' => 'Prénom',
			'lastname' => 'Famille',
			'status' => 'Statut',
			'create_time' => 'Date de création',
			'update_time' => 'Date de modification',
			'last_connexion' => 'Dernière connexion',
		);
	}

	public function afterSave() {
		ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
		parent::afterSave();
	}

	public function afterDelete() {
		ObjectAudExtend::insertAudit(self::OBJ_AUD, $this->attributes);
		parent::afterDelete();
	}

}