<?php

/**
 * This is the model class for table "swfstatuses".
 *
 * The followings are the available columns in table 'swfstatuses':
 * @property integer $id
 * @property string $name
 * @property integer $position
 * @property string $constraint
 * @property integer $status
 * @property string $create_time
 * @property string $update_time
 * @property integer $SWFWorkflow_Id
 *
 * The followings are the available model relations:
 * @property SWFWorkflowExtend $sWFWorkflow
 * @property SWFStatusesRelations[] $swfstatusesrelations
 * @property SWFStatusesRelations[] $swfstatusesrelations1
 */
class SWFStatuses extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'SWFStatuses';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, position, status', 'required'),
			array('position, status, SWFWorkflow_Id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>90),
			array('constraint', 'length', 'max'=>100),
			array('create_time, update_time', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, position, constraint, status, create_time, update_time, SWFWorkflow_Id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'sWFWorkflow' => array(self::BELONGS_TO, 'SWFWorkflowExtend', 'SWFWorkflow_Id'),
			'swfstatusesrelations' => array(self::HAS_MANY, 'SWFStatusesRelations', 'SWFStatuses_id_primary'),
			'swfstatusesrelations1' => array(self::HAS_MANY, 'SWFStatusesRelations', 'SWFStatuses_id_secondary'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'position' => 'Position',
			'constraint' => 'Constraint',
			'status' => 'Status',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'SWFWorkflow_Id' => 'SWFWorkflowExtend',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('constraint',$this->constraint,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('SWFWorkflow_Id',$this->SWFWorkflow_Id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Swfstatuses the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
