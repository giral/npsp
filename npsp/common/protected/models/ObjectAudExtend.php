<?php

class ObjectAudExtend extends ObjectAud {

	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'create_time',
				'updateAttribute' => 'update_time',
			),
		);
	}

	public static function insertAudit($object, $data, $action = 'none') {
		if($action == 'none') {
			$currentAction = Yii::app()->controller->action->id;
			if($currentAction == 'create')
				$action = Audit::ACTION_INSERT_LABEL;
			else if($currentAction == 'update' || $currentAction == 'updateWorkflow')
				$action = Audit::ACTION_MODIFICATION_LABEL;
			else if($currentAction == 'delete')
				$action = Audit::ACTION_DELETE_LABEL;
			else
				$action = $currentAction;
		}
		$model = new ObjectAudExtend;
		$model->username = Yii::app()->user->name;
		$model->action = $action;
		$model->object = $object;
		$model->content = self::formatFieldValue($data);
		$model->save();
	}

	/**
	 * @param $data
	 * @return string
	 */
	public static function formatFieldValue($data) {
		$output = '';
		$separator = '=>';
	    if($data) {
	        $keys = array_keys($data);
	        if($keys) {
	            foreach($keys as $key) {
	                $output .= $key.' '.$separator.' '.$data[$key].'\n';
	            }
	        }
	    }
	    return $output;
	}

	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('object',$this->object,true);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		$criteria->order = 'id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

}