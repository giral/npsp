<?php


$error = '';
$nom = '';
$email = '';
$objet = '';
$fonction = '';
$contact = '';
$message = '';

function clean_text($string)
{
	$string = trim($string);
	$string = stripslashes($string);
	$string = htmlspecialchars($string);
	return $string;
}


if(isset($_POST["submit"]))
{
	if(empty($_POST["nom"]))
	{
		$error .= '<p><label class="text-danger">Please Enter your nom</label></p>';
	}
	else
	{
		$nom = clean_text(utf8_decode($_POST["nom"]));
		if(!preg_match("/^[a-zA-Z ]*$/",$nom))
		{
			$error .= '<p><label class="text-danger">Juste votre nom SVP!</label></p>';
		}
	}
	if(empty($_POST["email"]))
	{
		$error .= '<p><label class="text-danger">Please Enter your Email</label></p>';
	}
	else
	{
		$email = clean_text($_POST["email"]);
		if(!filter_var($email, FILTER_VALIDATE_EMAIL))
		{
			$error .= '<p><label class="text-danger">Invalid email format</label></p>';
		}
	}
	if(empty($_POST["objet"]))
	{
		$error .= '<p><label class="text-danger">objet is required</label></p>';
	}
	else
	{
		$objet = clean_text($_POST["objet"]);
	}
	if(empty($_POST["message"]))
	{
		$error .= '<p><label class="text-danger">Message is required</label></p>';
	}
	else
	{
		$message = clean_text($_POST["message"]);
	}
	if($error == '')
	{
		require 'class/class.phpmailer.php';
        $mail->CharSet = 'UTF-8';
        $mail->Encoding = 'base64';
		$mail = new PHPMailer;
		$mail->IsSMTP();								//Sets Mailer to send message using SMTP
		$mail->Host = 'smtp.gmail.com';		//Sets the SMTP hosts of your Email hosting, this for Godaddy
		$mail->Port = '587';								//Sets the default SMTP server port
		$mail->SMTPAuth = true;							//Sets SMTP authentication. Utilizes the Username and Password variables
		$mail->Username = 'giral.zoro@ceenettechnologies.com';					//Sets SMTP username
		$mail->Password = '01007423S&z';					//Sets SMTP password
		$mail->SMTPSecure = 'tls';							//Sets connection prefix. Options are "", "ssl" or "tls"
		$mail->From = $_POST["email"];					//Sets the From email address for the message
		$mail->FromName = 'NPSP-CI SITE WEB';				//Sets the From name of the message
		$mail->AddAddress('giral581@gmail.com', 'NPSP');		//Adds a "To" address
		$mail->AddCC($_POST["email"], utf8_decode($_POST["nom"]));	//Adds a "Cc" address
		$mail->WordWrap = 50;							//Sets word wrapping on the body of the message to a given number of characters
		$mail->IsHTML(true);							//Sets message type to HTML				
		$mail->Subject = utf8_decode($_POST["objet"]);				//Sets the Subject of the message
		$mail->Body = $_POST["message"]."<br/>"."Fonction: ".$_POST["fonction"]."<br/>"."Contact: ".$_POST["contact"];				//An HTML or plain text message body
		
		$mail->SMTPDebug =0;
		if($mail->Send())								//Send an Email. Return true on success or false on error
		{
			/* var_dump($error);
			die();"\r\n" */
			$error = '<label class="text-success">Merci de nous avoir contactés</label>';

		}
		else
		{
			$error = '<label class="text-danger">Il y a une erreur</label>';
		}
		$nom = '';
        $email = '';
        $objet = '';
        $fonction = '';
        $contact = '';
        $message = '';
	}
}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <?php include ("header.php");
          $filepath = realpath (dirname(__FILE__));

          require_once($filepath."/webanalytics.php");
          include "websettings.php";
          ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135621511-2"></script>
    <script type='text/javascript' src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
    <script type='text/javascript' src='https://code.jquery.com/jquery-1.11.0.js'></script>
    <script>
        $(document).ready(function(){
        $(":input").inputmask();



        $("#phone").inputmask({
        mask: '99 99 99 9999',
        placeholder: ' ',
        showMaskOnHover: false,
        showMaskOnFocus: false,
        onBeforePaste: function (pastedValue, opts) {
        var processedValue = pastedValue;
        
        //do something with it
        
        return processedValue;
        }
        });
        });
    </script>
<script>

window.dataLayer = window.dataLayer || [];

function gtag(){dataLayer.push(arguments);}

gtag('js', new Date());        

gtag('config', 'UA-135621511-2');

</script>
<script src="wa.js"></script>

<!-- Primary Meta Tags -->
<title>Contacts Nouvelle Pharmacie de la Santé Publique de Côte d'Ivoire</title>
<meta name="title" content="Contacts Nouvelle Pharmacie de la Santé Publique Côte d'Ivoire">
<meta name="description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">

<!-- Open Graph / Facebook -->
<meta property="og:type" content="website">
<meta property="og:url" content="http://www.npsp.ci/">
<meta property="og:title" content="Contacts La Nouvelle Pharmacie de la Santé Publique de Côte d'Ivoire">
<meta property="og:description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">
<meta property="og:image" content="http://npsp.ci/assets/img/npsp-ci.png">

<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="http://www.npsp.ci/">
<meta property="twitter:title" content="La Nouvelle Pharmacie de la Santé de Publique Côte d'Ivoire">
<meta property="twitter:description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">
<meta property="twitter:image" content="http://npsp.ci/assets/img/npsp-ci.png">
<meta name="viewport" content="width=device-width, initial-scale=1">


<meta charset="UTF-8">
</head>
<body>


<section class="header10 cid-qpgqy642wa2" id="header11-y" data-rv-view="7364"> 

    
    <div class="container align-left">
        <div class="media-container-column mbr-white col-md-12">
             
            <h1 class="mbr-section-title py-3 mbr-fonts-style display-1"><strong>Nous contacter </strong></h1>
            
            
        </div>
    </div>

    
</section>

<div class="barbread">
    
<div class="container">
<div class="mbr-text col-12 col-md-12 text-black  mbr-fonts-style display-7">        
                <a class="text-black" href="index.php"> Accueil  </a>| Contact
</div>         
                
            
    </div>

</div><br/><br/>

<section class="mbr-section form4 cid-qpgOiHkGqX" id="form4-3k" data-rv-view="7857">
    
    

    
    <div class="container">
        <div class="row">
            <div class="col-md-6">
              <div class="google-map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.816262398898!2d-3.998597949176959!3d5.291345996142464!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfc1e950ea45e0f1%3A0x193180aac1bb6ee4!2sLa+Nouvelle+PSP+Cote+D&#39;Ivoire!5e0!3m2!1sfr!2sci!4v1550137875367" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
            </div>
            <div class="col-md-6">
                <h2 class="pb-3 align-left text-orange mbr-fonts-style display-2">
                    Nouvelle PSP Côte d'Ivoire
                </h2>
                <div>
                    
                    <div class="icon-contacts pb-3">
                    <h6 class="pb-3 align-left text-black mbr-fonts-style display-12">
                        Agence d'Abidjan
                        </h6>
                        <p class="mbr-text align-left mbr-fonts-style display-7">
                         
                        Direction générale : (+225) 27 21 21 73 02<br>
                        Direction Commerciale et Marketing : (+225) 27 21 21 73 41<br>
                        Direction Administrative et Financière : (+225) 27 21 21 73 11<br>
                        Direction des Programmes et Appuis Spéciaux : (+225) 27 21 21 73 05<br>
                        Direction de la Logistique Pharmaceutique : (+225) 27 21 21 73 21<br>
                        Direction de l'Information Strategique et de la Planification Operationnelle: (+225) 27 21 21 73 90 <br> 
                            Email: info@npsp.ci
                        </p>
                    </div>
                </div>
                <div data-form-type="formoid">
                    <div data-form-alert="" hidden="">
                        Veuillez remplir le formulaire.
                    </div>
                    <?php echo $error; ?>
                    <form class="block mbr-form" method="post" data-form-title="Mobirise Form"><input type="hidden" data-form-email="true" value="">
                        <div class="row">
                        <!-- /redirect/backoffice/PHPMailer/src/PHPMailer.php  action="sendMail.php"-->
                            <div class="col-md-6 multi-horizontal" data-for="name">
                                <input type="text" class="form-control input" name="nom" data-form-field="Name" placeholder="Nom" required="" id="name-form4-3k">
                            </div>
                            <div class="col-md-6 multi-horizontal" data-for="name">
                                <input type="text" class="form-control input" name="objet" data-form-field="Name" placeholder="Objet" required="" id="name-form4-3k">
                            </div>
                            <div class="col-md-6 multi-horizontal" data-for="name">
                                <input type="text" class="form-control input" name="fonction" data-form-field="Name" placeholder="Fonction" required="" id="name-form4-3k">
                            </div>
                            <div class="col-md-6 multi-horizontal" data-for="phone">
                                <input class="form-control" id="phone" data-inputmask="'alias': 'phonebe'">
                                <input type="text" name="phone" id="phone" placeholder="(000) 000-000">
                                <input type="text" class="form-control input" name="contact" data-form-field="Phone" placeholder="Contact" required="" id="phone-form4-3k">
                            </div>
                            <div class="col-md-12" data-for="email">
                                <input type="text" class="form-control input" name="email" data-form-field="Email" placeholder="Email" required="" id="email-form4-3k">
                            </div>
                            <div class="col-md-12" data-for="message">
                                <textarea class="form-control input" name="message" rows="3" data-form-field="Message" placeholder="Message" style="resize:none" id="message-form4-3k"></textarea>
                            </div>
                            <div class="input-group-btn col-md-12" >
							    <input type="submit" name="submit" value="Envoyer message" class="btn btn-sm btn-oranj" style="border-radius: 18px;"/>
						    </div>
                            <!-- <div class="input-group-btn col-md-12" style="margin-top: 10px;">
                                <button type="submit" class="btn btn-sm btn-oranj">Envoyer message</button>
                            </div> -->
                        </div>
                    </form>
                   
					<!-- <form method="post"class="block mbr-form" data-form-title="Mobirise Form"><input type="hidden" data-form-email="true" value="">
                    <div class="row">
						<div class="col-md-6 multi-horizontal" data-for="name">
							<label>Enter Name</label>
							<input type="text" name="nom" placeholder="Nom" class="form-control input" value="<?php echo $nom; ?>" />
						</div>
						<div class="col-md-6 multi-horizontal" data-for="name">
							<label>Enter Email</label>
							<input type="text" name="email" class="form-control" placeholder="Enter Email" value="<?php echo $email; ?>" />
						</div>
						<div class="col-md-6 multi-horizontal" data-for="name">
							<label>Enter Subject</label>
							<input type="text" name="objet" class="form-control" placeholder="Enter Subject" value="<?php echo $objet; ?>" />
						</div>
						<div class="col-md-6 multi-horizontal" data-for="name">
							<label>Enter Message</label>
							<textarea name="message" class="form-control" placeholder="Enter Message"><?php echo $message; ?></textarea>
						</div>
						<div class="input-group-btn col-md-12" style="border-radius: 12px;">
							<input type="submit" name="submit" value="Envoyer message" class="btn btn-sm btn-oranj" />
						</div>
                    </div>
					</form> -->
                </div>
            </div>
        </div>
    </div>
</section>

<!--
<section class="mbr-section form1 cid-qpgNZ1vwzD" id="form1-39" data-rv-view="7004">
    
    

    
    <div class="container">
        <div class="media-container-column title col-12 col-lg-8 offset-lg-2">
            <h2 class="mbr-section-title align-center mbr-black pb-3 mbr-fonts-style display-2">
                Formulaire de contact
            </h2>
            <h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-5">
                Easily add subscribe and contact forms without any server-side integration.
            </h3>
        </div>
    </div>

    <div class="container">
        <div class="media-container-column col-lg-8 offset-lg-2" data-form-type="formoid">
                <div data-form-alert="" hidden="">
                    Thanks for filling out the form!
                </div>

                <form class="mbr-form" action="https://mobirise.com/" method="post" data-form-title="Mobirise Form"><input type="hidden" data-form-email="true" value="LTBae+7Oz1gujWc96gu+rwOBHOnIurbWNPWXGm2Cm7dhdfBiuYj08gWyOdChK1kaZvVPQlJMTO75GGEhG1YaihOCbc9woZ0zVlwCUI6yHOT9o6khntez/NOZvUupbHxv">
                    <div class="row row-sm-offset">
                        <div class="col-md-4 multi-horizontal" data-for="name">
                            <div class="form-group">
                                <label class="form-control-label mbr-black mbr-fonts-style display-7" for="name-form1-39">Nom</label>
                                <input type="text" class="form-control" name="name" data-form-field="Name" required="" id="name-form1-39">
                            </div>
                        </div>
                        <div class="col-md-4 multi-horizontal" data-for="name">
                            <div class="form-group">
                                <label class="form-control-label mbr-black mbr-fonts-style display-7" for="name-form1-39">Fonction</label>
                                <input type="text" class="form-control" name="Fonction" data-form-field="Name" required="" id="name-form1-39">
                            </div>
                        </div>
                        <div class="col-md-4 multi-horizontal" data-for="name">
                            <div class="form-group">
                                <label class="form-control-label mbr-black mbr-fonts-style display-7" for="name-form1-39">Structure</label>
                                <input type="text" class="form-control" name="Structure" data-form-field="Name" required="" id="name-form1-39">
                            </div>
                        </div>
                        <div class="col-md-4 multi-horizontal" data-for="email">
                            <div class="form-group">
                                <label class="form-control-label mbr-black mbr-fonts-style display-7" for="email-form1-39">Email</label>
                                <input type="email" class="form-control" name="email" data-form-field="Email" required="" id="email-form1-39">
                            </div>
                        </div>
                        <div class="col-md-4 multi-horizontal" data-for="phone">
                            <div class="form-group">
                                <label class="form-control-label mbr-black mbr-fonts-style display-7" for="phone-form1-39">Telephone</label>
                                <input type="tel" class="form-control" name="phone" data-form-field="Phone" id="phone-form1-39">
                            </div>
                        </div>
                    </div>
                    <div class="form-group" data-for="message">
                        <label class="form-control-label mbr-black mbr-fonts-style display-7" for="message-form1-39">Message</label>
                        <textarea type="text" class="form-control" name="message" rows="7" data-form-field="Message" id="message-form1-39"></textarea>
                    </div>

                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-primary btn-form">ECRIVEZ NOUS</button>
                    </span>
                </form>
        </div>
    </div>
</section>-->

<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "Nouvelle PSP-CI",
    "url": "http://www.npsp.ci",
    "address": "Km4, Boulvard de Marseille, BP V5",
    "sameAs": [
      "https://web.facebook.com/NouvellePSPCI/",
      "https://www.linkedin.com/company/nouvelle-psp-ci/"
    ]
  }
</script>

<?php include ("footer.php");?>

<script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/popper/popper.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smooth-scroll/smooth-scroll.js"></script>
  <script src="assets/dropdown/js/script.min.js"></script>
  <script src="assets/touch-swipe/jquery.touch-swipe.min.js"></script>
  <script src="assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js"></script>
  <script src="assets/jquery-mb-ytplayer/jquery.mb.ytplayer.min.js"></script>
  <script src="assets/jquery-mb-vimeo_player/jquery.mb.vimeo_player.js"></script>
  <script src="assets/jarallax/jarallax.min.js"></script>
  <script src="assets/theme/js/script.js"></script>
  <script src="assets/mobirise-slider-video/script.js"></script>
</body>
</html>