<?php
include ("header.php"); 
$filepath = realpath (dirname(__FILE__));

require_once($filepath."/webanalytics.php");
include "websettings.php";
$result = new publication();
$datas = $result->selectdg();


?>
<!DOCTYPE html>
<html lang="fr">
<head>
   
    <!-- Global site tag (gtag.js) - Google Analytics -->

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135621511-2"></script>

<script>

window.dataLayer = window.dataLayer || [];

function gtag(){dataLayer.push(arguments);}

gtag('js', new Date());        

gtag('config', 'UA-135621511-2');

</script>
<script src="wa.js"></script>

<!-- Primary Meta Tags -->
<title>Mot du DG Nouvelle Pharmacie de la Santé Publique de Côte d'Ivoire</title>
<meta name="title" content="Mot du DG Nouvelle Pharmacie de la Santé Publique Côte d'Ivoire">
<meta name="description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">

<!-- Open Graph / Facebook -->
<meta property="og:type" content="website">
<meta property="og:url" content="http://www.npsp.ci/">
<meta property="og:title" content="Mot du DG La Nouvelle Pharmacie de la Santé Publique de Côte d'Ivoire">
<meta property="og:description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">
<meta property="og:image" content="http://npsp.ci/assets/img/npsp-ci.png">

<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="http://www.npsp.ci/">
<meta property="twitter:title" content="La Nouvelle Pharmacie de la Santé de Publique Côte d'Ivoire">
<meta property="twitter:description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">
<meta property="twitter:image" content="http://npsp.ci/assets/img/npsp-ci.png">
<meta name="viewport" content="width=device-width, initial-scale=1">


<meta charset="UTF-8">
</head>
<body>

<br/>

<div class="barbread">
    
<div class="container">
<div class="mbr-text col-12 col-md-12 text-black  mbr-fonts-style display-7">        
                Accueil | Mot du Directeur Général
                
</div>         
                
            
    </div>

</div><br/><br/>


<section class="mbr-section article content1 cid-qpgqBVyKll" id="content1-14" data-rv-view="7376">   
<div class="container"><?php foreach ($datas as $data):; $i = 2194 ; $i>2194;?>
<h3 class="mbr-section-subtitle align-left mbr-light mbr-green mbr-fonts-style display-5"><?= $data['titre_mess'];?></h3>

<section class="mbr-section article content3 cid-qpgqEKT12W" id="content3-18" data-rv-view="7390">  

     
              <div class="row col-12 col-md-12">
                  <div class="col-12 col-md-6 pr-lg-4 mbr-text text-justify  mbr-fonts-style display-7">             
                       
                       <div class="mbr-figure" style="width: 100%;"><br/>
                        <img src="redirect/backoffice/<?= $data['path_photo']?>" alt="DG NPSP-CI">
                       </div><br/> 
                       <?= nl2br(substr($data['contenu'],0,2194)); ?><br/><br/> <br/><br/><br/>                                       
                       
                  </div>
                  <div class="col-12 col-md-6 pl-lg-4 mbr-text text-justify mbr-fonts-style display-7">
                  <?= nl2br(substr($data['contenu'], $i )); ?><br/>
                  
                  <?= $data['nom'].$data['prenoms'];?>.
                  <?php endforeach;?>
              </div>
        
  </section>
        
    </div>
</section>

<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "Nouvelle PSP-CI",
    "url": "http://www.npsp.ci",
    "address": "Km4, Boulvard de Marseille, BP V5",
    "sameAs": [
      "https://web.facebook.com/NouvellePSPCI/",
      "https://www.linkedin.com/company/nouvelle-psp-ci/"
    ]
  }
</script>

<?php include ("footer.php");?>

<script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/popper/popper.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smooth-scroll/smooth-scroll.js"></script>
  <script src="assets/dropdown/js/script.min.js"></script>
  <script src="assets/touch-swipe/jquery.touch-swipe.min.js"></script>
  <script src="assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js"></script>
  <script src="assets/jquery-mb-ytplayer/jquery.mb.ytplayer.min.js"></script>
  <script src="assets/jquery-mb-vimeo_player/jquery.mb.vimeo_player.js"></script>
  <script src="assets/jarallax/jarallax.min.js"></script>
  <script src="assets/theme/js/script.js"></script>
  <script src="assets/mobirise-slider-video/script.js"></script>
</body>
</html>
  
          
     

      
     
   
    


        

 