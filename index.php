<?php
$filepath = realpath (dirname(__FILE__));

require_once($filepath."/webanalytics.php");
include "websettings.php";
include "header.php";

$result = new publication();
$datas = $result->selectdg();
$reponses = $result->selectActu();
$docs = $result->selectArticle();
$sliders = $result->selectSlider();
$flashInfo = $result->selectFlashFront();
$videos = $result->selectVideo();
$albums = $result->selectAlbumOld();
foreach ($albums as $album)
    {
        $idAlbum = $album['idAlbum'];
    };
$photos = $result->selectPhotoAlbumOld($idAlbum);

?>
<!DOCTYPE html>

<html lang="fr">
    <head>
      <!-- fancybox CSS library -->
        <link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css">
        <!-- JS library -->
        <!-- fancybox JS library -->
        <script src="fancybox/jquery.fancybox.js"></script>
        <script src="wa.js"></script>
    </head>

<body>  
<div class="contenneur-fluid">
      
    <section class="cid-qpgoEs1f0s" id="image2-r" data-rv-view="7136">

            <!-- Slider 1 -->
            <div class="slider" id="slider1"> 
            <?php foreach ($sliders as $slider):;?>
                <!-- Slides --> 
                <div style="background-image:url(redirect/backoffice/<?= $slider['pathPhoto'] ?>)"></div>               

            <?php endforeach;?>





                
                <!-- The Arrows -->
                <i class="left" class="arrows" style="z-index:2; position:absolute;"><svg viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z"></path></svg></i>
                <i class="right" class="arrows" style="z-index:2; position:absolute;"><svg viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z" transform="translate(100, 100) rotate(180) "></path></svg></i>
                
            </div>
        <div class="barbread">    
            <div class="container">
            <div class="mbr-text col-12 col-md-12 text-black  mbr-fonts-style display-7" style="text-align:left;">        
                Accueil | Presentation <span style="float:right;"><script type="text/javascript" src="usersontxt.php?uvon=showon"></script> </span>
        </div> 
</section>      

    
        
      <section class="testimonials2 cid-qpgOWQDvAs" id="testimonials2-3w" data-rv-view="6908"> <!--mbr-additionnal.css-3951-->
      <?php foreach ($datas as $data):;?>
        <div class="container">
            <div class="media-container-row">
                <div class="mbr-figure pr-lg-5" style="width: 100%; margin-top: 5%; margin-bottom: 7%">
                  <img src="assets/img/dg1.jpg" media-simple="true" alt="DG NPSP-CI">
                </div>
                <div class="media-content px-3 align-self-center mbr-white py-2">
                        <p>
                            <h1 class="mbr-section-subtitle display-5 align-left mbr-black mbr-fonts-style"><?= $data['titre_mess'];?></h1>
                        </p>
                        <p class="mbr-text text-justify mbr-black mbr-light mbr-fonts-style display-7">
                        <?= nl2br(substr($data['contenu'], 0,310));?>... <a href="dg.php" class=""> Lire la suite</a>
                        </p>
                        <p class="mbr-author-name pt-4 mb-2 mbr-fonts-style display-7 mbr-black">
                        <?= $data['nom'].$data['prenoms'];?>.
                        </p>
                        <p class="mbr-author-desc mbr-fonts-style display-7 mbr-black">
                           Directeur Général
                        </p>
                </div>
                <?php endforeach;?>
            <div class="barPublication mbr-text" > 
            <div class="titre"><h3 class="mbr-section-subtitle align-center mbr-light mbr-white mbr-fonts-style display-5">Nos publications</h3></div>   
            <br/><br/>
                                
            <div class="media-container-row">                    
                <div class="media-content px-3 align-self-center mbr-white py-2">
                    <?php foreach ($docs as $doc):;?>
                    <p class="mbr-text testimonial-text text-black mbr-fonts-style display-7" style="border-bottom: dashed 1px #EAECEF;">
                        <a href="redirect/backoffice/<?= $doc['path_file'] ?>" target="blank">
                            <?= $doc['titre_article'] ?> 
                        </a>
                    </p> 
                    <?php endforeach?>
                    <a class="text-secondary" href="publications.php">voir tout</a> 
                </div>   
                           
            </div> <br/>              
                
                
            </div>
            </div>
            
        </div> 
    </section>
    
     
   
<section class="features3 cid-qpV9Up2ikH" id="features3-8s" data-rv-view="7440">
    <div class="container">
    <p>
        <h1 class="mbr-section-subtitle display-5 align-center mbr-black mbr-fonts-style">Actualités Nouvelle PSP Côte d'Ivoire | <a href="actu.php?id=1">Voir tout</a></h1> 
    </p>
        <div class="media-container-row">
        <?php foreach ($reponses as $reponse):$id = $reponse['id'];?>
                <div class="card p-2 col-8 col-md-6 col-lg-3">
                <div class="card-wrapper">
                    <div class="card-img">
                    <img src="redirect/backoffice/<?= $reponse['path_photo'];?>" alt="npsp-ci" media-simple="true">
                    </div>
                    <div class="card-box">
                        <h4 class="card-title pb-4 mbr-black mbr-fonts-style display-7">
                        <?= strtoupper (substr ($reponse['titre_actu'], 0,100));?>
                        </h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                        <?= substr ($reponse['contenu'], 0,100);?>
                        </p>
                    </div>
                        <div class="mbr-section-btn text-center">
                        <a href="actu.php?id=<?= $id;?>"  data-target="#content4-10<?= $id;?>" class="btn btn-primary" action="" >
                                Lire la suite
                            </a>
                        </div>
                </div>
            </div>

        <?php endforeach;?>            
        </div>
        
    </div>
</section>



<section class="mbr-gallery mbr-slider-carousel cid-qpV9Up2ikH" id="gallery1-8u" data-rv-view="7255"> 

<div class="container "><h1 class="mbr-section-subtitle display-5 align-center mbr-black mbr-fonts-style">Multimedia </h1><hr><br/>
    <div><!-- Filter --><!-- Gallery -->
        <div class="mbr-gallery-row "><h5 class="mbr-green">Galerie photo | <a href="multimedia.php?id=<?=$idAlbum?>">Voir tout</a> </h5> <p class = "text-gris">Livraisons de materiel medical pour la lutte contre le COVID-19</p>
            <div class="mbr-gallery-layout-default align-center">
                <div>
                    
                <?php foreach($photos as $photo):;?>
                    <div class="mbr-gallery-item mbr-gallery-item--pNaN" data-video-url="false" data-tags="Awesome">
                    <a href="redirect/backoffice/images/uploads/galleriePhoto/<?=$photo['path']?>" data-fancybox="group" data-caption="<?= $photo["albumId"]; ?>" >
                        <img src="redirect/backoffice/images/uploads/galleriePhoto/<?=$photo['path']?>" alt="npsp-ci" />
                    </a>
                    </div>
                <?php endforeach;?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    
</div>

<br/><br/><hr>


<section class="cid-qpV9Up2ikH" id="video3-u" data-rv-view="7144">
<?php foreach ($videos as $video):; ?>
<div class="container "><h5 class="mbr-orange">Video | <a href ="https://www.youtube.com/channel/UCw6yVdfie4gX6fHrsaqOVPA" target="_blank">Voir tout</a></h5> <p class = "text-gris"><?= $video['titreVideo']?></p>
    
    <figure class="mbr-figure align-center container">
        <div class="video-block align-center" style="width: 100%;">
            <div class="align-center"><iframe width="1203" height="480" src="<?= $video['lienVideo']?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
            
    </figure>
    <br/><br/><?php endforeach;?>
</div>
</section>

</section>

<section class="features3 cid-qpV9Up2ikH" id="features3-1i" data-rv-view="7437">
    
<h1 class="mbr-section-subtitle display-5 align-center mbr-black mbr-fonts-style">Nos partenaires</h1><hr>  

<marquee scrollamount = "3" onmouseover="this.setAttribute('scrollamount',0);" onmouseout="this.setAttribute('scrollamount',3);">  
    <div class="container">
        <div class="media-container-row">
            <div class="card p-3 col-12 col-md-6 col-lg-2">
                <div class="card-wrapper">
                    <div class="card-img">
                        <a href="https://www.theglobalfund.org/fr/" target="blank"><img src="assets/img/Partenaires/FM.jpg" alt="npsp-ci" media-simple="true"></a>
                    </div>
                    
                </div>
            </div>
        
            <div class="card p-3 col-12 col-md-6 col-lg-2">
                <div class="card-wrapper">
                    <div class="card-img">
                    <a href="https://www.afd.fr/" target="blank"><img src="assets/img/Partenaires/AFD.jpg" alt="Agence France Développement" media-simple="true"></a>
                    </div>
                    
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-2">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="assets/img/Partenaires/C2D.jpg" alt="C2D" media-simple="true">
                    </div>
                    
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-2">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="assets/img/Partenaires/EF.jpg" alt="logo Expertise France" media-simple="true">
                    </div>
                    
                </div>
            </div>
            <div class="card p-3 col-12 col-md-6 col-lg-2">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="assets/img/Partenaires/EG.jpg" alt="logo Elisabeth Glazer" media-simple="true">
                    </div>
                    
                </div>
            </div>
            <div class="card p-3 col-12 col-md-6 col-lg-2">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="assets/img/Partenaires/MSHP.jpg" alt="logo MSHP" media-simple="true">
                    </div>
                    
                </div>
            </div>
            <div class="card p-3 col-12 col-md-6 col-lg-2">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="assets/img/Partenaires/OMS.jpg" alt="logo OMS" media-simple="true">
                    </div>
                    
                </div>
            </div>
            <div class="card p-3 col-12 col-md-6 col-lg-2">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="assets/img/Partenaires/UE.jpg" alt="logo ue" media-simple="true">
                    </div>
                    
                </div>
            </div>
            <div class="card p-3 col-12 col-md-6 col-lg-2">
                <div class="card-wrapper">
                    <div class="card-img">
                    <a href="https://www.unicef.org/" target="blank"><img src="assets/img/Partenaires/unicef.jpg" alt="logo unicef" media-simple="true">
                    </div>
                    
                </div>
            </div>
            <div class="card p-3 col-12 col-md-6 col-lg-2">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="assets/img/Partenaires/parssi.jpg" alt="logo parssi" media-simple="true">
                    </div>
                    
                </div>
            </div>

            
        </div>
    </div>

            </marquee>
</section>


</div>
   

        
<br/><br/>

<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "Nouvelle PSP-CI",
    "url": "http://www.npsp.ci",
    "address": "Km4, Boulvard de Marseille, BP V5",
    "sameAs": [
      "https://web.facebook.com/NouvellePSPCI/",
      "https://www.linkedin.com/company/nouvelle-psp-ci/"
    ]
  }
</script>
        
 <?php include ("footer.php");?>

        

    

  <script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/popper/popper.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smooth-scroll/smooth-scroll.js"></script>
  <script src="assets/dropdown/js/script.min.js"></script>
  <script src="assets/touch-swipe/jquery.touch-swipe.min.js"></script>
  <script src="assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js"></script>
  <script src="assets/jquery-mb-ytplayer/jquery.mb.ytplayer.min.js"></script>
  <script src="assets/jquery-mb-vimeo_player/jquery.mb.vimeo_player.js"></script>
  <script src="assets/jarallax/jarallax.min.js"></script>
  <script src="assets/theme/js/script.js"></script>
  <script src="assets/mobirise-slider-video/script.js"></script>
  <script src="assets/theme/js/slider.js"></script>
  <script src="assets/imagesloaded/imagesloaded.pkgd.min.js"></script>
  
  
      
    
</body>
</html>
