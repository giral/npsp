<?php
require_once("class.publication.php");
require_once("dbconfig.php");

$donnees = new publication();
$flashInfo = $donnees->selectFlashFront();
foreach ($flashInfo as $flash ) {
    var_dump($flash);
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
    <link rel="stylesheet" href="assets/socicon/css/styles.css">
    <link rel="stylesheet" href="footer.css">

</head>
<body>
<section class="footer4 cid-qpgQcJjS6c" id="footer4-46" data-rv-view="6823" style="padding-right: 200px;padding-left: 200px;">

<!--style="width: 90%"  -->

    

<div class="container">
    <div class="media-container-row content mbr-white">
        <div class="col-md-3 col-sm-4">
            <div class="mb-3 img-logo">
                <a href="index.php">
                    <img src="assets/img/logo.png" alt="logo NPSP-CI" media-simple="true">
                </a>
            </div>
            <p class="mb-3 mbr-fonts-style foot-title display-7">
                Nouvelle PSP CI
            </p>
            <p class="mbr-text mbr-fonts-style display-7">
                <a href="presentation.php" class="text-white">Presentation</a>
                <br><a href="organigramme.php" class="text-white">Organisation</a>
                <br><a href="metiers.php" class="text-white">Activités</a>
                <br><a href="contact.php" class="text-white">Contacts</a>
                <br><a href="../npsp/Espace_client/veille.com/" class="text-white">La veille</a>
                <br><a class="mbr-text text-white" href="redirect/backoffice/index.php">Administration</a>

            </p>
        </div>
        <!-- <?php
        if(isset($_GET['email'])) // On vérifie que la variable $_GET['email'] existe.
        {
    
            if( !empty($_POST['email']) AND $_GET['email']==1 AND isset($_POST['new'])) /* On vérifie que la variable $_POST['email'] contient bien quelque chose, que la variable $_GET['email'] est égale à 1 et que la variable $_POST['new'] existe. */
            {
            if (preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $_POST['email'])) // On vérifie qu'on a bien rentré une adresse e-mail valide.
            {
    
                if($_POST['new']==0) // Si la variable $_POST['new'] est égale à 0, cela signifie que l'on veut s'inscrire.
                {
    
                // On définit les paramètres de l'e-mail.
                $email = $_POST['email'];
                $message = 'Pour valider votre inscription à la newsletter de npsp.ci, <a href="http://www.npsp.ci/inscription.php?tru=1&amp;email='.$email.'">cliquez ici</a>.';
    
                $destinataire = $email;
                $objet = "Inscription à la newsletter de MonSite.fr" ;
    
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From: monsite@monsite.fr' . "\r\n";
                    if ( mail($destinataire, $objet, $message, $headers) ) // On envoie l'e-mail.
                    {
    
                    echo "Pour valider votre inscription, veuillez cliquer sur le lien dans l'e-mail que nous venons de vous envoyer.";
                    }
                    else
                    {
                    echo "Il y a eu une erreur lors de l'envoi du mail pour votre inscription.";
                    }
                }
                elseif($_POST['new']==1) // Si la variable $_POST['new'] est égale à 1, cela signifie que l'on veut se désinscrire.
                {
    
                // On définit les paramètres de l'e-mail.
                $email = $_POST['email'];
                $message = 'Pour valider votre désinscription de la newsletter de npsp.ci, <a href="http://www.npsp.ci/desinscription.php?tru=1&amp;email='.$email.'">cliquez ici</a>.';
    
                $destinataire = $email;
                $objet = "Désinscription de la newsletter de npsp.ci" ;
    
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From: npsp.ci' . "\r\n";
                    if ( mail($destinataire, $objet, $message, $headers) ) 
                    {
    
                    echo "Pour valider votre désinscription, veuillez cliquer sur le lien dans l'e-mail que nous venons de vous envoyer.";
                    }
                    else
                    {
                    echo "Il y a eu une erreur lors de l'envoi du mail pour votre désinscription.";
                    }
                }
                else
                {
                echo "Il y a eu une erreur !";
                }
            }
            else
            {
            echo "Vous n\'avez pas entré une adresse e-mail valide ! Veuillez recommencer !";
            }
            }
            else
            {
            echo "Il y a eu une erreur.";
            }
        }
        else // Si les champs n'ont pas été remplis.
        {
        ?> -->
    <!-- La newsletter :
    <form method="post" action="footer.php?email=1">
    Adresse e-mail : <input type="text" name="email" size="25" /><br />
    <input type="radio" name="new" value="0" />S''inscrire
    <input type="radio" name="new" value="1" />Se désinscrire<br />
    <input type="submit" value="Envoyer" name="submit" /> <input type="reset" name="reset" value="Effacer" />
    </form> -->
    <!-- <?php
        }
    ?> -->
        <div class="col-md-8 col-sm-8">
            <p class="mb-4 foot-title mbr-fonts-style display-7">
                LIENS UTILES
            </p>
            <p class="mbr-text mbr-fonts-style display-7 ">
                
                <a class="text-white" href="http://www.inspci.org/">INSTITUT NATIONAL DE LA SANTE PUBLIQUE</a> -
                <a class="text-white" href="http://www.cnts-ci.org/ci">CENTRE NATIONAL DE TRANSFUSION SANGUINE</a> -
                <a class="text-white" href="http://www.ica.ci">INSTITUT DE CARDIOLOGIE D'ABIDJAN</a> -
                <a class="text-white" href="http://www.inhp.ci">INSTITUT NATIONAL D'HYGIENE PUBLIQUE</a>-
                <a class="text-white" href="http://pasteur.ci">INSTITUT PASTEUR COTE D'IVOIRE</a> -
                <a class="text-white" href="http://www.samu.ci">SAMU</a> -
                <a class="text-white" href="http://www.pndap-ci.org">PROGRAMME NATIONAL DE DEVELOPPEMENT DE LACTIVITE PHARMACEUTIQUE (PNDAP) </a>
            </p>
            <p class="mbr-text mbr-fonts-style display-7">
                <a class="text-white" href="http://www.dpml.ci">DIRECTION DE LA PHARMACIE ET DU MEDICAMENT (DPM) </a> -
                <a class="text-white" href="http://www.ordrepharmacien.ci">CONSEIL NATIONAL DE LORDRE DES PHARMACIENS DE COTE DIVOIRE (CNOP-CI) </a> -
                <a class="text-white" href="http://www.who.int/fr">ORGANISATION MONDIALE DE LA SANTE (OMS) </a> -
                <a class="text-white" href="http://www.synacassci.org">SYNACASS-CI </a> -
                <a class="text-white" href="http://univ-fhb.edu.ci/index.php/ufr/sciences-de-la-sante/ufr-sm">UFR SCIENCES MEDICALES </a> -
                <a class="text-white" href="http://univ-fhb.edu.ci/index.php/ufr/sciences-de-la-sante/ufr-spb">UFR SCIENCES PHARMACEUTIQUES ET BIOLOGIQUES </a> -
                <a class="text-white" href="http://www.wahooas.org/web-ooas/fr">ORGANISATION OUEST AFRICAINE DE LA SANTE (OOAS) </a> -
                <a class="text-white" href="http://www.medecins.ci">ORDRE NATIONAL DES MEDECINS </a> -
                <a class="text-white" href="http://www.ordrechirurgiens-dentistes.ci/">ORDRE NATIONAL DES CHIRURGIENS-DENTISTES DE CÔTE D'IVOIRE
            </p>
        </div>
        <div class="col-md-4 offset-md-1 col-sm-12"hidden="" data-form-alert-success="true">
       
            <p class="mbr-text mbr-fonts-style display-7">
               Newsletter
            </p>
            <div class="mb-5" data-form-type="formoid" hidden="" data-form-alert-success="true">
                <div data-form-alert="true">
                    <div hidden="" data-form-alert-success="true"></div>
                </div>
                <form class="mbr-form" action="newsletters.php" method="post">
                    
                    <div class="mbr-subscribe mbr-subscribe-dark input-group">
                        <input type="email" class="form-control" name="email" required="" data-form-field="Email" id="email-footer4-46">
                        <span class="input-group-btn mx-2">
                        <button type="submit" name="submit" class="btn btn-sm btn-primary">S'inscrire</button>
                        </span>
                    </div>
                </form>
            </div>
            <p class="mb-4 mbr-fonts-style foot-title display-7" hidden="" data-form-alert-success="true">
                Suivez nous
            </p>
            <div class="social-list pl-0 mb-0" hidden="" data-form-alert-success="true">
                   
                    <div class="soc-item" >
                        <a href="https://www.facebook.com/NouvellePSPCI/?ref=br_rs" target="_blank">
                            <span class="socicon-facebook socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>
                        </a>
                    </div>
                    <div class="soc-item">
                        <a href="#" target="_blank">
                            <span class="socicon-twitter socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>
                        </a>
                    </div>
                    <div class="soc-item">
                        <a href="https://www.youtube.com/channel/UCw6yVdfie4gX6fHrsaqOVPA/" target="_blank">
                            <span class="socicon-youtube socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>
                        </a>
                    </div>
                    <div class="soc-item">
                        <a href="#" target="_blank">
                            <span class="socicon-instagram socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>
                        </a>
                    </div>
                    <div class="soc-item">
                        <a href="#" target="_blank">
                            <span class="socicon-googleplus socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>
                        </a>
                    </div>
            </div>
        </div>
        <div class="col-md-4 offset-md-1 col-sm-12">
            
            <p class="mbr-text mbr-fonts-style display-7">
               Newsletter
            </p>
            <div class="mb-5" data-form-type="formoid">
                <div data-form-alert="true">
                    <div hidden="" data-form-alert-success="true"></div>
                </div>
                <form class="mbr-form" action="newsletters.php" method="post" data-form-type="formoid" data-form-title="UNTITLED FORM">
                    <input type="hidden" value="" data-form-email="true">
                    <div class="mbr-subscribe mbr-subscribe-dark input-group">
                        <input type="email" class="form-control" name="email" required="" data-form-field="Email" id="email-footer4-46">
                        <span class="input-group-btn mx-2">
                        <button type="submit" class="btn btn-sm btn-primary">S'inscrire</button>
                        </span>
                    </div>
                    
                </form>
            </div>
            <p class="mb-4 mbr-fonts-style foot-title display-7">
                Suivez nous
            </p>
            <div class="social-list pl-0 mb-0">
                   
                    <div class="soc-item">
                        <a href="https://www.facebook.com/NouvellePSPCI/?ref=br_rs" target="_blank">
                            <span class="socicon-facebook socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>
                        </a>
                    </div>
                    <div class="soc-item">
                        <a href="#" target="_blank">
                            <span class="socicon-twitter socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>
                        </a>
                    </div>
                    <div class="soc-item">
                        <a href="https://www.youtube.com/channel/UCw6yVdfie4gX6fHrsaqOVPA/" target="_blank">
                            <span class="socicon-youtube socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>
                        </a>
                    </div>
                    <div class="soc-item">
                        <a href="#" target="_blank">
                            <span class="socicon-instagram socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>
                        </a>
                    </div>
                    <div class="soc-item">
                        <a href="#" target="_blank">
                            <span class="socicon-googleplus socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>
                        </a>
                    </div>
            </div>
        </div>
    </div>
    <div class="footer-lower">
        <div class="media-container-row">
            <div class="col-sm-12">
                <hr>
            </div>
        </div>
        <div class="media-container-row mbr-white">
            <div class="col-sm-12 copyright">
                <p class="mbr-text mbr-fonts-style display-7">
                    © Copyright 2019 Nouvelle PSP CI - All Rights Reserved
                </p>
            </div>
        </div>
    </div>
</div>
</section>




<div class="footerfixed">
    <div class="footerfixedTitre"><h2>INFO</h2></div>
    
    
    <div id="carousel">
        <div class="btn-bar">
            <div id="buttons"><a id="prev" href="#"></a><a id="next" href="#"></a> </div></div>
                <div id="slides">
                    <ul><?php foreach ($flashInfo as $flash):;?>
                        <li class="slide">
                            <div class="quoteContainer">
                                <p class="quote-phrase"><span class="quote-marks"></span> <?= $flash['contenu']?></span>

                                </p>
                            </div>
                        </li><?php endforeach;?>
                    
                    </ul>
                 </div>
        </div>
    </div>

    
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script>
        $(document).ready(function () {
    //rotation speed and timer
    var speed = 5000;
    
    var run = setInterval(rotate, speed);
    var slides = $('.slide');
    var container = $('#slides ul');
    var elm = container.find(':first-child').prop("tagName");
    var item_width = container.width();
    var previous = 'prev'; //id of previous button
    var next = 'next'; //id of next button
    slides.width(item_width); //set the slides to the correct pixel width
    container.parent().width(item_width);
    container.width(slides.length * item_width); //set the slides container to the correct total width
    container.find(elm + ':first').before(container.find(elm + ':last'));
    resetSlides();
    
    
    //if user clicked on prev button
    
    $('#buttons a').click(function (e) {
        //slide the item
        
        if (container.is(':animated')) {
            return false;
        }
        if (e.target.id == previous) {
            container.stop().animate({
                'left': 0
            }, 1500, function () {
                container.find(elm + ':first').before(container.find(elm + ':last'));
                resetSlides();
            });
        }
        
        if (e.target.id == next) {
            container.stop().animate({
                'left': item_width * -2
            }, 1500, function () {
                container.find(elm + ':last').after(container.find(elm + ':first'));
                resetSlides();
            });
        }
        
        //cancel the link behavior            
        return false;
        
    });
    
    //if mouse hover, pause the auto rotation, otherwise rotate it    
    container.parent().mouseenter(function () {
        clearInterval(run);
    }).mouseleave(function () {
        run = setInterval(rotate, speed);
    });
    
    
    function resetSlides() {
        //and adjust the container so current is in the frame
        container.css({
            'left': -1 * item_width
        });
    }
    
});
//a simple function to click next link
//a timer will call this function, and the rotation will begin

function rotate() {
    $('#next').click();
}
    </script>
</body>
</html>