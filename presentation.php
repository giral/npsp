<?php 
header('Content-Type: text/html; charset=utf-8');
include ("header.php");
$filepath = realpath (dirname(__FILE__));

require_once($filepath."/webanalytics.php");
include "websettings.php";
$page = $_GET['page'];
$result = new publication();
$datas = $result->selectPresentation($page);
$active =  "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
var_dump($titre); 
?>

<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8" />
<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135621511-2"></script>

<script>

window.dataLayer = window.dataLayer || [];

function gtag(){dataLayer.push(arguments);}

gtag('js', new Date());        

gtag('config', 'UA-135621511-2');

</script>


<!-- Primary Meta Tags -->
<title>La Nouvelle Pharmacie de la Santé Publique de Côte d'Ivoire</title>
<script src="wa.js"></script>
<meta name="title" content="Nouvelle Pharmacie de la Santé Publique Côte d'Ivoire">
<meta name="description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">

<!-- Open Graph / Facebook -->
<meta property="og:type" content="website">
<meta property="og:url" content="http://www.npsp.ci/">
<meta property="og:title" content="La Nouvelle Pharmacie de la Santé Publique de Côte d'Ivoire">
<meta property="og:description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">
<meta property="og:image" content="http://npsp.ci/assets/img/npsp-ci.png">

<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="http://www.npsp.ci/">
<meta property="twitter:title" content="La Nouvelle Pharmacie de la Santé de Publique Côte d'Ivoire">
<meta property="twitter:description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">
<meta property="twitter:image" content="http://npsp.ci/assets/img/npsp-ci.png">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>


<body>
<?php foreach ($datas as $data):; 
    if($data['couverture'] = "images/uploads/")
        {
            $bg = "linear-gradient(45deg, #f07502, #f56606)";
        } else 
        {
            $bg = 'url(redirect/backoffice/'.$data['couverture'].')';
        }
?>
<section class="mbr-section content5 cid-qpgqzWVZli mbr-parallax-background" id="content5-z" data-rv-view="7367" style="background:<?=$bg?> !important;"><!--mbr.additional.css--11757-->
    <div class="container">
        <div class="media-container-row">
            <div class="title col-12 col-md-10">
                <h2 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-1">
                    QUI SOMMES-NOUS?
                </h2>
            </div>
        </div>
    </div>
</section>

<div class="barbread">    
    <div class="container">
        <div class="mbr-text col-12 col-md-12 text-black  mbr-fonts-style display-7">        
            <a class="mbr-black" href="index.php">Accueil </a>| Presentation |<?=$data['page']?>             
        </div>               
    </div>
</div>
<br/><br/>


<section class="mbr-section article content1 cid-qpgqBVyKll" id="content1-14" data-rv-view="7376">    
<div class="container">

<h3 class="mbr-section-subtitle align-left mbr-light text-orange mbr-fonts-style display-5"><?= $data['titre_pre']?></h3><br/><br/>
        
                <div class="mbr-text col-12 col-md-12 text-black text-justify mbr-light  mbr-fonts-style display-7">
                <strong> <?= $data['page']?></strong> <br/> <br/> 

                <?= nl2br($data['contenu_pre'])?>

                <div class="media-container-row">
                <div class="mbr-figure" style="width: 100%;"><br/><br/>
                    <img src="redirect/backoffice/<?=$data['path_photo']?>" media-simple="true" alt="npsp-ci">
                </div>
                </div>

                 </div>
  
    </div>
</section>
<?php endforeach; ?>  

<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "Nouvelle PSP-CI",
    "url": "http://www.npsp.ci",
    "address": "Km4, Boulvard de Marseille, BP V5",
    "sameAs": [
      "https://web.facebook.com/NouvellePSPCI/",
      "https://www.linkedin.com/company/nouvelle-psp-ci/"
    ]
  }
</script>

<?php include ("footer.php");?>

<script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/popper/popper.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smooth-scroll/smooth-scroll.js"></script>
  <script src="assets/dropdown/js/script.min.js"></script>
  <script src="assets/touch-swipe/jquery.touch-swipe.min.js"></script>
  <script src="assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js"></script>
  <script src="assets/jquery-mb-ytplayer/jquery.mb.ytplayer.min.js"></script>
  <script src="assets/jquery-mb-vimeo_player/jquery.mb.vimeo_player.js"></script>
  <script src="assets/jarallax/jarallax.min.js"></script>
  <script src="assets/theme/js/script.js"></script>
  <script src="assets/mobirise-slider-video/script.js"></script>
</body>
</html>