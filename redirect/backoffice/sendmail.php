<?php
/*
 * DEVELOPED  BY BNS GROUP
 * DIRECTED BY ABOUBACAR OUATTARA
 */
session_start();
require('includes/config.php');
require('includes/init.php');
require_once("libs/JBBcode/Parser.php");
if (isset($_POST['contents'], $_POST['subject'])) {
    extract($_POST);
    if (!empty($contents) and !empty($subject)) {
        $parser = new JBBCode\Parser();
        $parser->addCodeDefinitionSet(new JBBCode\DefaultCodeDefinitionSet());
        $parser->addBBCode("quote", '<div class="quote">{param}</div>');
        $parser->addBBCode("code", '<pre class="code">{param}</pre>', false, false, 1);
        $parser->addBBCode("tr", '<tr style="font-family: Calibri">{param}</tr>');
        $parser->addBBCode("td", "<td style='font-family: \"Lato\",\"Helvetica Neue\",Helvetica,Arial,sans-serif;font-size: 12px; border:1px solid #0f0f0f; padding: 10px'>{param}</td>");
        $parser->addBBCode("table", '<table style="font-family: Calibri; border:1px solid #faebcc ;">{param}</table>');



        //$content = utf8_encode($content);
       // $content = str_replace('', , $subject_output)
         $parser->parse($contents);


        $q = $db->prepare('SELECT Email FROM emails WHERE status = :status');
        $q->execute([
            'status' => '1'
        ]);
        $mails = $q->fetchAll(PDO::FETCH_OBJ);
        if ($mails) {
            foreach ($mails as $mail) {
                //Envoi du message
                $to = $mail->Email;
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'content-type:text/html; charset=iso-8859-1' . "\r\n";
                mail($to, $subject, $parser->getAsHtml(), $headers);
            }

            set_flash(" Email envoyé aux  membres de la liste d'envoie", "success");
            redirect("npsp_newsletter.php");
        } else {
            set_flash("Vous devez d'abord ajouter au moins un email avant de pouvoir envoyé un Email", "danger");

        }
    } else {
        set_flash("Veuillez Remplir tous les champs avant de continuer", "danger");
        redirect('npsp_newsletter.php');
    }
} else {
    redirect('npsp_newsletter.php');

}