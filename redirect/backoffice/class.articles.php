<?php

require_once('dbconfig.php');

class ARTICLES{
 
    private $conn;
	
	public function __construct()
	{
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
    }
	
	
	public function runQuery($sql)
	{
		$stmt = $this->conn->prepare($sql);
		return $stmt;
	}
	
    public function redirect($url)
	{
		header("Location: $url");
	}

	public function insertionArt($titre,$description,$file,$pub,$acc,$target)
	{
		try
		{
			
			$stmt = $this->conn->prepare("INSERT INTO article(id,titre_article, description, path_file,publier,accueil, date_publication, target) 
													   VALUES (NULL, :titre_article, :description, :file, :publier, :accueil, NOW(), :target)");
													  
			$stmt->bindParam(':titre_article', $titre);
			$stmt->bindParam(':description', $description);
            $stmt->bindParam(':file', $file);
			$stmt->bindParam(':publier', $pub);
			$stmt->bindParam(':accueil', $acc);
			$stmt->bindParam(':target', $target);

																		  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}

	public function updateArticle($id,$titre,$description,$pub,$acc)
	{
		try
		{
			
			$stmt = $this->conn->prepare("UPDATE article SET titre_article = :titre_article , description = :description , publier = :publier , accueil = :accueil
											WHERE id = :id");
			$stmt->bindParam(':id', $id);
			$stmt->bindParam(':titre_article', $titre);
			$stmt->bindParam(':description', $description);
			$stmt->bindParam(':publier', $pub);
			$stmt->bindParam(':accueil', $acc);
																		  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}

	public function selectArticle()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM article WHERE target = "front"');
            $req->execute();
            $donnees = $req->fetchAll();

            return $donnees;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

	public function selectArticleBack()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM article WHERE target = "back"');
            $req->execute();
            $donnees = $req->fetchAll();

            return $donnees;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}



	public function disableArticle($id)
	{
		try
		{
			
			$stmt = $this->conn->prepare("UPDATE article SET publier = 'non' WHERE id = :id");
			$stmt->bindParam(':id', $id);																		  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
	
	
	
	public function insertdg( $nom, $prenoms, $titre,$contenu,$path_photo)
	{
		try
		{
			
			$stmt = $this->conn->prepare("INSERT INTO dg(id_dg,nom,prenoms,titre_mess,contenu, path_photo) 
													   VALUES (NULL, :nom, :prenoms, :titre, :contenu, :photo)");
													  
			$stmt->bindParam(':nom', $nom);
            $stmt->bindParam(':prenoms', $prenoms);
			$stmt->bindParam(':titre', $titre);
			$stmt->bindParam(':contenu', $contenu);
			$stmt->bindParam(':photo', $path_photo);
																		  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}

	public function updatedg( $id, $titre, $contenu)
	{
		try
		{
			
			$stmt = $this->conn->prepare("UPDATE dg  SET titre_mess = :titre, contenu = :contenu
											WHERE id_dg = :id");
													  
			
			$stmt->bindParam(':titre', $titre);
			$stmt->bindParam(':contenu', $contenu);
			$stmt->bindParam(':id', $id);
																		  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}
	
	public function insertProduitPromo($promoId, $cProd)
	{
		try
		{
			
			$stmt = $this->conn->prepare("INSERT INTO produits_promo(promoId, cProd) VALUES (:promoId, :cProd)");
			$stmt->bindParam(':promoId', $promoId);
			$stmt->bindParam(':cProd', $cProd);
			$stmt->execute();
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}			

	}

	//SLIDER--------------------------------------------------------------------------------------------

	public function insertSlider($pathPhoto, $activer)
	{
		try
		{
			
			$stmt = $this->conn->prepare("INSERT INTO slider(id_slider, pathPhoto, dateAjout, activer ) 
													   VALUES (NULL, :photo, NOW(), :activer)");
													  
			$stmt->bindParam(':photo', $pathPhoto);
            $stmt->bindParam(':activer', $activer);																		  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}
	
	public function selectSlider()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM slider ');
            $req->execute();
            $donnees = $req->fetchAll();

            return $donnees;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

	public function updateSlider($activer, $idSlider)
    {
        try
        {
			$stmt = $this->conn->prepare('UPDATE slider SET activer = :activer WHERE id_slider = :idSlider');
			$stmt->bindParam(':activer', $activer);
			$stmt->bindParam(':idSlider', $idSlider);

			$stmt->execute();
			
			
			return $stmt;       

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

	public function deleteSlider($idSlider)
    {
        try
        {
			$stmt = $this->conn->prepare('DELETE FROM slider WHERE id_slider = :idSlider ');
			$stmt->bindParam(':idSlider', $idSlider);

			$stmt->execute();
			
			
			return $stmt;       

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
//END SLIDER------------------------------------------------------------------------------------------------------------------


//VIDEOS--------------------------------------------------------------------------------------------

public function insertVideo($titre, $lien, $publier , $accueil)
{
	try
	{
		
		$stmt = $this->conn->prepare("INSERT INTO videos(idVideo, titreVideo, lienVideo, publier, accueil, dateVideo ) 
												   VALUES (NULL, :titreVideo, :lienVideo, :publier, :accueil, NOW())");
												  
		$stmt->bindParam(':titreVideo', $titre);
		$stmt->bindParam(':lienVideo', $lien);	
		$stmt->bindParam(':publier', $publier);																		  
		$stmt->bindParam(':accueil', $accueil);																		  
																	  
			
		$stmt->execute();	
		
		return $stmt;	
	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
	}				
}

public function selectVideo()
{
	try
	{
		$req = $this->conn->prepare('SELECT * FROM videos ');
		$req->execute();
		$donnees = $req->fetchAll();

		return $donnees;         

	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
	}
}

public function updateVideo($titre,$lien,$publier,$accueil,$id)
{
	try
	{
		$stmt = $this->conn->prepare("UPDATE videos SET titreVideo = :titreVideo, lienVideo = :lienVideo, publier = :publier, accueil = :accueil 
										 WHERE idVideo = :id");
		$stmt->bindParam(':titreVideo', $titre);
		$stmt->bindParam(':lienVideo', $lien);	
		$stmt->bindParam(':publier', $publier);																		  
		$stmt->bindParam(':accueil', $accueil);
		$stmt->bindParam(':id', $id);


		$stmt->execute();
		
		
		return $stmt;       

	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
	}
}

public function disableVideo($id)
{
	try
	{
		$stmt = $this->conn->prepare('UPDATE videos SET publier = "non", accueil ="non" WHERE idVideo = :id ');
		$stmt->bindParam(':id', $id);

		$stmt->execute();
		
		
		return $stmt;       

	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
	}
}

//END VIDEOS------------------------------------------------------------------------------------------------------------------

//PROMOTIONS--------------------------------------------------------------------------------------------

public function insertPromo($libelle, $taux, $debut , $fin, $info)
{
	try
	{
		
		$stmt = $this->conn->prepare("INSERT INTO promotions(idPromotion, libellePromotion,	tauxReduction, debutPromo,	finPromo, infoPromo) 
												   VALUES (NULL, :libellePromo, :tauxReduc, :debutPromo, :finPromo, :infoPromo)");
												  
		$stmt->bindParam(':libellePromo', $libelle);
		$stmt->bindParam(':tauxReduc', $taux);	
		$stmt->bindParam(':debutPromo', $debut);																		  
		$stmt->bindParam(':finPromo', $fin);
		$stmt->bindParam(':infoPromo', $info);																		  
																		  
																	  
			
		$stmt->execute();	
		
		return $stmt;	
	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
	}				
}

public function selectPromo()
{
	try
	{
		$req = $this->conn->prepare('SELECT * FROM promotions ');
		$req->execute();
		$donnees = $req->fetchAll();

		return $donnees;         

	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
	}
}

public function updatePromo($libelle, $taux, $debut , $fin, $info, $id)
{
	try
	{
		$stmt = $this->conn->prepare("UPDATE promotions SET libellePromotion = :libellePromo,	tauxReduction = :tauxReduc, debutPromo = :debutPromo,	finPromo = :finPromo, infoPromo = :infoPromo 
										 WHERE idPromotion = :id");
		$stmt->bindParam(':libellePromo', $libelle);
		$stmt->bindParam(':tauxReduc', $taux);	
		$stmt->bindParam(':debutPromo', $debut);																		  
		$stmt->bindParam(':finPromo', $fin);
		$stmt->bindParam(':infoPromo', $info);
		$stmt->bindParam(':id', $id);	
	


		$stmt->execute();
		
		
		return $stmt;       

	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
	}
}

public function disablePromo($id)
{
	try
	{
		$stmt = $this->conn->prepare('UPDATE promotions SET statut ="0", finPromo = now() WHERE idPromotion = :id ');
		$stmt->bindParam(':id', $id);

		$stmt->execute();
		
		
		return $stmt;       

	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
	}
}

//END PROMOTIONS------------------------------------------------------------------------------------------------------------------

//ALBUM--------------------------------------------------------------------------------------------

public function createAlbum($nomAlbum, $publier)
{
	try
	{
		
		$stmt = $this->conn->prepare("INSERT INTO albums ( idAlbum, nomAlbum, publier, dateCreationAlbum ) 
												   VALUES (NULL, :nomAlbum,  :publier, NOW())");
												  
		$stmt->bindParam(':nomAlbum', $nomAlbum);
		$stmt->bindParam(':publier', $publier);	
																	  
																	  
			
		$stmt->execute();	
		
		return $stmt;	
	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
	}				
}

public function selectAlbum()
{
	try
	{
		$req = $this->conn->prepare('SELECT * FROM albums ');
		$req->execute();
		$resultat = $req->fetchAll();

		return $resultat;         

	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
	}
}

public function updateAlbum($nomAlbum, $publier, $idAlbum)
{
	try
	{
		$stmt = $this->conn->prepare('UPDATE albums SET nomAlbum = :nomAlbum, publier = :publier WHERE idAlbum = :idAlbum');
		$stmt->bindParam(':nomAlbum', $nomAlbum);
		$stmt->bindParam(':publier', $publier);
		$stmt->bindParam(':idAlbum', $idAlbum);


		$stmt->execute();
		
		
		return $stmt;       

	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
	}
}

public function deleteAlbum($idSlider)
{
	try
	{
		$stmt = $this->conn->prepare('DELETE FROM slider WHERE id_slider = :idSlider ');
		$stmt->bindParam(':idSlider', $idSlider);

		$stmt->execute();
		
		
		return $stmt;       

	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
	}
}

//END ALBUM------------------------------------------------------------------------------------------------------------------


//GALLERIE PHOTOS--------------------------------------------------------------------------------------------

public function insertGalleriePhoto($file_name, $albumId)
{
	try
	{
		
		$stmt = $this->conn->prepare("INSERT INTO photos(idPhoto, path, albumId ) 
												   VALUES (NULL, :path, :albumId)");
												  
		$stmt->bindParam(':path', $file_name);
		$stmt->bindParam(':albumId', $albumId);																		  
			
		$stmt->execute();	
		
		return $stmt;	
	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
	}				
}

public function selectGalleriePhoto()
{
	try
	{
		$req = $this->conn->prepare('SELECT * FROM slider ');
		$req->execute();
		$donnees = $req->fetchAll();

		return $donnees;         

	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
	}
}

public function updateGalleriePhoto($activer, $idSlider)
{
	try
	{
		$stmt = $this->conn->prepare('UPDATE slider SET activer = :activer WHERE id_slider = :idSlider');
		$stmt->bindParam(':activer', $activer);
		$stmt->bindParam(':idSlider', $idSlider);

		$stmt->execute();
		
		
		return $stmt;       

	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
	}
}

public function deleteGalleriePhoto($idSlider)
{
	try
	{
		$stmt = $this->conn->prepare('DELETE FROM slider WHERE id_slider = :idSlider ');
		$stmt->bindParam(':idSlider', $idSlider);

		$stmt->execute();
		
		
		return $stmt;       

	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
	}
}

//END GALLERIE------------------------------------------------------------------------------------------------------------------


//ACTUALITES------------------------------------------------------------------------------------------------------------------
public function selectActu()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM actualites');
            $req->execute();
            $resultat = $req->fetchAll();

            return $resultat;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

public function insertionActu($titre,$contenu,$photo,$pub,$acc)
	{
		try
		{
			
			$stmt = $this->conn->prepare("INSERT INTO actualites(id,titre_actu,contenu,path_photo,publier,accueil, date_publication) 
													   VALUES (NULL, :titre_actu, :contenu, :path_photo, :publier, :accueil, NOW())");
													  
			$stmt->bindParam(':titre_actu', $titre);
            $stmt->bindParam(':contenu', $contenu);
			$stmt->bindParam(':path_photo', $photo);
			$stmt->bindParam(':publier', $pub);
			$stmt->bindParam(':accueil', $acc);
																		  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}

	public function updateActu($id,$titre,$contenu,$photo,$pub,$acc)
	{
		try
		{
			
			$stmt = $this->conn->prepare("UPDATE actualites SET titre_actu = :titre_actu, contenu = :contenu, path_photo = :path_photo, publier = :publier, accueil = :accueil, date_publication = NOW()
											WHERE id = :id"); 
													   
			$stmt->bindParam(':id', $id);										  
			$stmt->bindParam(':titre_actu', $titre);
            $stmt->bindParam(':contenu', $contenu);
			$stmt->bindParam(':path_photo', $photo);
			$stmt->bindParam(':publier', $pub);
			$stmt->bindParam(':accueil', $acc);
																		  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}

	public function deleteActu($id)
	{
		try
		{
			
			$stmt = $this->conn->prepare("DELETE FROM actualites WHERE 	id = :id");													  
			$stmt->bindParam(':id', $id);           
																		  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}

//END ACTUALITES--------------------------------------------------------------------------------------------------------------

//PRESENTATION----------------------------------------------------------------------------------------------------------------

public function selectPresentation()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM presentation');
            $req->execute();
            $resultat = $req->fetchAll();

            return $resultat;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

	public function insertPresentation($page,$titre_pre,$contenu_pre,$photo_pre, $cover_pre)
	{
		try
		{
			
			$stmt = $this->conn->prepare("INSERT INTO presentation(id_pre,page, titre_pre,contenu_pre,path_photo,couverture, date_publication) 
													   VALUES (NULL, :page, :titre_pre, :contenu_pre, :path_photo, :couverture,  NOW())");
			
			$stmt->bindParam(':page', $page);
			$stmt->bindParam(':titre_pre', $titre_pre);
            $stmt->bindParam(':contenu_pre', $contenu_pre );
			$stmt->bindParam(':path_photo', $photo_pre);
			$stmt->bindParam(':couverture', $cover_pre);
															  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}


	public function updatePresentation($idPresent,$page,$titre_pre,$contenu_pre,$photo_pre, $cover_pre) 
	{
		try {
			$stmt = $this->conn->prepare("UPDATE presentation SET page = :page , titre_pre = :titre_pre, contenu_pre = :contenu_pre, path_photo = :path_photo, couverture = :couverture,  date_publication = NOW()
											  WHERE id_pre = :id_pre");
			$stmt->bindParam(':id_pre',$idPresent);
			$stmt->bindParam(':page',$page);
			$stmt->bindParam(':titre_pre',$titre_pre);
			$stmt->bindParam(':contenu_pre',$contenu_pre);
			$stmt->bindParam(':path_photo',$photo_pre);
			$stmt->bindParam(':couverture', $cover_pre);


			$stmt->execute();

			return $stmt;
		} 
		catch (PDOException $e) 
		
		{
			echo $e->getMessage();
		}
	}

	public function deletePresentation($id)
	{
		try
		{
			
			$stmt = $this->conn->prepare("DELETE FROM presentation WHERE id_pre = :id");													  
			$stmt->bindParam(':id', $id);           
																		  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}

//END PRESENTATION------------------------------------------------------------------------------------------------------------

//PRESENTATION----------------------------------------------------------------------------------------------------------------

	public function selectOrganisation()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM organisation');
            $req->execute();
            $datas = $req->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	

	public function insertOrganisation($page_org,$titre_org,$contenu_org,$photo_org, $cover_org)
	{
		try
		{
			
			$stmt = $this->conn->prepare("INSERT INTO organisation(id_org, page, titre_org, contenu_org, path_photo, couverture, date_publication) 
													   VALUES (NULL, :page, :titre_org, :contenu_org, :path_photo, :couverture,  NOW())");
			
			$stmt->bindParam(':page', $page_org);
			$stmt->bindParam(':titre_org', $titre_org);
            $stmt->bindParam(':contenu_org', $contenu_org );
			$stmt->bindParam(':path_photo', $photo_org);
			$stmt->bindParam(':couverture', $cover_org);
															  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
    }


	public function updateOrganisation($idOrg,$page_org,$titre_org,$contenu_org,$photo_org, $cover_org) 
	{
		try {
			$stmt = $this->conn->prepare("UPDATE organisation SET page = :page , titre_org = :titre_org, contenu_org = :contenu_org, path_photo = :path_photo, couverture = :couverture
											  WHERE id_org = :id_org");
			$stmt->bindParam(':id_org',$idOrg);
			$stmt->bindParam(':page', $page_org);
			$stmt->bindParam(':titre_org', $titre_org);
            $stmt->bindParam(':contenu_org', $contenu_org);
			$stmt->bindParam(':path_photo', $photo_org);
			$stmt->bindParam(':couverture', $cover_org);


			$stmt->execute();

			return $stmt;
		} 
		catch (PDOException $e) 
		
		{
			echo $e->getMessage();
		}
	}

	public function deleteOrganisation($id)
	{
		try
		{
			
			$stmt = $this->conn->prepare("DELETE FROM organisation WHERE id_pre = :id");													  
			$stmt->bindParam(':id', $id);           
																		  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}

//END PRESENTATION------------------------------------------------------------------------------------------------------------

//MENU ACTIVITES--------------------------------------------------------------------------------------------------------------

public function selectActivites()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM activites');
            $req->execute();
            $resultat = $req->fetchAll();

            return $resultat;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

	public function insertActivites($page_act,$titre_act,$contenu_act,$photo_act,$cover_act)
	{
		try
		{
			
			$stmt = $this->conn->prepare("INSERT INTO activites(id_act,page, titre_act, contenu_act, path_photo, couverture, date_publication) 
													   VALUES (NULL, :page, :titre_act, :contenu_act, :path_photo, :couverture,  NOW())");
			
			$stmt->bindParam(':page', $page_act);
			$stmt->bindParam(':titre_act', $titre_act);
            $stmt->bindParam(':contenu_act', $contenu_act);
			$stmt->bindParam(':path_photo', $photo_act);
			$stmt->bindParam(':couverture', $cover_act);

															  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}

	public function updateActivites($id_act,$page_act,$titre_act,$contenu_act,$photo_act,$cover_act) 
	{
		try {
			$stmt = $this->conn->prepare("UPDATE activites SET page = :page , titre_act = :titre_act, contenu_act = :contenu_act, path_photo = :path_photo, couverture = :couverture
											  WHERE id_act = :id_act");
			$stmt->bindParam(':id_act',$id_act);
			$stmt->bindParam(':page',$page_act);
			$stmt->bindParam(':titre_act',$titre_act);
			$stmt->bindParam(':contenu_act',$contenu_act);
			$stmt->bindParam(':path_photo',$photo_act);
			$stmt->bindParam(':couverture', $cover_act);


			$stmt->execute();

			return $stmt;
		} 
		catch (PDOException $e) 
		
		{
			echo $e->getMessage();
		}
	}

	public function disableActivites($id_act) 
	{
		try {
			$stmt = $this->conn->prepare("UPDATE activites SET activer = 'non'
											  WHERE id_act = :id_act");
			$stmt->bindParam(':id_act',$id_act);

			$stmt->execute();

			return $stmt;
		} 
		catch (PDOException $e) 
		
		{
			echo $e->getMessage();
		}
	}
//END ACTVITES----------------------------------------------------------------------------------------------------------------

	public function insertFlash($contenu, $addBy, $publier, $target)
	{
		try
		{
			
			$stmt = $this->conn->prepare("INSERT INTO flashinfo( id, contenu, date, add_by, publier, target ) 
													   VALUES (NULL, :contenu, NOW(), :add_by, :publier, :target)");
													  
			$stmt->bindParam(':contenu', $contenu);
			$stmt->bindParam(':add_by', $addBy);
			$stmt->bindParam(':publier', $publier);	
			$stmt->bindParam(':target', $target);																		  
																	  
																		  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}
	
	public function selectFlash()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM flashinfo WHERE target = "back"');
            $req->execute();
            $donnees = $req->fetchAll();

            return $donnees;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

	public function selectFlashFront()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM flashinfo WHERE target = "front"');
            $req->execute();
            $donnees = $req->fetchAll();

            return $donnees;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

	public function updateFlash($id, $contenu, $publier)
    {
        try
        {
			$stmt = $this->conn->prepare("UPDATE flashinfo SET contenu = :contenu ,  publier = :publier WHERE id = :id");
			$stmt->bindParam(':id', $id);
			$stmt->bindParam(':contenu', $contenu);
			$stmt->bindParam(':publier', $publier);


            $stmt->execute();

            return $stmt;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

	public function deleteFlash($id)
    {
        try
        {
			$stmt = $this->conn->prepare('DELETE FROM flashinfo WHERE id = :id ');
			$stmt->bindParam(':id', $id);

			$stmt->execute();
			
			
			return $stmt;       

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}


	
	


	public function selectdg()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM dg');
            $req->execute();
            $datas = $req->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function selectB64()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM testB64');
            $req->execute();
            $datas = $req->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

    public function update()
    {
        try
        {
            $up = $this->conn->prepare('UPDATE `articles` SET `titre_article` = TitreNews1 WHERE `articles`.`id` = 2;');
        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }



    public function getList()
  {
    $list = array();
    
    $q = $this->conn->prepare('SELECT  id, titre_article, contenu, path_photo, publier,	accueil, date_publication FROM articles ');
    $q->execute(array());
    
    while ($donnees = $q->fetch(PDO::FETCH_ASSOC))
    {
      $list[] = new ARTICLES($donnees);
    }
    
    return $list;
  }


}
?>