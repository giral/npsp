<?php
require_once("header3.php");

$filepath = realpath (dirname(__FILE__));

require_once($filepath."./../../webanalytics.php");
include "./../../websettings.php";

// if user is logged in and this user is NOT an super-admin user, redirect them to landing page
if (isset($_SESSION['user_session']) && is_null($_SESSION['user_session'].''.$userRow['role_id'])) {
  header("location: " . "actualites.php");
}

$resultats = new CLIENTS();
$clients=$resultats-> getClientByCommercial();
$commerciale = $resultats->getCommerciale();
$projets = $resultats->getProjet();

//var_dump($projets);



if(isset($_POST["submit"]))
{
 if($_FILES['file']['name'])
 {
  $filename = explode(".", $_FILES['file']['name']);
  if($filename[1] == 'csv')
  {
   $handle = fopen($_FILES['file']['tmp_name'], "r");
   while($data = fgetcsv($handle))
   {
                $item1 = mysqli_real_escape_string($data[0]);  
                $item2 = mysqli_real_escape_string($data[1]);
                $item3 = mysqli_real_escape_string($data[2]);
                $item4 = mysqli_real_escape_string($data[3]);
                $item5 = mysqli_real_escape_string($data[4]);
                $item6 = mysqli_real_escape_string($data[5]);
                $item7 = mysqli_real_escape_string($data[6]);
                //$var = "%".$data[6]."%";
                //$item8 = mysqli_real_escape_string($data[7]);
                //var_dump($var);               
                if($resultats->insertClient($data[0],$data[1],$data[2],$data[3],$data[4],$data[5],$data[6])!="")
                {
                  //echo "<script>alert('Importation reussi');</script>";
                }
                else
                {
                  //echo "<script>alert('erreur importation');</script>";
                }
               
   }
   fclose($handle);

   
  }
 }
}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <script>
    $(document).ready(function(){
    $('#tableClient').DataTable( {
    ajax: {
        url: 'listeClient.php',
        dataSrc: 'data',
        datatype : 'json',
        
    },
    columns: [
        { data: 'nom_client'},
        { data: 'categorie_prix_client'}, 
        { data: 'categorie_commerciale' },
        { data: 'login' },
        { data: 'email' },
        { data: 'nom_commerciale'},
        {
          "render": function (data, type, row) {
          return "<div class='table-data-feature'><button id='edit' type='submit' class=' class='item' data-toggle='modal' data-id= "+row.id_client+" data-name= "+row.nom_client+"  data-target='#mediumModal"+row.id_client+"'><i class='zmdi zmdi-edit'></i></button><button id='edit' type='submit' class=' class='item' data-toggle='modal' data-id= "+row.id_client+" data-name= "+row.nom_client+"  data-target='#litleModal"+row.id_client+"'> <i class='zmdi zmdi-delete'></i> </button><button id='edit' type='submit' class=' class='item' data-toggle='modal' data-id= "+row.id_client+" data-target='#largeModal"+row.id_client+"'> <i class='far fa-plus-square'></i> </button></div>";
          
              }
                
            },
                    
    ]
   
});
/*
$("#mediumModal").on('show.bs.modal', function (e) {
    var triggerLink = $(e.relatedTarget);
    var id = triggerLink.data("id");
    $.ajax({
    url: 'clientById.php',
    type: 'GET',
    data: {id},
    dataType: 'json',
    success: function(data){ 
      console.log( data );

      $.each(data, function(i, val){
    $('.modal-title').append(val.nom_client);
    });
      
    }
    
    //$(this).find(".modal-body").html("<h6>id: "+data.nom_client+"</h6>");

  });
    
  });*/
  
 
    

  
    
    
});
    </script>
    <script src="../../wa.js"></script>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <!-- PAGE CONTAINER-->
        <div class="page-container"> 
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">               
               
                        <div class="row">
                            <div class="col-md-12">                  

                                <!-- DATA TABLE-->
                                <h1 class="title-1 m-b-35 text-danger">Liste clients<hr/></h1>
                                 <div class="table-data__tool">
                                    <div class="table-data__tool-left">                               
                                        <form method="post" id="myForm" enctype="multipart/form-data">
                                        <label>Chargez fichier csv</label><br/>
                                        <input type="file" name="file" /><br/> 
                                          <input type="submit" name="submit" value="Import" class="btn btn-info" />
                                        </form>                                          
                                        <!--<div class="rs-select2--light rs-select2--sm">
                                            <select class="js-select2" name="time">
                                                <option selected="selected">Récentes</option>
                                                <option value="">1 Mois</option>
                                                <option value="">Plus ancien</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div> -->                                       
                                    </div>
                                    <div class="table-data__tool-right">                                        
                                        <button class="au-btn au-btn-icon au-btn--green au-btn--small"  data-toggle="modal" data-target="#scrollmodal" type="button">
                                            <i class="zmdi zmdi-plus"></i>Inserer Nouveau Client
                                        </button>                                                                                                                         
                                    </div>
                                </div> 
                                <div class="table-responsive m-b-30">
                                     <table class="table table-borderless table-data3" id="tableClient">
                                        <thead>
                                            <tr>
                                                <th>Nom Client</th>
                                                <th>Categorie Prix Client</th>
                                                <th>Categorie commerciale</th>                                        
                                                <th>Login</th>
                                                <th>Email</th>
                                                <th>commerciale</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                                 
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                </div>
                                <!-- END DATA TABLE-->
                            </div>                   
                        </div>                  
                   

                        
                    </div>
                  </div>
                  
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Nouvelle <a href="https://npsp.ci">PSP</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="modalTitle"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- modal medium --> <?php foreach($clients as $client):; $id_client = $client['id_client']; $idComm = $client['id_commerciale']?>
<div class="modal fade" id="mediumModal<?=$id_client?>" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="mediumModalLabel"></h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
						<p>                              
              <h6></h6>          
            <form action="insertClient.php" method="post" id="myForm" enctype="multipart/form-data">
                          <input type="hidden" id="text-input" name="id" placeholder="Text" class="form-control" value="<?=$id_client?>">

                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nom Client</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="nom_client" name="nomClient" value="<?=$client['nom_client'] ?>" placeholder="" class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Categorie Prix Client</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="catPrix" value="<?=$client['categorie_prix_client'] ?>" placeholder="" class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Categorie commerciale</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="catComm" name="catComm" value="<?=$client['categorie_commerciale'] ?>" placeholder="" class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Login</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="login" name="login" value="<?=$client['login'] ?>" placeholder="" class="form-control"></div>
                          </div>                          
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Email</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="email" name="email" value="<?=$client['email'] ?>" placeholder="" class="form-control"></div>
                          </div>
                          
                                        
                                    <!--<div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label">Choisir Projets</label>
                                                </div>                                               
                                                <div class="col col-md-9">
                                                    <div class="form-check">
                                                    
                                                        <div class="checkbox">
                                                            <label for="checkbox1" class="form-check-label ">
                                                              <?php foreach ($projets as $projet):;  ?>
                                                                <input type="checkbox" id="checkbox1" checked name="checkboxProjet" value="<?= $projet['codeProjet'] ?>" class="form-check-input"><?= $projet['libelleProjet'] ?><br/>
                                                              <?php endforeach; ?> 
                                                            </label>
                                                             
                                                        </div>
                                                    </div>
                                                </div>                                                                                              
                                    </div>-->
                          
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="selectSm" class=" form-control-label" value="">Choisir Commercial</label></div>
                            <div class="col-12 col-md-9">
                                
                              <select name="select" id="SelectLm" class="form-control-sm form-control">
                              <?php $commercialeId = $resultats->getCommercialeById($idComm);
                                foreach ($commercialeId as $commercial):; //$id_commerciale = $commercial['id_commercial'] ?>
                                <option value="<?= $commercial['id_commercial']; ?>"> <?= $commercial['nom_commerciale']; ?></option>
                                <?php endforeach ?>
                                <?php $commerciale = $resultats->getCommerciale();
                                foreach ($commerciale as $commercial):; //$id_commerciale = $commercial['id_commercial'] ?>
                                <option value="<?= $commercial['id_commercial']; ?>"> <?= $commercial['nom_commerciale']; ?></option>
                                <?php endforeach ?>                                                             
                              </select>
                            </div>
                          </div>                 
                          
                                    

                          <div class="row form-group">
                          </div> 
                          <?php
                              $member = $userRow['role_id'];
                              if($member == 2) {
                                  echo "
                                  <button type='reset' class='btn btn-danger btn-sm' value='submit'>annuler</button>";
                              } elseif($member == 1) {
                                  echo "
                                  <button type='submit' class='btn btn-success btn-sm' name='submitU'  value='submit'>Valider</button>
                                  <button type='reset' class='btn btn-danger btn-sm' value='submit'>annuler</button>";  
                              } 
                              ?>                     
                             <!-- <button type="submit" class="btn btn-success btn-sm" name="submitU"  value="submit">Valider</button>
                             <button type="reset" class="btn btn-danger btn-sm" value="submit">Annuler</button> -->
                            
                        </form>
                        </p>
                        </div>
						
						
					</div>
				</div>
</div><?php endforeach?>
            <!-- end modal medium -->
            
            <!--litle modal --><?php foreach ($clients as $client):; $id_client = $client['id_client']?> 
        <div class="modal fade" id="litleModal<?= $id_client;?>" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="largeModalLabel">Large Modal</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<p>
                            <form action="insertClient.php" method="post" id="myForm" enctype="multipart/form-data">
                            <div class="row form-group">
                            <div class="col col-md-8"><label for="selectSm" class=" form-control-label">Voulez vous Supprimer définitivement ce Client?</label></div>
                        </div>                          
                          
                         
                            <input type="hidden" id="text-input" name="id" placeholder="Text" class="form-control" value="<?=$id_client?>">
                         
                            <?php
                                $member = $userRow['role_id'];
                                if($member == 2) {
                                    echo "
                                    <button type='reset' class='btn btn-danger btn-sm' value='submit'>annuler</button>";
                                } elseif($member == 1) {
                                    echo "
                                    <button type='submit' class='btn btn-success btn-sm' name='submitD'  value='submit'>Valider</button>
                                    <button type='reset' class='btn btn-danger btn-sm' value='submit'>annuler</button>";  
                                } 
                                ?>
                            
                             <!-- <button type="submit" class="btn btn-success btn-sm" name="submitD"  value="submit">Valider</button>
                             <button type="reset" class="btn btn-danger btn-sm" value="submit">annuler</button> -->
                             
                          </form>
							</p>
						</div>
						
					</div>
				</div>
       
        </div><?php endforeach;?>
        <!-- end lilte-->

        <!--litle modal --><?php foreach ($clients as $client):; $id_client = $client['id_client']?> 
        <div class="modal fade" id="largeModal<?= $id_client;?>" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="largeModalLabel">Ajouter des Projets</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<p>
                            <form action="insertClient.php" method="post" id="myForm" enctype="multipart/form-data">
                            <div class="row form-group">                       </div>                          
                          
                         
                            <input type="hidden" id="text-input" name="id" placeholder="Text" class="form-control" value="<?=$id_client?>">
                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label">Choisir Projets</label>
                                                </div>                                               
                                                <div class="col col-md-9">
                                                    <div class="form-check">
                                                    
                                                        <div class="checkbox">
                                                            <label for="checkbox1" class="form-check-label ">
                                                              <?php foreach ($projets as $projet):;  ?>
                                                                <input type="checkbox" id="checkbox1" name="checkboxProjet[]" value="<?= $projet['code_projet'] ?>" class="form-check-input"><?= $projet['libelleProjet'] ?><br/>
                                                              <?php endforeach; ?> 
                                                            </label>
                                                             
                                                        </div>
                                                    </div>
                                                </div>                                                                                              
                                    </div>
                         
                                    <?php
                                        $member = $userRow['role_id'];
                                        if($member == 2) {
                                            echo "
                                            <button type='reset' class='btn btn-danger btn-sm' value='submit'>annuler</button>";
                                        } elseif($member == 1) {
                                            echo "
                                            <button type='submit' class='btn btn-success btn-sm' name='submitP'  value='submit'>Valider</button>
                                            <button type='reset' class='btn btn-danger btn-sm' value='submit'>annuler</button>";  
                                        } 
                                        ?>
                             <!-- <button type="submit" class="btn btn-success btn-sm" name="submitP"  value="submit">Valider</button>
                             <button type="reset" class="btn btn-danger btn-sm" value="submit">annuler</button> -->
                             
                          </form>
							</p>
						</div>
						
					</div>
				</div>
       
        </div><?php endforeach;?>
        <!-- end lilte-->


            <!-- modal scroll -->
           
			<div class="modal fade" id="scrollmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="scrollmodalLabel">Créer compte client</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<p>
              <form action="insertClient.php" method="post" id="myForm" enctype="multipart/form-data">                        
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nom Client</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="nomClient" placeholder="" class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Categorie Prix Client</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="catPrix" placeholder="" class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Categorie commerciale</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="catComm" placeholder="" class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Email</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="email" placeholder="" class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Login</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="login" placeholder="" class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Mot de passe</label></div>
                            <div class="col-12 col-md-9"><input type="password" id="text-input" name="password" placeholder="" class="form-control"></div>
                          </div>
                                        
                                    <!--<div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label">Choisir Projets</label>
                                                </div>                                               
                                                <div class="col col-md-9">
                                                    <div class="form-check">
                                                    
                                                        <div class="checkbox">
                                                            <label for="checkbox1" class="form-check-label "><?php //foreach ($projets as $projet):;  ?>
                                                                <input type="checkbox" id="checkbox1" name="checkboxProjet[]" value="<?= $projet['code_projet'] ?>" class="form-check-input"><?= $projet['libelleProjet'] ?><br/>
                                                                <?php //endforeach; ?> 
                                                            </label>
                                                             
                                                        </div> 
                                                    </div>
                                                </div>                                                                                              
                                      </div>-->
                                                  
                                                  <div class="row form-group">
                                                    <div class="col col-md-3"><label for="selectSm" class=" form-control-label" value="">Choisir Commercial</label></div>
                                                    <div class="col-12 col-md-9">
                                                        
                                                      <select name="select" id="SelectLm" class="form-control-sm form-control">
                                                      <option value=""> Selectionner</option>
                                                        <?php $commerciale = $resultats->getCommerciale();
                                                        foreach ($commerciale as $commercial):; //$id_commerciale = $commercial['id_commercial'] ?>
                                                        <option value="<?= $commercial['id_commercial']; ?>"> <?= $commercial['nom_commerciale']; ?></option>
                                                        <?php endforeach ?>                                                             
                                                      </select>
                                                    </div>
                                                  </div>                 
                                                  
                                                            
                        
                                                  <div class="row form-group">
                                                  </div> 
                                                  <?php
                                                    $member = $userRow['role_id'];
                                                    /* var_dump($member); */
                                                    if($member == 2) {
                                                        echo "<button type='reset' class='btn btn-danger btn-sm' value='submit'>Annuler</button>";
                                                    } elseif($member == 1) {
                                                        echo "
                                                        <button type='submit' class='btn btn-success btn-sm' name='btn-submit'  value='submit'>Valider</button>
                                                        <button type='reset' class='btn btn-danger btn-sm' value='submit'>Annuler</button>";  
                                                    } 
                                                    ?>                         
                                                     <!-- <button type="submit" class="btn btn-success btn-sm" name="btn-submit"  value="submit">Valider</button>
                                                     <button type="reset" class="btn btn-danger btn-sm" value="submit">Annuler</button> -->
                                                    
                                                </form>								
							</p>
						</div>
						
					</div>
				</div>
            </div>
           
			<!-- end modal scroll -->

        <!-- END MAIN CONTENT-->
    <!-- END PAGE CONTAINER-->
</div>

    </div>

    <!-- Jquery JS-->
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>
<!-- end document-->
