<?php
require_once("session.php");
require_once("class.user.php");
require_once("class.articles.php");
require_once("dbconfig.php");
$filepath = realpath (dirname(__FILE__));

require_once($filepath."./../../webanalytics.php");
include "./../../websettings.php";

$insertArt = new ARTICLES();


/*$target_dir = "images/uploads/";*/
$target_dir = "images/uploads/";

$target_pic = $target_dir . filter_var(basename($_FILES["photo"]["name"], FILTER_SANITIZE_STRING));
$uploadOk = 1;

$pathPhoto = $target_pic;
$activer = filter_var($_POST['activer'], FILTER_SANITIZE_STRING);
$idSlider = filter_var($_POST['idSlider'], FILTER_SANITIZE_STRING);

if (isset($_POST['btn-submit']))
{
    if ($target_pic == "images/uploads/") {
        $msg = "cannot be empty";
        $uploadOk = 0;
    } // Check if file already exists
    else if (file_exists($target_pic)) {
        $msg = "Sorry, file already exists.";
        $uploadOk = 0;
    } // Check file size
    else if ($_FILES["photo"]["size"] > 1000000) {
        $msg = "Sorry, your file is too large.";
        $uploadOk = 0;
    } // Check if $uploadOk is set to 0 by an error
    else if ($uploadOk == 0) {
        $msg = "Sorry, your file was not uploaded.";

        // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_pic)) {
            $msg = "Le fichier" . basename($_FILES["photo"]["name"]) . " a été téléchargé.";
        }
    }

    if($insertArt->insertSlider($pathPhoto, $activer)!="")
   {
       echo "Données inserées"; 
       $insertArt->redirect('gestSlid.php');
   }
      else{
          echo "probleme insertion";
      }
          
}

if (isset($_POST['btn-submitV']))
{
    

    if($insertArt->updateSlider($activer, $idSlider)!="")
   {
       echo "Photo Modifiée"; 
       $insertArt->redirect('gestSlid.php');
   }
      else{
          echo "Erreur modification";
      }
    
}

if (isset($_POST['btn-submitD']))
{
    

    if($insertArt->deleteSlider( $idSlider)!="")
   {
       echo "Photo Supprimée"; 
       $insertArt->redirect('gestSlid.php');
   }
      else{
          echo "Erreur Supression";
      }
    
}
   

?>


