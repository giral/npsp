

<section class="testimonials3 cid-qpIrbULw1a" id="testimonials3-51" data-rv-view="6911">
    <div class="container">
                        <div class="col-md-12">
                            <div class="card">
                                    <div class="card-header" style="background:#385E02">
                                       
                                        <strong class="card-title pl-2 text-white">Votre commercial dédié </strong>
                                    </div>
                                    <div class="card-body">
                                    <?php foreach ($datas as $data):; //var_dump($data) ?>
                                        <div class="mx-auto d-block">
                                            <img class="rounded-circle mx-auto d-block" src="<?= $data['photo_commercial'];?>" alt="Card image cap"/>
                                            <h5 class="text-sm-center mt-2 mb-1"><?= $data['nom_commerciale'];?> </h5>
                                            <div class="location text-sm-center">
                                            <i class="fas fa-user-md"></i> <?= $data['fonction_commerciale'];?><br/>
                                                <i class="fas fa-address-book"></i> <?= $data['contact_commerciale'];?><br/>
                                                <i class="fas fa-at"></i> <?= $data['email_commerciale'];?>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="card-text text-sm-center">
                                            <a href="#">
                                                <i class="fa fa-facebook pr-1"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-twitter pr-1"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-linkedin pr-1"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-pinterest pr-1"></i>
                                            </a>
                                        </div>
                                        <?php endforeach ?>
                                    </div>
                                </div>
                            </div>
        
        
    </div>
</section><br/><br/>
            <!-- END COPYRIGHT-->