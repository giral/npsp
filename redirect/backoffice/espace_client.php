<!DOCTYPE html>
<html lang="en">

<head>
    
<?php include ("header3.php");
$filepath = realpath (dirname(__FILE__));

require_once($filepath."./../../webanalytics.php");
include "./../../websettings.php";?>

<script src="../../wa.js"></script>
</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->      

            <!-- WELCOME-->
            <section class="welcome p-t-10">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="title-4 text-success">Bienvenue Michée
                            </h1>
                            <hr class="line-seprate">
                        </div>
                        
                    </div>
                </div>
            </section>
            <!-- END WELCOME-->

            <!-- STATISTIC-->
            <section class="statistic statistic2">
                <div class="container">


                
                <div class="col-md-12">
                <h3 class="title-5 m-b-35 text-danger">Promotions produits | <a class="text-secondary" href ="produits.php">Voir tous les produits</a></h6>
                </div>

                        
                    <div class="row">

                    
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item ">
                                <img src="images/uploads/ketamin.jpg" alt="Nouvelle PSP"  />
                               
                                <span class="statistic-chart-1-note"><br/><br/>AMXF002<br/>members online</span>
                                
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item ">
                            <img src="images/uploads/doli.jpg" alt="Nouvelle PSP"; />
                            <span class="statistic-chart-1-note"><br/><br/>AMXF002<br/>members online</span>
                                
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item ">
                            <img src="images/uploads/phyto.jpg" alt="Nouvelle PSP" />
                            <span class="statistic-chart-1-note"><br/><br/>AMXF002<br/>members online</span>
                                
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item ">
                            <img src="images/uploads/microscope.jpg" alt="Nouvelle PSP"  />
                            <span class="statistic-chart-1-note"><br/><br/>AMXF002<br/>members online</span>
                                
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item ">
                            <img src="images/uploads/microscope.jpg" alt="Nouvelle PSP"  />
                            <span class="statistic-chart-1-note"><br/><br/>AMXF002<br/>members online</span>
                                
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item ">
                            <img src="images/uploads/microscope.jpg" alt="Nouvelle PSP"  />
                            <span class="statistic-chart-1-note"><br/><br/>AMXF002<br/>members online</span>
                                
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item ">
                            <img src="images/uploads/microscope.jpg" alt="Nouvelle PSP"  />
                            <span class="statistic-chart-1-note"><br/><br/>AMXF002<br/>members online</span>
                                
                            </div>
                        </div>
                    
                        
                    
                    
                    </div>
                </div>
                
            </section>
            <!-- END STATISTIC-->

            

            <!-- DATA TABLE-->
            <section class="p-t-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-5 m-b-35 text-danger">Liste produits</h3>
                            
                            <div class="table-responsive m-b-40">
                                     <table class="table table-borderless table-data3">
                                        <thead>
                                            <tr>
                                                
                                                <th>Nom Client</th>
                                                <th>Categorie prix client</th>                                        
                                                <th>Categorie commerciale</th>
                                                <th>Login</th>
                                                <th>commerciale</th>
                                                <th>action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($clients as $client):; $id_client = $client['id_client']; ?> 
                                            <tr>                                            
                                            <td> <?= $client['nom_client'];?></td>
                                            <td> <?= $client['categorie_prix_client'];?></td>
                                            <td> <?= $client['categorie_commerciale'];?></td>
                                            <td> <?= $client['login'];?></td>
                                            <td>  Dr AHOUSSI</td>
                                                 <td>                                                
                                                 <div class="table-data-feature">
                                                        <button class="item" data-placement="top" title="Edit" data-target="#mediumModal" data-toggle="modal" type="button" >
                                                            <i class="zmdi zmdi-edit"></i>
                                                        </button>
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </button>
                                                        <button class="item" data-placement="top" title="Edit" data-target="#scrollmodal<?= $id_client ?>" data-toggle="modal" type="button" >
                                                            <i class="zmdi zmdi-edit"></i>
                                                        </button>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END DATA TABLE-->

            <!-- COPYRIGHT-->
            <section class="p-t-60 p-b-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2019 <a href="http://www.npsp.ci/">Nouvelle PSP</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END COPYRIGHT-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>
<!-- end document-->