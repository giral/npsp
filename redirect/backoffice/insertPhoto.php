<?php
require_once("session.php");
require_once("class.user.php");
require_once("class.articles.php");
require_once("dbconfig.php");


$insertArt = new ARTICLES();

if (isset($_POST['submitAlbum'])){
    $nomAlbum = filter_var($_POST['nomAlbum'], FILTER_SANITIZE_STRING);
    $publier = filter_var($_POST['publier'], FILTER_SANITIZE_STRING);
    $insertArt->createAlbum($nomAlbum, $publier);
    $insertArt->redirect('gestAlbum.php');
}

if (isset($_POST["submitPicture"])) {
    $albumId = filter_var($_POST['idAlbum'], FILTER_SANITIZE_STRING);
    //var_dump($albumId);
    // image mime to be checked 
    $imagetype = array(image_type_to_mime_type(IMAGETYPE_GIF), image_type_to_mime_type(IMAGETYPE_JPEG),
                 image_type_to_mime_type(IMAGETYPE_PNG), image_type_to_mime_type(IMAGETYPE_BMP));    
    $FOLDER = "images/uploads/galleriePhoto/";
    $myfile = filter_var($_FILES["files"], FILTER_SANITIZE_STRING);
    $keepName = false; // change this for file name.
    $response = array();
    for ($i = 0; $i < count($myfile["name"]); $i++) {
        if ($myfile["name"][$i] <> "" && $myfile["error"][$i] == 0) {
            // file is ok
            if (in_array($myfile["type"][$i], $imagetype)) {
                //Set file name
                if($keepName) {
                    $file_name =  $myfile["name"][$i];
                } else {
                    // get extention and set unique name
                    $file_extention = @strtolower(@end(@explode(".", $myfile["name"][$i])));
                    $file_name = date("Ymd") . '_' . rand(10000, 990000) . '.' . $file_extention;
                }
                if (move_uploaded_file($myfile["tmp_name"][$i], $FOLDER . $file_name) === FALSE) {
                    //Set Original File Name if Upload Error
                    $response[] = array ('error'=>true,'msg'=>"Error While Uploading the File",'fileName'=>$myfile["name"][$i]);
                } else {
                    // Set Name Used to Store file on Server
                    $response[] = array ('error'=>false,'msg'=>"File Uploaded",'fileName'=>$file_name);
                }
                $insertArt->insertGalleriePhoto($file_name, $albumId);
                $insertArt->redirect('gestAlbum.php');
            } else {
                //Set Original File Name if Invalid Image Type
                $response[] = array ('error'=>true,'msg'=>" Invalid Image Type.",'fileName'=>$myfile["name"][$i]);
            }
        }
    }
    echo json_encode($response);
}

/*
$target_dir = "images/uploads/";
$target_dir = "images/uploads/";

$target_pic = $target_dir . basename($_FILES["photo"]["name"]);
$uploadOk = 1;

$pathPhoto = $target_pic;
$activer = $_POST['activer'];
$idSlider = $_POST['idSlider'];

var_dump($idSlider);
var_dump($activer);

}



if (isset($_POST['btn-submit']))
{
    if ($target_pic == "images/uploads/") {
        $msg = "cannot be empty";
        $uploadOk = 0;
    } // Check if file already exists
    else if (file_exists($target_pic)) {
        $msg = "Sorry, file already exists.";
        $uploadOk = 0;
    } // Check file size
    else if ($_FILES["photo"]["size"] > 1000000) {
        $msg = "Sorry, your file is too large.";
        $uploadOk = 0;
    } // Check if $uploadOk is set to 0 by an error
    else if ($uploadOk == 0) {
        $msg = "Sorry, your file was not uploaded.";

        // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_pic)) {
            $msg = "Le fichier" . basename($_FILES["photo"]["name"]) . " a été téléchargé.";
        }
    }

    if($insertArt->insertSlider($pathPhoto, $activer)!="")
   {
       echo "Données inserées"; 
       $insertArt->redirect('gestSlid.php');
   }
      else{
          echo "probleme insertion";
      }
          
}

if (isset($_POST['btn-submitV']))
{
    

    if($insertArt->updateSlider($activer, $idSlider)!="")
   {
       echo "Photo Modifiée"; 
       $insertArt->redirect('gestSlid.php');
   }
      else{
          echo "Erreur modification";
      }
    
}

if (isset($_POST['btn-submitD']))
{
    

    if($insertArt->deleteSlider( $idSlider)!="")
   {
       echo "Photo Supprimée"; 
       $insertArt->redirect('gestSlid.php');
   }
      else{
          echo "Erreur Supression";
      }
    
}
*/






   

?>


