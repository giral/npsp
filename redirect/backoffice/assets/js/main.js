/**
 * Created by toshiba on 26/05/2016.
 */
$("#fbloader").hide();
$("#ajoutEmailForm").submit(function (e) {
    e.preventDefault();
    var url = 'Ajax/ajoutEmail.php';
    var query = $("#email").val();
    $.ajax({
        type: 'POST',
        url: url,
        data: {
            query: query
        },
        beforeSend: function () {
            $("#fbloader").show();
        },
        success: function (data) {
            $("#listMail").append(data);
            $("#fbloader").hide();

        }
    });
    var url = 'Ajax/help1.php';
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'HTML',
        beforeSend: function () {

        },
        success: function (data) {
            $("#help_").append(data);
            $(function () {
                $('[data-toggle="popover"]').popover();
            });
            $(document).ready(function () {
                $('#help1popmsg').popover('show');
            });
            $("#ajoutEmailForm .email").val('');

        }
    });


});
$("#addMail").on('click', function () {
    $('#addMail').hide();
    var url = 'Ajax/ajoutEmail.php';
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'HTML',
        beforeSend: function () {
            $("#fbloader").show();
        },
        success: function (data) {
            $(".btnAjout").html(data);
            $("#fbloader").hide();

        }
    });
});
function addMail() {

    var url = 'Ajax/ajoutEmail.php';
    var query = $("#email1").val();
    $.ajax({
        type: 'POST',
        url: url,
        data: {
            query: query
        },
        beforeSend: function () {
            $("#fbloader").show();
        },
        success: function (data) {
            $("#listMail").append(data);
            $("#fbloader").hide();
            $("#ajoutEmailForm1 .email").val('');
        }
    });

    return false;
}
$('.mail').on('click', function () {
    var id = $(this).attr("id");
    var url = 'Ajax/checked.php';
    var id = id.split("mailList")[1];
    if ($(this).is(':checked') == true) {
        var action = 'ajout';

    } else {
        var action = 'supp';
    }
    $.ajax({
        type: 'POST',
        url: url,
        data: {
            action: action,
            id: id
        },
        beforeSend: function () {

        },
        success: function (data) {
            if (data !== "") {
                $("#info").html(data);
            } else {

            }


        }
    });
});
function oza(id) {
    var url = 'Ajax/checked.php';
    if ($("#mailList" + id).is(':checked') == true) {
        var action = 'ajout';


    } else {
        var action = 'supp';
    }
    $.ajax({
        type: 'POST',
        url: url,
        data: {
            action: action,
            id: id
        },
        beforeSend: function () {

        },
        success: function (data) {
            if (data !== "") {
                $("#info").html(data);
            } else {

            }
        }
    });
    var url = 'Ajax/help1.php';
    $.ajax({
        type: 'GET',
        url: url,
        data: {
            check: action
        },
        dataType: 'HTML',
        beforeSend: function () {

        },
        success: function (data) {

            if (data !== "") {
                $("#info").html('  <div id="information" class="col-md-8 alert alert-success ">'
                    + '<button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>'
                    + '<h4>Félicitations vous avez ajouté ' +  data  + '  à votre liste d\'envoie</h4>'
                    + '</div> ');
            } else {

            }
            $(function () {
                $('[data-toggle="popover"]').popover();
            });
            $(document).ready(function () {
                $('#help1popmsg').popover('hide');
            });


        }
    });
}
var select_all = document.getElementById("select_all"); //select all checkbox
var checkboxes = document.getElementsByClassName("checkbox"); //checkbox items

//select all checkboxes
select_all.addEventListener("change", function(e){
	for (i = 0; i < checkboxes.length; i++) { 
		checkboxes[i].checked = select_all.checked;
	}
});


for (var i = 0; i < checkboxes.length; i++) {
	checkboxes[i].addEventListener('change', function(e){ //".checkbox" change 
		//uncheck "select all", if one of the listed checkbox item is unchecked
		if(this.checked == false){
			select_all.checked = false;
		}
		//check "select all" if all checkbox items are checked
		if(document.querySelectorAll('.checkbox:checked').length == checkboxes.length){
			select_all.checked = true;
		}
	});
}
function toggle(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}
