<?php
require('../includes/config.php');
require('../includes/functions.php');
if (isset($_POST['content'],$_POST['subject'])){
    extract($_POST);
    if (!empty($content) and !empty($subject)){
        $q = $db->prepare('SELECT Email FROM emails WHERE status = :status');
        $q->execute([
            'status' => '1'
        ]);
        $mails = $q->fetchAll(PDO::FETCH_OBJ);
        if ($mails){
            foreach ($mails as $mail){
                //Envoi du message
                $to = $mail->Email;
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type:text/html; charset=iso-8859-1' . "\r\n";
                mail($to, $subject, $content, $headers);
            }
            $subject_output = "
            *********************************************************************
            /*                                                                 */
            /*               ".$subject."                                      */
            /*                                                                 */
            /*                                                                 */
            *********************************************************************

            ";
            $randomFileName = md5(uniqid(rand())).date("Y_m_d_s");
            $file = fopen("../output/".$randomFileName.".txt", "w+");
            fwrite($file, $subject_output.$content);
            fclose($file);
            $q = $db->prepare("SELECT ID, Email,status FROM emails WHERE status = :status");
            $q->execute([
                'status' => '1'
            ]);
            $emails = $q->fetchAll(PDO::FETCH_OBJ);
            if (!empty($emails)){
                foreach ($emails as $email){
                    $q = $db->prepare("INSERT INTO archives(Email,link) VALUES(:Email,:link)");
                    $q->execute([
                        'Email' => $email->Email,
                        'link' => "output/".$randomFileName
                    ]);
                }
            }

        }else{
            
        }
    }else{
        
    }
}