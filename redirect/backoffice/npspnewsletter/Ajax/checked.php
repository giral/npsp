<?php
require('../includes/config.php');
require('../includes/functions.php');

if (isset($_POST['action'], $_POST['id'])) {
    extract($_POST);
    $count = count_emails("WHERE id =" . $id);
    if ($count > 0) {


        if (!empty($action) and !empty($id)) {
            if ($action == 'ajout') {
                //Ajouter a la liste d'envoi
                update_status($id, '1');
                $q = $db->prepare("SELECT Email,ID,status FROM emails WHERE id = :id");
                $q->execute([
                    'id' => $id
                ]);

                $data = $q->fetch(PDO::FETCH_OBJ);
                echo '<div id="information" class="col-md-8 alert alert-success ">
                    <button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4>'.$data->Email.'a été ajouté à votre liste d\'envoie</h4>
                    </div>';

            } else {
                if (verifie_email_status($id, '1')) {
                    update_status($id, '0');
                }
            }


        }
    }else{
        echo '<div  class="col-md-8 alert alert-danger ">
                    <button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4>L\'addresse Email n\'existe pas dans la base de donnée.</h4>
                    </div>';
    }
}