<?php
/*
 * DEVELOPED  BY BNS GROUP 
 * DIRECTED BY ABOUBACAR OUATTARA
 */
session_start();

//Gere l'etat actif de nos differents liens
if (!function_exists('set_active')) {
    function set_active($file, $class = 'active')
    {
        $page = array_pop(explode('/', $_SERVER['SCRIPT_NAME']));
        if ($page == $file . '.php') {
            return $class;
        } else {
            return "";
        }
    }
}
include ('views/mentionslegales.view.php');