<?php
/*
 * DEVELOPED  BY BNS GROUP 
 * DIRECTED BY ABOUBACAR OUATTARA
 */
session_start();
require('includes/init.php');

if (isset($_POST['install'])) {
    extract($_POST);
    if (not_empty(['host', 'name', 'username'])) {
        if (not_empty(['password'])){
            if ($password !=$password_confirm){
                set_flash("Les deux mots de passe doivent être identiques.","danger");

            }
        }
        //Ecriture du fichier config.php

        $content = '<?php
    //functions flash
    if (!function_exists("set_flash")) {
    function set_flash($message, $type = \'info\')
    {
        $_SESSION[\'notification\'][\'message\'] = $message;
        $_SESSION[\'notification\'][\'type\'] = $type;

    }
}
     
//Database credentials
define(\'DB_HOST\',\''.$host.'\');
define(\'DB_NAME\',\''.$name.'\');
define(\'DB_USERNAME\',\''.$username.'\');
define(\'DB_PASSWORD\',\''.$password.'\');


try {
    $db = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USERNAME, DB_PASSWORD);
    $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e){
    $_SESSION[\'error\'][\'config\'] = "yes";
    if (preg_match("#1049#i", $e->getMessage())){
        set_flash("Erreur : La Base de donnée  \"".DB_NAME."\" n\'existe pas.");
    }else{
       set_flash("Erreur: ".$e->getMessage());
    }

    header(\'Location: install.php\');
    exit();
}';
        $file = fopen("includes/config.php", "w+");
        fwrite($file, $content);
        fclose($file);
        set_flash("Votre fichier de configuration à été créé avec success", "success");
        //redirect('index.php');
    } else {
        set_flash("Veuillez remplir tous les champs avant de continuer", "danger");
        
    }

}
//Verification du fichier config.php
if (file_exists("includes/config.php")){
    if (!isset($_SESSION['error']['config'])){
        set_flash('Un fichier de configuration existe déjà');
        redirect('index.php');
    }else{

        $_SESSION['error'] = [];
    }

}

include('views/install.view.php');

