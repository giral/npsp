<?php include('partials/_header.php');
?>
    <div class="row">
        <div class="col-md-6 col-lg-offset-2">

            <div class="panel panel-default ">
                <div class="panel-heading">Installation du système de Newsletter</div>
                <div class="panel-body">
                    <form action="" method="post" autocomplete="off" data-parsley-validate>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="host">Adresse du serveur</label>
                                    <input type="text" name="host" class="form-control" id="host" required="required"/>
                                    <span class="text-danger">* </span>
                    <span class="text-info">Si vous êtes en local
                    laisser <em class="text-danger">Localhost</em></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">Nom de la base de donnée</label>
                                    <input type="text" name="name" class="form-control" id="name" required="required"/>
                                    <span class="text-danger">* </span>
                            <span class="text-info">Vous devez d'abord créer une base de donnée sur
                            votre serveur via <em class="text-danger">PHPMYADMIN</em></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="username">Nom d'utilsateur (username)</label>
                                    <input type="text" name="username" class="form-control" id="username"
                                           required="required"/>
                                    <span class="text-danger">* </span>
                                    <span class="text-info">Votre nom d'utilisateur</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="password">Mot de passe</label>
                                    <input type="password" name="password" class="form-control" id="password"
                                    />

                                    <span class="text-info">Votre mot de passe de connection à la base de donnée (Facultatif)</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="password_confirm">Mot de passe(confirmation)</label>
                                    <input type="password" name="password_confirm" class="form-control"
                                           data-parsley-equalto="#password"  id="password_confirm"
                                    />

                                    <span class="text-info">Confirmer le mot de passe</span>
                                </div>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary" name="install" value="Installer"/>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php
include('partials/_footer.php');
?>