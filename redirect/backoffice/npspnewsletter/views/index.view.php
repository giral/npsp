<?php
include('partials/_header.php'); ?>
    <div class="row">
        <div class="col-md-8 col-lg-offset-1">
            <div class="panel panel-default ">
                <div class="panel-heading">Newsletter</div>
                <div class="panel-body">
                    <form action="sendmail.php"  method="post" id="SendEmail" data-parsley-validate autocomplete="off">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="form-group">
                                    <label for="subject">Sujet</label>
                                    <input type="text" name="subject"
                                           class="form-control" id="subject" placeholder="ex: Salutation..." required="required"/>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="content">Contenu</label>
                                    <textarea name="content" id="content" style="color: #000;" cols="30" rows="10"
                                              class="form-control"></textarea>
                                </div>
                                
                                <br>
                                <input type="submit" name="sendMail" id="SendMailSubmitBtn" class="btn btn-success"
                                       value="Envoyer">
                            </div>

                        </div>
                    </form>


                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default ">
                <div class="panel-heading">Emails
                    <button class="pull-right btn btn-primary <?= $emails ? '' : ' hide' ?>" id="addMail">Ajouter
                    </button>
                    <img src="assets/img/fbloader.gif" alt="patientez..." class="pull-right" id="fbloader"/>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btnAjout"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="listMail">
                            <div id="help_"></div>
                            </div>
                            
                            <?php
                            if (!empty($emails)) {
                                foreach ($emails as $email) {
                                    if ($email->status == '0') {
                                        $checked = "";
                                    } else {
                                        $checked = "checked";
                                    }
                                    ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="mailList<?= $email->ID ?>" id="select_all">
                                                <input type="checkbox" 
                                                       id="mailList<?= $email->ID ?>"
                                                       class="mail" <?= $checked ?>> <?= $email->Email ?>

                                            </label>
                                        </div>

                                    </div>
                                    
                                    <?php
                                }
                            } else {
                                ?>

                                <form action="" id="ajoutEmailForm" method="post" data-parsley-validate>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="email">Ajouter un Email</label>
                                                <input type="email" name="email"
                                                       class="form-control email" id="email"
                                                       required="required"/>

                                            </div>
                                        </div>
                                    </div>
                                     <?php
                                    if (isset($debut)){
                                        echo '<input type="submit" name="ajouter" value="Ajouter"
                                            id="popmsg" data-toggle="popover"
                                           title="Prémier Pas!!"
                                           data-content="Ajoutez un email avant de commencer"
                                           class="btn btn-primary"/>';
                                    }else{
                                        echo '<input type="submit" name="ajouter" value="Ajouter"
                                         
                                           class="btn btn-primary"/>';
                                    }  ?>
                                </form>
                                <?php

                            }

                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    


<?php
include('partials/_footer.php');