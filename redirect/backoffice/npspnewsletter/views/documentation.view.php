<?php
include('partials/_header.php'); ?>
<div class="col-md-10 col-md-offset-1">
    <article class="media status-media">
        <div class="media-body">
            <h4>Où placé le dossier <kbd>BNS</kbd></h4>
            Vous devez placé le dossier <kbd>BNS</kbd> sous votre dossier <strong class="text-danger">admin</strong> et
            le protéger par un fichier <code>.htaccess</code>
        </div>

    </article>
    <article class="media status-media " id="commentInstaller">
        <div class="media-body">
            <h4> Installation de Basic Newsletter Sender sur le serveur.</h4>

            <ol>
                <li id="verficationPHPVER">
                    <h5><u><strong>Vérification de la Version de php</strong></u></h5>
                    <p>L'installation <kbd>Basic Newsletter Sender </kbd> sur votre serveur requiert une version
                        supérieur à la version 5 de php : <code>php
                            5+
                            (Version 5 en allant)</code> et <strong class="text-danger">l'Ajax</strong> . <br/><strong
                            class="text-danger">Par conséquent vous devez activer Le Javascript de votre
                            Navigateur.</strong> <br/>
                        Veuillez donc verifier votre version php avant de commencer l'installation de <kbd>BNS</kbd>.
                        Vous
                        pouvez trouver les informations sur la Version de php en consultant la page PHPinfo de votre
                        serveur ou en conctatant votre Hébergeur.
                        <br/></p>
                </li>
                <li id="creationDB">
                    <h5><u><strong>Indiquer la Base de donnée</strong></u></h5>
                    <p>Dans le formulaire d'installation vous devez indiquer le nom de la base de donnée à utiliser dans
                        le champ <code>Nom de la base de donnée.</code> Si vous êtes en <b>local</b> ou sur un <b>serveur
                            dedié</b> vous pouvez créer une autre base de donnée, mais il est recommandé de garder les
                        tables de <kbd>BNS</kbd> dans la base de donnée de votre site. <br/>
                        <u><strong>Exemple de requête pour la création d'une base de donnée</strong></u> <br/>
                        <br><code>

                            /* Creation de la base de donnée */
                            <br/>
                            CREATE DATABASE bns &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


                        </code>
                    </p>
                </li>
            </ol>


    </article>
    <article class="media status-media" id="integrerSite">
        <div class="media-body">
            <h4>
                Intégrer BNS à mon site
            </h4>
            <p>
                Pour intégrer <kbd>BNS</kbd> à votre site vous devez d'abord l'installer ensuite <strong
                    class="text-info ">vous devez ajoutez cette requête en bas de votre requête
                    <br> d'inscription de membres dans la base de donnée.</strong>
                <br>
                <code>
                    <br>
                    //Integration de Basic Newsletter Sender à mon site <br>
                    $q = $db->prepare("INSERT INTO emails(Email) VALUES (?)");<br>
                    $q->execute([$email_de_l_utilisateur]);
                </code>


            </p>
            <p>
                Vous pouvez aussi créer un formulaire d'inscription à la newsletter en demandant à l'utilisateur son email que
                <br> aller insérer dans la base de donnée via la <span class="text-info">requête d'ajout d'emails ci-dessus</span>
            </p>

        </div>
    </article>
    <article class="media status-media" id="ajoutEmail">
        <div class="media-body">
            <h4>Ajout d'un Email à la liste d'envoi</h4>
        </div>
        <p>Avant de pouvoir envoyer des emails aux membres de votre liste d'envoie, vous devez d'abord ajouter leurs
            Adresses emails à cette liste.
            Pour cela regarder un peu à droite et vous verrez un panel consacrer à l'ajout des emails.</p>

        <img src="docs/img/panel_ajout.jpg" alt="Panel Ajout"/>
        <img src="docs/img/panel_ajout_2.jpg" alt="Panel Ajout"/> <br>
        <p>
            <br>
            Ensuite Veuillez coché la case pour selectionnez et finalisez L'ajout. Un message de type success doit
            apparaître pour vous notifier que l'adresse Email a été bel et <br>bien ajouté à la liste d'envoie.
        </p>
        <img src="docs/img/panel_ajout_3.jpg" alt="Panel Ajout- Finalisez"/>
        <br>
        <br>
        <p>
            Maintenant vous pouvez commencer à envoyer vos newsletter!!! <code>Let's go..</code>
        </p>


    </article>
    <article class="media status-media">
        <div class="media-body">
            <h4>Envoi d'un Newsletter</h4>
            <p>
                Maintenant que vous pouvez ajouter des Adresses Emails, Commençons à envoyer nos Newsletters. <br>
                A droite vous avez un autre panel titré <code>Newsletter</code>. Ce panel vous permet d'envoyer un
                Message à tout une liste d'émails.
            </p>
            <img src="docs/img/send_mail.jpg" alt="Panel D'envoie"/>
            <p>
                <br>
                <br>
                Vous n'avez qu'à écrire un Message et lui attribuant un sujet et de l'envoyer!!
                La vie devient facile avec <kbd>Aboubacar Ouattara (OZA)</kbd> <br>
                Je vous invite à lire les <a href="mentionslegales.php">mentions l'égales</a>
            </p>
        </div>
    </article>
</div>


<?php
include('partials/_footer.php'); ?>
