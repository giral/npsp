<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="libs/parsley/parsley.min.js"></script>
<script src="libs/parsley/i18n/fr.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/jquery.wysibb.min.js"></script>
<script src="assets/js/wysibb.fr.js"></script>
<script type="text/javascript">
    $(function () {
        $('[data-toggle="popover"]').popover();
    });
    $(document).ready(function () {
        $('#popmsg').popover('show');
        $('#popmsg').on('click', function () {
            $('#popmsg').popover('hide');
        });
    });
    $(function () {
        var optionsWbb = {
            buttons: "bold,|,italic,|,underline,|,fontcolor,|,fontfamilly,|,img,|,link,|,quote,|,table",
            lang: "fr"

        }
        $("#content").wysibb(optionsWbb);
    })
</script>

</body>
</html>
