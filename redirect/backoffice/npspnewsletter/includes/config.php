<?php
    //functions flash
    if (!function_exists("set_flash")) {
    function set_flash($message, $type = 'info')
    {
        $_SESSION['notification']['message'] = $message;
        $_SESSION['notification']['type'] = $type;

    }
}
     
//Database credentials
define('DB_HOST','Localhost');
define('DB_NAME','africannews');
define('DB_USERNAME','root'); 
define('DB_PASSWORD','27433003');


try {
    $db = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USERNAME, DB_PASSWORD);
    $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e){
    $_SESSION['error']['config'] = "yes";
    if (preg_match("#1049#i", $e->getMessage())){
        set_flash("Erreur : La Base de donnée  \"".DB_NAME."\" n'existe pas.");
    }else{
       set_flash("Erreur: ".$e->getMessage());
    }

    header('Location: install.php');
    exit();
}