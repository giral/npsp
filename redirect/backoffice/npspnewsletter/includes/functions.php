<?php

//fonction d'echapement minimun de secrurité
if (!function_exists('e')) {
    function e($string)
    {
        if ($string) {
            return htmlspecialchars($string);
        }
    }
}
//Verifier qu'il n'ya pas d'enregistrement d'emails
if (!function_exists('verifie_enregistrement')) {
    function verifie_enregistrement($email)
    {
        global $db;
        $q = $db->prepare("SELECT Email from emails WHERE Email = :email");
        $q->execute([
            'email' => $email
        ]);
        return (bool) $q->rowCount();
    }
}
//Verifie le status de l'email en cas de supp
if (!function_exists('verifie_email_status')) {
    function verifie_email_status($id, $status)
    {
        global $db;
        $q = $db->prepare("SELECT ID,status FROM emails WHERE id = :id and status = :status");
        $q->execute([
            'id' =>$id,
            'status' => $status
        ]);
        return (bool) $q->rowCount();
    }
}
//Fonction update status
if (!function_exists('update_status')) {
    function update_status($id, $status)
    {
        global $db;
        $q = $db->prepare('UPDATE emails set status = :status WHERE ID = :id');
        $q->execute([
            'status' => $status,
            'id' => $id
        ]);
    }
}

//Trouver des infos sur un emails
if (!function_exists('find_info_on_a_email')) {
    function find_info_on_a_email($email)
    {
        global $db;
        $q = $db->prepare("SELECT Email,ID,status FROM emails WHERE Email = :email");
        $q->execute([
            'email' =>$email
        ]);
        return $q->fetch(PDO::FETCH_OBJ);
    }
}

//Recherche tous les emails dans la base de donnée
if (!function_exists('find_emails')) {
    function find_emails()
    {
        global $db;
        $q = $db->prepare("SELECT Email,ID,status FROM emails");
        $q->execute();
        return $q->fetchAll(PDO::FETCH_OBJ);
    }
}
//count emails
if (!function_exists('count_emails')) {
    function count_emails($condition = "")
    {
         global $db;
        $q = $db->prepare("SELECT Email,ID,status FROM emails $condition");
        $q->execute();
        return $q->rowCount();
    }
}

//Gere l'etat actif de nos differents liens
if (!function_exists('set_active')) {
    function set_active($file, $class = 'active')
    {
        $page = array_pop(explode('/', $_SERVER['SCRIPT_NAME']));
        if ($page == $file . '.php') {
            return $class;
        } else {
            return "";
        }
    }
}
//Get a session value by key
if (!function_exists('get_session')) {
    function get_session($key)
    {
        if ($key) {
            return !empty($_SESSION[$key])
                ? e($_SESSION[$key])
                : null;
        }
    }
}
if (!function_exists("set_flash")) {
    function set_flash($message, $type = 'info')
    {
        $_SESSION['notification']['message'] = $message;
        $_SESSION['notification']['type'] = $type;

    }
}
if (!function_exists("redirect")) {
    function redirect($page)
    {
        header('Location: ' . $page);
        exit();

    }
}

if (!function_exists('not_empty')) {
    function not_empty($fields = [])
    {
        if (count($fields) != 0) {
            foreach ($fields as $field) {
                if (empty($_POST[$field]) || trim($_POST[$field]) == "") {
                    return false;
                }
            }
            return true;
        }
    }
}
