<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 26/05/2016
 * Time: 01:03
 */
require('../includes/config.php');
require('../includes/functions.php');


if (isset($_POST['query'])) {

    extract($_POST);
    if (!empty($query)) {
        if (!filter_var($query, FILTER_VALIDATE_EMAIL) === false) {


            //Verifier si il n'ya pas d'enregistrement avec le meme email
            if (!verifie_enregistrement($query)) {
                $q = $db->prepare("INSERT INTO emails(Email) VALUES (?)");
                $q->execute([$query]);
                $data = find_info_on_a_email($query);
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <label for="mailList<?= $data->ID ?>">
                            <input type="checkbox"
                                   id="mailList<?= $data->ID ?>" class="mail" onclick="return oza(<?= $data->ID ?>); ">  <?= $data->Email ?>

                        </label>
                    </div>
                </div>
                <?php
            }
        }
    }
} else {
    if (isset($_GET)) {

        ?>
        <form action="" id="ajoutEmailForm1" onsubmit="return addMail();" method="post" data-parsley-validate>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="email1">Ajouter un Email</label>
                        <input type="email" name="email1"
                               class="form-control email" id="email1" required="required"/>

                    </div>
                </div>
            </div>
            <input type="submit" name="ajouter" value="Ajouter" class="btn btn-primary"/>
        </form>
        <hr>
        <?php
    }
}

?>

