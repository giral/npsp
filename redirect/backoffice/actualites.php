<?php
require_once ("header.php");

$filepath = realpath (dirname(__FILE__));

require_once($filepath."./../../webanalytics.php");
include "./../../websettings.php";

$resultat = new ARTICLES();
$donnees=$resultat->selectActu();

?>
 
<!DOCTYPE html>
<html lang="fr">
<head>

<script src="../../wa.js"></script>
</head>
<body class="animsition">
<script type="text/javascript">
function myAjax() {
      $.ajax({
           type: "POST",
           url: 'your_url/ajax.php',
           data:{action:'call_this'},
           success:function(html) {
             alert(html);
           }

      });
 }
</script>


    <div class="page-wrapper">
        <!-- PAGE CONTAINER-->
        <div class="page-container"> 
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">               
               
                        <div class="row">
                            <div class="col-md-12">                  

                                <!-- DATA TABLE-->
                                <h3 class="title-5 m-b-35">Actualités</h3>
                                 <div class="table-data__tool">
                                    <div class="table-data__tool-left">                                        
                                        <div class="rs-select2--light rs-select2--sm">
                                            <select class="js-select2" name="time">
                                                <option selected="selected">Récentes</option>
                                                <option value="">1 Mois</option>
                                                <option value="">Plus ancien</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>                                        
                                    </div>
                                    <div class="table-data__tool-right">
                                        <button class="au-btn au-btn-icon au-btn--green au-btn--small"  data-toggle="modal" data-target="#mediumModal" type="button">
                                            <i class="zmdi zmdi-plus"></i>Nouvelle actualité
                                        </button>                                                                                  
                                    </div>
                                </div> 
                                <div class="table-responsive m-b-40">
                                     <table class="table table-borderless table-data3" id="table">
                                        <thead>
                                            <tr>
                                                <th>Date </th>
                                                <th>Titre</th>
                                                <th>Contenu</th>
                                                <th>Path Photo</th>
                                                <th>Publier</th>
                                                <th>accueil</th>
                                                <th>action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($donnees as $donnee):; $id = $donnee['id']?> 
                                            <tr>
                                            <td> <?= $donnee['date_publication'];?></td>
                                            <td> <?= $donnee['titre_actu'];?></td>
                                            <td> <?= substr($donnee['contenu'], 0,100);?>...</td>
                                            <td> <?= $donnee['path_photo'];?></td>
                                            <td> <?= $donnee['publier'];?></td>
                                            <td> <?= $donnee['accueil'];?></td>
                                                 <td>                                                
                                                    <div class="table-data-feature">
                                                        <button class="item" data-placement="top" title="Edit" data-target="#largeModal<?= $id;?>" data-toggle="modal" type="button" >
                                                            <i class="zmdi zmdi-edit"></i>
                                                        </button>
                                                        <button class="item" data-toggle="modal" data-target="#litleModal<?= $id;?>" data-placement="top" title="Delete" type="button">
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </button>                                                        
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                    
                                </div>
                                <!-- END DATA TABLE-->
                            </div>                   
                        </div>                  
                   

                        
                    </div>
                  </div>
                  
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Nouvelle <a href="https://npsp.ci">PSP</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


<!-- modal medium --> 
<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
<script type="text/javascript">
var uploadField = document.getElementById("file");
console.log("fonction")
uploadField.onchange = function() {
    if(this.files[0].size > 22000){
       alert("File is too big!");
       this.value = "";
    };
};
</script>

				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="mediumModalLabel">Nouvelle actualité2</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
						<p>                                
 
                        	<form action="insert.php" method="post" id="myForm" enctype="multipart/form-data">
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Titre actualité</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="titre" placeholder="Text" class="form-control"></div>
                          </div>                          
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Contenu</label></div>
                            <div class="col-12 col-md-9"><textarea name="contenu" id="textarea-input" rows="9" placeholder="Contenu de l'actualité..." class="form-control"></textarea></div>
                          </div> 
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="selectSm" class=" form-control-label">Publier</label></div>
                            <div class="col-12 col-md-9">
                              <select name="publier" id="SelectLm" class="form-control-sm form-control">
                              <?php
                                $member = $userRow['role_id'];

                                if($member == 2) {
                                    echo "<option value='0'>Non</option>";
                                } elseif($member == 1) {
                                    echo "<option value='1'>Oui</option>";
                                    echo "<option value='0'>Non</option>";
                                } 

                                ?>
                              <!--  <option value="0">Non</option>
                               <option value="1">Oui</option>  -->                                                              
                              </select>
                            </div>
                          </div> 
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="selectSm" class=" form-control-label">Accueil</label></div>
                            <div class="col-12 col-md-9">
                              <select name="accueil" id="SelectLm" class="form-control-sm form-control">
                                <option value="0">Non</option>
                                <option value="1">Oui</option>                                                               
                              </select>
                            </div>
                          </div> 
                          picture 
                          <button type="button" class="btn btn-success btn-sm" name="btn-submit"  onclick="myAjax()">sub</button>
                          <input type="file" id="file" onclick="myAjax()"> <br><br>

                            Ajouter une photo<br/>
                             <input type="file" name="photo" id="photo"> <br/><br/>
                             <button type="submit" class="btn btn-success btn-sm" name="btn-submit"  value="submit">Valider</button>
                             <button type="reset" class="btn btn-danger btn-sm" value="submit">Annuler</button>
                            
                        </form><br><br>

                        
                        </p>
                        </div>
						
						
					</div>
				</div>
</div>
            <!-- end modal medium -->
            
            <!-- modal large --><?php foreach ($donnees as $donnee):; $id = $donnee['id']?>
			<div class="modal fade" id="largeModal<?= $id;?>" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="largeModalLabel">Large Modal</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<p>
                            <form action="insert.php" method="post" id="myForm" enctype="multipart/form-data">
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Titre actualité <?= $id;?> </label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="titre" value="<?= $donnee['titre_actu']?>" class="form-control"></div>
                          </div>                          
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Contenu</label></div>
                            <div class="col-12 col-md-9"><textarea name="contenu" id="textarea-input" rows="9" placeholder="" class="form-control"><?= $donnee['contenu']?></textarea></div>
                          </div> 
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="selectSm" class=" form-control-label">Publier</label></div>
                            <div class="col-12 col-md-9">
                              <select name="publier" id="SelectLm" class="form-control-sm form-control" value="<?= $donnee['publier']?>">
                                <option value="0">Non</option>
                                <option value="1">Oui</option>                                                               
                              </select>
                            </div>
                          </div> 
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="selectSm" class=" form-control-label" value="<?= $donnee['accueil']?>">Accueil</label></div>
                            <div class="col-12 col-md-9">
                              <select name="accueil" id="SelectLm" class="form-control-sm form-control">
                                <option value="0">Non</option>
                                <option value="1">Oui</option>                                                               
                              </select>
                            </div>
                          </div> 
                          <div class="row form-group">
                            <div class="col-12 col-md-9"><input type="hidden" id="text-input" name="originPic" placeholder="Text" class="form-control" value="<?=$donnee['path_photo']?>"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col-12 col-md-9"><input type="hidden" id="text-input" name="idPhoto" placeholder="Text" class="form-control" value="<?=$id?>"></div>
                          </div>
                         
                         
                            Ajouter une photo<br/>
                            
                             <input type="file" name="photo" id="photo"> <br/><br/>
                             <button type="submit" class="btn btn-success btn-sm" name="btn-submitM"  value="submit">Valider</button>
                             <button type="reset" class="btn btn-danger btn-sm" value="submit">annuler</button>
                             
                          </form>
							</p>
						</div>
						
					</div>
				</div>
            </div>
            <?php endforeach;?>
            <!-- end modal large -->

             <!-- modal litle --><?php foreach ($donnees as $donnee):; $id = $donnee['id']?>
			<div class="modal fade" id="litleModal<?= $id;?>" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="largeModalLabel">Large Modal</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<p>
                            <form action="insert.php" method="post" id="myForm" enctype="multipart/form-data">
                            <div class="row form-group">
                            <div class="col col-md-8"><label for="selectSm" class=" form-control-label">Voulez vous supprimer cette actualité de la Base de donnée?</label></div>
                        </div>                          
                          
                          <div class="row form-group">
                            <div class="col-12 col-md-9"><input type="hidden" id="text-input" name="idPhoto" placeholder="Text" class="form-control" value="<?=$id?>"></div>
                          </div>
                         
                            
                             <button type="submit" class="btn btn-success btn-sm" name="btn-submitD"  value="submit">Valider</button>
                             <button type="reset" class="btn btn-danger btn-sm" value="submit">annuler</button>
                             
                          </form>
							</p>
						</div>
						
					</div>
				</div>
            </div>
            <?php endforeach;?>
            <!-- end modal litle -->

        <!-- END MAIN CONTENT-->
    <!-- END PAGE CONTAINER-->
</div>

    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>
<!-- end document-->
