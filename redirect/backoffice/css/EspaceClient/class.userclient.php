<?php

require_once('dbconfig.php');

class CLIENTS
{	

	private $conn;
	
	public function __construct()
	{
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
    }
	
	public function runQuery($sql)
	{
		$stmt = $this->conn->prepare($sql);
		return $stmt;
	}
	
	public function registerClient($uname,$ucategorie,$ucommercial, $login, $upass)
	{
		try
		{
			$new_password = password_hash($upass, PASSWORD_DEFAULT);
			
			$stmt = $this->conn->prepare("INSERT INTO client(name,categorie,commercial,login,mot_de_passe) 
		                                               VALUES(:uname, :ucategorie, :ucommercial, :ulogin, :upass)");
												  
			$stmt->bindparam(":uname", $uname);
			$stmt->bindparam(":ucategorie", $ucategorie);
			$stmt->bindparam(":ucommercial", $ucommercial);	
			$stmt->bindparam(":ulogin", $login);
			$stmt->bindparam(":upass", $upass);									  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}

	public function getClient()
	{
		try {
			$stmt = $this->conn->prepare('SELECT * FROM client LIMIT 50');
            $stmt->execute();
            $resultats = $stmt->fetchAll();

            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

	public function compteClient($numero_compte,$code_projet,$code_client)
	{
		try
		{
						
			$stmt = $this->conn->prepare("INSERT INTO compte_client(numero_compte, code_projet, code_client) 
		                                               VALUES(:ucompte, :ucode_projet, :ucode_client)");
												  
			$stmt->bindparam(":ucompte", $numero_compte);
			$stmt->bindparam(":ucode_projet", $code_projet);
			$stmt->bindparam(":ucode_client", $code_client);	
												  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}
	
	public function getCommerciale()
	{
		try {
			$stmt = $this->conn->prepare('SELECT * FROM commerciale');
            $stmt->execute();
            $datas = $stmt->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }
	
	public function insertCommerciale($nom, $prenoms, $fonction, $email, $contact)
	{
		try
		{
						
			$stmt = $this->conn->prepare("INSERT INTO commerciale(nom, prenoms, contact, email, fonction) 
		                                               VALUES(:unom, :uprenoms, :ucontact, :uemail, :ufonction)");
												  
			$stmt->bindparam(":unom", $nom);
			$stmt->bindparam(":uprenoms", $prenoms);
			$stmt->bindparam(":ucontact", $contact);
			$stmt->bindparam(":uemail", $email);
			$stmt->bindparam(":ufonction", $fonction);	
												  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}

	public function updateCommerciale($nom, $prenoms, $fonction, $email, $contact)
	{
		try
		{
						
			$stmt = $this->conn->prepare("UPDATE commerciale SET (id, nom, prenoms, contact, email, fonction)
		                                               VALUES(:uid, :unom, :uprenoms, :ucontact, :uemail, :ufonction)  ");
												  
			$stmt->bindparam(":unom", $nom);
			$stmt->bindparam(":uprenoms", $prenoms);
			$stmt->bindparam(":ucontact", $contact);
			$stmt->bindparam(":uemail", $email);
			$stmt->bindparam(":ufonction", $fonction);	
												  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}

	public function getProductPromotionAll()
	{
		try {
			$stmt = $this->conn->prepare('SELECT code_Produit, designation, unite, autorisation, PrixBaseFormule, debutPromo, finPromo, tauxReduction, photoProduit, libellePromotion 
                                            FROM produits 
											join produits_promo on produits.code_Produit = produits_promo.cProd 
											join promotions on promotions.idPromotion = produits_promo.promoId
											WHERE statut = "1"										 
                                            ORDER BY designation');
			$stmt->execute();
            $resultats = $stmt->fetchAll();
            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }
	

}


?>


