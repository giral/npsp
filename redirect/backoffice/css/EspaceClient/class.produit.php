<?php

require_once('dbconfigclient.php');

class PRODUIT
{	

	private $conn;
	
	public function __construct()
	{
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
    }
	
	public function runQuery($sql)
	{
		$stmt = $this->conn->prepare($sql);
		return $stmt;
	}
	
	public function ajoutProduit($uname,$ucategorie,$ucommercial, $login, $upass)
	{
		try
		{
			$new_password = password_hash($upass, PASSWORD_DEFAULT);
			
			$stmt = $this->conn->prepare("INSERT INTO produit(name,categorie,commercial,login,mot_de_passe) 
		                                               VALUES(:uname, :ucategorie, :ucommercial, :ulogin, :upass)");
												  
			$stmt->bindparam(":uname", $uname);
			$stmt->bindparam(":ucategorie", $ucategorie);
			$stmt->bindparam(":ucommercial", $ucommercial);	
			$stmt->bindparam(":ulogin", $login);
			$stmt->bindparam(":upass", $upass);									  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}

	public function getClient()
	{
		try {
			$stmt = $this->conn->prepare('SELECT * FROM client');
            $stmt->execute();
            $datas = $stmt->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

	public function compteClient($numero_compte,$code_projet,$code_client)
	{
		try
		{
						
			$stmt = $this->conn->prepare("INSERT INTO compte_client(numero_compte, code_projet, code_client) 
		                                               VALUES(:ucompte, :ucode_projet, :ucode_client)");
												  
			$stmt->bindparam(":ucompte", $numero_compte);
			$stmt->bindparam(":ucode_projet", $code_projet);
			$stmt->bindparam(":ucode_client", $code_client);	
												  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}
	
	public function getCommerciale()
	{
		try {
			$stmt = $this->conn->prepare('SELECT * FROM commerciale');
            $stmt->execute();
            $datas = $stmt->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }
	
	public function insertCommerciale($nom, $prenoms, $fonction, $email, $contact)
	{
		try
		{
						
			$stmt = $this->conn->prepare("INSERT INTO commerciale(nom, prenoms, contact, email, fonction) 
		                                               VALUES(:unom, :uprenoms, :ucontact, :uemail, :ufonction)");
												  
			$stmt->bindparam(":unom", $nom);
			$stmt->bindparam(":uprenoms", $prenoms);
			$stmt->bindparam(":ucontact", $contact);
			$stmt->bindparam(":uemail", $email);
			$stmt->bindparam(":ufonction", $fonction);	
												  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}

	public function updateCommerciale($nom, $prenoms, $fonction, $email, $contact)
	{
		try
		{
						
			$stmt = $this->conn->prepare("UPDATE commerciale SET (id, nom, prenoms, contact, email, fonction)
		                                               VALUES(:uid, :unom, :uprenoms, :ucontact, :uemail, :ufonction)  ");
												  
			$stmt->bindparam(":unom", $nom);
			$stmt->bindparam(":uprenoms", $prenoms);
			$stmt->bindparam(":ucontact", $contact);
			$stmt->bindparam(":uemail", $email);
			$stmt->bindparam(":ufonction", $fonction);	
												  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}


	public function doLogin($uname,$umail,$upass)
	{
		try
		{
			$stmt = $this->conn->prepare("SELECT id, name, email, password FROM users WHERE name=:uname OR email=:umail ");
			$stmt->execute(array(':uname'=>$uname, ':umail'=>$umail));
			$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
			if($stmt->rowCount() == 1)
			{
				if(password_verify($upass, $userRow['password']))
				{
					$_SESSION['user_session'] = $userRow['id'];					
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function is_loggedin()
	{
		if(isset($_SESSION['user_session']))
		{
			return true;
		}
	}
	
	public function redirect($url)
	{
		header("Location: $url");
	}
	
	public function doLogout()
	{
		session_destroy();
		unset($_SESSION['user_session']);
		return true;
	}


	public function client()
	{

	}

}


?>


