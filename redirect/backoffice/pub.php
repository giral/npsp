

<?php
include ("header4.php");
$resultats = new CLIENTS();
$datas = $resultats->getCommerciale($id_client);
$flashInfo = $resultats->selectFlash();
$donnees=$resultats->selectArticleBackAll();
$filepath = realpath (dirname(__FILE__));

require_once($filepath."./../../webanalytics.php");
include "./../../websettings.php";

?>

<!DOCTYPE html>
<html lang="en">

<head>
<script src="../../wa.js"></script>
<link rel="stylesheet" href="../../footer.css">

</head>

<body class="animsition"><br/><br/>
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->      

            <!-- WELCOME-->
            <section class="welcome p-t-10">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-7">Bienvenue <?php echo $userRow['nom_client']; ?>
                            </h3>
                            <hr class="line-seprate">
                        </div>
                        
                    </div>
                </div>
            </section>
            <!-- END WELCOME-->

                            

            <!-- STATISTIC-->
            <section class="statistic statistic2">
                <div class="container">
                <h2 class="title-3 text-green">Nos publications</h2><br/>
                    <div class="row">
                            


                <?php
                    $first = true;
                    foreach ( $donnees as $donnee )
                    {
                        if ( $first )
                        {
                            echo '
                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">'.$donnee['titre_article'].'
                                            <small>
                                                <span class="badge badge-danger float-right mt-1">Nouveau</span>
                                            </small>
                                        </strong>
                                    </div>
                                    <div class="card-body">
                                   
                                        <p class="card-text">'.$donnee['description'].' </p><br/>
                                        <p class="card-text"><small>'.$donnee['date_publication'].' </small></p>
                                        <a class="text-danger" href ="'.$donnee['path_file'].'" target ="_blank">Consulter</a>
                                    </div>
                                </div>
                            </div> ';
                            // do something
                            $first = false;
                        }
                        else
                        {
                            echo '
                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">'.$donnee['titre_article'].'
                                           
                                        </strong>
                                    </div>
                                    <div class="card-body">
                                   
                                        <p class="card-text">'.$donnee['description'].' </p><br/>
                                        <p class="card-text"><small>'.$donnee['date_publication'].' </small></p>
                                        <a class="text-danger" href ="'.$donnee['path_file'].'" target ="_blank">Consulter</a>
                                    </div>
                                </div>
                            </div> ';
                        }

                        // do something
                    }
                ?>
                        
                        
                    </div><hr/>
                </div>
            </section>
            <!-- END STATISTIC-->
            

     




            <div class="container">
                <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header" style="background:#385E02">
                                       
                                        <strong class="card-title pl-2 text-white">Votre commercial dédié </strong>
                                    </div>
                                    <div class="card-body">
                                    <?php foreach ($datas as $data):; ?>
                                        <div class="mx-auto d-block">
                                            <img class="rounded-circle mx-auto d-block" src="images/uploads/mahoussi.jpg" alt="Card image cap">
                                            <h5 class="text-sm-center mt-2 mb-1"><?= $data['nom_commerciale'];?></h5>
                                            <div class="location text-sm-center">
                                            <i class="fas fa-user-md"></i> <?= $data['fonction_commerciale'];?><br/>
                                                <i class="fas fa-address-book"></i> <?= $data['contact_commerciale'];?><br/>
                                                <i class="fas fa-at"></i> <?= $data['email_commerciale'];?>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="card-text text-sm-center">
                                            <a href="#">
                                                <i class="fa fa-facebook pr-1"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-twitter pr-1"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-linkedin pr-1"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-pinterest pr-1"></i>
                                            </a>
                                        </div>
                                        <?php endforeach ?>
                                    </div>
                                </div>
                            </div>
        
        
    </div>
</section><br/><br/>
            <!-- END COPYRIGHT-->
        </div>

    </div>

    <div class="footerfixed">
        <div class="footerfixedTitre"><h2>INFO</h2></div>
        <div id="carousel">
        <div class="btn-bar">
            <div id="buttons"><a id="prev" href="#"></a><a id="next" href="#"></a> </div></div>
                <div id="slides">
                    <ul><?php foreach ($flashInfo as $flash):;?>
                        <li class="slide">
                            <div class="quoteContainer">
                                <p class="quote-phrase"><span class="quote-marks"></span> <?= $flash['contenu']?></span>

                                </p>
                            </div>
                        </li><?php endforeach;?>
                    
                    </ul>
                 </div>
        </div>
    </div>

    

<script>
        $(document).ready(function () {
    //rotation speed and timer
    var speed = 5000;
    
    var run = setInterval(rotate, speed);
    var slides = $('.slide');
    var container = $('#slides ul');
    var elm = container.find(':first-child').prop("tagName");
    var item_width = container.width();
    var previous = 'prev'; //id of previous button
    var next = 'next'; //id of next button
    slides.width(item_width); //set the slides to the correct pixel width
    container.parent().width(item_width);
    container.width(slides.length * item_width); //set the slides container to the correct total width
    container.find(elm + ':first').before(container.find(elm + ':last'));
    resetSlides();
    
    
    //if user clicked on prev button
    
    $('#buttons a').click(function (e) {
        //slide the item
        
        if (container.is(':animated')) {
            return false;
        }
        if (e.target.id == previous) {
            container.stop().animate({
                'left': 0
            }, 1500, function () {
                container.find(elm + ':first').before(container.find(elm + ':last'));
                resetSlides();
            });
        }
        
        if (e.target.id == next) {
            container.stop().animate({
                'left': item_width * -2
            }, 1500, function () {
                container.find(elm + ':last').after(container.find(elm + ':first'));
                resetSlides();
            });
        }
        
        //cancel the link behavior            
        return false;
        
    });
    
    //if mouse hover, pause the auto rotation, otherwise rotate it    
    container.parent().mouseenter(function () {
        clearInterval(run);
    }).mouseleave(function () {
        run = setInterval(rotate, speed);
    });
    
    
    function resetSlides() {
        //and adjust the container so current is in the frame
        container.css({
            'left': -1 * item_width
        });
    }
    
});
//a simple function to click next link
//a timer will call this function, and the rotation will begin

function rotate() {
    $('#next').click();
}
</script>
    


    <!-- Jquery JS-->
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

    

</body>

</html>
<!-- end document-->