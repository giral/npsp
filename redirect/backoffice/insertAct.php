<?php
require_once("session.php");
require_once("class.user.php");
require_once("class.articles.php");
require_once("dbconfig.php");


$insertArt = new ARTICLES();



      if(isset($_POST['btn-submitUActivites']))
      {
          $pic= $_POST['originPic'];
          $cov = $_POST['originCov'];
          $tof = filter_var(basename($_FILES["photo"]["name"], FILTER_SANITIZE_STRING));
          $cover = filter_var(basename($_FILES["cover"]["name"], FILTER_SANITIZE_STRING));
      
          $target_dir = "images/uploads/";
          $target_file = $target_dir . basename($_FILES["photo"]["name"]);
          $target_cover = $target_dir . basename($_FILES["cover"]["name"]);
          $uploadOk = 1;
          $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
      
          $id_act = filter_var($_POST['idAct'], FILTER_SANITIZE_NUMBER_INT);
          $page_act = filter_var($_POST['page'], FILTER_SANITIZE_STRING);
          $titre_act = filter_var($_POST['titre_act'], FILTER_SANITIZE_STRING);
          $contenu_act = filter_var($_POST['contenu_act'], FILTER_SANITIZE_STRING);
          $photo_act = $target_file;
          $cover_act = $target_cover;         
      
      
          if(!empty($tof) && !empty($cover)){
          $photo_act = $target_file;
          $cover_act = $target_cover;
          if ($target_file == "images/uploads/") {
              $msg = "cannot be empty";
              $uploadOk = 0;
          } // Check if file already exists
          else if (file_exists($target_file)) {
              $msg = "Sorry, file already exists.";
              $uploadOk = 0;
          }
          else if (file_exists($target_cover)) {
              $msg = "Sorry, file already exists.";
              $uploadOk = 0;
          } // Check file size
          else if ($_FILES["photo"]["size"] > 1000000) {
              $msg = "Sorry, your file is too large.";
              $uploadOk = 0;
          } 
          // Check file size
          else if ($_FILES["cover"]["size"] > 1000000) {
              $msg = "Sorry, your file is too large.";
              $uploadOk = 0;
          } // Check if $uploadOk is set to 0 by an error
          else if ($uploadOk == 0) {
              $msg = "Sorry, your file was not uploaded.";
      
              // if everything is ok, try to upload file
          } else {
              if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {
                  $msg = "The file " . basename($_FILES["photo"]["name"]) . " has been uploaded.";
              }
              if (move_uploaded_file($_FILES["cover"]["tmp_name"], $target_cover)) {
                  $msg = "The file " . basename($_FILES["cover"]["name"]) . " has been uploaded.";
              }
              if($insertArt->updateActivites($id_act,$page_act,$titre_act,$contenu_act,$photo_act,$cover_act)!="")
              {
                  echo "inertion OK";
                  $insertArt->redirect('gestionActivites.php');
  
              }
              else{
                  echo "probleme insertion";
              }
            
          }
      
          }
          elseif(!empty($tof)){
              $photo_act = $target_file; 
              $cover_act = $cov;       
              //var_dump($tof);
              if ($target_file == "images/uploads/") {
                  $msg = "cannot be empty";
                  $uploadOk = 0;
              } // Check if file already exists
              else if (file_exists($target_file)) {
                  $msg = "Sorry, file already exists.";
                  $uploadOk = 0;
              } // Check file size
              else if ($_FILES["photo"]["size"] > 1000000) {
                  $msg = "Sorry, your file is too large.";
                  $uploadOk = 0;
              } // Check if $uploadOk is set to 0 by an error
              else if ($uploadOk == 0) {
                  $msg = "Sorry, your file was not uploaded.";
          
                  // if everything is ok, try to upload file
              } else {
                  if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {
                      $msg = "The file " . basename($_FILES["photo"]["name"]) . " has been uploaded.";
                  }
              }
              if($insertArt->updateActivites($id_act,$page_act,$titre_act,$contenu_act,$photo_act,$cover_act)!="")
              {
                  echo "inertion OK";
                  $insertArt->redirect('gestionActivites.php');
  
              }
              else{
                  echo "probleme insertion";
              }
          }
      
          elseif(!empty($cover)){
              $cover_act = $target_cover;
              $photo_act = $pic;        
              //var_dump($tof);
              if ($target_cover == "images/uploads/") {
                  $msg = "cannot be empty";
                  $uploadOk = 0;
              } // Check if file already exists
              else if (file_exists($target_cover)) {
                  $msg = "Sorry, file already exists.";
                  $uploadOk = 0;
              } // Check file size
              else if ($_FILES["cover"]["size"] > 1000000) {
                  $msg = "Sorry, your file is too large.";
                  $uploadOk = 0;
              } // Check if $uploadOk is set to 0 by an error
              else if ($uploadOk == 0) {
                  $msg = "Sorry, your file was not uploaded.";
          
                  // if everything is ok, try to upload file
              } else {
                  if (move_uploaded_file($_FILES["cover"]["tmp_name"], $target_cover)) {
                      $msg = "The file " . basename($_FILES["cover"]["name"]) . " has been uploaded.";
                  }
              } 
              if($insertArt->updateActivites($id_act,$page_act,$titre_act,$contenu_act,$photo_act,$cover_act)!="")
              {
                  echo "inertion OK";
                  $insertArt->redirect('gestionActivites.php');
  
              }
              else{
                  echo "probleme insertion";
              }
          }
          
      
          else{
              $photo_act = $pic;
              $cover_act = $cov;
              if($insertArt->updateActivites($id_act,$page_act,$titre_act,$contenu_act,$photo_act,$cover_act)!="")
              {
                  echo "inertion OK";
                  $insertArt->redirect('gestionActivites.php');
  
              }
              else{
                  echo "probleme insertion";
              }
            //$insertArt->redirect('gestionActivites.php');
              
          }
          
      
      }
      
      
      if (isset($_POST['btn-submitActivites']))
      {
          $page = filter_var($_POST['page'], FILTER_SANITIZE_STRING);
          $titre_act = filter_var($_POST['titre_act'], FILTER_SANITIZE_STRING);
          $contenu_act = filter_var($_POST['contenu_act'], FILTER_SANITIZE_STRING);
          $target_dir = "images/uploads/";
          $target_file = $target_dir . filter_var(basename($_FILES["photo"]["name"], FILTER_SANITIZE_STRING));
          $target_cover = $target_dir . filter_var(basename($_FILES["cover"]["name"], FILTER_SANITIZE_STRING));
          $uploadOk = 1;
          $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
          $photo_act = $target_file;
          $cover_act = $target_cover;
      
          if ($target_file == "images/uploads/") {
              $msg = "cannot be empty";
              $uploadOk = 0;
          } // Check if file already exists
          else if (file_exists($target_file)) {
              $msg = "Sorry, file already exists.";
              $uploadOk = 0;
          } // Check file size
          else if ($_FILES["photo"]["size"] > 1000000) {
              $msg = "Sorry, your file is too large.";
              $uploadOk = 0;
          } 
          // Check file size
          else if ($_FILES["cover"]["size"] > 1000000) {
              $msg = "Sorry, your file is too large.";
              $uploadOk = 0;
          } // Check if $uploadOk is set to 0 by an error
          else if ($uploadOk == 0) {
              $msg = "Sorry, your file was not uploaded.";
      
              // if everything is ok, try to upload file
          } else {
              if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {
                  $msg = "The file " . basename($_FILES["photo"]["name"]) . " has been uploaded.";
              }
              if (move_uploaded_file($_FILES["cover"]["tmp_name"], $target_cover)) {
                  $msg = "The file " . basename($_FILES["cover"]["name"]) . " has been uploaded.";
              }
          }
      
          if($insertArt->insertActivites($page_act,$titre_act,$contenu_act,$photo_act, $cover_act))
      {
          echo "Données inserées"; 
          $insertArt->redirect('gestionActivites.php');
      }
         else{
             echo "probleme insertion";
         }
      
      }

      if (isset($_POST['deleteAct']))
      {
        $id_act = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);
        $insertArt->disableActivites($id_act);
        $insertArt->redirect('gestionActivites.php');
      }
      

?>


