<?php
	require_once('sessionCustomer.php');
	require_once('class.customer.php');
	$user_logout = new CLIENTS();
	
	if($user_logout->is_loggedinCustomer()!="")
	{
		$user_logout->redirectCustomer('../../accueilClient.php');
	}
	if(isset($_GET['logout']) && $_GET['logout']=="true")
	{
		$user_logout->doLogoutCustomer();
		$user_logout->redirectCustomer('../../index.php');
	}
