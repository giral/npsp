<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Basic Newsletter Sender">
    <meta name="author" content="Aboubacar Ouattara">
    <title>Newsletter</title>

    <!-- Bootstrap core CSS -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../assets/css/wbbtheme.css"/>
    <link href="../assets/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="../assets/css/tinyform.css">
    <script src="../assets/js/tiny.js"></script>

    <!-- Bootstrap theme -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body role="document">


<!-- <?php include('partials/_nav.php'); ?> -->
<?php include('../partials/_flash.php'); ?>
<div class="row" >
    <div class="col-md-12 col-md-offset-1" id="info"></div>
</div>
