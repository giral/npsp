<?php
include ("header4.php");

$resultats = new CLIENTS();
$datas = $resultats->getCommerciale($id_client);
$promos = $resultats->getProductPromotionAll();
$filepath = realpath (dirname(__FILE__));

require_once($filepath."./../../webanalytics.php");
include "./../../websettings.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    
<script src="../../wa.js"></script>
</head>

<body class="animsition"><br/><br/>
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->      

            <!-- WELCOME-->
            <section class="welcome p-t-10">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-7">Bienvenue <?php echo $userRow['nom_client']; ?>
                            </h3>
                            <hr class="line-seprate">
                        </div>
                        
                    </div>
                </div>
            </section>
            <!-- END WELCOME-->

            <!-- STATISTIC-->
            

 <!-- STATISTIC-->
 <section class="statistic statistic2">
                <div class="container"  >
                    <div class="col-md-12">
                    <h3 class="title-3 text-secondary">produits en promotion</h3><br/>

                    <div class="row" >
                    
                    <?php 
                     $promos = $resultats->getProductPromotionAll();
                     /* echo'<pre>';
                     var_dump($promos);
                     echo'<pre>'; */
                     if (empty($promos)){
                        echo '<div class="card">
                        
                        <div class="card-body">
                            <div class="alert alert-danger" role="alert">
                                <h4 class="alert-heading">Promotions terminées</h4>
                                <p>Chers clients de la nouvelle PSP CI, read this important alert message. This example text is going to run a bit longer so
                                    that you can see how spacing within an alert works with this kind of content.</p>
                                <hr>
                                <p class="mb-0">Rendez vous très bientot pour la prohaine promo</p>
                            </div>
                        </div>
                    </div>';

                     }                      
                    
                    else{ 
                        foreach($promos as $promo){
                        $prixPromo = $promo['PrixBaseFormule'] - ($promo['PrixBaseFormule'] * $promo['tauxReduction']) ;
                         
                        echo'
                        <div class="col-md-6 col-lg-3">
                        <div class="statistic__item ">
                        <img src="'.$promo['photoProduit'].'" alt="Nouvelle PSP" style="width: 100%; " /><br/><br/>
                        <p class="title-8 text-danger">'.$promo['code_Produit'].'</p>
                        <div style="height:50px !important;">'.strtolower($promo['designation']).'</div><br/><br/>
                        <h3 class="title-8 text-danger">'.$promo['libellePromotion'].'</h3>
                        <h5 class="title-5">Taux = '.round((float)$promo['tauxReduction'] * 100).'% </h5>

                            
                        </div>
                    </div>
                        
                        
                        ';
                    }
                }
                
                    ?>   
                        
                        
                    </div>
                    </div>
                    
                </div>
                
</section>
            <!-- END STATISTIC-->       
                
<?php 
include ("footer.php")
?>

    <script src="../../assets/web/assets/jquery/jquery.min.js"></script>
  <script src="../../assets/popper/popper.min.js"></script>
  <script src="../../assets/tether/tether.min.js"></script>
  <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../../assets/smooth-scroll/smooth-scroll.js"></script>
  <script src="../../assets/dropdown/js/script.min.js"></script>
  <script src="../../assets/touch-swipe/jquery.touch-swipe.min.js"></script>
  <script src="../../assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js"></script>
  <script src="../../assets/jquery-mb-ytplayer/jquery.mb.ytplayer.min.js"></script>
  <script src="../../assets/jquery-mb-vimeo_player/jquery.mb.vimeo_player.js"></script>
  <script src="../../assets/jarallax/jarallax.min.js"></script>
  <script src="../../assets/theme/js/script.js"></script>
  <script src="../../assets/mobirise-slider-video/script.js"></script>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

    

</body>

</html>
<!-- end document-->
