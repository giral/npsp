<?php
require_once("session.php");
require_once("class.user.php");
require_once("class.articles.php");
require_once("dbconfig.php");


$insertArt = new ARTICLES();



//----------Insertion Organisation-------------------------
if(isset($_POST['btn-submitUOrganisation']))
{
    $pic= $_POST['originPic'];
    $cov = $_POST['originCov'];
    $tof = basename($_FILES["photo"]["name"]);
    $cover = basename($_FILES["cover"]["name"]);

    $target_dir = "images/uploads/";
    $target_file = $target_dir . basename($_FILES["photo"]["name"]);
    $target_cover = $target_dir . basename($_FILES["cover"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

    $idOrg = $_POST['idOrg'];
    $page_org = $_POST['page'];
    $titre_org = $_POST['titre_org'];
    $contenu_org = $_POST['contenu_org'];
    $photo_pre = $target_file;
    $cover_pre = $target_cover;
    /*var_dump($tof);
    var_dump($cover);
    var_dump($cov);
    var_dump($pic);
    var_dump($idOrg);
    var_dump($page_org);
    var_dump($titre_org);
    var_dump($contenu_org);*/

    if(!empty($tof) && !empty($cover)){
    $photo_org = $target_file;
    $cover_org = $target_cover;
    if ($target_file == "images/uploads/") {
        $msg = "cannot be empty";
        $uploadOk = 0;
    } // Check if file already exists
    else if (file_exists($target_file)) {
        $msg = "Sorry, file already exists.";
        $uploadOk = 0;
    }
    else if (file_exists($target_cover)) {
        $msg = "Sorry, file already exists.";
        $uploadOk = 0;
    } // Check file size
    else if ($_FILES["photo"]["size"] > 1000000) {
        $msg = "Sorry, your file is too large.";
        $uploadOk = 0;
    } 
    // Check file size
    else if ($_FILES["cover"]["size"] > 1000000) {
        $msg = "Sorry, your file is too large.";
        $uploadOk = 0;
    } // Check if $uploadOk is set to 0 by an error
    else if ($uploadOk == 0) {
        $msg = "Sorry, your file was not uploaded.";

        // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {
            $msg = "The file " . basename($_FILES["photo"]["name"]) . " has been uploaded.";
        }
        if (move_uploaded_file($_FILES["cover"]["tmp_name"], $target_cover)) {
            $msg = "The file " . basename($_FILES["cover"]["name"]) . " has been uploaded.";
        }
    $insertArt->updateOrganisation($idOrg,$page_org,$titre_org,$contenu_org,$photo_org, $cover_org);
    $insertArt->redirect('gestionOrganisation.php');
    }

    }
    elseif(!empty($tof)){
        $photo_pre = $target_file; 
        $cover_pre = $cov;       
        //var_dump($tof);
        if ($target_file == "images/uploads/") {
            $msg = "cannot be empty";
            $uploadOk = 0;
        } // Check if file already exists
        else if (file_exists($target_file)) {
            $msg = "Sorry, file already exists.";
            $uploadOk = 0;
        } // Check file size
        else if ($_FILES["photo"]["size"] > 1000000) {
            $msg = "Sorry, your file is too large.";
            $uploadOk = 0;
        } // Check if $uploadOk is set to 0 by an error
        else if ($uploadOk == 0) {
            $msg = "Sorry, your file was not uploaded.";
    
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {
                $msg = "The file " . basename($_FILES["photo"]["name"]) . " has been uploaded.";
            }
        }
    $insertArt->updateOrganisation($idOrg,$page_org,$titre_org,$contenu_org,$photo_org, $cover_org);
    $insertArt->redirect('gestionOrganisation.php');        
    }

    elseif(!empty($cover)){
        $cover_org = $target_cover;
        $photo_org = $pic;        
        //var_dump($tof);
        if ($target_cover == "images/uploads/") {
            $msg = "cannot be empty";
            $uploadOk = 0;
        } // Check if file already exists
        else if (file_exists($target_cover)) {
            $msg = "Sorry, file already exists.";
            $uploadOk = 0;
        } // Check file size
        else if ($_FILES["cover"]["size"] > 1000000) {
            $msg = "Sorry, your file is too large.";
            $uploadOk = 0;
        } // Check if $uploadOk is set to 0 by an error
        else if ($uploadOk == 0) {
            $msg = "Sorry, your file was not uploaded.";
    
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["cover"]["tmp_name"], $target_cover)) {
                $msg = "The file " . basename($_FILES["cover"]["name"]) . " has been uploaded.";
            }
        } 
    $insertArt->updateOrganisation($idOrg,$page_org,$titre_org,$contenu_org,$photo_org, $cover_org);
    $insertArt->redirect('gestionOrganisation.php');       
    }
    

    else{
        $photo_org = $pic;
        $cover_org = $cov;
        $insertArt->updateOrganisation($idOrg,$page_org,$titre_org,$contenu_org,$photo_org, $cover_org);
        $insertArt->redirect('gestionOrganisation.php');
        
    }
    

}


if (isset($_POST['btn-submitAOrganisation']))
{
    $page_org = $_POST['page'];
    $titre_org = $_POST['titre_org'];
    $contenu_org = $_POST['contenu_org'];
    $target_dir = "images/uploads/";
    $target_file = $target_dir . basename($_FILES["photo"]["name"]);
    $target_cover = $target_dir . basename($_FILES["cover"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
    $photo_org = $target_file;
    $cover_org = $target_cover;

    if ($target_file == "images/uploads/") {
        $msg = "cannot be empty";
        $uploadOk = 0;
    } // Check if file already exists
    else if (file_exists($target_file)) {
        $msg = "Sorry, file already exists.";
        $uploadOk = 0;
    } // Check file size
    else if ($_FILES["photo"]["size"] > 1000000) {
        $msg = "Sorry, your file is too large.";
        $uploadOk = 0;
    } 
    // Check file size
    else if ($_FILES["cover"]["size"] > 1000000) {
        $msg = "Sorry, your file is too large.";
        $uploadOk = 0;
    } // Check if $uploadOk is set to 0 by an error
    else if ($uploadOk == 0) {
        $msg = "Sorry, your file was not uploaded.";

        // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {
            $msg = "The file " . basename($_FILES["photo"]["name"]) . " has been uploaded.";
        }
        if (move_uploaded_file($_FILES["cover"]["tmp_name"], $target_cover)) {
            $msg = "The file " . basename($_FILES["cover"]["name"]) . " has been uploaded.";
        }
    }

    if($insertArt->insertOrganisation($page_org,$titre_org,$contenu_org,$photo_org, $cover_org))
{
    echo "Données inserées"; 
    $insertArt->redirect('gestionOrganisation.php');
}
   else{
       echo "probleme insertion";
   }

}

//-----------Fin Organisation-------------

/*$pic= $_POST['originPic'];
$cov = $_POST['originCov'];
$tof = basename($_FILES["photo"]["name"]);
$cover = basename($_FILES["cover"]["name"]);

$target_dir = "redirect/backoffice/images/uploads/";
$target_file = $target_dir . basename($_FILES["photo"]["name"]);
$target_cover = $target_dir . basename($_FILES["cover"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

$idPresent = $_POST['idPresent'];
$page = $_POST['page'];
$titre_pre = $_POST['titre_pre'];
$contenu_pre = $_POST['contenu_pre'];

//var_dump($target_file);*/


/*if (isset($_POST['btn-submitUPresent'])){

    $idPresent = $_POST['idPresent'];
    $page = $_POST['page'];
    $titre_pre = $_POST['titre_pre'];
    $contenu_pre = $_POST['contenu_pre'];
    $pic= $_POST['originPic'];
    $cov = $_POST['originCov'];
    $tof = basename($_FILES["photo"]["name"]);
    $cover = basename($_FILES["cover"]["name"]);
    
    var_dump($tof);
    var_dump($cover);
    var_dump($cov);
    var_dump($pic);
    var_dump($idPresent);
    var_dump($page);
    var_dump($titre_pre);
    var_dump($contenu_pre);

}*/


?>


