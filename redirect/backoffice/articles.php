<?php
require_once ("header.php"); 

$filepath = realpath (dirname(__FILE__));

require_once($filepath."./../../webanalytics.php");
include "./../../websettings.php";

$resultat = new ARTICLES();
$donnees=$resultat->selectArticle();
?>

<!DOCTYPE html>
<html lang="fr">
<head>
<script src="../../wa.js"></script>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <!-- PAGE CONTAINER-->
        <div class="page-container"> 
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">               
               
                        <div class="row">
                            <div class="col-md-12">                  

                                <!-- DATA TABLE-->
                                <h3 class="title-5 m-b-35">Gestion des publications</h3>
                                 <div class="table-data__tool">
                                    <div class="table-data__tool-left">                                        
                                        <div class="rs-select2--light rs-select2--sm">
                                            <select class="js-select2" name="time">
                                                <option selected="selected">Récentes</option>
                                                <option value="">1 Mois</option>
                                                <option value="">Plus ancien</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>                                        
                                    </div>
                                    <div class="table-data__tool-right">
                                        <button class="au-btn au-btn-icon au-btn--green au-btn--small"  data-toggle="modal" data-target="#mediumModal" type="button">
                                            <i class="zmdi zmdi-plus"></i>Nouvel Article
                                        </button>                                                                                  
                                    </div>
                                </div> 
                                <div class="table-responsive m-b-40">
                                     <table class="table table-borderless table-data3">
                                        <thead>
                                            <tr>
                                                <th>Date </th>
                                                <th>Titre</th>
                                                <th>Path Fichier</th>
                                                <th>Path Photo</th>
                                                <th>Publier</th>
                                                <th>accueil</th>
                                                <th>action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($donnees as $donnee):; $id = $donnee['id']?>
                                            <tr>
                                            <td> <?= $donnee['date_publication'];?></td>
                                            <td> <?= $donnee['titre_article'];?></td>
                                            <td> <?= $donnee['path_file'];?></td>
                                            <td> <?= $donnee['path_file'];?></td>
                                            <td> <?= $donnee['publier'];?></td>
                                            <td> <?= $donnee['accueil'];?></td>
                                                 <td>                                                
                                                    <div class="table-data-feature">
                                                        <button class="item" data-placement="top" title="Edit" data-target="#largeModal<?= $id;?>" data-toggle="modal" type="button" >
                                                            <i class="zmdi zmdi-edit"></i>
                                                        </button>
                                                        <button class="item" data-toggle="modal" data-placement="top" title="Desactiver" data-target="#litleModal<?= $id;?>" type="button">
                                                        <i class="fas fa-power-off"></i>
                                                        </button>                                                        
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                    
                                </div>
                                <!-- END DATA TABLE-->
                            </div>                   
                        </div>                  
                   

                        
                    </div>
                  </div>
                  
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Nouvelle <a href="https://npsp.ci">PSP</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


<!-- modal medium --> 
<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="mediumModalLabel">Nouvel Article</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
						<p>                                

                        	<form action="insertArt.php" method="post" id="myForm" enctype="multipart/form-data">
                          <input type="hidden" id="text-input" name="target" placeholder="Text" class="form-control" value="front">
                          <input type="hidden" id="text-input" name="page" placeholder="Text" class="form-control" value="articles.php">
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Titre article</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="titre" placeholder="" class="form-control"></div>
                          </div> 
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Description</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="description" placeholder="" class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Ajouter un Fichier</label></div>
                            <div class="col-12 col-md-9"><input type="file" name="fichier" id="fichier"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="selectSm" class=" form-control-label">Publier</label></div>
                            <div class="col-12 col-md-9">
                              <select name="publier" id="SelectLm" class="form-control-sm form-control">
                              <?php
                                $isadmin = $userRow['role_id'];

                                if($isadmin == 2) {
                                    echo "<option value='0'>Non</option>";
                                } elseif($isadmin == 1) {
                                    echo "<option value='1'>Oui</option>";
                                    echo "<option value='0'>Non</option>";
                                } 

                                ?>
                              <!-- <option value="non">Non</option>
                                <option value="oui">Oui</option> -->                                                               
                              </select>
                            </div>
                          </div> 
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="selectSm" class=" form-control-label">Accueil</label></div>
                            <div class="col-12 col-md-9">
                              <select name="accueil" id="SelectLm" class="form-control-sm form-control">
                                <option value="non">Non</option>
                                <option value="oui">Oui</option>                                                               
                              </select>
                            </div>
                          </div> 
                            
                             <button type="submit" class="btn btn-success btn-sm" name="btn-submit"  value="submit">Valider</button>
                             <button type="reset" class="btn btn-danger btn-sm" value="submit">Annuler</button>
                            
                        </form>
                        </p>
                        </div>
						
						
					</div>
				</div>
</div>
            <!-- end modal medium -->
            
            <!-- modal large --><?php foreach ($donnees as $donnee):; $id = $donnee['id']?>
			<div class="modal fade" id="largeModal<?= $id;?>" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="largeModalLabel">Modifier Article</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<p>
                            <form action="insertArt.php" method="post" id="myForm" enctype="multipart/form-data">
                          <div class="row form-group">
                          <input type="hidden" id="text-input" name="page" placeholder="Text" class="form-control" value="articles.php">
                          <input type="hidden" id="text-input" name="id" placeholder="Text" class="form-control" value="<?= $id;?>">

                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Titre Article  </label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="titre" value="<?= $donnee['titre_article']?>" class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Description</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="description"value="<?= $donnee['description']?>" class="form-control"></div>
                          </div> 
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Remplacer Fichier <?= $donnee['path_file']?></label></div>
                            <div class="col-12 col-md-9"><input type="file" name="file" id="file" value="<?= $donnee['path_file']?>"></div>
                          </div>                                               
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="selectSm" class=" form-control-label">Publier</label></div>
                            <div class="col-12 col-md-9">
                              <select name="publier" id="SelectLm" class="form-control-sm form-control" value="">
                              <?php
                                $isadmin = $userRow['role_id'];

                                if($isadmin == 2) {
                                    echo "<option value='non'>Non</option>";
                                } elseif($isadmin == 1) {
                                    echo "<option value='non'>Non</option>";
                                    echo "<option value='oui'>Oui</option>";
                                } 
                                ?>
                               
                                <!-- <option value="non">Non</option>
                                <option value="oui">Oui</option> -->                                                                                                                              
                              </select>
                            </div>
                          </div> 
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="selectSm" class=" form-control-label" value="">Accueil</label></div>
                            <div class="col-12 col-md-9">
                              <select name="accueil" id="SelectLm" class="form-control-sm form-control">
                                <option value="<?= $donnee['accueil']?>"><?= $donnee['accueil']?></option>
                                <option value="non">Non</option>
                                <option value="oui">Oui</option>                                                               
                              </select>
                            </div>
                          </div> 
                         
                            
                             <button type="submit" class="btn btn-success btn-sm" name="submitU"  value="submit">Valider</button>
                             <button type="reset" class="btn btn-danger btn-sm" value="submit">Annuler</button>
                             
                          </form>
							</p>
						</div>
						
					</div>
				</div>
            </div>
            
            <!-- end modal large -->
            <!--litle modal -->
      <div class="modal fade" id="litleModal<?= $id;?>" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="largeModalLabel">Large Modal</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<p>
                            <form action="insertArt.php" method="post" id="myForm" enctype="multipart/form-data">
                            <div class="row form-group">
                            <div class="col col-md-8"><label for="selectSm" class=" form-control-label">Désactiver cette publication?</label></div>
                        </div>                          
                          
                            <input type="hidden" id="text-input" name="page" placeholder="Text" class="form-control" value="articles.php">
                            <input type="hidden" id="text-input" name="id" placeholder="Text" class="form-control" value="<?=$id?>">
                         
                         
                            
                             <button type="submit" class="btn btn-success btn-sm" name="submitD"  value="submit">Valider</button>
                             <button type="reset" class="btn btn-danger btn-sm" value="submit">annuler</button>
                             
                          </form>
							</p>
						</div>
						
					</div>
				</div>
       
       </div>
        <!-- end lilte-->
        <?php endforeach;?>
        <!-- END MAIN CONTENT-->
    <!-- END PAGE CONTAINER-->
</div>

    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>
<!-- end document-->
