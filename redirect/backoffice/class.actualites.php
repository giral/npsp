<?php

require_once('dbconfig.php');

class ACTUALITES{

    private $conn;
	
	public function __construct()
	{
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
    }
	
	
	public function runQuery($sql)
	{
		$stmt = $this->conn->prepare($sql);
		return $stmt;
	}
	
    public function redirect($url)
	{
		header("Location: $url");
	}

	public function insertionActu($titre,$contenu,$photo,$pub,$acc)
	{
		try
		{
			
			$stmt = $this->conn->prepare("INSERT INTO actualites(id,titre_actu,contenu,path_photo,publier,accueil, date_publication) 
													   VALUES (NULL, :titre_actu, :contenu, :path_photo, :publier, :accueil, NOW())");
													  
			$stmt->bindParam(':titre_actu', $titre);
            $stmt->bindParam(':contenu', $contenu);
			$stmt->bindParam(':path_photo', $photo);
			$stmt->bindParam(':publier', $pub);
			$stmt->bindParam(':accueil', $acc);
																		  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}
	
	public function insertionPresentation($titre,$contenu,$photo)
	{
		try
		{
			
			$stmt = $this->conn->prepare("INSERT INTO presentation(id_pre,titre_pre,contenu_pre,path_photo, date_modification) 
													   VALUES (NULL, :titre_pre, :contenu_pre, :path_photo, NOW())");
													  
			$stmt->bindParam(':titre_pre', $titre_pre);
            $stmt->bindParam(':contenu_pre', $contenu_pre);
			$stmt->bindParam(':path_photo', $photo_pre);
																		  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
    }
	
	public function insertdg( $nom, $prenoms, $titre,$contenu,$path_photo)
	{
		try
		{
			
			$stmt = $this->conn->prepare("INSERT INTO dg(id_dg,nom,prenoms,titre_mess,contenu, path_photo) 
													   VALUES (NULL, :nom, :prenoms, :titre, :contenu, :photo)");
													  
			$stmt->bindParam(':nom', $nom);
            $stmt->bindParam(':prenoms', $prenoms);
			$stmt->bindParam(':titre', $titre);
			$stmt->bindParam(':contenu', $contenu);
			$stmt->bindParam(':photo', $path_photo);
																		  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
    }

    
	

	public function selectdg()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM dg');
            $req->execute();
            $datas = $req->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

    public function updateActu()
    {
        try
        {
            $up = $this->conn->prepare('UPDATE `articles` SET `titre_article` = TitreNews1 WHERE `articles`.`id` = 2;');
        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }


	public function selectClients()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM client');
            $req->execute();
            $clients = $req->fetchAll();

            return $clients;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

}
?>