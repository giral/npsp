<?php
include ("header4.php");
$resultats = new CLIENTS();
$datas = $resultats->getCommerciale($id_client);
$projets = $resultats->getProjetCustomer($id_client);
//var_dump($projets);
$filepath = realpath (dirname(__FILE__));

require_once($filepath."./../../webanalytics.php");
include "./../../websettings.php";

?>
<head>
<script src="../../wa.js"></script>
<script type="text/javascript">
$(document).ready(function(){    
    
    $('select#button').change(function() {
        var code = $(this).val();
        $('#myTable').dataTable({
            
        "bDestroy": true,        
        "ajax":{
            "type" : "GET",
            "url" : "projetClient.php",
            "data" : { code },            
            "bProcessing" : true,
            "bServerSide" : true,            
            "dataSrc": function ( json ) {
                
                 return json.data;             

        },  
          
    }, 
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', {
            extend: 'pdfHtml5',
            orientation: 'landscape',
            pageSize: 'LEGAL',
           title: "Person Details"
        }, 'print'
    ],
      
        "columns": [
            { "data": "designation" },
            { "data": "code_Produit" },
            { "data": "unite" },
            { "data": "autorisation" },
            { "data": "PrixProd",  render: $.fn.dataTable.render.number( ' ', ',' ) 
            }
            
        ],
        "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
        
     });     
    });
    });


    
</script>

</head>
<body class="animsition"><br/><br/>
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->

            <!-- WELCOME-->
            <section class="welcome p-t-10">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                        <h3 class="title-7">Bienvenue <?php echo $userRow['nom_client']; ?>
                            </h1>
                            <hr class="line-seprate">
                        </div>
                        
                    </div>
                </div>
            </section>
            <!-- END WELCOME-->

            <section class="statistic statistic2">
                <div class="container">
                    <div class="col-md-12">
                        <!-- USER DATA-->
                                <div class="user-data m-b-30">
                                    <h3 class="title-3 m-b-30">
                                        <i class="zmdi zmdi-account-calendar"></i>Selectionnez un projet</h3>
                                    <div class="filters m-b-45">
                                        <div class="rs-select2--dark text-black rs-select2--md m-r-10 rs-select2--border">  
                                                                                                                     

                                            <select class="js-select2" name="property" id="button">
                                            <option value="">Selectionner</option>                                             
                                            <?php foreach ($projets as $projet):; ?>                                                                                                                   
                                                <option value="<?= $projet['code_projet'] ?>"><?=$projet['libelleProjet']  ?></option>                                               
                                            <?php endforeach; ?>
                                            </select>
                                        
                                            <div class="dropDownSelect2"></div>
                                            <div id="content2"></div>
                                        </div>  
                                        <hr/>                                      
                                    </div>
                                <div class="col-md-12">
                                <!-- DATA TABLE-->
                                <div class="table-responsive m-b-40 text-black" id="content">
                                    <table class="table table-borderless table-data3 " id="myTable">
                                        <thead>
                                            <tr>
                                                <th>Désignation</th>
                                                <th>Code Produit</th>
                                                <th>Unite</th>
                                                <th>Autorisation</th>
                                                <th>Prix</th>                                                
                                            </tr>
                                        </thead>
                                        <tbody id="produit" class="text-black" >
                                            <tr>                                                
                                                <td class="codeProd"> </td>
                                                <td class="designation"></td>
                                                <td class="unite"></td>
                                                <td class="autorisation"></td>
                                                <td class="PrixProd"></td>
                                                                                         
                                                
                                            </tr>
                                            
                                            
                                        </tbody>
                                    </table>
                                </div>    
                            </div>
                                <!-- END DATA TABLE-->
                            
                    </div>                
                </div>
            <br>
        </section>


        <div class="container">
    <div class="col-md-12">
    <div class="card">
                                    <div class="card-header" style="background:#385E02">
                                       
                                        <strong class="card-title pl-2 text-white">Votre commercial dédié </strong>
                                    </div>
                                    <div class="card-body">
                                    <?php foreach ($datas as $data):; ?>
                                        <div class="mx-auto d-block">
                                            <img class="rounded-circle mx-auto d-block" src="images/uploads/mahoussi.jpg" alt="Card image cap">
                                            <h5 class="text-sm-center mt-2 mb-1"><?= $data['nom_commerciale'];?></h5>
                                            <div class="location text-sm-center">
                                            <i class="fas fa-user-md"></i> <?= $data['fonction_commerciale'];?><br/>
                                                <i class="fas fa-address-book"></i> <?= $data['contact_commerciale'];?><br/>
                                                <i class="fas fa-at"></i> <?= $data['email_commerciale'];?>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="card-text text-sm-center">
                                            <a href="#">
                                                <i class="fa fa-facebook pr-1"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-twitter pr-1"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-linkedin pr-1"></i>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-pinterest pr-1"></i>
                                            </a>
                                        </div>
                                        <?php endforeach ?>
                                    </div>
                                </div>
                            </div>
        
        
    </div>
</section><br/><br/>
            
            

            <!-- COPYRIGHT
            <section class="p-t-60 p-b-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2019 <a href="https://colorlib.com">Nouvelle PSP</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
             END COPYRIGHT-->
        </div>

    </div>

    <!-- Jquery JS-->
    
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>
<!-- end document-->