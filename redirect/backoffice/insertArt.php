<?php
require_once("session.php");
require_once("class.user.php");
require_once("class.articles.php");
require_once("dbconfig.php");


$insertArt = new ARTICLES();

if (isset($_POST['btn-submit'])) 
{
    $files_dir = "files/uploads/";
    $target_file =$files_dir . filter_var(basename($_FILES["fichier"]["name"], FILTER_SANITIZE_STRING));
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
    $titre = filter_var($_POST['titre'],FILTER_SANITIZE_STRING);
    $description = filter_var($_POST['description'], FILTER_SANITIZE_STRING);
    $file = $target_file;
    $pub = filter_var($_POST['publier'], FILTER_SANITIZE_STRING);
    $acc = filter_var($_POST['accueil'], FILTER_SANITIZE_STRING);
    $target = filter_var($_POST['target'], FILTER_SANITIZE_STRING);
    $page = filter_var($_POST['page'], FILTER_SANITIZE_STRING);
    //var_dump($page);

    if ($target_file == "files/uploads/") {
        $msg = "cannot be empty";
        $uploadOk = 0;
    } // Check if file already exists
    else if (file_exists($target_file)) {
        $msg = "Sorry, file already exists.";
        $uploadOk = 0;
    } // Check file size
    else if ($_FILES["fichier"]["size"] > 1000000) {
        $msg = "Sorry, your file is too large.";
        $uploadOk = 0;
    } // Check if $uploadOk is set to 0 by an error
    else if ($uploadOk == 0) {
        $msg = "Sorry, your file was not uploaded.";

        // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fichier"]["tmp_name"], $target_file)) {
            $msg = "The file " . basename($_FILES["fichier"]["name"]) . " has been uploaded.";
        }
    }

    if($insertArt->insertionArt($titre, $description, $file, $pub, $acc, $target)!="")
{
    echo "Données inserées"; 
    $insertArt->redirect("$page");
}
   else{
       echo "probleme insertion";
   }

}

else if(isset($_POST['submitU']))
{
    $id = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);
    $titre = filter_var($_POST['titre'], FILTER_SANITIZE_STRING);
    $description = filter_var($_POST['description'], FILTER_SANITIZE_STRING);
    $pub = filter_var($_POST['publier'], FILTER_SANITIZE_STRING);
    $acc = filter_var($_POST['accueil'], FILTER_SANITIZE_STRING); 
    $page = filter_var($_POST['page'], FILTER_SANITIZE_STRING);
    //var_dump($pub); 
    
    if($insertArt->updateArticle($id,$titre,$description,$pub,$acc)!="")
{
    echo "Données inserées"; 
    $insertArt->redirect("$page");
}
   else{
       echo "probleme insertion";
   }

}

elseif(isset($_POST['submitD']))
{
    $page = filter_var($_POST['page'], FILTER_SANITIZE_STRING);
    $id = filter_var($_POST['id'], FILTER_SANITIZE_STRING);
    if($insertArt->disableArticle($id)!="");
    {
      echo "Données inserées"; 
    $insertArt->redirect("$page");  
    }
}

//Insertion de la Video
else if(isset($_POST['submitVideo']))
{
    $titre = filter_var($_POST['titreVideo'], FILTER_SANITIZE_STRING);
    $lien = filter_var($_POST['lienVideo'], FILTER_SANITIZE_STRING);
    $publier = filter_var($_POST['publierVideo'], FILTER_SANITIZE_STRING);
    $accueil = filter_var($_POST['accueilVideo'], FILTER_SANITIZE_STRING); 
    //var_dump($pub); 
    
    if($insertArt->insertVideo($titre, $lien, $publier , $accueil)!="")
{
    echo "Données inserées"; 
    $insertArt->redirect('gestVideos.php');
}
   else{
       echo "probleme insertion";
   }

}

else if(isset($_POST['updateVideo']))
{
    $id = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);
    $titre = filter_var($_POST['titreVideo'], FILTER_SANITIZE_STRING);
    $lien = filter_var($_POST['lienVideo'], FILTER_SANITIZE_STRING);
    $publier = filter_var($_POST['publierVideo'], FILTER_SANITIZE_STRING);
    $accueil = filter_var($_POST['accueilVideo'], FILTER_SANITIZE_STRING);
    
    
    if($insertArt->updateVideo($titre,$lien,$publier,$accueil,$id)!="")
{
    echo "Données inserées"; 
    $insertArt->redirect('gestVideos.php');
}
   else{
       echo "probleme insertion";
   }

}


elseif(isset($_POST['disableVideo']))
{
    $id = $_POST['id'];
    if($insertArt->disableVideo($id)!="")
    {
      echo "Données inserées"; 
    $insertArt->redirect('gestVideos.php');  
    }
    
}

if(isset($_POST['submitPromo']))
{
    $libelle = filter_var($_POST['libelle'], FILTER_SANITIZE_STRING);
    $taux = filter_var($_POST['taux'], FILTER_SANITIZE_STRING) ;
    $debut = $_POST['datepicker'];
    $fin = $_POST['datepicker2'] ;
    $info = filter_var($_POST['info'], FILTER_SANITIZE_STRING);
    
    if($insertArt->insertPromo($libelle, $taux, $debut , $fin, $info)!="")
    {
        echo "Données inserées"; 
    $insertArt->redirect('gestPromotions.php'); 
    }
    
}

if(isset($_POST['updatePromo']))
{
    $libelle = filter_var($_POST['libelle'], FILTER_SANITIZE_STRING);
    $taux = $_POST['taux'] ;
    $debut = $_POST['datepicker3'];
    $fin = $_POST['datepicker4'] ;
    $info = filter_var($_POST['info'], FILTER_SANITIZE_STRING);
    $id = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);

    if($insertArt->updatePromo($libelle, $taux, $debut , $fin, $info, $id)!="")
    {
        $insertArt->redirect('gestPromotions.php');
    }
}

elseif(isset($_POST['disablePromo']))
{
    $id = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);
    if($insertArt->disablePromo($id)!="")
    {
      echo "Données inserées"; 
    $insertArt->redirect('gestPromotions.php');  
    }
    
}

elseif(isset($_POST["promoProduit"]))

{
 if(filter_var($_FILES['file']['name'], FILTER_SANITIZE_STRING))
 {
  $filename = explode(".", $_FILES['file']['name']);
  if($filename[1] == 'csv')
  {
   $handle = fopen($_FILES['file']['tmp_name'], "r");
   while($data = fgetcsv($handle))
   {            
                $promoId = $_POST['id'];
                $cProd = mysqli_real_escape_string($data[0]);
                
                //var_dump($promoId);
                //var_dump($data[0]); 
                if($insertArt->insertProduitPromo($promoId, $data[0])!="")
                {
                echo "Données inserées"; 
                $insertArt->redirect('gestPromotions.php');  
                } 
                //$insertArt->insertProduitPromo($promoId, $data[0]);         
                                         
               
   }
   fclose($handle);

   
  }
 }
}


  
?>


