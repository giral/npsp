<?php
require_once ("header.php"); 

$filepath = realpath (dirname(__FILE__));

require_once($filepath."./../../webanalytics.php");
include "./../../websettings.php";

// if user is logged in and this user is NOT an super-admin user, redirect them to landing page
if (isset($_SESSION['user']) && is_null($_SESSION['user']['role'])) {
    header("location: " . "actualites.php");
  }

$resultat = new ARTICLES();
$donnees=$resultat->selectdg();
?>

<!DOCTYPE html>
<html lang="fr">
<head>
<script src="../../wa.js"></script>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <!-- PAGE CONTAINER-->
        <div class="page-container"> 
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">               
               
                        <div class="row">
                            <div class="col-md-12">                  

                                <!-- DATA TABLE-->
                                <h2 class="title-2 m-b-35 text-black">Mot du DG<hr/></h2>
                                 <div class="table-data__tool">
                                    
                                    <div class="table-data__tool-right">
                                        <!--<button class="au-btn au-btn-icon au-btn--green au-btn--small"  data-toggle="modal" data-target="#mediumModal" type="button">
                                            <i class="zmdi zmdi-plus"></i>Ajouter
                                        </button> -->                                                                                 
                                    </div>
                                </div> 
                                <div class="table-responsive m-b-40">
                                     <table class="table table-borderless table-data3">
                                        <thead>
                                            <tr>
                                                <th>Nom</th>
                                                <th>Prénoms</th>
                                                <th>Titre</th>
                                                <th>Contenu</th>
                                                <th>Path Photo</th>
                                                <th>action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($donnees as $donnee):; $id = $donnee['id_dg']?> 
                                            <tr>
                                            <td> <?= $donnee['nom'];?></td>
                                            <td> <?= $donnee['prenoms'];?></td>
                                            <td> <?= $donnee['titre_mess'];?></td>
                                            <td> <?= substr($donnee['contenu'], 0,200);?></td>
                                            <td> <?= $donnee['path_photo'];?></td>
                                                <td>                                                
                                                    <div class="table-data-feature">
                                                        <button class="item" data-placement="top" title="Edit" data-target="#largeModal<?= $id;?>" data-toggle="modal" type="button" >
                                                            <i class="zmdi zmdi-edit"></i>
                                                        </button>
                                                        <!--<button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </button>-->                                                        
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                    
                                </div>
                                <!-- END DATA TABLE-->
                            </div>                   
                        </div>                  
                   

                        
                    </div>
                  </div>
                  
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Nouvelle <a href="https://npsp.ci">PSP</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


<!-- modal medium -->
<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="mediumModalLabel">Ajouter</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
						<p>                                

                        <form action="insert2.php" method="post" id="myForm" enctype="multipart/form-data">
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nom</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="nom" placeholder="Text" class="form-control"></div>
                          </div> 
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Prénoms</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="prenoms" placeholder="Text" class="form-control"></div>
                          </div> 
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Titre</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="titre" placeholder="Text" class="form-control"></div>
                          </div>                          
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Contenu</label></div>
                            <div class="col-12 col-md-9"><textarea name="contenu" id="textarea-input" rows="9" placeholder="Contenu de l'actualité..." class="form-control"></textarea></div>
                          </div>                           
                          
                            Ajouter une photo<br/>
                             <input type="file" name="photo" id="photo"> <br/><br/>
                             <?php
                                $member = $userRow['role_id'];
                                if($member == 2) {
                                    echo "<span style='color:red'>Desolé vous n'êtes pas autorisé!</span>";
                                } elseif($member == 1) {
                                    echo "
                                    <button type='submit' class='btn btn-success btn-sm' name='btn-submit'  value='submit'>Valider</button>
                                    <button type='reset' class='btn btn-danger btn-sm' value='submit'>Annuler</button>";
                                } 
                                ?>
                             <!-- <button type="submit" class="btn btn-success btn-sm" name="btn-submit"  value="submit">Valider</button>
                             <button type="reset" class="btn btn-danger btn-sm" value="submit">Annuler</button> -->
                            
                        </form>
                        </p>
                        </div>
						
						
					</div>
				</div>
</div>
            <!-- end modal medium -->
            
            <!-- modal large --><?php foreach ($donnees as $donnee):; $id = $donnee['id_dg']?> 
			<div class="modal fade" id="largeModal<?= $id;?>" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="largeModalLabel">Modifier</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<p>
                            <form action="insert2.php" method="post" id="myForm" enctype="multipart/form-data">
                          <div class="row form-group">
                            <input type="hidden" id="text-input" name="id" value="<?=$id?>" class="form-control">
                          </div>
                           
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Titre <?= $id;?> </label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="titre" value="<?= $donnee['titre_mess']?>" class="form-control"></div>
                          </div>                        
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Contenu</label></div>
                            <div class="col-12 col-md-9"><textarea name="contenu" id="textarea-input" rows="9" placeholder="" class="form-control"><?= $donnee['contenu']?></textarea></div>
                          </div> 
                          
                          <?php
                                $member = $userRow['role_id'];
                                if($member == 2) {
                                    echo "<span style='color:red'>Desolé vous n'êtes pas autorisé!</span>";
                                } elseif($member == 1) {
                                    echo "
                                    <button type='submit' class='btn btn-success btn-sm' name='updatedg'  value='submit'>Valider</button>
                                    <button type='reset' class='btn btn-danger btn-sm' value='submit'>Annuler</button>";
                                } 
                                ?>
                           
                             <!-- <button type="submit" class="btn btn-success btn-sm" name="updatedg"  value="submit">Valider</button>
                             <button type="reset" class="btn btn-danger btn-sm" value="submit">Valider</button> -->
                             
                          </form>
							</p>
						</div>
						
					</div>
				</div>
            </div>
            <?php endforeach;?>
            <!-- end modal large -->

        <!-- END MAIN CONTENT-->
    <!-- END PAGE CONTAINER-->
</div>

    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>
<!-- end document-->
