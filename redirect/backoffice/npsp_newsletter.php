<?php
/*
 * DEVELOPED  BY BNS GROUP
 * DIRECTED BY ABOUBACAR OUATTARA
 * REWRITE BY GIRAL ZORO
 */
session_start();
if (file_exists("includes/config.php")){
    include('includes/config.php');
    require ('includes/init.php');
    $q = $db->prepare("CREATE TABLE IF NOT EXISTS `emails` (
    `ID` int(11) unsigned NOT NULL auto_increment,
    `Email` varchar(255) NOT NULL default '',
    `status` ENUM('0','1') NOT NULL DEFAULT '0',
    PRIMARY KEY  (`ID`)
)");
    $q->execute();
    $emails = find_emails();
    require ('partials/_info.php');
    include ('views/index.view.php');
}

else{
    header('Location: install.php');
    exit();
    
}
