<?php
require_once("session.php");
require_once("class.user.php");
require_once("class.articles.php");
require_once("dbconfig.php");


$insertArt = new ARTICLES();



//----------Insertion Presentation-------------------------
if(isset($_POST['btn-submitUPresent']))
{
    $pic= filter_var($_POST['originPic'], FILTER_SANITIZE_STRING);
    $cov = filter_var($_POST['originCov'], FILTER_SANITIZE_STRING);
    $tof = filter_var(basename($_FILES["photo"]["name"], FILTER_SANITIZE_STRING));
    $cover = filter_var(basename($_FILES["cover"]["name"], FILTER_SANITIZE_STRING));

    $target_dir = "images/uploads/";
    $target_file = $target_dir . $tof;
    $target_cover = $target_dir . $cover;
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

    $idPresent = filter_var($_POST['idPresent'], FILTER_SANITIZE_NUMBER_INT);
    $page = filter_var($_POST['page'],FILTER_SANITIZE_STRING);
    $titre_pre =filter_var( $_POST['titre_pre'],FILTER_SANITIZE_STRING);
    $contenu_pre = filter_var($_POST['contenu_pre'],FILTER_SANITIZE_STRING);
    $photo_pre = filter_var($target_file,FILTER_SANITIZE_STRING);
    $cover_pre = filter_var($target_cover,FILTER_SANITIZE_STRING);


    if(!empty($tof) && !empty($cover)){
    $photo_pre = $target_file;
    $cover_pre = $target_cover;
    if ($target_file == "images/uploads/") {
        $msg = "cannot be empty";
        $uploadOk = 0;
    } // Check if file already exists
    else if (file_exists($target_file)) {
        $msg = "Sorry, file already exists.";
        $uploadOk = 0;
    }
    else if (file_exists($target_cover)) {
        $msg = "Sorry, file already exists.";
        $uploadOk = 0;
    } // Check file size
    else if ($_FILES["photo"]["size"] > 1000000) {
        $msg = "Sorry, your file is too large.";
        $uploadOk = 0;
    } 
    // Check file size
    else if ($_FILES["cover"]["size"] > 1000000) {
        $msg = "Sorry, your file is too large.";
        $uploadOk = 0;
    } // Check if $uploadOk is set to 0 by an error
    else if ($uploadOk == 0) {
        $msg = "Sorry, your file was not uploaded.";

        // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {
            $msg = "The file " . basename($_FILES["photo"]["name"]) . " has been uploaded.";
        }
        if (move_uploaded_file($_FILES["cover"]["tmp_name"], $target_cover)) {
            $msg = "The file " . basename($_FILES["cover"]["name"]) . " has been uploaded.";
        }
    $insertArt->updatePresentation($idPresent,$page,$titre_pre,$contenu_pre,$photo_pre, $cover_pre);
    $insertArt->redirect('gestionPresentation.php');
    }

    }
    elseif(!empty($tof)){
        $photo_pre = $target_file; 
        $cover_pre = $cov;       
        //var_dump($tof);
        if ($target_file == "images/uploads/") {
            $msg = "cannot be empty";
            $uploadOk = 0;
        } // Check if file already exists
        else if (file_exists($target_file)) {
            $msg = "Sorry, file already exists.";
            $uploadOk = 0;
        } // Check file size
        else if ($_FILES["photo"]["size"] > 1000000) {
            $msg = "Sorry, your file is too large.";
            $uploadOk = 0;
        } // Check if $uploadOk is set to 0 by an error
        else if ($uploadOk == 0) {
            $msg = "Sorry, your file was not uploaded.";
    
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {
                $msg = "The file " . basename($_FILES["photo"]["name"]) . " has been uploaded.";
            }
        }
    $insertArt->updatePresentation($idPresent,$page,$titre_pre,$contenu_pre,$photo_pre, $cover_pre);
    $insertArt->redirect('gestionPresentation.php');        
    }

    elseif(!empty($cover)){
        $cover_pre = $target_cover;
        $photo_pre = $pic;        
        //var_dump($tof);
        if ($target_cover == "images/uploads/") {
            $msg = "cannot be empty";
            $uploadOk = 0;
        } // Check if file already exists
        else if (file_exists($target_cover)) {
            $msg = "Sorry, file already exists.";
            $uploadOk = 0;
        } // Check file size
        else if ($_FILES["cover"]["size"] > 1000000) {
            $msg = "Sorry, your file is too large.";
            $uploadOk = 0;
        } // Check if $uploadOk is set to 0 by an error
        else if ($uploadOk == 0) {
            $msg = "Sorry, your file was not uploaded.";
    
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["cover"]["tmp_name"], $target_cover)) {
                $msg = "The file " . basename($_FILES["cover"]["name"]) . " has been uploaded.";
            }
        } 
    $insertArt->updatePresentation($idPresent,$page,$titre_pre,$contenu_pre,$photo_pre, $cover_pre);
    $insertArt->redirect('gestionPresentation.php');       
    }
    

    else{
        $photo_pre = $pic;
        $cover_pre = $cov;
        $insertArt->updatePresentation($idPresent,$page,$titre_pre,$contenu_pre,$photo_pre, $cover_pre);
        $insertArt->redirect('gestionPresentation.php');
        
    }
    

}


if (isset($_POST['btn-submitAPresent']))
{
    $page = filter_var($_POST['page'], FILTER_SANITIZE_STRING);
    $titre_pre = filter_var($_POST['titre_pre'], FILTER_SANITIZE_STRING);
    $contenu_pre = filter_var($_POST['contenu_pre'], FILTER_SANITIZE_STRING);
    $target_dir = "images/uploads/";
    $target_file = $target_dir . basename(filter_var($_FILES["photo"]["name"], FILTER_SANITIZE_STRING));
    $target_cover = $target_dir . basename(filter_var($_FILES["cover"]["name"], FILTER_SANITIZE_STRING));
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
    $photo_pre = $target_file;
    $cover_pre = $target_cover;

    if ($target_file == "images/uploads/") {
        $msg = "cannot be empty";
        $uploadOk = 0;
    } // Check if file already exists
    else if (file_exists($target_file)) {
        $msg = "Sorry, file already exists.";
        $uploadOk = 0;
    } // Check file size
    else if ($_FILES["photo"]["size"] > 1000000) {
        $msg = "Sorry, your file is too large.";
        $uploadOk = 0;
    } 
    // Check file size
    else if ($_FILES["cover"]["size"] > 1000000) {
        $msg = "Sorry, your file is too large.";
        $uploadOk = 0;
    } // Check if $uploadOk is set to 0 by an error
    else if ($uploadOk == 0) {
        $msg = "Sorry, your file was not uploaded.";

        // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {
            $msg = "The file " . basename($_FILES["photo"]["name"]) . " has been uploaded.";
        }
        if (move_uploaded_file($_FILES["cover"]["tmp_name"], $target_cover)) {
            $msg = "The file " . basename($_FILES["cover"]["name"]) . " has been uploaded.";
        }
    }

    if($insertArt->insertPresentation($page,$titre_pre,$contenu_pre,$photo_pre, $cover_pre))
{
    echo "Données inserées"; 
    $insertArt->redirect('gestionPresentation.php');
}
   else{
       echo "probleme insertion";
   }

}

//-----------Fin Presentation-------------

/*$pic= $_POST['originPic'];
$cov = $_POST['originCov'];
$tof = basename($_FILES["photo"]["name"]);
$cover = basename($_FILES["cover"]["name"]);

$target_dir = "redirect/backoffice/images/uploads/";
$target_file = $target_dir . basename($_FILES["photo"]["name"]);
$target_cover = $target_dir . basename($_FILES["cover"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

$idPresent = $_POST['idPresent'];
$page = $_POST['page'];
$titre_pre = $_POST['titre_pre'];
$contenu_pre = $_POST['contenu_pre'];

//var_dump($target_file);*/


/*if (isset($_POST['btn-submitUPresent'])){

    $idPresent = $_POST['idPresent'];
    $page = $_POST['page'];
    $titre_pre = $_POST['titre_pre'];
    $contenu_pre = $_POST['contenu_pre'];
    $pic= $_POST['originPic'];
    $cov = $_POST['originCov'];
    $tof = basename($_FILES["photo"]["name"]);
    $cover = basename($_FILES["cover"]["name"]);
    
    var_dump($tof);
    var_dump($cover);
    var_dump($cov);
    var_dump($pic);
    var_dump($idPresent);
    var_dump($page);
    var_dump($titre_pre);
    var_dump($contenu_pre);

}*/


?>


