<?php
include('partials/_header.php'); ?>
<div class="col-md-10 col-md-offset-1">
    <article class="media status-media">
        <div class="media-body">
            <h4 class="center-block"> Charte de Basic Newsletter Sender</h4>
            <p>
                <kbd>Basic Newsletter Sender (BNS)</kbd> est soummis à la loi
                <code>Burkinabé</code>, par conséquent, il est interdit <br>
                d'<strong>envoyer du contenu illégal aux
                    yeux de la loi</strong> , sont considérés illégaux entres
                autres les contenus suivants : <br> <span class="text-danger">contenu à caractère raciste,
               contenu diffamatoire, contenu incitant à la haine,
               à la violence,<br>  contenu violant les droits
               d'auteur.</span> <br/>
                À cette liste non exhaustive vient s'ajouter l'interdiction
                d'envoyer du contenu à caractère sexuel.<br/>
                Cette liste étant non exhaustive, nous faisons appel à votre
                bon sens pour <br> discerner ce que vous pouvez <code>envoyer
                    de ce que vous ne pouvez pas envoyer</code>.<br/>
                Les propos insultants, dégradants, agressifs ou tout
                comportement néfaste <br> à une ambiance correcte sont interdits.<br/>
                Certes <kbd> <a href="mailto:Abouba181@gmail.com">Aboubacar Ouattara </a> et Basic Newsletter Sender
                </kbd> déclinent toutes <br>
                <code>responsabilité d'une mauvaise utilisation de cette Application.</code>

            </p>
        </div>
    </article>
</div>


<?php
include('partials/_footer.php'); ?>
