<?php
require_once ("header3.php"); 

$resultat = new ARTICLES();
$promos=$resultat->selectPromo();

$filepath = realpath (dirname(__FILE__));

require_once($filepath."./../../webanalytics.php");
include "./../../websettings.php";

?>

<!DOCTYPE html>
<html lang="fr">
<head>
<script src="../../wa.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

<script>
    $(document).ready(function(){
    $('#tableProduits').DataTable({
    ajax: {
        url: 'prod.php',
        dataSrc: 'data',
        datatype : 'json',
        /*success: function(data){
                console.log(data);
             }*/
        
    },
    columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]],
    columns: [
        { data: null, defaultContent: '' },
        { data: 'code_Produit'},
        { data: 'designation'}, 
        { data: 'unite' },
        { data: 'autorisation' },
        { data: 'PrixBaseFormule'},
        
                    
    ]
   
});   
    
});
    </script> 
    
</head>
<body class="animsition">
    <div class="page-wrapper">
        <!-- PAGE CONTAINER-->
        <div class="page-container"> 
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">               
               
                        <div class="row">
                            <div class="col-md-12">                  

                                <!-- DATA TABLE-->
                                <h3 class="title-5 m-b-35">Gestion des promotions</h3>
                                 <div class="table-data__tool">
                                    <div class="table-data__tool-left">                                        
                                                                                
                                    </div>
                                    <div class="table-data__tool-right">
                                    <?php
                                            $member = $userRow['role_id'];
                                            if($member == 2) {
                                                echo "
                                                <span style='color:red'>Vous n'êtes pas autorisé à faire des promotions!</span>";
                                            } elseif($member == 1) {
                                                echo "
                                                <button class='au-btn au-btn-icon au-btn--green au-btn--small'  data-toggle='modal' data-target='#mediumModal' type='button'>
                                                <i class='zmdi zmdi-plus'></i>Nouvelle promotion
                                                </button>";  
                                            } 
                                        ?>
                                        <!-- <button class="au-btn au-btn-icon au-btn--green au-btn--small"  data-toggle="modal" data-target="#mediumModal" type="button">
                                            <i class="zmdi zmdi-plus"></i>Nouvelle promotion
                                        </button> -->                                                                                  
                                    </div>
                                </div> 
                                <div class="table-responsive m-b-40">
                                     <table class="table table-borderless table-data3">
                                        <thead>
                                            <tr>
                                                <th>Libelle promotion </th>
                                                <th>Taux de reduction</th>
                                                <th>Date de debut</th>
                                                <th>Date de fin</th>
                                                <th>Infos</th>
                                                <th>Actions</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($promos as $promo):; $id = $promo['idPromotion']?> 
                                            <tr>
                                            <td> <?= $promo['libellePromotion'];?></td>
                                            <td> <?= $promo['tauxReduction'];?>%</td>
                                            <td> <?= $promo['debutPromo'];?></td>
                                            <td> <?= $promo['finPromo'];?></td>
                                            <td> <?= $promo['infoPromo'];?></td>
                                                 <td>                                                
                                                    <div class="table-data-feature">
                                                        <button class="item" data-placement="top" title="Edit" data-target="#largeModal<?= $id;?>" data-toggle="modal" type="button" >
                                                        <i class="zmdi zmdi-edit"></i>
                                                        </button>
                                                        <?php
                                                            $member = $userRow['role_id'];
                                                            if($member == 2) {
                                                                echo "?";
                                                            } elseif($member == 1) {
                                                                echo "
                                                                <button class='item' data-toggle='modal' data-placement='top' title='Desactiver' data-target='#litleModal<?= $id;?>' type='button'>
                                                                <i class='fas fa-power-off'></i>
                                                                </button> 
                                                                <button class='item' data-toggle='modal' data-placement='top' title='Desactiver' data-target='#scrollmodall<?= $id;?>' type='button',>
                                                                <i class='far fa-plus-square'></i>
                                                                </button>";  
                                                            } 
                                                        ?>
                                                        <!-- <button class="item" data-toggle="modal" data-placement="top" title="Desactiver" data-target="#litleModal<?= $id;?>" type="button">
                                                        <i class="fas fa-power-off"></i>
                                                        </button> 
                                                        <button class="item" data-toggle="modal" data-placement="top" title="Desactiver" data-target="#scrollmodall<?= $id;?>" type="button">
                                                        <i class="far fa-plus-square"></i>
                                                        </button> -->                                                        
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                    
                                </div>
                                <!-- END DATA TABLE-->
                            </div>                   
                        </div>                  
                   

                        
                    </div>
                  </div>
                  
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Nouvelle <a href="https://npsp.ci">PSP</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


<!-- modal medium --> 
<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="mediumModalLabel">Nouvelle Promotion</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
						<p>                                

                            <form action="insertArt.php" method="post" id="myForm" enctype="multipart/form-data">
                            <!--<input type="hidden" name="statut" value="en cours">-->
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Libelle Promotion</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="libelle" placeholder="" class="form-control"></div>
                          </div> 
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Taux de reduction</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="taux" placeholder="" class="form-control"></div>
                          </div>
                         
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Date de debut</label></div>
                            <div class="col-12 col-md-9">
                            <input id="datepicker" data-format="yyyy/mm/dd" width="276" name="datepicker" value="Date" />
                                <script>
                                    $('#datepicker').datepicker({
                                        uiLibrary: 'bootstrap4'
                                    });
                                </script>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Date de fin</label></div>
                            <div class="col-12 col-md-9">
                            <input id="datepicker2" data-format="yyyy/mm/dd" width="276" name="datepicker2" value="Date" />
                                <script>
                                    $('#datepicker2').datepicker({
                                        uiLibrary: 'bootstrap4'
                                    });
                                </script>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Infos Promotion</label></div>
                            <div class="col-12 col-md-9"><textarea name="info" id="textarea-input" rows="9" placeholder="" class="form-control"></textarea></div>
                          </div> 
                          
                            
                             <button type="submit" class="btn btn-success btn-sm" name="submitPromo"  value="submit">Valider</button>
                             <button type="reset" class="btn btn-danger btn-sm" value="submit">Annuler</button>
                            
                        </form>
                        </p>
                        </div>
						
						
					</div>
				</div>
</div>
            <!-- end modal medium -->
            
            <!-- modal large --><?php foreach ($promos as $promo):; $id = $promo['idPromotion']?> 
			<div class="modal fade" id="largeModal<?= $id;?>" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="largeModalLabel">Modifier Article</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<p>
                            <form action="insertArt.php" method="post" id="myForm" enctype="multipart/form-data">
                          <div class="row form-group">
                          <input type="hidden" id="text-input" name="id" placeholder="Text" class="form-control" value="<?= $id;?>">

                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Libelle promotion  </label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="libelle" value="<?= $promo['libellePromotion']?>" class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Taux de reduction</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="taux"value="<?= $promo['tauxReduction']?>" class="form-control"></div>
                          </div> 
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Date de debut</label></div>
                            <div class="col-12 col-md-9">
                            <input id="datepicker3<?= $id;?>" data-format="yyyy/mm/dd" width="276" name="datepicker3" value="<?= $promo['debutPromo']?>" />
                                <script>
                                    $('#datepicker3<?= $id;?>').datepicker({
                                        uiLibrary: 'bootstrap4'
                                    });
                                </script>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Date de fin</label></div>
                            <div class="col-12 col-md-9">
                            <input id="datepicker4<?= $id;?>" data-format="yyyy/mm/dd" width="276" name="datepicker4" value="<?= $promo['finPromo']?>" />
                                <script>
                                    $('#datepicker4<?= $id;?>').datepicker({
                                        uiLibrary: 'bootstrap4'
                                    });
                                </script>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Infos Promotion</label></div>
                            <div class="col-12 col-md-9"><textarea name="info" id="textarea-input" rows="9" placeholder="" class="form-control"><?= $promo['infoPromo']?></textarea></div>
                          </div>
                          <?php
                                $member = $userRow['role_id'];
                                if($member == 2) {
                                    echo "
                                    <span style='color:red'>Vous n'êtes pas autorisé à faire des modifications!</span>";
                                } elseif($member == 1) {
                                    echo "
                             <button type='submit' class='btn btn-success btn-sm' name='updatePromo'  value='submit'>Valider</button>
                             <button type='reset' class='btn btn-danger btn-sm' value='submit'>Annuler</button>";  
                                } 
                                ?>
                            
                             <!-- <button type="submit" class="btn btn-success btn-sm" name="updatePromo"  value="submit">Valider</button>
                             <button type="reset" class="btn btn-danger btn-sm" value="submit">Annuler</button> -->
                             
                          </form>
							</p>
						</div>
						
					</div>
				</div>
            </div>
            
            <!-- end modal large -->
            <!--litle modal -->
      <div class="modal fade" id="litleModal<?= $id;?>" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="largeModalLabel">Modifier Statut promotion</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<p>
                            <form action="insertArt.php" method="post" id="myForm" enctype="multipart/form-data">
                            <div class="row form-group">
                            <div class="col col-md-8"><label for="selectSm" class=" form-control-label">Terminer cette promotion maintenant?</label></div>
                        </div>                          
                          
                         
                            <input type="hidden" id="text-input" name="id" placeholder="Text" class="form-control" value="<?=$id?>">
                         
                            <?php
                                $member = $userRow['role_id'];
                                if($member == 2) {
                                    echo "
                                    <span style='color:red'>Vous n'êtes pas autorisé à faire des modifications!</span>";
                                } elseif($member == 1) {
                                    echo "
                             <button type='submit' class='btn btn-success btn-sm' name='disablePromo' value='submit'>Valider</button>
                             <button type='reset' class='btn btn-danger btn-sm' value='submit'>Annuler</button>";  
                                } 
                                ?>
                            
                             <!-- <button type="submit" class="btn btn-success btn-sm" name="disablePromo" value="submit">Valider</button>
                             <button type="reset" class="btn btn-danger btn-sm" value="submit">annuler</button> -->
                             
                          </form>
							</p>
						</div>
						
					</div>
				</div>
       
       </div>
        <!-- end lilte-->
        <?php endforeach;?>


        <!-- modal scroll -->
        <?php foreach ($promos as $promo):; $id = $promo['idPromotion']?> 
			<div class="modal fade" id="scrollmodall<?= $id;?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="scrollmodalLabel"><?$id?>Ajouter Produits à la promotion</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
                        </div>
                                
						<div class="modal-body">
							<p>
                            <div class="table-responsive m-b-30">
                            <form method="post" id="myForm" action="insertArt.php" enctype="multipart/form-data">
                                    <label><h6 class="text-danger">Chargez fichier csv</h6></label><br/>
                                    <input type="hidden" name="id" value="<?=$id?>"/>
                                    <input type="file" name="file" /> 
                                    <input type="submit" name="promoProduit" value="Import" class="btn btn-info" />
                                </form><br/><br/>
                                     <table class="table table-borderless table-data3" id="tableProduits">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Code</th>
                                            <th>Designation</th>
                                            <th>Unité</th>
                                            <th>Autorisation</th>
                                            <th>prix</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="tr-shadow">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <!--<td>
                                                <div class="table-data-feature">
                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Send">
                                                        <i class="zmdi zmdi-mail-send"></i>
                                                    </button>
                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                        <i class="zmdi zmdi-edit"></i>
                                                    </button>
                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="zmdi zmdi-delete"></i>
                                                    </button>
                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="More">
                                                        <i class="zmdi zmdi-more"></i>
                                                    </button>
                                                </div>
                                            </td>-->
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>							
							</p>
						</div>
						
					</div>
				</div>
            </div>
            <?php endforeach;?>
            <!-- end modal scroll -->
            
        <!-- END MAIN CONTENT-->
    <!-- END PAGE CONTAINER-->
</div>

    </div>

   <!-- Bootstrap JS-->
   <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>
<!-- end document-->
