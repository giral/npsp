<?php
class Database
{   
    private $host = "localhost";
    private $db_name = "npsp-ci-test";
    private $username = "root";
    private $password = "30031991";
    public $conn;
    /* private $host = "localhost";
    private $db_name = "npsp-ci";
    private $username = "npsp";
    private $password = "m2Ae7yF6U53qMfcC"; */
    
     
    public function dbConnection()
	{
     
	    $this->conn = null;    
        try
		{
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->conn->exec("set names utf8");
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
        }
		catch(PDOException $exception)
		{
            echo "Connection error: " . $exception->getMessage();
        }
         
        return $this->conn;
    }
}
?>