    $(document).ready(function(){ 
    $('#tableClient').DataTable( {
    ajax: {
        url: 'listeClient.php',
        dataSrc: 'data',
        datatype : 'json',
        
    },
    columns: [
        { data: 'id_client' },
        { data: 'nom_client', 
            "render": function(data, type, row, meta){
            if(type === 'display'){
              
              $('#nomClient').val(data);
                data = '<button class="item" data-placement="top" title="Edit" data-target="#mediumModal" data-toggle="modal" type="button" >'+data+'</button>';
            }

            return data;
         }
        },
        { data: 'categorie_commerciale' },
        { data: 'login' },
        { data: 'email' },
        { data: 'nom_commerciale'},
        {
                "class":          "details-control",
                "orderable":      false,
                "data":           null,
                "defaultContent": ""
            },        
    ]
   
} );



$("#myModal").on('show.bs.modal', function (e) {
    var triggerLink = $(e.relatedTarget);
    var id = triggerLink.data("id");
    var title = triggerLink.data("title");
    var name = triggerLink.data("name");
    var cover_small = triggerLink.data("cover_small");
  
    
    $("#modalTitle").text(title);
    $(this).find(".modal-body").html("<h5>id: "+id+"</h5><img src='"+cover_small+"'/>");
});

$.ajax({
    url: 'clientById.php',
    type: 'GET',
    data: {id},
    dataType: 'json',
    success: function(data){ 
      console.log( data );
      
    }
  });
});