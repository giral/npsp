$(document).ready(function(){   
   
        $('#Client').dataTable({
            "columnDefs": [{
                "defaultContent": "-",
                "targets": "_all"
              }],
        "bDestroy": true,
        ajax: {
            url: "listeClient.php",
            dataSrc: 'data'
        },        
       
    /*dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', {
            extend: 'pdfHtml5',
            orientation: 'landscape',
            pageSize: 'LEGAL',
           title: "Person Details"
        }, 'print'
    ],*/
      
        "columns": [
            { "data": "nom_client" },
            { "data": "categorie_commerciale" },
            { "data": "login" },
            { "data": "email" },
            { "data": "nom_commerciale" }
            
        ],
        "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
        
     });     
    });


    