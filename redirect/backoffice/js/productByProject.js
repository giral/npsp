$(document).ready(function(){    
    
    $('select#button').change(function() {
        var code = $(this).val();
        $('#myTable').dataTable({
            "columnDefs": [{
                "defaultContent": "-",
                "targets": "_all"
              }],
        "bDestroy": true,        
        "ajax":{
            "type" : "GET",
            "url" : "projetClient.php",
            "data" : { code },            
            "bProcessing" : true,
            "bServerSide" : true,            
            "dataSrc": function ( json ) {
                
                 return json.data;             

        },  
          
    }, 
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', {
            extend: 'pdfHtml5',
            orientation: 'landscape',
            pageSize: 'LEGAL',
           title: "Person Details"
        }, 'print'
    ],
      
        "columns": [
            { "data": "designation" },
            { "data": "code_Produit" },
            { "data": "unite" },
            { "data": "autorisation" },
            { "data": "PrixProd" }
            
        ],
        "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
        
     });     
    });
    });


    