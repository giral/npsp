<!-- modal large -->

<!DOCTYPE html>
<html lang="fr">
<head>
<script src="../../wa.js"></script>
</head>
<body>
<?php foreach ($donnees as $donnee): ; $id = $donnee['id']; ?>
			<div class="modal fade" id="largeModal<?= $id;?>" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="largeModalLabel">Modifier un article</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<p>
                      <form action="insert.php" method="post" id="myForm" enctype="multipart/form-data">
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Titre actualité </label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="titre" value="<?= $donnee['titre_article']?>" class="form-control"></div>
                          </div>                          
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Contenu</label></div>
                            <div class="col-12 col-md-9"><textarea name="contenu" id="textarea-input" rows="9" placeholder="<?= $donnee['contenu']?>" class="form-control"></textarea></div>
                          </div> 
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="selectSm" class=" form-control-label">Publier</label></div>
                            <div class="col-12 col-md-9">
                              <select name="publier" id="SelectLm" class="form-control-sm form-control" value="<?= $donnee['publier']?>">
                                <option value="Non">Non</option>
                                <option value="Oui">Oui</option>                                                               
                              </select>
                            </div>
                          </div> 
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="selectSm" class=" form-control-label" value="<?= $donnee['accueil']?>">Accueil</label></div>
                            <div class="col-12 col-md-9">
                              <select name="accueil" id="SelectLm" class="form-control-sm form-control">
                                <option value="Non">Non</option>
                                <option value="Oui">Oui</option>                                                               
                              </select>
                            </div>
                          </div> 
                          <?php endforeach;?>
                            Ajouter une photo<br/>
                             <input type="file" name="photo" id="photo"> <br/><br/>
                             <button type="submit" class="btn btn-success btn-sm" name="btn-submit"  value="submit">Valider</button>
                             <button type="reset" class="btn btn-danger btn-sm" value="submit">Valider</button>
                             
                      </form>
							</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
							<button type="button" class="btn btn-primary">Confirm</button>
						</div>
					</div>
				</div>
			</div>
			<!-- end modal large -->


<!-- modal medium Empty-->

<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="mediumModalLabel">Ajouter un article</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<p>
                            
						<form action="insert.php" method="post" id="myForm" enctype="multipart/form-data">
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Titre actualité</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="titre" placeholder="Text" class="form-control"></div>
                          </div>                          
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Contenu</label></div>
                            <div class="col-12 col-md-9"><textarea name="contenu" id="textarea-input" rows="9" placeholder="Contenu de l'actualité..." class="form-control"></textarea></div>
                          </div> 
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="selectSm" class=" form-control-label">Publier</label></div>
                            <div class="col-12 col-md-9">
                              <select name="publier" id="SelectLm" class="form-control-sm form-control">
                                <option value="Non">Non</option>
                                <option value="Oui">Oui</option>                                                               
                              </select>
                            </div>
                          </div> 
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="selectSm" class=" form-control-label">Accueil</label></div>
                            <div class="col-12 col-md-9">
                              <select name="accueil" id="SelectLm" class="form-control-sm form-control">
                                <option value="Non">Non</option>
                                <option value="Oui">Oui</option>                                                               
                              </select>
                            </div>
                          </div> 
                            Ajouter une photo<br/>
                             <input type="file" name="photo" id="photo"> <br/><br/>
                             <button type="submit" class="btn btn-success btn-sm" name="btn-submit"  value="submit">Valider</button>
                             <button type="reset" class="btn btn-danger btn-sm" value="submit">Valider</button>
                            
                        </form>
							</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
							<button type="button" class="btn btn-primary">Confirm</button>
						</div>
					</div>
				</div>
			</div>
			<!-- end modal medium -->
      </body>
      </html>