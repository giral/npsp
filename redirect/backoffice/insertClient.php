<?php
require_once("session.php");
require_once("class.userclient.php");
require_once("dbconfig.php");

$insertBd = new CLIENTS();


/*$postData = array (
       'nom_client' => $_POST['nomClient'],	
       'categorie_prix_client' => $_POST['catPrix'], 	
       'categorie_commerciale' => $_POST['catComm'],
       'email' => $_POST['email'], 	
       'login' => $_POST['login'], 	
       'mot_de_passe' => $_POST['password'] , 
       'id_commerciale' => $_POST['select'],
       'projet' => $_POST['checkboxProjet']
    
);

var_dump($postData);*/

if(isset($_POST['btn-submit']))
{
    $nom_client = filter_var($_POST['nomClient'], FILTER_SANITIZE_STRING);
    $categorie_prix = filter_var($_POST['catPrix'], FILTER_SANITIZE_STRING);
    $categorie_comm = filter_var($_POST['catComm'], FILTER_SANITIZE_STRING);
    $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
    $login = filter_var($_POST['login'], FILTER_SANITIZE_STRING);
    $upass = $_POST['password'];
    $commercial = filter_var($_POST['select'], FILTER_SANITIZE_STRING);
    $insertBd->registerClient($nom_client, $categorie_prix, $categorie_comm, $login, $upass, $email, $commercial );
    /*$codeProjet = $_POST['checkboxProjet'];
    foreach ($codeProjet as $code ) {
        if($insertBd->registerProjetClient($code, $login)!="")
        {
            echo "Données inserées"; 
            
        }
           else{
               echo "probleme insertion";
           }
    
}*/
$insertBd->redirect('client.php');
}

if(isset($_POST['submitU']))
{
    $id_client = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);
    $nom_client = filter_var($_POST['nomClient'], FILTER_SANITIZE_STRING);
    $categorie_prix = filter_var($_POST['catPrix'], FILTER_SANITIZE_STRING);
    $categorie_comm = filter_var($_POST['catComm'], FILTER_SANITIZE_STRING);
    $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
    $login = filter_var($_POST['login'], FILTER_SANITIZE_STRING);
    $commercial = filter_var($_POST['select'], FILTER_SANITIZE_STRING);
    $insertBd->updateClient($id_client, $nom_client, $categorie_prix, $categorie_comm, $login, $email, $commercial);
    $insertBd->redirect('client.php');        
    
}

if(isset($_POST['submitP'])){
    $id_client = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);    
    $insertBd->updateProjetClient($id_client);
    $codeProjet = filter_var($_POST['checkboxProjet'], FILTER_SANITIZE_STRING);
    //var_dump($id_client);
    foreach ($codeProjet as $code ) {
        //var_dump($code)
        $insertBd->registerProjetClient($id_client,$code);        
    }
    $insertBd->redirect('client.php');
}

if(isset($_POST['submitD']))
{
    $id_client = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);
    $insertBd->deleteClient($id_client);
   
    $insertBd->redirect('client.php');  
    
}



//var_dump($codeProjet);




/*
if($insertBd->registerClient($nom_client, $categorie_prix, $categorie_comm, $login, $upass, $email, $commercial, $codeProjet )!="")
{
   
    echo "Données inserées"; 
    $insertBd->redirect('utilisateurs.php');
}
   else{
       echo "probleme insertion";
   }



?>


