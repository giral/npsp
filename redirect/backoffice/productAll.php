<?php
 /* $conn = new mysqli("localhost", "root", "27433003", "npsp-ci-test"); 
  */
 
 
$filepath = realpath (dirname(__FILE__));

require_once($filepath."./../../webanalytics.php");
include "./../../websettings.php";

require_once("header3.php"); 
    // if user is logged in and this user is NOT an super-admin user, redirect them to landing page
    function isAdmin($user_id) {
        try {
            $sql = "SELECT * FROM users WHERE id=? AND role_id IS NOT NULL LIMIT 1";
            $user = getSingleRecord($sql, 'i', [$user_id]); // get single user from database
             if (!empty($user)) {
               return true;
             } else {
               return false;
                      }
            }
            catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    } 
    
 /*  */ function getSingleRecord($sql, $types, $params) {
    global $conn;
    $stmt = $conn->prepare($sql);
    $stmt->bind_param($types, ...$params);
    $stmt->execute();
    $result = $stmt->get_result();
    $user = $result->fetch_assoc();
    $stmt->close();
    return $user;
  }
    
    


$resultats = new CLIENTS();
 

if(isset($_POST["submit"]))
{
 if($_FILES['file']['name'])
 {
  $filename = explode(".", $_FILES['file']['name']);
  if($filename[1] == 'csv')
  {
   $handle = fopen($_FILES['file']['tmp_name'], "r");
   while($data = fgetcsv($handle))
   {
                $code = mysqli_real_escape_string($data[0]);  
                $designation = mysqli_real_escape_string($data[1]);
                $unite = mysqli_real_escape_string($data[2]);
                $autorisation = mysqli_real_escape_string($data[3]);
                $categorie = mysqli_real_escape_string($data[4]);
                $quantite = mysqli_real_escape_string($data[5]);
                $stock = mysqli_real_escape_string($data[6]);
                $prix = mysqli_real_escape_string($data[7]);
                $projet = mysqli_real_escape_string($data[8]);
                //var_dump($data);               
                if($resultats->insertProduct($data[0],$data[1],$data[2],$data[3],$data[4],$data[5],$data[6],$data[7],$data[8])!="")
                {
                   //var_dump($data);
                  echo "<script>alert('Import done');</script>";
                }
                else
                {
                  echo "<script>alert('Probleme CSV');</script>";
                }
               
   }
   fclose($handle);

   
  }
 }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    

    <script>
    $(document).ready(function(){
    $('#tableProduits').DataTable({
    ajax: {
        url: 'prod.php',
        dataSrc: 'data',
        datatype : 'json',
        
    },
    columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]],
    columns: [
        { data: null, defaultContent: '' },
        { data: 'code_Produit'},
        { data: 'designation'}, 
        { data: 'unite' },
        { data: 'autorisation' },
        { data: 'CategoriePrix' },
        { data: 'PrixBaseFormule'},
        
                    
    ]
   
});   
    
});
    </script>
    <script src="../../wa.js"></script>
</head>
<body>
<body class="animsition">
    <div class="page-wrapper">
        <!-- PAGE CONTAINER-->
        <div class="page-container"> 
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">               
               
                        <div class="row">
                            <div class="col-md-12">                  
<!-- DATA TABLE-->
<section class="p-t-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                        <h1 class="title-1 m-b-35 text-danger">Liste des produits<hr/></h1>
                                 <div class="table-data__tool">
                                    <div class="table-data__tool-left">                               
                                        <form method="post" id="myForm" enctype="multipart/form-data">
                                        <label>Chargez fichier csv</label><br/>
                                        <input type="file" name="file" /><br/>
                                        <!-- <input type='submit' name='submit' value='Import' class='btn btn-info' />"; --> 
                                        <?php
                                            $member = $userRow['role_id'];
                                            if($member == 2) {
                                                echo "
                                                <span style='color:red'>Vous n'êtes pas autorisé à faire des importations!</span>";
                                            } elseif($member == 1) {
                                                echo "
                                                <input type='submit' name='submit' value='Import' class='btn btn-info' />";  
                                            } 
                                        ?> 
                                          <!-- <input type="submit" name="submit" value="Import" class="btn btn-info" /> -->
                                        </form>                                          
                                        <!--<div class="rs-select2--light rs-select2--sm">
                                            <select class="js-select2" name="time">
                                                <option selected="selected">Récentes</option>
                                                <option value="">1 Mois</option>
                                                <option value="">Plus ancien</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div> -->                                       
                                    </div>
                                    <!--<div class="table-data__tool-right">                                        
                                        <button class="au-btn au-btn-icon au-btn--green au-btn--small"  data-toggle="modal" data-target="#scrollmodal" type="button">
                                            <i class="zmdi zmdi-plus"></i>Inserer Nouveau Client
                                        </button> -->                                                                                                                        
                                    </div>
                                </div> 
                                <div class="table-responsive m-b-30">
                                     <table class="table table-borderless table-data3" id="tableProduits">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Code Produit</th>
                                            <th>Designation</th>
                                            <th>unite</th>
                                            <th>autorisation</th>
                                            <th>categorie Prix</th>
                                            <th>Prix de base</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="tr-shadow">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <!--<td>
                                                <div class="table-data-feature">
                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Send">
                                                        <i class="zmdi zmdi-mail-send"></i>
                                                    </button>
                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                        <i class="zmdi zmdi-edit"></i>
                                                    </button>
                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="zmdi zmdi-delete"></i>
                                                    </button>
                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="More">
                                                        <i class="zmdi zmdi-more"></i>
                                                    </button>
                                                </div>
                                            </td>-->
                                        </tr>
                                       
                                       
                                         
                                       
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END DATA TABLE-->
                                
                            </div>                   
                        </div>                  
                   

                        
                    </div>
                  </div>
                  
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Nouvelle <a href="https://npsp.ci">PSP</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            
            
        <!-- END MAIN CONTENT-->
    <!-- END PAGE CONTAINER-->
</div>

    </div> 
    
<!-- Jquery JS-->

   <!-- Bootstrap JS-->
   <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>
</body>
</html>

