<?php

require_once('dbconfig.php');

class CLIENTS
{	

	private $conn;
	
	public function __construct()
	{
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
    }
	
	public function runQuery($sql)
	{
		$stmt = $this->conn->prepare($sql);
		return $stmt;
	}
    
    public function doLoginCustomer2($ulogin,$upass)
	{
		try
		{
			$stmt = $this->conn->prepare("SELECT id_client, nom_client, login, mot_de_passe FROM client WHERE login=:ulogin  ");
			$stmt->execute(array(':ulogin'=>$ulogin));
			$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
			if($stmt->rowCount() == 1)
			{
				if(password_verify($upass, $userRow['mot_de_passe']))
				{
					$_SESSION['user_session'] = $userRow['id_client'];					
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

	public function doLoginCustomer($ulogin,$upass)
	{
		try
		{
			$stmt = $this->conn->prepare("SELECT id_client, nom_client, login, mot_de_passe, statutPass FROM client WHERE login=:ulogin  ");
			$stmt->execute(array(':ulogin'=>$ulogin));
			$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
			if($stmt->rowCount() == 1)
			{
				$hash = '$2y$10$2SKfNu6pTfe7mWcmWvxjtORWs6i..DLCZRnoaT7RpkD4gxoY.0.L2';
				if(password_verify($upass, $hash) && $userRow['statutPass'] == 0)
				{
									
					return true;
				}
				else
				{
					$_SESSION['user_session'] = $userRow['id_client'];
					return false;
				}
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function updatePassword($ulogin,$umail,$upass)
	{
		try
		{
			$new_password = password_hash($upass, PASSWORD_DEFAULT);
			
			$stmt = $this->conn->prepare("UPDATE client SET mot_de_passe = :upass, email = :umail, statutPass = 1 WHERE login= :ulogin ");
												  
			$stmt->bindparam(":ulogin", $ulogin);
			$stmt->bindparam(":umail", $umail);
			$stmt->bindparam(":upass", $new_password);										  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}



	public function getClientByCommercial($id_client)
	{
		try {
			$stmt = $this->conn->prepare('SELECT nom_client, login, email, categorie_commerciale, nom_commerciale
											FROM client 
											JOIN commerciale 
											ON client.id_commerciale = commerciale.id_commercial
											WHERE id_client = :id_client');
            $stmt->execute(array(':id_client'=>$id_client));
            $resultats = $stmt->fetchAll();

            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function getClientByCommercial2($id_client)
	{
		try {
			$stmt = $this->conn->prepare('SELECT nom_client, login, email, categorie_commerciale, nom_commerciale
											FROM client 
											JOIN commerciale 
											ON client.id_commerciale = commerciale.id_commercial
											WHERE id_client = :id_client');
            $stmt->execute(array(':id_client'=>$id_client));
            $resultats = $stmt->fetchAll();

            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

	
	
	public function getCommerciale($id_client)
	{
		try {
			$stmt = $this->conn->prepare('SELECT * 
                                          FROM commerciale, client WHERE client.id_client = :id AND commerciale.id_commercial = client.id_commerciale');
            $stmt->execute(array(':id'=>$id_client));
            $resultats = $stmt->fetchAll();
            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

    public function getProduitClient($id_client, $code_projet)
	{
		try {
			$stmt = $this->conn->prepare('SELECT  id_produit, code_Produit,	designation, unite, autorisation,PrixBaseFormule , id_projet, code_projet, libelleProjet
											FROM produits, projet_produit, projet, client_projet, client 
											WHERE client.id_client = client_projet.clientId 
											AND projet.code_projet = client_projet.projetCode
											AND projet_produit.cProjet = projet.code_projet 
											AND projet_produit.cProduit = produits.code_Produit 
											AND client.id_client = :id
											/*AND projet.code_projet = :projet*/
                                           ;');
			$stmt->execute(array(':id'=>$id_client ));
			/*$stmt->execute(array(':projet' => $code_projet));*/
            $resultats = $stmt->fetchAll();
            return $resultats;

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }
	
	
	
	public function getProjetCustomer($id_client)
	{
		try {
			$stmt = $this->conn->prepare('SELECT libelleProjet, code_projet FROM projet
											JOIN client_projet on  client_projet.projetCode = projet.code_projet
											JOIN client on client_projet.clientId = client.id_client
											WHERE client.id_client = :id');
            $stmt->execute(array(':id'=>$id_client));
            $resultats = $stmt->fetchAll();

            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function getProductByProject($code_projet)
	{
		
		try {
			$stmt = $this->conn->prepare('SELECT designation, code_Produit, unite, autorisation, libelleProjet, code_projet FROM projet
										  join  projet_produit on projet.code_projet = projet_produit.cProjet 
										  join  produits on produits.code_Produit = projet_produit.cProduit 
										  WHERE  projet.code_projet = :proj');
            $stmt->execute(array(':proj'=>$code_projet));
            $resultats = $stmt->fetchAll();
            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function getProductByTarif($id_client)
	{
		
		try {
			$stmt = $this->conn->prepare('SELECT produits.code_Produit, (produits.PrixBaseFormule*tarif.CoefficientPrix) as PrixMedoc 
											FROM client, produits, tarif 
											WHERE CONCAT(client.categorie_prix_client,produits.CategoriePrix) = tarif.tarifprix 
											AND client.id_client = :id');
            $stmt->execute(array(':id'=>$id_client));
            $resultats = $stmt->fetchAll();
            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function updateStatut()
	{		

		try {
			$stmt = $this->conn->prepare('UPDATE promotions SET statut = "0" WHERE DATE(finPromo) < NOW()');

			$stmt->execute();
			$resultats = $stmt->fetchAll();
			return $resultats;

			foreach ($resultats as $result) 
			{
				if($date > 'date'){
					$stmt = $this->conn->prepare('UPDATE promotions SET statut = "0" ');
					$stmt->execute();

					return $resultats; 
				}
			}
			

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

	

	public function getProductPromotion()
	{
		

		try {
			$stmt = $this->conn->prepare('UPDATE promotions SET statut = "0" WHERE DATE(finPromo) < NOW()');
			$stmt->execute();
			$stmt = $this->conn->prepare('UPDATE promotions SET statut = "1" WHERE DATE(finPromo) > NOW() AND DATE(debutPromo) < NOW()');
			$stmt->execute();
			$stmt = $this->conn->prepare('SELECT code_Produit, designation, unite, autorisation, PrixBaseFormule, debutPromo, finPromo, tauxReduction, photoProduit, libellePromotion 
                                            FROM produits 
											join produits_promo on produits.code_Produit = produits_promo.cProd 
											join promotions on promotions.idPromotion = produits_promo.promoId
											WHERE statut = "1"										 
                                            ORDER BY designation ASC LIMIT 4  ');
			$stmt->execute();
			$resultats = $stmt->fetchAll();

            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

	public function getProductPromotionAll()
	{
		try {
			$stmt = $this->conn->prepare('SELECT code_Produit, designation, unite, autorisation, PrixBaseFormule, debutPromo, finPromo, tauxReduction, photoProduit, libellePromotion 
                                            FROM produits 
											join produits_promo on produits.code_Produit = produits_promo.cProd 
											join promotions on promotions.idPromotion = produits_promo.promoId
											WHERE statut = "1"										 
                                            ORDER BY designation');
			$stmt->execute();
            $resultats = $stmt->fetchAll();
            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

	public function selectFlash()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM flashinfo WHERE target = "back"');
            $req->execute();
            $donnees = $req->fetchAll();

            return $donnees;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
	

    public function getCompte()
	{
		try {
			$stmt = $this->conn->prepare('SELECT * FROM compte_client');
            $stmt->execute();
            $resultats = $stmt->fetchAll();

            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
		
	public function is_loggedinCustomer()
	{
		if(isset($_SESSION['user_session']))
		{
			return true;
		}
	}
	
	public function redirectCustomer($url)
	{
		header("Location: $url");
	}
	
	public function doLogoutCustomer()
	{
		session_destroy();
		unset($_SESSION['user_session']);
		return true;
	}


	public function getClientByLogin($ulogin)
	{
			try{
				$stmt = $this->conn->prepare('SELECT * FROM client 
											WHERE login = :login');
				$stmt->execute(array(':login'=>$ulogin));
				$resultats = $stmt->fetchAll();
				return $resultats;
				
			}
			catch(PDOException $e)
			{
				echo $e->getMessage();
			}
	}


	public function insertionProduits($tabResult)
	{
		try
		{
			
			$stmt = $this->conn->prepare("INSERT INTO testProduits(id_produit,	code_Produit,	designation, unite,	autorisation,	CategoriePrix,	QtePhysique,	NbMoisStock, PrixBaseFormule,	codeProjet ) 
													   VALUES :id_produit,	:code_Produit,	:designation, :unite,	:autorisation,	:CategoriePrix,	:QtePhysique,	:NbMoisStock, :PrixBaseFormule,	:codeProjet )");
													  
      		$stmt->bindParam(':id_produit', $id_produit);
			$stmt->bindParam(':code_Produit', $code_Produit);
			$stmt->bindParam(':designation', $designation);
      		$stmt->bindParam(':unite', $unite);
      		$stmt->bindParam(':autorisation', $autorisation);
      		$stmt->bindParam(':CategoriePrix', $CategoriePrix);
			$stmt->bindParam(':QtePhysique', $QtePhysique);
			$stmt->bindParam(':NbMoisStock', $NbMoisStock);
			$stmt->bindParam(':PrixBaseFormule', $PrixBaseFormule);
			$stmt->bindParam(':codeProjet', $codeProjet);

																		  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}			

	}

	public function selectArticleBack()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM article WHERE target = "back" GROUP BY id DESC LIMIT 4');
            $req->execute();
            $donnees = $req->fetchAll();

            return $donnees;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

	public function selectArticleBackAll()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM article WHERE target = "back" GROUP BY id DESC');
            $req->execute();
            $donnees = $req->fetchAll();

            return $donnees;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

	public function redirect($url)
	{
		header("Location: $url");
	}

	
    

}






?>


