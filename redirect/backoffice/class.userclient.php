<?php

require_once('dbconfig.php');

class CLIENTS
{	

	private $conn;
	
	public function __construct()
	{
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
    }
	
	public function runQuery($sql)
	{
		$stmt = $this->conn->prepare($sql);
		return $stmt;
	}
	
	public function registerClient($nom_client, $categorie_prix, $categorie_comm, $login, $upass, $email, $commercial )
	{
		try  
		{			
            
			$new_password = password_hash($upass, PASSWORD_DEFAULT);
			
			$stmt = $this->conn->prepare("INSERT INTO client(id_client,	nom_client,	categorie_prix_client, 	categorie_commerciale, 	login, 	mot_de_passe, email, statutPass, id_commerciale, activer) 
		                                               VALUES(NULL, :unom_client, :ucategorie_prix, :ucategorie_comm,  :ulogin, :upass, :umail,'0', :ucommercial, 'oui')");
												  
			$stmt->bindparam(":unom_client", $nom_client);
			$stmt->bindparam(":ucategorie_prix", $categorie_prix);
			$stmt->bindparam(":ucategorie_comm", $categorie_comm);				
			$stmt->bindparam(":ulogin", $login);
			$stmt->bindparam(":upass", $new_password);
			$stmt->bindparam(":umail", $email);
			$stmt->bindparam(":ucommercial", $commercial);									  
				
			$stmt->execute();

			/*$stmt = $this->conn->prepare("SELECT LAST_INSERT_ID()");
			$stmt->execute();
            $lastId = $stmt->fetchColumn();*/			
			
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}

	public function updateClient($id_client, $nom_client, $categorie_prix, $categorie_comm, $login, $email, $commercial)
	{
		try  
		{			
            
			
			$stmt = $this->conn->prepare("UPDATE client SET nom_client = :unom_client,	categorie_prix_client = :ucategorie_prix, 	categorie_commerciale = :ucategorie_comm, 	login = :ulogin, email = :umail, id_commerciale = :ucommercial
		                                    WHERE id_client = :id");
												  
			$stmt->bindparam(":unom_client", $nom_client);
			$stmt->bindparam(":ucategorie_prix", $categorie_prix);
			$stmt->bindparam(":ucategorie_comm", $categorie_comm);				
			$stmt->bindparam(":ulogin", $login);
			$stmt->bindparam(":umail", $email);
			$stmt->bindparam(":ucommercial", $commercial);
			$stmt->bindparam(":id", $id_client);
									  
				
			$stmt->execute();

			/*$stmt = $this->conn->prepare("SELECT LAST_INSERT_ID()");
			$stmt->execute();
            $lastId = $stmt->fetchColumn();*/			
			
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}

	public function registerProjetClient($id_client,$code)
	{
		try 
		{
			$stmt = $this->conn->prepare("INSERT INTO client_projet (clientId, projetCode) 
											VALUES (:clientId, :projetCode)" );
			$stmt->bindparam(":clientId", $id_client);
			$stmt->bindparam(":projetCode", $code);
			$stmt->execute();
			
			return $stmt;
		} 
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}

	}

	public function registerProjetClient2($code, $login)
	{
		try 
		{
			$stmt = $this->conn->prepare("INSERT INTO client_projet (clientId, projetCode) 
											VALUES ((SELECT id_client FROM client 
											WHERE login = :login), :projetCode)" );
			$stmt->bindparam(":login", $login);
			$stmt->bindparam(":projetCode", $code);
			$stmt->execute();
			
			return $stmt;
		} 
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}

	}

	public function updateProjetClient($id_client)
	{
		try 
		{

			$stmt = $this->conn->prepare("DELETE FROM client_projet WHERE clientId = :id" );
			$stmt->bindparam(":id", $id_client);
			$stmt->execute();
			
			return $stmt;
		} 
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}

	}

	

	public function getClientByCommercial()
	{
		try {
			$stmt = $this->conn->prepare('SELECT id_client, nom_client, login, email, categorie_commerciale, nom_commerciale, categorie_prix_client, id_commerciale
											FROM client 
											JOIN commerciale 
											ON client.id_commerciale = commerciale.id_commercial
											WHERE activer = "1"');
            $stmt->execute();
            $resultats = $stmt->fetchAll();

            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function getClientByCommercialById($id)
	{
		try {
			$stmt = $this->conn->prepare('SELECT id_client, nom_client, login, email, categorie_commerciale, nom_commerciale, categorie_prix_client
											FROM client 
											JOIN commerciale 
											ON client.id_commerciale = commerciale.id_commercial
											WHERE id_client = :id');
            $stmt->execute(array(':id'=>$id));
            $resultats = $stmt->fetchAll();

            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

	public function getProjetByClient($id_client)
	{
		try {
			$stmt = $this->conn->prepare('SELECT id_client,code_projet, libelleProjet FROM projet 
											JOIN client_projet ON projet.code_projet = client_projet.projetCode 
											JOIN client ON client.id_client = client_projet.clientId 
											WHERE id_client = :id');
            $stmt->execute(array(':id'=>$id_client));
            $resultats = $stmt->fetchAll();

            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function deleteClient($id_client)
	{
		try 
		{

			$stmt = $this->conn->prepare('UPDATE client SET activer = "0" WHERE id_client = :id' );
			$stmt->bindparam(":id", $id_client);
			$stmt->execute();
			
			return $stmt;
		} 
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}

	}
	
	public function getCommerciale()
	{
		try {
			$stmt = $this->conn->prepare('SELECT * FROM commerciale');
            $stmt->execute();
            $resultats = $stmt->fetchAll();

            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function getCommercialeById($idComm)
	{
		try {
			$stmt = $this->conn->prepare('SELECT * FROM commerciale WHERE id_commercial = :id');
            $stmt->execute(array(':id'=>$idComm));
            $resultats = $stmt->fetchAll();

            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }
	
	public function insertCommerciale($nom, $prenoms, $fonction, $email, $contact)
	{
		try
		{
						
			$stmt = $this->conn->prepare("INSERT INTO commerciale(nom, prenoms, contact, email, fonction) 
		                                               VALUES(:unom, :uprenoms, :ucontact, :uemail, :ufonction)");
												  
			$stmt->bindparam(":unom", $nom);
			$stmt->bindparam(":uprenoms", $prenoms);
			$stmt->bindparam(":ucontact", $contact);
			$stmt->bindparam(":uemail", $email);
			$stmt->bindparam(":ufonction", $fonction);	
												  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}

	public function updateCommerciale($nom, $prenoms, $fonction, $email, $contact)
	{
		try
		{
						
			$stmt = $this->conn->prepare("UPDATE commerciale SET (id, nom, prenoms, contact, email, fonction)
		                                               VALUES(:uid, :unom, :uprenoms, :ucontact, :uemail, :ufonction)  ");
												  
			$stmt->bindparam(":unom", $nom);
			$stmt->bindparam(":uprenoms", $prenoms);
			$stmt->bindparam(":ucontact", $contact);
			$stmt->bindparam(":uemail", $email);
			$stmt->bindparam(":ufonction", $fonction);	
												  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}



	public function registerCompte($code_client, $projet_id, $client_id )
	{
		try  
		{
			$stmt = $this->conn->prepare("INSERT INTO compte_client (id_compte, code_client, projet_id, client_id) 
		                                               VALUES(NULL, :code_client, :projet_id, :client_id)");
												  
			$stmt->bindparam(":code_client", $code_client);
			$stmt->bindparam(":projet_id", $projet_id);
			$stmt->bindparam(":client_id", $client_id);
			
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}

	public function getCompte()
	{
		try {
			$stmt = $this->conn->prepare('SELECT * FROM compte_client');
            $stmt->execute();
            $resultats = $stmt->fetchAll();

            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

	public function getProduct()
	{
		try {
			$stmt = $this->conn->prepare('SELECT * FROM produits');
            $stmt->execute();
            $resultats = $stmt->fetchAll();

            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function getProjet()
	{
		try {
			$stmt = $this->conn->prepare('SELECT * FROM projet');
            $stmt->execute();
            $resultats = $stmt->fetchAll();

            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

	

	public function doLogin($uname,$umail,$upass)
	{
		try
		{
			$stmt = $this->conn->prepare("SELECT id, name, email, password FROM users WHERE name=:uname OR email=:umail ");
			$stmt->execute(array(':uname'=>$uname, ':umail'=>$umail));
			$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
			if($stmt->rowCount() == 1)
			{
				if(password_verify($upass, $userRow['password']))
				{
					$_SESSION['user_session'] = $userRow['id'];					
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

	public function doLoginCustomer($ulogin,$upass)
	{
		try
		{
			$stmt = $this->conn->prepare("SELECT id_client, nom_client, login, mot_de_passe FROM client WHERE login=:ulogin  ");
			$stmt->execute(array(':ulogin'=>$ulogin));
			$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
			if($stmt->rowCount() == 1)
			{
				if(password_verify($upass, $userRow['password']))
				{
					$_SESSION['user_session'] = $userRow['id_client'];					
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function is_loggedin()
	{
		if(isset($_SESSION['user_session']))
		{
			return true;
		}
	}
	
	public function redirect($url)
	{
		header("Location: $url");
	}
	
	public function doLogout()
	{
		session_destroy();
		unset($_SESSION['user_session']);
		return true;
	}


	public function client()
	{

	}

	public function insertClient($item1,$item2,$item3,$item4,$item5,$item6,$item7)
	{
		try
		{
			if(empty($item7)){
				$comm = "99";
			}
			else{
				$comm = $item7;
			}
			$stmt = $this->conn->prepare("INSERT INTO client( id_client, nom_client, categorie_prix_client,	categorie_commerciale, login, mot_de_passe,	email, statutPass, id_commerciale, activer) 
													   VALUES (NULL, :nom_client, :categorie_prix_client,	:categorie_commerciale, :login, :mot_de_passe,	:email, '0', :id_commerciale, '1')");
													  
													  
      		
			$stmt->bindParam(':nom_client', $item1);
			$stmt->bindParam(':categorie_prix_client', $item2);
      		$stmt->bindParam(':categorie_commerciale', $item3);
      		$stmt->bindParam(':login', $item4);
      		$stmt->bindParam(':mot_de_passe', $item5);
			$stmt->bindParam(':email', $item6);			
			$stmt->bindParam(':id_commerciale', $comm);

																		  
				
			$stmt->execute();
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}			

	}

	public function insertClient2($item0,$item1,$item2,$item3,$item4,$item5,$item6,$item7)
	{
		try
		{	
			/*$var = "%".$item7."%";
			$stmt = $this->conn->prepare("SELECT * FROM commerciale WHERE nom_commerciale = :nomCommercial");
			$stmt->execute(array(':nomCommercial'=>$item7));
            $req = $stmt->fetchAll();
			//var_dump($req);
			foreach ($req as $comm) {
				$idComm = $comm['id_commercial'];
			}									  
			//var_dump($idComm);*/
			if(empty($item0)){
				$log = "ND";
			}else{
				$log = $item0;
			}
			$stmt = $this->conn->prepare("INSERT INTO clientTest2( id_client, login2, nom_client, categorie_prix_client,	categorie_commerciale, login, mot_de_passe,	email, statutPass, id_commerciale, activer) 
													   VALUES (NULL, :loginClient, :nom_client, :categorie_prix_client,	:categorie_commerciale, :login, :mot_de_passe,	:email, '0', '99', '1')");
													  
													  
			$stmt->bindParam(':loginClient', $log);
			$stmt->bindParam(':nom_client', $item1);
			$stmt->bindParam(':categorie_prix_client', $item2);
      		$stmt->bindParam(':categorie_commerciale', $item3);
      		$stmt->bindParam(':login', $item4);
      		$stmt->bindParam(':mot_de_passe', $item5);
			$stmt->bindParam(':email', $item6);


																		  
				
			$stmt->execute();
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}			

	}

	public function insertClientProjet($item0,$item1)
	{
		try
		{
			
			$stmt = $this->conn->prepare("INSERT INTO client_projet(clientId ,projetCode) 
													   VALUES (:id, :code)");			  
			$stmt->bindParam(':id', $item0);
			$stmt->bindParam(':code', $item1);
																				  
				
			$stmt->execute();
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}			

	}

	

	public function insertProduct($code, $designation, $unite, $autorisation, $categorie, $quantite, $stock, $prix, $projet)
	{
		try
		{
			
			$stmt = $this->conn->prepare("INSERT INTO produits( id_produit,	code_Produit, designation, 	unite,	autorisation, 	CategoriePrix, 	QtePhysique, NbMoisStock, PrixBaseFormule, 	photoProduit) 
													   VALUES (NULL, :code, :designation,	:unite, :autorisation, :categorie,	:quantite, :stock, :prix, 'images/uploads/medicTest.jpg')");
													  
													  
      		
			$stmt->bindParam(':code', $code);
			$stmt->bindParam(':designation', $designation);
      		$stmt->bindParam(':unite', $unite);
      		$stmt->bindParam(':autorisation', $autorisation);
      		$stmt->bindParam(':categorie', $categorie);
			$stmt->bindParam(':quantite', $quantite);
			$stmt->bindParam(':stock', $stock);
			$stmt->bindParam(':prix', $prix);																		  
				
			$stmt->execute();


			$stmt = $this->conn->prepare("INSERT INTO projet_produit(id, cProduit, cProjet)
											VALUES(NULL, :cProduit, :cProjet)");
			$stmt->bindParam(':cProduit', $code);
			$stmt->bindParam(':cProjet', $projet);																		  
				
			$stmt->execute();
			
			return $stmt;

			
			
			
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}			

	}


	public function insertProjetProduit($code, $projet)
	{
		try
		{
			$stmt = $this->conn->prepare("INSERT INTO projet_produit(id, cProduit, cProjet)
											VALUES(NULL, :cProduit, :cProjet)");
			$stmt->bindParam(':cProduit', $code);
			$stmt->bindParam(':cProjet', $projet);																		  
				
			$stmt->execute();
			
			return $stmt;																			  		
			
			
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}			

	}



}


?>


