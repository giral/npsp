<?php
require_once ("header.php"); 

$resultat = new ARTICLES();
$datas=$resultat->selectActivites();

$filepath = realpath (dirname(__FILE__));

require_once($filepath."./../../webanalytics.php");
include "./../../websettings.php";

?>

<!DOCTYPE html>
<html lang="fr">
<head>
<script src="../../wa.js"></script>
</head>
<body class="animsition">
    <div class="page-wrapper">
        <!-- PAGE CONTAINER-->
        <div class="page-container"> 
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">               
               
                        <div class="row">
                            <div class="col-md-12">                  

                                <!-- DATA TABLE-->
                                <h3 class="title-5 m-b-35">Menu Activités</h3>
                                 <div class="table-data__tool">
                                    <div class="table-data__tool-left">                                        
                                        <!--<div class="rs-select2--light rs-select2--sm">
                                            <select class="js-select2" name="time">
                                                <option selected="selected">Récentes</option>
                                                <option value="">1 Mois</option>
                                                <option value="">Plus ancien</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div> -->                                       
                                    </div>
                                    <div class="table-data__tool-right">
                                        <button class="au-btn au-btn-icon au-btn--green au-btn--small"  data-toggle="modal" data-target="#mediumModal" type="button">
                                            <i class="zmdi zmdi-plus"></i>Créer un menu
                                        </button>                                                                                 
                                    </div>
                                </div> 
                                <div class="table-responsive m-b-40">
                                     <table class="table table-borderless table-data3">
                                        <thead>
                                            <tr>
                                                
                                                <th>Nom page</th>
                                                <th>Titre article</th>
                                                <th>Contenu</th>
                                                <th>Photo de l'article</th>
                                                <th>Photo de couverture</th>
                                                <th>activer</th>
                                                <th>action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($datas as $data):; $idAct = $data['id_act']?> 
                                            <tr>                                            
                                            <td> <?= $data['page'];?></td>
                                            <td> <?= $data['titre_act'];?></td>
                                            <td> <?= substr($data['contenu_act'], 0,100);?>...</td>
                                            <td> <?= $data['path_photo'];?></td>
                                            <td> <?= $data['couverture'];?></td>
                                            <td> <?= $data['activer'];?></td>                                            
                                                 <td>                                                
                                                    <div class="table-data-feature">
                                                        <button class="item" data-placement="top" title="Edit" data-target="#largeModal<?= $idAct;?>" data-toggle="modal" type="button" >
                                                            <i class="zmdi zmdi-edit"></i>
                                                        </button>
                                                        <button class="item" data-target="#litleModal<?= $idAct;?>" data-toggle="modal" type="button"  title="Delete">
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </button>                                                       
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                    
                                </div>
                                <!-- END DATA TABLE-->
                            </div>                   
                        </div>                  
                   

                        
                    </div>
                  </div>
                  
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Nouvelle <a href="https://npsp.ci">PSP</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


<!-- modal medium --> 
<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="mediumModalLabel">Créer un sous-menu dans Activités</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
						<p>                                

                        <form action="insertAct.php" method="post" id="myForm" enctype="multipart/form-data">
                         <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nom Page</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="page" placeholder="" class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Titre article</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="titre_act" placeholder="" class="form-control"></div>
                          </div>                          
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Contenu</label></div>
                            <div class="col-12 col-md-9"><textarea name="contenu_act" id="textarea-input" rows="9" placeholder="Contenu..." class="form-control"></textarea></div>
                          </div> 
                          
                            Ajouter une photo au contenu<br/>
                             <input type="file" name="photo" id="photo_act"> <br/><br/>
                             Ajouter une photo de couverture<br/>
                             <input type="file" name="cover" id="cover"> <br/><br/>
                             <?php
                                $member = $userRow['role_id'];
                                if($member == 2) {
                                    echo "<span style='color:red'>Desolé vous n'êtes pas autorisé!</span>";
                                } elseif($member == 1) {
                                    echo "
                                    <button type='submit' class='btn btn-success btn-sm' name='btn-submitActivites'  value='submit'>Valider</button>
                                    <button type='reset' class='btn btn-danger btn-sm' value='submit'>Annuler</button>";
                                } 
                                ?>
                             <!-- <button type="submit" class="btn btn-success btn-sm" name="btn-submitActivites"  value="submit">Valider</button>
                             <button type="reset" class="btn btn-danger btn-sm" value="submit">Annuler</button> -->
                            
                        </form>
                        </p>
                        </div>
						
						
					</div>
				</div>
</div>
            <!-- end modal medium -->
            
            <!-- modal large --><?php foreach ($datas as $data):; $idAct = $data['id_act']?>
			<div class="modal fade" id="largeModal<?= $idAct;?>" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="largeModalLabel">Large Modal</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<p>
                            <form action="insertAct.php" method="post" id="myForm" enctype="multipart/form-data">
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nom page </label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="page" value="<?= $data['page']?>" class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Titre actualité  </label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="titre_act" value="<?= $data['titre_act']?>" class="form-control"></div>
                          </div>                          
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Contenu</label></div>
                            <div class="col-12 col-md-9"><textarea name="contenu_act" id="textarea-input" rows="9" placeholder="" class="form-control"><?= $data['contenu_act']?></textarea></div>
                          </div>
                          <div class="row form-group">
                            <div class="col-12 col-md-9"><input type="hidden" id="text-input" name="originPic" placeholder="Text" class="form-control" value="<?=$data['path_photo']?>"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col-12 col-md-9"><input type="hidden" id="text-input" name="originCov" placeholder="Text" class="form-control" value="<?=$data['couverture']?>"></div>
                          </div> 
                          <div class="row form-group">
                            <div class="col-12 col-md-9"><input type="hidden" id="text-input" name="idAct" placeholder="Text" class="form-control" value="<?=$idAct?>"></div>
                          </div>
                         
                            Photo de l'article<br/>
                             <img src="<?=$data['path_photo']?>" width="20%"> 
                             <input type="file" name="photo" id="photo"><br/><br/>
                             Photo de couverture<br/>
                             <img src="<?=$data['couverture']?>" width="20%">
                             <input type="file" name="cover" id="photo2" ><br/><br/>
                             <?php
                                $member = $userRow['role_id'];
                                if($member == 2) {
                                    echo "<span style='color:red'>Desolé vous n'êtes pas autorisé!</span>";
                                } elseif($member == 1) {
                                    echo "
                                    <button type='submit' class='btn btn-success btn-sm' name='btn-submitUActivites'  value='submit'>Valider</button>
                                    <button type='reset' class='btn btn-danger btn-sm' value='submit'>Annuler</button>";
                                } 
                                ?>
                             <!-- <button type="submit" class="btn btn-success btn-sm" name="btn-submitUActivites"  value="submit">Valider</button>
                             <button type="reset" class="btn btn-danger btn-sm" value="submit">Annuler</button> -->
                             
                          </form>
							</p>
						</div>
						
					</div>
				</div>
            </div>
            <?php endforeach;?>
            <!-- end modal large -->

            <!-- modal litle --><?php foreach ($datas as $data):; $idAct = $data['id_act']?>
            <div class="modal fade" id="litleModal<?= $idAct;?>" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="largeModalLabel">Large Modal</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<p>
                            <form action="insertAct.php" method="post" id="myForm" enctype="multipart/form-data">
                            <div class="row form-group">
                            <div class="col col-md-8"><label for="selectSm" class=" form-control-label">Voulez vous désactiver ce menu activité?</label></div>
                        </div>                          
                          
                          <div class="row form-group">
                            <div class="col-12 col-md-9"><input type="hidden" id="text-input" name="id" placeholder="Text" class="form-control" value="<?= $idAct;?>"></div>
                          </div>
                         
                          <?php
                                $member = $userRow['role_id'];
                                if($member == 2) {
                                    echo "<span style='color:red'>Desolé vous n'êtes pas autorisé!</span>";
                                } elseif($member == 1) {
                                    echo "
                                    <button type='submit' class='btn btn-success btn-sm' name='deleteAct'  value='submit'>Valider</button>
                                    <button type='reset' class='btn btn-danger btn-sm' value='submit'>Annuler</button>";
                                } 
                                ?>
                             <!-- <button type="submit" class="btn btn-success btn-sm" name="deleteAct"  value="submit">Valider</button>
                             <button type="reset" class="btn btn-danger btn-sm" value="submit">annuler</button> -->
                             
                          </form>
							</p>
						</div>
						
					</div>
				</div>
            </div>
            <?php endforeach;?>
            <!-- end modal litle -->

        <!-- END MAIN CONTENT-->
    <!-- END PAGE CONTAINER-->
</div>

    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>
<!-- end document-->
