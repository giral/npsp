<?php
require_once("class.publication.php");
require_once("dbconfig.php");
$filepath = realpath (dirname(__FILE__));

require_once($filepath."/webanalytics.php");
include "websettings.php";
$resultats = new publication();
$datas = $resultats->selectArticlePublication();

$tabResult = [];
foreach($datas as $data){
    $object = new stdClass();
    //$object->designation=htmlspecialchars(($data['designation']));
    $object->titre_article=($data['titre_article']);
    $object->description=($data['description']);
    $object->date_publication=($data['date_publication']);
    $object->path_file=($data['path_file']);
    $tabResult['data'][] = $object;
}
header('Content-Type: application/json; charset=UTF-8');
echo  json_encode($tabResult);
//echo $tabResult;
//var_dump($tabResult);
//var_dump($code_projet);

?>

