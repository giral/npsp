<?php

require_once('dbconfig.php');

class PUBLICATION{

    private $conn;
	
	public function __construct()
	{
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
    }
	
	
	public function runQuery($sql)
	{
		$stmt = $this->conn->prepare($sql);
		return $stmt;
	}
	
    public function redirect($url)
	{
		header("Location: $url");
	}

	
   	

	public function selectdg()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM dg');
            $req->execute();
            $datas = $req->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

    
    public function getPage()
    {
        try
        {
            $req = $this->conn->prepare('SELECT page FROM presentation');
            $req->execute();
            $pages = $req->fetchAll();

            return $pages;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

    public function getPageOrg()
    {
        try
        {
            $req = $this->conn->prepare('SELECT page FROM organisation');
            $req->execute();
            $pageOrg = $req->fetchAll();

            return $pageOrg;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

    public function getPageAct()
    {
        try
        {
            $req = $this->conn->prepare('SELECT page FROM activites WHERE activer = "oui"');
            $req->execute();
            $pageAct = $req->fetchAll();

            return $pageAct;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

    public function selectPresentation($page)
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM presentation WHERE page = :page');
            $req->execute(array('page'=>$page));
            $datas = $req->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

    public function selectOrganisation($page)
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM organisation WHERE page = :page');
            $req->execute(array('page'=>$page));
            $datas = $req->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

    public function selectActivites($page)
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM activites WHERE page = :page');
            $req->execute(array('page'=>$page));
            $datas = $req->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

    public function selectMission()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM presentation WHERE page = "Missions"');
            $req->execute();
            $datas = $req->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

    public function selectVision()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM presentation WHERE page = "vision"');
            $req->execute();
            $result = $req->fetchAll();

            return $result;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }



	public function selectActu()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM actualites WHERE accueil = 1 ORDER BY id DESC LIMIT 4 ');
            $req->execute();
            $reponses = $req->fetchAll();

            return $reponses;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }
    
    public function selectActuId($id_actu)
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM actualites WHERE id = :id_actu ');
            $req->bindParam(':id_actu', $id_actu);
            $req->execute();
            $reponses = $req->fetchAll();

            return $reponses;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
    


    public function selectActuAll()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM actualites WHERE publier = 1 GROUP BY id DESC');
            $req->execute();
            $reponses = $req->fetchAll();

            return $reponses;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }


    public function selectMetiers()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM activites WHERE page_act = "metiers"');
            $req->execute();
            $datas = $req->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

    public function selectServices()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM activites WHERE page_act = "services"');
            $req->execute();
            $datas = $req->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

    public function selectArticle()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM article WHERE publier = "oui" AND accueil = "oui" ORDER BY id DESC LIMIT 4');
            $req->execute();
            $donnees = $req->fetchAll();

            return $donnees;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

    public function selectArticlePublication()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM article WHERE publier = "oui" ORDER BY id DESC');
            $req->execute();
            $donnees = $req->fetchAll();

            return $donnees;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }
    
    

    public function selectQualite()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM activites WHERE page_act = "qualite"');
            $req->execute();
            $datas = $req->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

    public function selectControleQualite()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM activites WHERE page_act = "controle"');
            $req->execute();
            $datas = $req->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

    public function selectPerspectives()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM activites WHERE page_act = "perspectives"');
            $req->execute();
            $datas = $req->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

    public function selectSlider()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM slider  WHERE activer = "OUI"');
            $req->execute();
            $donnees = $req->fetchAll();

            return $donnees;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

    public function selectAlbum()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM albums ORDER BY idAlbum DESC');
            $req->execute();
            $datas = $req->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }
    public function selectAlbumOld()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM albums 
                                            WHERE idAlbum = (SELECT max(idAlbum) 
                                            FROM albums) AND publier = "oui" ');
            $req->execute();
            $datas = $req->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }
    public function selectPhotoAlbumOld($idAlbum)
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM photos 
                                            JOIN albums 
                                            ON photos.albumId = albums.idAlbum
                                            WHERE albums.idAlbum = :id');
            $req->execute(array(':id'=>$idAlbum));
            $datas = $req->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }
    public function selectAlbumPhoto($idAlbum)
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM photos 
                                            JOIN albums 
                                            ON photos.albumId = albums.idAlbum
                                            WHERE albums.idAlbum = :id');
            $req->execute(array(':id'=>$idAlbum));
            $datas = $req->fetchAll();

            return $datas;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }
    
    public function selectFlashFront()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM flashinfo WHERE target = "front" AND publier = "oui" ');
            $req->execute();
            $donnees = $req->fetchAll();

            return $donnees;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

    public function selectFlash()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM flashinfo WHERE publier = "oui"');
            $req->execute();
            $donnees = $req->fetchAll();

            return $donnees;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

    public function selectVideo()
    {
        try
        {
            $req = $this->conn->prepare('SELECT * FROM videos WHERE publier = "oui" AND accueil = "oui" ');
            $req->execute();
            $donnees = $req->fetchAll();
    
            return $donnees;         
    
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
    public function getProduct()
	{
		try {
			$stmt = $this->conn->prepare('SELECT  id_produit, code_Produit,	designation, unite, autorisation, PrixBaseFormule
											FROM produits 
                                            ORDER BY designation ASC ');
			$stmt->execute();
            $resultats = $stmt->fetchAll();
            return $resultats;         

        }
        catch(PDOException $e)
		{
			echo $e->getMessage();
		}
    }

   
	

}
?>