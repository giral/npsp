<?php 
header('Content-Type: text/html; charset=utf-8');
  $filepath = realpath (dirname(__FILE__));
  
  require_once($filepath."/webanalytics.php");
include "websettings.php";

include ("header.php");
$resultats = new publication();
$datas = $resultats->selectArticlePublication();

?>

<!DOCTYPE html>
<html lang="fr">
<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->

        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135621511-2"></script>

        <script>

        window.dataLayer = window.dataLayer || [];

        function gtag(){dataLayer.push(arguments);}

        gtag('js', new Date());        

        gtag('config', 'UA-135621511-2');

        </script>


        <!-- Primary Meta Tags -->
        <title>La Nouvelle Pharmacie de la Santé Publique de Côte d'Ivoire</title>
        <meta name="title" content="Nouvelle Pharmacie de la Santé Publique Côte d'Ivoire">
        <meta name="description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">

        <!-- Open Graph / Facebook -->
        <meta property="og:type" content="website">
        <meta property="og:url" content="http://www.npsp.ci/">
        <meta property="og:title" content="La Nouvelle Pharmacie de la Santé Publique de Côte d'Ivoire">
        <meta property="og:description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">
        <meta property="og:image" content="http://npsp.ci/assets/img/npsp-ci.png">

        <!-- Twitter -->
        <meta property="twitter:card" content="summary_large_image">
        <meta property="twitter:url" content="http://www.npsp.ci/">
        <meta property="twitter:title" content="La Nouvelle Pharmacie de la Santé de Publique Côte d'Ivoire">
        <meta property="twitter:description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">
        <meta property="twitter:image" content="http://npsp.ci/assets/img/npsp-ci.png">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <meta charset="UTF-8">
    <script src="assets/theme/js/jquery-3.2.1.min.js"></script>
    <script src="assets/theme/js/jquery-ui.min.js"></script>

<script>
/*$(document).ready(function(){ 
    
        $('#publication').dataTable({ 
            "bDestroy": true,       
        "ajax":{
            "type" : "GET",
            "url" : "docs.php",                       
            "dataSrc": function ( json ) {
                
                 return json.data;             

        },    
    }, 
        
        "columns": [
            { "data": "titre_article" },
            { "data": "description" },            
            { "data": "date_publication" },
            { "data": "path_file", 
                "render": function (data, type, row) {
                    return '<a  href= redirect/backoffice/'+row.path_file+' target=blank>' + 'consulter' + '</a>';                        }
            
            }
                        
        ],
        "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            },
        
     });     
    });*/

    <script src="wa.js"></script>

</script>
</head>
<body>

<section class="mbr-section content5 cid-qpgqzWVZli8 mbr-parallax-background" id="content5-z" data-rv-view="7367"><!--mbr.additional.css--11757-->
    <div class="container">
        <div class="media-container-row">
            <div class="title col-12 col-md-8">
                <h2 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-1">
                   
                   
                </h2>
                <h3 class="mbr-section-subtitle align-center mbr-light mbr-white pb-3 mbr-fonts-style display-5">
                    
                </h3>
                
                
            </div>
        </div>
    </div>
</section>

<div class="barbread">    
    <div class="container">
        <div class="mbr-text col-12 col-md-12 text-black  mbr-fonts-style display-7">        
                Accueil | Publications
        </div>
    </div>
</div>
<br/><br/>


    
    

    

<section class="mbr-section article content1 cid-qpgqBVyKll" id="content1-14" data-rv-view="7376">

    
<div class="container">
<h3 class="mbr-section-subtitle align-center mbr-light text-green mbr-fonts-style display-1">Nos Publications<hr/></h3><br/><br/>
<div class="list-group">
  <?php foreach ($datas as $data):; ?>
  <a href="redirect/backoffice/<?= $data['path_file']?>" target="blank" class="list-group-item list-group-item-action flex-column align-items-start">
    <div class="d-flex w-100 justify-content-between">
      <h4 class="mb-1 text-black"><?=$data['titre_article']?></h4>
    </div>      <small><img src="assets/img/pdf.png" width="4%" media-simple="true" style="float:right" alt="npsp-ci"></small>

    <p class="mb-1"><?=$data['description']?></p>
    <small><?=$data['date_publication'] ?></small>
  </a>
  <?php endforeach; ?>

</div>
</div>
</section>

<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "Nouvelle PSP-CI",
    "url": "http://www.npsp.ci",
    "address": "Km4, Boulvard de Marseille, BP V5",
    "sameAs": [
      "https://web.facebook.com/NouvellePSPCI/",
      "https://www.linkedin.com/company/nouvelle-psp-ci/"
    ]
  }
</script>

<?php include ("footer.php");?>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>

<script src="assets/popper/popper.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smooth-scroll/smooth-scroll.js"></script>
  <script src="assets/dropdown/js/script.min.js"></script>
  <script src="assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js"></script>
  <script src="assets/jarallax/jarallax.min.js"></script>
  <script src="assets/theme/js/script.js"></script>
  <script src="assets/mobirise-slider-video/script.js"></script> 
</body>
</html>