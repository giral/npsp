<?php
require_once("class.publication.php");
require_once("dbconfig.php");
$filepath = realpath (dirname(__FILE__));

require_once($filepath."/webanalytics.php");
include "websettings.php";
$resultats = new publication();
$datas = $resultats->getProduct();

$tabResult = []; 
foreach($datas as $data){
    $promo =  $data['PrixBaseFormule'] - ($data['PrixBaseFormule'] * 0.5) ;
    $object = new stdClass();
    $object->designation=htmlspecialchars(($data['designation']));
    $object->code_Produit=($data['code_Produit']);
    $object->unite=($data['unite']);
    $object->autorisation=($data['autorisation']);
    $object->PrixBaseFormule=($data['PrixBaseFormule'] . ' XOF');
    $object->PrixPromo = $promo . ' XOF';
    $tabResult['data'][] = $object;
}
header('Content-Type: application/json; charset=UTF-8');
echo  json_encode($tabResult);
//echo $tabResult;
//var_dump($tabResult);
//var_dump($code_projet);
?>

