<?php
header('Content-Type: text/html; charset=utf-8');
require_once("class.publication.php");
require_once("dbconfig.php");

$menu = new publication;
$pages = $menu->getPage();
$pageOrg = $menu->getPageOrg();
$pageAct = $menu->getPageAct();
?>
<!DOCTYPE html>
<html lang="fr">
    <head>

    <!-- Global site tag (gtag.js) - Google Analytics -->

        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135621511-2"></script>

        <script>

        window.dataLayer = window.dataLayer || [];

        function gtag(){dataLayer.push(arguments);}

        gtag('js', new Date());        

        gtag('config', 'UA-135621511-2');

        </script>
        <script src="wa.js"></script>

        <!-- Primary Meta Tags -->
        <title>La Nouvelle Pharmacie de la Santé Publique de Côte d'Ivoire</title>
        <meta name="title" content="Nouvelle Pharmacie de la Santé Publique Côte d'Ivoire">
        <meta name="description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">

        <!-- Open Graph / Facebook -->
        <meta property="og:type" content="website">
        <meta property="og:url" content="http://www.npsp.ci/">
        <meta property="og:title" content="La Nouvelle Pharmacie de la Santé Publique de Côte d'Ivoire">
        <meta property="og:description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">
        <meta property="og:image" content="http://npsp.ci/assets/img/npsp-ci.png">

        <!-- Twitter -->
        <meta property="twitter:card" content="summary_large_image">
        <meta property="twitter:url" content="http://www.npsp.ci/">
        <meta property="twitter:title" content="La Nouvelle Pharmacie de la Santé de Publique Côte d'Ivoire">
        <meta property="twitter:description" content="La Pharmacie de la Santé Publique Côte d'Ivoire (NPSP-CI) à pour mission d’assurer la disponibilité géographique et l’accessibilité financière des médicaments de qualité dans les établissements sanitaires publics et parapublics sur toute l’étendue du territoire national.                                        ">
        <meta property="twitter:image" content="http://npsp.ci/assets/img/npsp-ci.png">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <meta charset="UTF-8">
        <link rel="shortcut icon" href="assets/img/logo.png" type="image/x-icon">                                  
        <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
        <link rel="stylesheet" href="assets/tether/tether.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="assets/dropdown/css/style.css">
        <link rel="stylesheet" href="assets/theme/css/style.css">
        <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
        <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
        <link rel="stylesheet" href="assets/tether/tether.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="assets/dropdown/css/style.css">
        <link rel="stylesheet" href="assets/theme/css/style.css">
        <link rel="stylesheet" href="assets/mobirise-gallery/style.css">
        <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
        <link rel="stylesheet" type="text/css" media="screen" href="assets/theme/css/slider.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="assets/theme/css/bread.css" />
        <script src="assets/web/assets/jquery/jquery.min.js"></script>


        
        
        
      </head>

      <body>
     
      <div class="menu cid-qpg357PDtf2" id="menu2-x" data-rv-view="7362" >
      <section class="menu cid-qpg357PDtf" id="menu2-x" data-rv-view="7362"> <!--mbr-additionnal.css-11730-->
      <div class="container">
          <nav class="navbar navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color " style="width: 80%;"><!--navbar mbr-additionnal.css-20221 20337-->
              <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <div class="hamburger">
                      <span></span>
                      <span></span>
                      <span></span>
                      <span></span>
                  </div>
              </button>
              <div class="menu-logo">
                  <div class="navbar-brand">
                      
                      <span class="navbar-caption-wrap">
                          <a class="navbar-caption display-4 text-warning" href="index.php">
                              <img src="assets/img/logo.png" width = "96" height="60" alt="logo NPSP-CI"/>  </a>
                      </span>
                  </div>
              </div>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav nav-dropdown display-4" data-app-modern-menu="true"><!--mbr-additionnal.css-39-->
                      <li class="nav-item"><a class="nav-link link text-black" href="index.php" aria-expanded="true">Accueil</a></li>
                      <li class="nav-item dropdown open">
                          <a class="nav-link link dropdown-toggle text-black" href="#" data-toggle="dropdown-submenu" aria-expanded="true">
                              Presentation</a>
                              
                              <div class="dropdown-menu">
                                  <?php foreach ($pages as $page):; ?>                                  
                                  <a class="dropdown-item text-white" href="presentation.php?page=<?= $page['page']?>" ><?= $page['page']?></a>
                                  <?php endforeach;?>                                 
                                  
                              </div>
                            
                      </li>
                      <li class="nav-item dropdown open">
                          <a class="nav-link link dropdown-toggle text-black" href="#" data-toggle="dropdown-submenu" aria-expanded="true">
                              Organisation</a>
                              <div class="dropdown-menu">
                                  <?php foreach ($pageOrg as $page):; ?>                                  
                                  <a class="dropdown-item text-white" href="organisation.php?page=<?= $page['page']?>" ><?= $page['page']?></a>
                                  <?php endforeach;?>                                 
                                  
                              </div>
                      </li>
                      
                      <li class="nav-item dropdown open">
                          <a class="nav-link link dropdown-toggle text-black" href="#" data-toggle="dropdown-submenu" aria-expanded="true">
                              Activités</a>
                              <div class="dropdown-menu">
                                  <?php foreach ($pageAct as $page):; ?>                                  
                                  <a class="dropdown-item text-white" href="activites.php?page=<?= $page['page']?>" ><?= $page['page']?></a>
                                  <?php endforeach;?>                                 
                                  
                              </div>
                      </li>
                      <li class="nav-item"><a class="nav-link link text-black" href="INDEX-PHARMACEUTIQUE-EDITION-2019-VF.pdf" aria-expanded="true">Liste produits</a></li>
                      <li class="nav-item"><a class="nav-link link text-black" href="../npsp/Espace_client/veille.com/" aria-expanded="true">Veille</a></li>
                      <li class="nav-item"><a class="nav-link link text-black" href="contact.php" aria-expanded="true">Contact</a></li>
                    </ul>
                  <div class="navbar-buttons mbr-section-btn">
                      <a class="btn btn-sm btn-oranj" href="redirect/backoffice/index_client.php" style="width: 120px; height: 29px;"><!--mbr-additionnal.css-188-->
                          Clients
                      </a>
                      <a class="btn btn-sm btn-oranj" href="redirect/backoffice/index_client.php" style="width: 120px; height: 29px; font-size:12px"   ><!--mbr-additionnal.css-188-->
                          Partenaires
                      </a> 
                  </div>
              </div>
          </nav>
         
        </div>
      </section>
</div><br/><br/>
    
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "Nouvelle PSP-CI",
    "url": "http://www.npsp.ci",
    "address": "Km4, Boulvard de Marseille, BP V5",
    "sameAs": [
      "https://web.facebook.com/NouvellePSPCI/",
      "https://www.linkedin.com/company/nouvelle-psp-ci/"
    ]
  }
</script>

</body>
</html>