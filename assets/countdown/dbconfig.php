<?php
class Database
{   
    private $host = "localhost";
    private $db_name = "npsp-ci-test";
    private $username = "root";
    private $password = "30031991";
    /*private $host = "localhost";
    private $db_name = "npspci";
    private $username = "root";
    private $password = "michee2018";*/
    public $conn;
     
    public function dbConnection()
	{
     
	    $this->conn = null;    
        try
		{
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->conn->exec("set names utf8");
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
        }
		catch(PDOException $exception)
		{
            echo "Connection error: " . $exception->getMessage();
        }
         
        return $this->conn;
    }
}
?>